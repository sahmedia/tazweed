<?php
/**
 * WooCommerce Contact Form 7 compatibility
 *
 * @author      Aldaba Digital
 * @category    Admin
 * @package     woocommerce-quotation/modules/
 * @version     2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'ADQ_PDF_Invoices' ) ) :
    
    class ADQ_PDF_Invoices {
        
            public function __construct() {     
                    add_action( 'wpo_wcpdf_after_order_data', array( $this, 'adq_wcpdf_after_order_data' ), 10, 2 );                 
                    
                    add_filter( 'wpo_wcpdf_invoice_title', array( $this, 'adq_wcpdf_invoice_title' ) );
                    add_filter( 'wpo_wcpdf_woocommerce_totals', array( $this, 'adq_wcpdf_woocommerce_totals' ), 10, 2 );                    
            }
    
            function adq_wcpdf_after_order_data ($template_type, $order) {
                
                    do_action( 'adq_email_proposal_after_header', $order, false, false );
            }
            
            
            function adq_wcpdf_invoice_title ( $invoice_title ) {
                    global $wpo_wcpdf;
                    
                    $order = wc_get_order( (int)$wpo_wcpdf->get_order_number() );
                    
                    if ( !$order )
                        return $invoice_title;
                    
                    switch ( $order->get_status() ) {
                        case 'request': $invoice_title = __('Quote request', 'woocommerce-quotation'); break;
                        case 'proposal-sent': $invoice_title = __('Proposal sent', 'woocommerce-quotation'); break;
                        case 'proposal-accepted': $invoice_title = __('Proposal accepted', 'woocommerce-quotation'); break;
                        case 'proposal-rejected': $invoice_title = __('Proposal rejected', 'woocommerce-quotation'); break;
                    }
                
                    return $invoice_title;
            }
            
            function adq_wcpdf_woocommerce_totals ( $totals, $order ) {
                
                    if ( ( $order->get_status() == 'proposal' || $order->get_status() == 'request' ) ) {
                            $totals['order_total']['value'] = __( 'Not yet proposed', 'woocommerce-quotation' );
                    }
                    
                    return $totals;
            }
            
}    
endif;

return new ADQ_PDF_Invoices();