<?php
/**
 * Admin new quote request email
 *
 * @author 		Adquote
 * @package 	WooCommerce/Templates/Emails
 * @version     1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php do_action( 'adq_email_header', $email_heading ); ?>

<p><?php if ( empty( $order->billing_first_name ) || empty( $order->billing_last_name ) ) {
	printf( __( 'You have received a quote request from %s. Their request is as follows:', 'woocommerce-quotation' ), $order->billing_email );	
	}
	else {

		printf( __( 'You have received a quote request from %s. Their request is as follows:', 'woocommerce-quotation' ), $order->billing_first_name . ' ' . $order->billing_last_name );	
	}
	?></p>

<?php do_action( 'adq_email_proposal_after_header', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_before_order_table', $order, true, false ); ?>

<h2><a href="<?php echo admin_url( 'post.php?post=' . $order->id . '&action=edit' ); ?>"><?php printf( __( 'Request: %s', 'woocommerce-quotation'), $order->get_order_number() ); ?></a> (<?php printf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $order->order_date ) ), date_i18n( wc_date_format(), strtotime( $order->order_date ) ) ); ?>)</h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<thead>
		<tr>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( array( 	'show_sku'    => false, 	'show_image'  => false, 	'$image_size' => array( 32, 32 ), 	'plain_text'  => false ) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
                                                <td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>">
                                                <?php 
                                                if( ( StaticAdqQuoteRequest::can_show_price() && StaticAdqQuoteRequest::can_show_product_price( $order ) ) 
                                                        || ( $total['value'] == "" ) ) { 
                                                        echo $total['value'];
                                                }
                                                else {
                                                        echo __( 'Not yet proposed', 'woocommerce-quotation' );
                                                }
                                                ?>
                                                </td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'adq_email_footer' ); ?>