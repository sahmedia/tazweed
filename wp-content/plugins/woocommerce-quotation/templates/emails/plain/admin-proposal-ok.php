<?php
/**
 * Admin new quote request email (plain text)
 *
 * @author 		Adquote
 * @package 	WooCommerce/Templates/Emails/Plain
 * @version     1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo $email_heading . "\n\n";

if ( empty( $order->billing_first_name ) || empty( $order->billing_last_name ) ) {
    echo sprintf( __( 'The proposal from %s has been accepted. The request was as follows:', 'woocommerce-quotation' ), $order->billing_email ) . "\n\n";
    }
    else {
    echo sprintf( __( 'The proposal from %s has been accepted. The request was as follows:', 'woocommerce-quotation' ), $order->billing_first_name . ' ' . $order->billing_last_name ) . "\n\n";
    }


if ( $order->get_user() ) :

        echo sprintf( __( 'You can access your account area to view your orders here: %s.', 'woocommerce-quotation' ), get_permalink( wc_get_page_id( 'myaccount' ) ) ) . "\n\n"; 

endif;

echo "****************************************************\n\n";

do_action( 'adq_email_proposal_after_header', $order, $sent_to_admin, $plain_text );
echo "----------\n\n";

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text );

echo sprintf( __( 'Order number: %s', 'woocommerce'), $order->get_order_number() ) . "\n";
echo sprintf( __( 'Quote request link: %s', 'woocommerce-quotation'), admin_url( 'post.php?post=' . $order->id . '&action=edit' ) ) . "\n";
echo sprintf( __( 'Quote request date: %s', 'woocommerce-quotation'), date_i18n( __( 'jS F Y', 'woocommerce' ), strtotime( $order->order_date ) ) ) . "\n";

do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text );

echo "\n" . $order->email_order_items_table( array( 	'show_sku'    => false, 	'show_image'  => false, 	'$image_size' => array( 32, 32 ), 	'plain_text'  => true ) );

echo "----------\n\n";

if ( $totals = $order->get_order_item_totals() ) {
	foreach ( $totals as $total ) {
		echo $total['label'] . "\t " . $total['value'] . "\n";
	}
}

echo "\n****************************************************\n\n";

do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text );


/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

echo "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );
