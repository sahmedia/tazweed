<?php
/**
 * Customer processing order email
 *
 * @author 		Aldaba Digital
 * @package 	Woocommerce Quotation
 * @version     
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo $email_heading . "\n\n";

echo __( "Our proposal has been accepted. The proposal is shown below for your reference:", 'woocommerce-quotation' ) . "\n\n";

if ( get_option('adq_enable_pay_button') !== 'no' ) :     

        printf( __( 'You can proceed with the payment: %s.', 'woocommerce-quotation' ), '<a href="'.$order->get_checkout_payment_url().'">'.__( 'Pay', 'woocommerce-quotation' ).'</a>' );
    
endif;

echo __( "« Attention, the Pay link only works if you have not yet paid the order. If you have already paid the order, this link will not work, you will still have the order details in the receipt email that will follow.", 'woocommerce-quotation' ) . "\n\n";

if ( $order->get_user() ) :

        echo sprintf( __( 'You can access your account area to view and pay your orders here: %s.', 'woocommerce-quotation' ), get_permalink( wc_get_page_id( 'myaccount' ) ) ) . "\n\n"; 

endif;

echo "****************************************************\n\n";

do_action( 'adq_email_proposal_after_header', $order, $sent_to_admin, $plain_text );
echo "----------\n\n";

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text );

echo sprintf( __( 'Quote request number: %s', 'woocommerce-quotation'), $order->get_order_number() ) . "\n";
echo sprintf( __( 'Quote request date: %s', 'woocommerce-quotation'), date_i18n( wc_date_format(), strtotime( $order->order_date ) ) ) . "\n";

do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text );

echo "\n" . $order->email_order_items_table( array( 	'show_sku'    => false, 	'show_image'  => false, 	'$image_size' => array( 32, 32 ), 	'plain_text'  => true ) );

echo "----------\n\n";

if ( $totals = $order->get_order_item_totals() ) {
	foreach ( $totals as $total ) {
		echo $total['label'] . "\t " . $total['value'] . "\n";
	}
}

echo "\n****************************************************\n\n";

do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text );


/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

echo "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );
