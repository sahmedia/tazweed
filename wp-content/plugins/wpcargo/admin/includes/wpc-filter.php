<?php

if (!defined('ABSPATH')){

	exit; // Exit if accessed directly

}


add_action('restrict_manage_posts', 'wpc_filter_status');
function wpc_filter_status() {
	global $typenow;
	$post_type = 'wpcargo_shipment'; // change to your post type
	$status  = 'wpcargo_status'; // change to your taxonomy
	if ($typenow == $post_type) {
		$wpcargo_option_settings = get_option('wpcargo_option_settings');
		$get_all_status = $wpcargo_option_settings['settings_shipment_status'];
		$explode_status = explode(",", $get_all_status);
		echo '<select name="wpcargo_status">';
			echo '<option value="">'.__('-- Select All Status --', 'wpcargo').'</option>';
		foreach($explode_status as $val){	
		$selected_val = isset($_REQUEST[$status]) && $_REQUEST[$status] == $val ? 'selected' : '';	
			echo '<option value="'.trim($val).'" '.$selected_val.'>'.trim($val).'</option>';
		}
		echo '</select>';
		
	};
}

add_filter('parse_query', 'wpc_status_query');
function wpc_status_query($query) {
	global $pagenow;
	$post_type = 'wpcargo_shipment'; 
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($_GET['wpcargo_status']) && $_GET['wpcargo_status'] != '') {
		$query->query_vars['meta_key'] = 'wpcargo_status';
        $query->query_vars['meta_value'] = $_GET['wpcargo_status'];
	}
}