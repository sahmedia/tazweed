<form method="post" action="options.php" class="email-setting-admin-form">
  <h5 style="color: #F00;">
    <?php _e('WPCargo Merge Tags','wpcargo'); ?>
  </h5>
  <p style="font-size: 12px; color: #F00;"> {wpcargo_tracking_number} =
    <?php _e('Tracking Number','wpcargo'); ?>
    <br>
    {shipper_email} =
    <?php _e('Shipper Email','wpcargo'); ?>
    <br>
    {receiver_email} =
    <?php _e('Receiver Email','wpcargo'); ?>
    <br>
    {shipper_phone} =
    <?php _e('Shipper Phone','wpcargo'); ?>
    <br>
    {receiver_phone} =
    <?php _e('Receiver Phone','wpcargo'); ?>
    <br>
    {admin_email} =
    <?php _e('Admin Email','wpcargo'); ?>
    <br>
    {shipper_name} =
    <?php _e('Name of the Shipper','wpcargo'); ?>
    <br>
    {receiver_name} =
    <?php _e('Name of the Receiver','wpcargo'); ?>
    <br>
    {status} =
    <?php _e('Shipment Status','wpcargo'); ?>
  </p>
  <?php settings_fields( 'wpcargo_mail_settings' ); 
	
	do_settings_sections( 'wpcargo_mail_settings' ); ?>
  <table class="form-table">
    <tbody>
      <tr>
        <th scope="row">Activate Email?</th>
        <td><input type="checkbox" name="wpcargo_mail_settings[wpcargo_active_mail]" <?php checked(isset($options['wpcargo_active_mail']), 1); ?> value="1">
          <p style="font-size: 10px;">
            <?php _e('Please check if you want to send email after updating the shipment.','wpcargo'); ?>
          </p></td>
      </tr>
      <tr>
        <th scope="row">Headers:</th>
        <td><input type="text" placeholder="WPCargo <do-not-reply@wpcargo.com>" name="wpcargo_mail_settings[wpcargo_mail_header]" value="<?php
    echo $options['wpcargo_mail_header']; ?>" >
          <p style="font-size: 10px;">
            <?php _e('Edit this if you want to change the header of your email automation.','wpcargo'); ?>
          </p></td>
      </tr>
      <tr>
        <th scope="row">Mail To:</th>
        <td><input type="text" name="wpcargo_mail_settings[wpcargo_mail_to]" placeholder="{shipper_email}, {receiver_email}, {admin_email}" value="<?php echo $options['wpcargo_mail_to']; ?>">
          <p style="font-size: 10px;">Add emails with comma separated.(You can add WPCargo Merge Tags)<br>
            sample_1@mail.com, @sample_2@mail.com </p></td>
      </tr>
      <tr>
        <th scope="row"><?php _e('Subject:','wpcargo'); ?></th>
        <td><input type="text" name="wpcargo_mail_settings[wpcargo_mail_subject]" value="<?php echo $options['wpcargo_mail_subject']; ?>">
          <p style="font-size: 10px;">
            <?php _e('Email Subject','wpcargo'); ?>
          </p></td>
      </tr>
      <tr>
        <th scope="row"><?php _e('Message:','wpcargo'); ?></th>
        <td><textarea cols="40" rows="5" placeholder='<h4>Hello {shipper_name}!</h4>
<br>
<p>The Status of your shipment has been updated to {status}. For more info please visit our website and check your shipment.
<a href="http://www.wpcargo.com/">WPCargo</a></p>' name='wpcargo_mail_settings[wpcargo_mail_message]'><?php echo $options['wpcargo_mail_message']; ?></textarea></td>
      </tr>
    </tbody>
  </table>
  <?php submit_button(); ?>
</form>