############################## WPCargo ##############################

All notable changes of WPCargo including the Add-ons will be documented in this file.

## [3.1.6] - 2017-03-09
### Update 
- Update Mobile Responsive in results
- Update Mobile Responsive in shipment admin dashboard

## [3.1.5] - 2017-03-08
### Update 
- Update the date format on frontend
- Update the time format of shipment based on WordPress(General Settings)
- Update Multiple Package Mobile Responsive
### Added 
- Added Filter by Status on Admin

## [3.1.4] - 2017-03-07
### Update 
- Update the date format of shipment based on WordPress(General Settings)

## [3.1.3] - 2017-02-27
### Fixed 
- Fix Issues on Multiple Package Frontend and Backend

## [3.1.2] - 2017-02-23
### Fixed 
- Fix Issue of Multple Package at the shipment results
### Added 
- Added Enable and Disable Settings for Multiple Package

## [3.1.1] - 2017-02-20
### Added 
- Added Multiple Packages on each shipment


## [3.1.0] - 2017-01-10
### Fixed 
- Fix Issue on saving shipments

## [3.0.10] - 2017-01-09 
### Fixed 
- Fix Issue/bugs on Email 
- Fix Error on Email Settings 
- Fix Error on Headers after activation

## [3.0.8] - 2016-12-20
### Changed
- Improve GUI on front-end and back-end

## [3.0.7] - 2016-11-29
### Fixed
- Fix issue on Print

## [3.0.6] - 2016-11-07
### Changed
- Fix CSS conflict on other theme's

## [3.0.5] - 2016-09-06
### Fixed
- Fix Email Update Notification
- Fix jQuery Date Format

### Added
- Add Settings for the barcode to display at the frontend

## [3.0.4] - 2016-08-30
### Changed
- Improve Internationalization
- Improve Mobile Responsive

## [3.0.3] - 2016-08-24
### Removed
- Remove Flush Rewrite Rules Function

### Added
- Add Priority Parameter on Registering Custom Post Type


## [3.0.2] - 2016-08-15
### Removed
- Remove "wp_verify_nonce" to avoid conflict on other themes


## [3.0.1] - 2016-08-08
### Added
- Added Print on Admin Shipments
- Added Developer Hooks on Print
- Added Filters

### Fixed
- Fix Email Notification
- Fix Issue on Date and Time on Firefox Browser


## [3.0.0] - 2016-08-05
### Added
- Added Developer Hooks
- Add Print on result page
- Add Email Notification when updating shipments
- Add Template Folder
- Add Language Folder
- Add Localization
- Create Separate Settings
- Add Title Prefix Settings

### Fixed
- Fix jQuery Bugs

### Changed
- Enhance Script
- Update Styles
- Organize Scripts and Directories


## [2.0.1] - 2016-06-24
### Removed
- Remove tabs
- Remove jQuery Bugs

### Fixed
- Fix Admin Notice Bugs

### Added
- Additional Settings for the WPCargo Title Prefix
- Add Functionality for the auto generation of the title

## [2.0.0] - 2016-05-27
### Changed
- Update the Track Form and Track Result to one(1) page
- Update for SMS Settings (Twilio Gateway)
- Update for WPCargo Email Add-ons

### Added
- Add a search functionality on "No results found"
- Add Filter on Forms


## [1.0.5] - 2016-05-01
### Added
- Add Filter to edit other titles - http://www.wpcargo.com/added-apply-filter-on-version-1-0-5/

### Fixed
- Fix conflicts between the filter of the post and wpcargo
- Fix Print Layout on Result Page

## [1.0.4] - 2016-04-20
### Fixed
- Fix the div tags closing on track result page

## [1.0.3] - 2016-02-12
### Fixed
- Fix Error on Filter

### Added
- Add the Add-ons Settings (Email Settings, SMS Settings)
- Add details on Add-ons Page (WPCargo Email Add-ons, WPCargo SMS Add-ons)

## [1.0.2]
### Fixed
- Fix bugs in inserting/updating into database

## [1.0.1]
### Removed
- Removed License Manager

### Fixed
- Fixed bugs for the add-ons
- Fixed the Shipment Status
- Fixed jQuery conflicts

### Changed
- Change other strings to make it readable for developers

### Added
- Add Add-ons Page that might help you

## [1.0.0]
### Added
- Filter by Shipment Status Functionality
- Filter by Shipment Category Functionality
- Front-end Search Functionality



############################## WPCargo Client Accounts Add-ons ##############################

## [3.0.2] - 2016-09-06
### Added
- Added Settings on custom field to display on the front end

## [3.0.1] - 2016-08-08
### Added
- Added Front End Hooks

### Fixed
- Fix and improve ajax retrieval of data


## [3.0.0] - 2016-08-05
### Changed
- Organize Scripts and Directories

### Added
- Added Developer Hooks


## [1.0.0]
### Added
- Auto generated users at the admin dashboard
- Client's can view there Accounts on there "My Account Page"




############################## WPCargo Custom Field Add-ons ##############################


## [3.0.6] - 2016-12-20
### Changed
- Improve GUI on front-end and back-end


## [3.0.1] - 2016-08-08
### Added
- Added Print Templates

### Fixed
- Fix jQuery Bugs on Date and Time
- Fix Issue on Date and Time on Firefox Browser


## [3.0.0] - 2016-08-05
### Changed
- Organize Scripts and Directories

### Added
- Added Developer Hooks

## [1.0.1] - 2016-06-01
### Fixed
- Fix bugs on custom field settings
- Fix bug on retrieving empty value data


## [1.0.0] - 2015-10-10
### Added
- Added a dynamic function for the custom fields to create and retrieve data



############################## WPCargo Email Add-ons ################################ 


## [3.0.0] - 2016-08-05
### Removed
- Removed the Add-ons and added to WPCargo Version 3.0.0


## [1.0.0] - 2015-10-10
### Added
- Added a function that will send an email to the user after the status is updated



############################## WPCargo Import and Export Add-ons ################################ 

## [3.0.5] - 2016-12-20
### Changed
- Improve GUI on front-end and back-end

## [3.0.3] - 2016-09-01
### Changed
- Improve the Import on Shipment History
- Update Retrieving of data on excel file
- Fix Multiple retrieving and importing of data on excel file

## [3.0.2] - 2016-08-30
### Removed
- Removed bugs on Import

## [3.0.1] - 2016-08-22
### Removed
- Remove the field options on Import Page


## [3.0.0] - 2016-08-05
### Changed
- Organize Scripts and Directories

### Added
- Added Developer Hooks


## [2.0.0] - 2016-05-30
### Changed
- Reupgrade the plugin because of many bugs and errors



## [1.0.0] - 2015-10-10
### Added
- Added a function that will Import and Export Shipment Data's







############################## WPCargo Print Add-ons ################################ 

## [3.0.0] - 2016-08-05
### Removed
- Removed the Add-ons and added to WPCargo Version 3.0.0


## [1.0.0] - 2015-10-10
### Added
- Added a function that will print the the shipment data's at the front-end and back-end



############################## WPCargo Shipment History Add-ons ################################ 

## [3.0.6] - 2017-03-09
### Update
- Update Mobile Responsive in shipment history results
- Update Mobile Responsive in shipment history admin dashboard

## [3.0.5] - 2017-03-07
### Fixed
- Update the time and date format of shipment based on WordPress(General Settings)

## [3.0.2] - 2016-09-05
### Fixed
- Fix Bugs on Dates
- Fix Shipment History Details

## [3.0.1] - 2016-08-08
### Fixed
- Fix Shipment History Status Update
- Fix jQuery Bugs on Date and Time
- Fix Issue on Date and Time on Firefox Browser

## [3.0.0] - 2016-08-05
### Changed
- Organize Scripts and Directories

### Added
- Added Developer Hooks

## [1.0.0] - 2015-10-10
### Added
- Added a function that will view the history of the shipments







############################## WPCargo SMS Add-ons ################################ 

## [3.0.1] - 2016-09-05
### Added
- Added Mobily SMS Gateway

### Fixed
- Fixed Error when updating using one way sms gateway

## [3.0.0] - 2016-08-05
### Changed
- Organize Scripts and Directories

### Added
- Added Developer Hooks


## [1.0.5] - 2016-15-27
### Added
- Added a third party services of Twilio


## [1.0.0] - 2015-10-10
### Added
- Added a function that will send an automated SMS after the status is updated.
- Added a third party services of One Way SMS
