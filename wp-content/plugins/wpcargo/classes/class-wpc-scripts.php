<?php


class WPCargo_Scripts{

	function __construct(){
		add_action('wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );
	}

	function frontend_scripts(){

		wp_register_style('wpcargo-styles', WPCARGO_PLUGIN_URL . 'assets/css/wpcargo-style.css');
		wp_enqueue_style('wpcargo-styles');

		wp_register_style('wpcargo-styles-themefy', WPCARGO_PLUGIN_URL . 'assets/css/themify-icons.css');
		wp_enqueue_style('wpcargo-styles-themefy');

		wp_register_style('wpcargo-styles-flaticon', WPCARGO_PLUGIN_URL . 'assets/css/font-awesome.css');
		wp_enqueue_style('wpcargo-styles-flaticon');

		wp_register_style('wpcargo-styles-flaticon-min', WPCARGO_PLUGIN_URL . 'assets/css/font-awesome.min.css');
		wp_enqueue_style('wpcargo-styles-flaticon-min');
		
		wp_enqueue_style( 'wpcargo-multiple-package-style', WPCARGO_PLUGIN_URL. 'assets/css/wpc-mp-style.css' );

	}

}

$class_wpcargo_scripts = new WPCargo_Scripts;
