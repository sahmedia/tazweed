﻿=== WPCargo Track & Trace===

Contributors: WPTaskforce

Donate link: http://wpcargo.com/



Tags: WPCargo, Track and Trace,tracking number, track, tracking, trace, courier, package, balikbayan box tracking, package tracking, exporter, freight forwarder, importer, supplier, customs broker, overseas agent, warehouse operator, Courier Service, cargo, shipment, shipping details, pick-up, delivery 



Requires at least: 3.8.1



Tested up to: 4.7



Stable tag: 4.3



License: GPLv2 or later



License URI: http://www.gnu.org/licenses/gpl-2.0.html



WPCargo is wordpress plugin for track &amp; trace courier, cargo, shipment, ideal solution for freight forwarder, customs broker, balikbayan forwarder, importer, exporter, supplier, overseas agent, transporter, &amp; warehouse operator.​​



== Description ==



[Main Site](http://www.wpcargo.com/) | [Documentation](http://www.wpcargo.com/documentation/) | [Showcase](http://www.wpcargo.com/showcase/) | [Premium Addons](http://www.wpcargo.com/purchase) | [Demo](http://wpcargo.com/complete-demo/)



WPCargo is a WordPress plug-in designed to provide ideal technology solution for your Cargo and Courier Operations. Whether you are an exporter, freight forwarder, importer, supplier, customs broker, overseas agent, or warehouse operator, WPCargo helps you to increase the visibility, efficiency, and quality services of your cargo and shipment business.



= Core Plugin Features =



* Shipment Track Form



* Manage Shipment



* Shipment Settings



* Email Notification  



* Auto Generate Tracking Number



* Multilingual Support



* Support Barcode 



* Print Label



* Report (version 3.9 - still under development) 



* Multiple Packages





= Premium Features =



* [Fully customizable form fields](http://www.wpcargo.com/product/wpcargo-custom-field-add-ons/)



* [Add Signature in custom field manager](http://www.wpcargo.com/product/wpcargo-signature-add-ons/)



* [SMS Notification](http://www.wpcargo.com/product/wpcargo-sms-add-ons/)



* [Allow user to manage thier shipment](http://www.wpcargo.com/product/wpcargo-client-accounts-add-ons/)



* [CSV Import/Export](http://www.wpcargo.com/product/wpcargo-importexport-add-ons/)



* [Shipment History - track location , date and status of the cargo](http://www.wpcargo.com/product/wpcargo-shipment-history-add-ons/)



* [Online Booking](http://www.wpcargo.com/product/wpcargo-pick-management-add-ons/)



* Payment Gateway (Stripe.com)



* [Proof of Delivery - allow driver or delivery to sign by reciever  and add images as a proof that the cargo has been delivered](http://www.wpcargo.com/product/wpcargo-proof-delivery/) 



* [Vehicle Manager can assign the delivery to your driver or vehicle](http://www.wpcargo.com/product/wpcargo-vehicles-add-ons/)



* Delivery Calculator 



* Detrack.com Integration 



* Request Quotes 



* Collection Point



* [Ability to track shipment from other company (Fedex , USPS)](http://www.wpcargo.com/product/wpcargo-courier-add-ons/)







= Demo =



Login for Admin:



http://wpcargo.com/complete-demo/wp-admin/



Username - usmanilaforwarders



Password - usmanilaforwarders



Login for Client Side:



http://wpcargo.com/complete-demo/my-account/



Username - wpcargo_client



Password - wpcargo_client





[View More features](http://www.wpcargo.com/features/)



Contact Skype: arni.cinco





== Installation ==



1. Upload `WPCargo.zip` to the `/wp-content/plugins/` directory



2. Activate the plugin through the 'Plugins' menu in WordPress





== Frequently asked questions ==



- How can I add the Pages for Track Form and Track Result?

First thing to do is to create a pages and setup the Track Form and Track Result under the WPCargo Settings->Page Settings



- How can I add options on each fields?

In WPCargo Settings Page add the option that you needed on each fields.(New line on each option)



Example on Add Shipment Status section:



On-Hold



Pending



Delivered 





- How to add an Agent?

Add a new user and change the role into "Agent".



- What is the shortcode to use?

Single Page with results:

[wpcargo_trackform]



2 Page with results:

[wpcargo_trackform id=page id of results]

[wpcargo_trackresults]





== Screenshots ==

1. All Shipments

2. Track Shipment

3. Shipment Results





== Changelog ==

= 3.1.6 =
- Update Mobile Responsive in results
- Update Mobile Responsive in shipment admin dashboard


= 3.1.5 =

- Update the date format on frontend
- Update the time format of shipment based on WordPress(General Settings)
- Added Filter by Status on Admin
- Update Multiple Package Mobile Responsive


= 3.1.4 =

- Update the date format of shipment based on WordPress(General Settings)


= 3.1.3 =

- Fix Issues on Multiple Package Frontend and Backend


= 3.1.2 =

- Fix Issue of Multple Package at the shipment results

- Added Enable and Disable Settings for Multiple Package



= 3.1.1 =

- Added Multiple Packages



= 3.1.0 =

- Fix Issue on saving shipments





= 3.0.10 =

- Fix Issue/bugs on Email

- Fix Error on Email Settings

- Fix Error on Headers after activation





= 3.0.8 =

- Improve GUI on front-end and back-end





= 3.0.7 =

- Fix issue on print





= 3.0.6 =

- Fix CSS conflict on other theme's





= 3.0.5 =

- Fix Email Update Notification

- Fix jQuery Date Format

- Add Settings for the barcode to display at the frontend





= 3.0.4 =

- Improve Internationalization

- Improve Mobile Responsive





= 3.0.3 =

- Remove Flush Rewrite Rules Function

- Add Priority Parameter on Registering Custom Post Type





= 3.0.2 =

- Remove "wp_verify_nonce" to avoid conflict on other themes





= 3.0.1 =

- Added Print on Admin Shipments

- Added Developer Hooks on Print

- Fix Email Notification

- Fix Issue on Date and Time on Firefox Browser

- Added Filters





= 3.0.0 =

- Added Developer Hooks

- Fix jQuery Bugs

- Add Template Folder

- Add Language Folder

- Add Localization

- Enhance Script

- Update Styles

- Create Separate Settings

- Add Title Prefix Settings

- Organize Scripts and Directories

- Add Print on result page

- Add Email Notification when updating shipments





= 2.0.1 =

- Remove tabs

- Remove jQuery Bugs

- Fix Admin Notice Bugs

- Additional Settings for the WPCargo Title Prefix

- Add Functionality for the auto generation of the title





= 2.0.0 =

- Update the Track Form and Track Result to one(1) page

- Add a search functionality on "No results found"

- Add Filter on Forms

- Update for SMS Settings (Twilio Gateway)

- Update for WPCargo Email Add-ons





= 1.0.5 =

- Add Filter to edit other titles - http://www.wpcargo.com/added-apply-filter-on-version-1-0-5/

- Fix conflicts between the filter of the post and wpcargo

- Fix Print Layout on Result Page





= 1.0.4 =

- Fix the div tags closing on track result page





= 1.0.3 =

- Fix Error on Filter

- Add the Add-ons Settings (Email Settings, SMS Settings)

- Add details on Add-ons Page (WPCargo Email Add-ons, WPCargo SMS Add-ons)





= 1.0.2 =

- Fix bugs in inserting/updating into database





= 1.0.1 =

- Removed License Manager

- Fixed bugs for the add-ons

- Fixed the Shipment Status

- Change other strings to make it readable for developers

- Add Add-ons Page that might help you

- Fixed jQuery conflicts





= 1.0.0 =

- Add Filter by Shipment Status

- Add Filter by Shipment Category





== Upgrade Notice ==

Just upgrade via Wordpress.

