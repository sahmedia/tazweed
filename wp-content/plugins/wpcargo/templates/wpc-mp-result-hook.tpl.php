<?php

if (!defined('ABSPATH')){

	exit; // Exit if accessed directly

}



?>

<div class="wpc-multiple-package">

	<p id="wpc-mp-title"><?php _e('Packages', 'wpcargo-multiple-package'); ?></p>

	<table class="wpc-mp-table" id="list-container">							

	<?php

		$shipment_id 				= $shipment_detail->ID;

		$get_multiple_package_data 	= get_post_meta($shipment_id, 'wpc-multiple-package', true);

		$unserialized_mp_data		= unserialize($get_multiple_package_data);	

		$options 					= get_option( 'wpc_mp_settings' );

		$wpc_mp_dimension_unit 		= !empty($options['wpc_mp_dimension_unit']) ? $options['wpc_mp_dimension_unit'] : 'cm';

		$wpc_mp_weight_unit 		= !empty($options['wpc_mp_weight_unit']) ? $options['wpc_mp_weight_unit'] : 'lbs';

		echo '<tr class="wpc-ca-list-tr">';

			echo '<th class="wpc-ca-list-th">'.__('Qty.', 'wpcargo').'</th>';

			echo '<th class="wpc-ca-list-th">'.__('Piece Type', 'wpcargo').'</th>';

			echo '<th class="wpc-ca-list-th">'.__('Length ('.$wpc_mp_dimension_unit.')', 'wpcargo').'</th>';

			echo '<th class="wpc-ca-list-th">'.__('Width ('.$wpc_mp_dimension_unit.')', 'wpcargo').'</th>';

			echo '<th class="wpc-ca-list-th">'.__('Height ('.$wpc_mp_dimension_unit.')', 'wpcargo').'</th>';

			echo '<th class="wpc-ca-list-th">'.__('Weight ('.$wpc_mp_weight_unit.')', 'wpcargo').'</th>';

			echo '<th class="wpc-ca-list-th">'.__('Description', 'wpcargo').'</th>';

			do_action('wpc_mp_header_results');

		echo '</tr>';

		$sum_pm_price = array();

		if(!empty($unserialized_mp_data)) {

			foreach($unserialized_mp_data as $val){

			

				$get_qty 			= $val['wpc-pm-qty'];

				$get_piece_type 	= $val['wpc-pm-piece-type'];

				$get_pm_length 		= $val['wpc-pm-length'];

				$get_pm_width 		= $val['wpc-pm-width'];

				$get_pm_height		= $val['wpc-pm-height'];

				$get_pm_weight		= $val['wpc-pm-weight'];

				$get_pm_desc 		= $val['wpc-pm-description'];

				

				echo '<tr class="wpc-ca-list-tr">';

					echo '<td class="wpc-ca-list-td">'.$get_qty.'</td>';

					echo '<td class="wpc-ca-list-td">'.$get_piece_type.'</td>';

					echo '<td class="wpc-ca-list-td">'.$get_pm_length.'</td>';

					echo '<td class="wpc-ca-list-td">'.$get_pm_width.'</td>';

					echo '<td class="wpc-ca-list-td">'.$get_pm_height.'</td>';

					echo '<td class="wpc-ca-list-td">'.$get_pm_weight.'</td>';

					echo '<td class="wpc-ca-list-td">'.$get_pm_desc.'</td>';

					do_action('wpc_mp_field_results', $val);

				echo '</tr>';

							

			}

		}

	

		do_action('wpc_mp_after_results', $shipment_id);	

	?>

	

	</table>

</div>



