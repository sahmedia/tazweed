<?php
// hacks and mods will go here
add_filter( 'bp_do_register_theme_directory', '__return_true' );
function show_last_n_images($n = 5) {
    if ($user_id = bp_displayed_user_id()) {
        $model = new RTMediaModel();
        $results = $model->get(array('media_type' => 'album', 'media_author' => $user_id), 0, $n);


        if ($results) {
            echo '<ul id="groups-list" class="item-list">';
            foreach ($results as $image) {
                echo '<li><a href="' . get_rtmedia_permalink($image->id) . '"><img src="';
                rtmedia_image("rt_media_thumbnail", $image->id);
                echo '" /></a></li>';
		            }
            echo '</ul>';
        }
    }
}
function show_last_n_images_m_directory($n = 5) {
    if ( $user_id = bp_get_member_user_id()) {
        $model = new RTMediaModel();	
        $results = $model->get(array('media_type' => 'album', 'media_author' => $user_id), 0, $n);
        if ($results) {
            echo '<ul id="groups-list" class="item-list">';
            foreach ($results as $image) {
                echo '<li><a href="' . get_rtmedia_permalink($image->id) . '"><img src="';
                rtmedia_image("rt_media_thumbnail", $image->id);
                echo '" /></a></li>';
					}
            echo '</ul>';
        }
    }
	
	
	
echo do_shortcode('[rtmedia_uploader context="post" context_id="2" album_id="34" privacy="40"]');
 echo do_shortcode('[rtmedia_gallery global="true" media_type="album"]');
}
function show_last_n_images_test($n=5){
    if(bp_displayed_user_id()){
    global $bp;
    $args = array(
                'post_type' => 'rtmedia_album',
                'author' => $bp->displayed_user->id,
                'posts_per_page' => $n
            );
    $q=new WP_Query($args);

    if ($q && $q->have_posts()):
        echo '<ul id="groups-list" class="bp-media-gallery item-list">';
        while ($q->have_posts()) : $q->the_post();
          rt_media_the_content();
        endwhile;
        echo '</ul>';
    endif;
    }
}
add_action( 'bp_before_member_header_meta', 'show_last_n_images' );
add_action( 'bp_directory_members_item', 'show_last_n_images_m_directory' );
// die;
// disable the WP Toolbar and revert back to the BuddyBar
//add_filter( 'bp_use_wp_admin_bar', '__return_false' );
define ( 'BP_AVATAR_THUMB_WIDTH', 50 );
define ( 'BP_AVATAR_THUMB_HEIGHT', 50 );
define ( 'BP_AVATAR_FULL_WIDTH', 150 );
define ( 'BP_AVATAR_FULL_HEIGHT', 150 );
define ( 'BP_AVATAR_ORIGINAL_MAX_WIDTH', 640 );
define ( 'BP_AVATAR_DEFAULT', $img_url );
define ( 'BP_AVATAR_DEFAULT_THUMB', $img_url );

// Changing Internal Configuration Settings

define( 'BP_DEFAULT_COMPONENT', 'profile' )


?>
