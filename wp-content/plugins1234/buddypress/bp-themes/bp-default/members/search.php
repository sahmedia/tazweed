search.php
<?php
/** search.php
 *
 * The template for displaying Search Results pages.
 *
 * @author		Konstantin Obenland
 * @package		The Bootstrap
 * @since		1.0.0 - 07.02.2012
 */

get_header(); ?>
<?
/* Template Name: Search Results */
$search_refer = $_GET["post_types"];
if (($search_refer == 'rtmedia_album')&&(!empty($_GET["Type_de_produit"]))) { load_template(TEMPLATEPATH . '/search-in-rtmedia_album.php'); }
elseif ($search_refer == 'user') { load_template(TEMPLATEPATH . '/members/members-loop.php'); };

?>
 <pre>
 <?php
// print_r($_GET);
// if($_GET["Type_de_produit"] === "") echo "a is an empty string\n";
// if($_GET["Type_de_produit"] === false) echo "a is false\n";
// if($_GET["Type_de_produit"] === null) echo "a is null\n";
// if(isset($_GET["Type_de_produit"])) echo "a is set\n";
// if(!empty($_GET["Type_de_produit"])) echo "a is not empty";
// ?>
</pre> 
<section id="primary" class="span8">
	<?php tha_content_before(); ?>
	<div id="content" role="main">
		<?php tha_content_top();

		if ( have_posts() ) : ?>
	
			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'the-bootstrap' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header>
	
			<?php
			while ( have_posts() ) {
				the_post();
				get_template_part( '/partials/content', get_post_format() );
			}
			the_bootstrap_content_nav();
		else :
			get_template_part( '/partials/content', 'not-found' );
		endif; 

		tha_content_bottom(); ?>
	</div><!-- #content -->
	<?php tha_content_after(); ?>
</section><!-- #primary -->

<?php
get_footer();


/* End of file search.php */
/* Location: ./wp-content/themes/the-bootstrap/search.php */