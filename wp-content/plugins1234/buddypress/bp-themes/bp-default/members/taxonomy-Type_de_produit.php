
<?php get_header(); ?>


<?php

//echo do_shortcode('[rtmedia_gallery global="true" media_type="album"]'); 

?>


<h1>This is Taxonomy page for showing posts under the each Term</h1>

<?php
 global $wp_query;
global $rtmedia_query;


if (have_posts()) :

    /* Start the Loop */
    while (have_posts()) : the_post();
$model = new RTMediaModel();	
$album_post =$post->ID;
$results = $model->get(array('media_type' => 'album', 'media_id' => $album_post));
	?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="author-box">		<?php 
		echo get_avatar( get_the_author_meta( 'user_email' ), '50' ); ?>		<p><?php printf( _x( 'by %s', 'Post written by...', 'status' ), str_replace( '<a href=', '<a rel="author" href=', bp_core_get_userlink( $post->post_author ) ) ); ?></p>	</div>

				<?php echo '<li><a href="' . get_rtmedia_permalink($results[0]->id) . '">'.get_rtmedia_title ( $results[0]->id).'<img src="';
                rtmedia_image("rt_media_thumbnail", $results[0]->id);
                echo '" /></a>';
				
				the_excerpt();?>
</article>
    <?php endwhile;

endif; ?>
<?php get_search_form(); ?>
<?php
/** index.php
 *
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author		Konstantin Obenland
 * @package		The Bootstrap
 * @since		1.0.0 - 05.02.2012
 */

get_header(); ?>

<section id="primary" class="span8">
	<?php tha_content_before(); ?>
	<div id="content" role="main">
		<?php tha_content_top();
		
		if ( have_posts() ) {
			while ( have_posts() ) {
			 
				the_post();
				
				get_template_part( '/partials/content', 'rtmedia_album' );
			}
			the_bootstrap_content_nav( 'nav-below' );
		}
		else {
			get_template_part( '/partials/content', 'not-found' );
		}
	
		tha_content_bottom(); ?>
	</div><!-- #content -->
	<?php tha_content_after(); ?>
</section><!-- #primary -->

<?php
get_sidebar();
get_footer();


/* End of file index.php */
/* Location: ./wp-content/themes/the-bootstrap/index.php */