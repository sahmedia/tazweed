<?php
/*
Plugin Name: custom_promofoire
Description: Site specific code changes for promofoire
*/
/* Start Adding Functions Below this Line */

/**
*                                
*                           personalisation 
*                             PROMOFOIRE
*//**



/**
* surcharger le post type rtmedia album
* 
* @param string $post_type Registered post type name. 
* @param array $args Array of post type parameters.
*/
 // Kint::trace();
add_action( 'registered_post_type', 'gs_books_label_rename', 10, 2 );
function gs_books_label_rename( $post_type, $args ) {
   if ( 'rtmedia_album' === $post_type ) {
       global $wp_post_types;
           $args->public = true;
           $args-> publicly_queryable = true;
           $args->show_ui = true;
           $args->show_in_menu = true;
           $args->has_archive = true;
		   $args->exclude_from_search = false;    
           $args-> menu_position = 20;
          
           
       $wp_post_types[ $post_type ] = $args;
   }
} 

/**
* Force custom post type to be publish
*/

function force_type_publish($post)
{
   if ($post['post_type'] == 'rtmedia_album'||$post['post_type'] == 'user')
   $post['post_status'] = 'publish';
   return $post;
}
add_filter('wp_insert_post_data', 'force_type_publish');

/**
*  Highlight keywords in search results within the_excerpt and the_title
*/
function wps_highlight_results($text){
    if(is_search()){
    $sr = get_query_var('s');
    $keys = explode(" ",$sr);
    $text = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">'.$sr.'</strong>', $text);
    }
    return $text;
}
add_filter('the_excerpt', 'wps_highlight_results');
add_filter('the_title', 'wps_highlight_results');

/**
*  Redirect to post when search query returns single result
*/
// add_action('template_redirect', 'single_result');
function single_result() {
   if (is_search()) {
       global $wp_query;
       if ($wp_query->post_count == 1) {
           wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );
       }
   }
}

/**
*  display-search-terms-from-google-visitors
*/
$refer = $_SERVER["HTTP_REFERER"];
         if (strpos($refer, "google")) {
                        $refer_string = parse_url($refer, PHP_URL_QUERY);
                        parse_str($refer_string, $vars);
                        $search_terms = $vars['q'];
                        echo 'Welcome Google visitor! You searched for the following terms to get here: ';
                        echo $search_terms;
             };
/* 
Protect WordPress Against Malicious URL Requests
*/
global $user_ID; if($user_ID) {
if(!current_user_can('level_10')) {
if (strlen($_SERVER['REQUEST_URI']) > 255 || 
stripos($_SERVER['REQUEST_URI'], "eval(") || 
stripos($_SERVER['REQUEST_URI'], "CONCAT") || 
stripos($_SERVER['REQUEST_URI'], "UNION+SELECT") || 
stripos($_SERVER['REQUEST_URI'], "base64")) {
@header("HTTP/1.1 414 Request-URI Too Long");
@header("Status: 414 Request-URI Too Long");
@header("Connection: Close");
@exit;
}
}
}
add_filter( 'show_admin_bar', '__return_false' );

/* Buddypress Xprofile Custom Fields Type
Avatar (Image) */
function display_avatar($avatar, $params) {
    $bxcft_avatar = bp_get_member_profile_data(array('field' => 'logo', 'user_id' => $params['item_id']));
    if ($bxcft_avatar != '') {
        $avatar = $bxcft_avatar;
    }
    return $avatar;
}
add_filter('bp_core_fetch_avatar_url', 'display_avatar', 15, 2);

function display_avatar_html($avatar_html, $params) {
    $new_avatar = bp_get_member_profile_data(array('field' => 'logo', 'user_id' => $params['item_id']));
    if ($new_avatar != '') {
        $upload_dir = wp_upload_dir();
        $new_avatar = $upload_dir['baseurl'] . $new_avatar;
        $parts = explode('"', $avatar_html);
        for ($i=0; $i<count($parts);$i++) {
            if (strpos($parts[$i], 'src=')) {
                break;
            }
        }
       $i++;
       $prev_avatar = $parts[$i];
       $avatar_html = str_replace($prev_avatar, $new_avatar, $avatar_html);
    }

    return $avatar_html;
}
//add_filter('bp_core_fetch_avatar', 'display_avatar_html', 15, 2);

// Setup the navigation
// Props to http://wordpress.stackexchange.com/questions/16223/add-buddypress-profile-menu-item for helping me figure this out
// http://themekraft.com/customize-profile-and-group-menus-in-buddypress/
function my_setup_nav() {
      global $bp;
 
      // Change the order of menu items
      $bp->bp_nav['messages']['position'] = 100;
 
      // Remove a menu item
      $bp->bp_options_nav['profile']['change-avatar'] = true;
 
      // Change name of menu item
      $bp->bp_nav['media-personal-li']['name'] = 'My Conversations';
}
 
add_action( 'bp_setup_nav', 'my_setup_nav', 1000 );


/******nos testes *****/

function taxo_album() {
       global $rtmedia_media;
       $id = $rtmedia_media->id;
?>
<!-- Styles -->
<style>
#custom_taxonomy_post_filter ul {
display: inline-block;
padding:20px;border:1px solid #eee;margin: 5px 0;
background: #fafafa;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
-o-border-radius: 3px;
-ms-border-radius: 3px;
border-radius: 3px;
}
#custom_taxonomy_post_filter ul label {
width: 120px;display: inline-block;
}
#custom_taxonomy_post_filter ul select {
min-width: 150px;
}
#cat {
display: none;
}
</style>

<!-- Taxonomy box -->
<li id="custom_taxonomy_post_filter">
<ul>
<li><h3><strong>Filter by taxonomies:</strong></h3></li>
<?php

 // Get all post taxonomies
$taxonomies = get_object_taxonomies( 'rtmedia_album');

// For each taxonomy get a select input with al terms
foreach ( $taxonomies as $taxonomy ) {
$tax = get_taxonomy( $taxonomy );
$name = $tax->labels->name;
$terms = get_terms( $taxonomy,'orderby=count&hide_empty=0&hierarchical=1');
?>
<li class="custom_taxonomy_post_filter">
<label for=""><?php echo $name; ?></label>
<select name="<?php echo $taxonomy; ?>" id="<?php echo $taxonomy; ?>">
<option value="0"><?php _e( 'All' ); ?></option>
            <?php
            $excluded_term = get_term_by('slug', 'podroze', $taxonomy);
            $args = array(
                'orderby' => 'slug',
                'hierarchical' => 'true',
                'exclude' => $excluded_term->term_id,
                'hide_empty' => '0',
                'parent' => $excluded_term->term_id,
            );              
            $hiterms = get_terms($taxonomy, $args);
            foreach ($hiterms AS $hiterm) :
                echo "<option value='".$hiterm->slug."'".($_POST[$taxonomy] == $hiterm->slug ? ' selected="selected"' : '').">".$hiterm->name."</option>\n";

                $loterms = get_terms($taxonomy, array("orderby" => "slug", "parent" => $hiterm->term_id,'hide_empty' => '0',));
                if($loterms) :
                    foreach($loterms as $key => $loterm) :

                    echo "<option value='".$loterm->slug."'".($_POST[$taxonomy] == $loterm->slug ? ' selected="selected"' : '').">&nbsp;-&nbsp;".$loterm->name."</option>\n";

                    $lo2terms = get_terms($taxonomy, array("orderby" => "slug", "parent" => $loterm->term_id,'hide_empty' => '0',));
                    if($lo2terms) :
                        foreach($lo2terms as $key => $lo2term) :

                        echo "<option value='".$lo2term->slug."'".($_POST[$taxonomy] == $lo2term->slug ? ' selected="selected"' : '').">&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;".$lo2term->name."</option>\n";



                        endforeach;
                    endif;

                    endforeach;
                endif;

            endforeach;
            ?>
         </select>
<?php
}


?>
</li>
</ul> 
    <fieldset class="tags">
        <label for="post_tags">Tags:</label>
        <input type="text" value="" tabindex="35" name="postTags" id="postTags" />
    </fieldset>

    <fieldset>
        <input type="hidden" name="submitted" id="submitted" value="true" />
        <?php wp_nonce_field( 'post_nonce', 'post_nonce_field' ); ?>
        <button class="button" type="submit"><?php _e('Tag Product', 'framework') ?></button>
    </fieldset>

<?php
// Check to see if correct form is being submitted, if so continue to process
if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_term") {

    // Check to see if input field for new term is set 
    if (isset ($_POST['term'])) {

        // If set, stores user input in variable
        $new_term =  $_POST['term'];

        // Function to handle inserting of new term into taxonomy
        wp_insert_term(

          // The term (user input)
          $new_term,

          // The club taxonomy
          'Tag'        

        );

    } else { 

        // Else throw an error message
        echo 'Please enter a club name!';     

    }

}


}
add_action( 'rtmedia_before_add_album', 'taxo_album' );
// add_action( 'rtmedia_add_album_privacy', 'taxo_album' );
add_action( 'rtmedia_add_album_privacy', 'taxo_album' );	
// add_action( 'reccherche_par_taxonomy', 'taxo_album' );	

    function add_taxonomy ( $media ) {	
//print '<pre>';		
// Get all post taxonomies		
	$taxonomies = get_object_taxonomies( 'rtmedia_album' ); 								
global $rtmedia_query;
$model = new RTMediaModel();

foreach ( $taxonomies as $taxonomy ) {	
$media = $model->get_media(array('id' => $rtmedia_query->media_query['album_id']), false, false);
global $rtmedia_media;
$rtmedia_media = $media[0];

$terms= esc_attr( $_POST[$taxonomy] );
$terms = get_terms( $taxonomy,'orderby=count&hide_empty=0&hierarchical=1');		
	// $terms = isset($_POST[$taxonomy] ) ? (array) $_POST[$taxonomy]  : array(); //Cast array values as integers if $_POST[$taxonomy]  contains IDs
	// $terms = array_map('intval',$terms);		
$terms= esc_attr( $_POST[$taxonomy] );
$terms_tag=explode( ',', $_POST['postTags'] );
	if ( ! taxonomy_exists ( $taxonomy ) ) {  
	continue;                 }             
	wp_set_object_terms( $media[0]->media_id, array( $terms ), $taxonomy , FALSE );   

  

    //Tagging
    wp_set_object_terms( $media[0]->media_id, $terms_tag,  'Tag' , true);
	echo "***********<pre>";
print_r($terms_tag);
echo "</pre>";
die;
	}    
clean_object_term_cache( $media[0]->media_id, $taxonomy );	
	}		  

   add_action('rtmedia_after_add_album', 'add_taxonomy', 10,1);  
	add_action('rtmedia_after_update_album', 'add_taxonomy', 10,1);
	add_action('wp_ajax_rtmedia_create_album', 'add_taxonomy', 10,1);

/* Landing pages */
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

if ( ! function_exists( 'theme_register_menus' ) ) :
function theme_register_menus() {
	register_nav_menu('top_menu', __('Top Menu', TEMPLATE_DOMAIN));
	register_nav_menu('logged_out_top_menu', __('Logged Out Top Menu', TEMPLATE_DOMAIN));
	
}
add_action( 'init', 'theme_register_menus' );
endif;

// Add filter to the_content
	add_filter('the_content', 'my_excerpts');
function my_excerpts($content = false) {

// If is the home page, an archive, or search results
	if(is_front_page() || is_archive() || is_search()) :
		global $post;
		$content = $post->post_excerpt;

	// If an excerpt is set in the Optional Excerpt box
		if($content) :
			$content = apply_filters('the_excerpt', $content);

	// If no excerpt is set
		else :
			$content = $post->post_content;
			$excerpt_length = 100;
			$words = explode(' ', $content, $excerpt_length + 1);
			$more = '[...] Read more';
			if(count($words) > $excerpt_length) :
				array_pop($words);
				array_push($words, $more);
				$content = implode(' ', $words);
			endif;
			$content = '' . $content . '';

		endif;
	endif;

// Make sure to return the content
	return $content;

}


function block_wp_admin_init() {

if (strpos(strtolower($_SERVER['REQUEST_URI']),'/wp-admin/') !== false) {

if ( !is_site_admin() ) {

wp_redirect( get_option('siteurl'), 302 );
}
}
}

//add_action('init','block_wp_admin_init',0);
function block_wp_admin_init_redirect() {

if (strpos(strtolower($_SERVER['REQUEST_URI']),'/wp-login.php') !== false) {

if ( !is_site_admin() ) {

wp_redirect( get_option('siteurl'), 302 );

}

}

}

add_action('init','block_wp_admin_init_redirect',0);

   function restrict_access_admin_panel(){
                global $current_user;
                get_currentuserinfo();
                if ($current_user->user_level <  4) {
                        wp_redirect( get_bloginfo('url') );
                        exit;
                }
        }
   // add_action('admin_init', 'restrict_access_admin_panel', 1);
		
		
		
/**
*   How to Add a Custom Default Avatar For BuddyPress Members and Groups */

function myavatar_add_default_avatar( $url )
{
return get_stylesheet_directory_uri() .'/_inc/images/wpmudev-placeholder.png';
}
add_filter( 'bp_core_mysteryman_src', 'myavatar_add_default_avatar' );


function my_default_get_group_avatar($avatar) {
global $bp, $groups_template;

if( strpos($avatar,'group-avatars') ) {
return $avatar;
}
else {
$custom_avatar = get_stylesheet_directory_uri() .'/_inc/images/wpmudev-placeholder.png';

if($bp->current_action == "")
return '<img class="avatar" alt="' . attribute_escape( $groups_template->group->name ) . '" src="'.$custom_avatar.'" width="'.BP_AVATAR_THUMB_WIDTH.'" height="'.BP_AVATAR_THUMB_HEIGHT.'" />';
else
return '<img class="avatar" alt="' . attribute_escape( $groups_template->group->name ) . '" src="'.$custom_avatar.'" width="'.BP_AVATAR_FULL_WIDTH.'" height="'.BP_AVATAR_FULL_HEIGHT.'" />';
}
}
add_filter( 'bp_get_group_avatar', 'my_default_get_group_avatar');
		

		
/*Extra leaderboard fields myCRED*/
//load fields on myCRED leaderboard
function my_custom_ranking_rows( $layout, $template, $row, $position )
{
$avatar = get_avatar( $row['ID'], 50 );
$userank = do_shortcode('[mycred_my_rank user_id="'. $row['ID'] .'" show_title="1"]');
$link= bp_core_get_userlink($row['ID'], false, true );
$link_avatar= '<a href="'.bp_core_get_userlink($row['ID'], false, true ).'">'.$avatar.'</a>';
 
//get user role
$user = new WP_User( $row['ID'] );
if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
        foreach ( $user->roles as $role )
                $cargo = $role;
}
// roles to pt-br
if ($cargo == "administrator") {$cargo = "Administrador";}
else if ($cargo == "contributor") {$cargo = "Colaborador";}
else if ($cargo == "subscriber") {$cargo = "Assinante";}                                       
else if ($cargo == "author") {$cargo = "Autor";}
else if ($cargo == "editor") {$cargo = "Editor";}                                                      
else if ($cargo == "super admin") {$cargo = "Administrador+";}
else {$cargo = "Desconhecido";}                                                        
 
return str_replace(
array ('%avatar%','%rank%','%cargo%','%nicename%','%link%', '%link_avatar'),
array( $avatar, $rank, $cargo, $nicename, $link, $link_avatar),
$layout );
}
add_filter( 'mycred_ranking_row', 'my_custom_ranking_rows', 10, 4 );





// add_action( 'bp_before_member_header_meta', 'show_users_rank_in_header' );
function show_users_rank_in_header() {
	$user_id = bp_displayed_user_id();
	if ( mycred_exclude_user( $user_id ) ) return;
	$rank_name = mycred_get_users_rank( $user_id );
	echo 'Rank: ' . $rank_name;
	 echo S2MEMBER_CURRENT_USER_REGISTRATION_DAYS;
}



 


/* MYCRED HOOKS 101 */

// Register Hook
//add_filter( 'mycred_setup_hooks', 'register_my_custom_hook' );
function register_my_custom_hook( $installed )
{
	$installed['complete_profile'] = array(
		'title'       => __( '%plural% for Profile Completion', 'textdomain' ),
		'description' => __( 'This hook award / deducts points from users who fill out their first and last name.', 'textdomain' ),
		'callback'    => array( 'my_custom_hook_class' )
	);
	return $installed;
}

// myCRED Custom Hook Class
class my_custom_hook_class extends myCRED_Hook {

	/**
	 * Construct
	 */
	function __construct( $hook_prefs, $type = 'mycred_default' ) {
		parent::__construct( array(
			'id'       => 'complete_profile',
			'defaults' => array(
				'creds'   => 1,
				'log'     => '%plural% for completing your profile'
			)
		), $hook_prefs, $type );
	}

	/**
	 * Hook into WordPress
	 */
	public function run() {
		// Since we are running a single instance, we do not need to check
		// if points are set to zero (disable). myCRED will check if this
		// hook has been enabled before calling this method so no need to check
		// that either.
		add_action( 'personal_options_update',  array( $this, 'profile_update' ) );
		add_action( 'edit_user_profile_update', array( $this, 'profile_update' ) );
	}

	/**
	 * Check if the user qualifies for points
	 */
	public function profile_update( $user_id ) {
		// Check if user is excluded (required)
		if ( $this->core->exclude_user( $user_id ) ) return;

		// Check to see if user has filled in their first and last name
		if ( empty( $_POST['first_name'] ) || empty( $_POST['last_name'] ) ) return;

		// Make sure this is a unique event
		if ( $this->has_entry( 'completing_profile', '', $user_id ) ) return;

		// Execute
		$this->core->add_creds(
			'completing_profile',
			$user_id,
			$this->prefs['creds'],
			$this->prefs['log'],
			'',
			'',
			$this->mycred_type
		);
	}

	/**
	 * Add Settings
	 */
	 public function preferences() {
		// Our settings are available under $this->prefs
		$prefs = $this->prefs; ?>

<!-- First we set the amount -->
<label class="subheader"><?php echo $this->core->plural(); ?></label>
<ol>
	<li>
		<div class="h2"><input type="text" name="<?php echo $this->field_name( 'creds' ); ?>" id="<?php echo $this->field_id( 'creds' ); ?>" value="<?php echo $this->core->format_number( $prefs['creds'] ); ?>" size="8" /></div>
	</li>
</ol>
<!-- Then the log template -->
<label class="subheader"><?php _e( 'Log template', 'mycred' ); ?></label>
<ol>
	<li>
		<div class="h2"><input type="text" name="<?php echo $this->field_name( 'log' ); ?>" id="<?php echo $this->field_id( 'log' ); ?>" value="<?php echo $prefs['log']; ?>" class="long" /></div>
	</li>
</ol>
<?php
	}

	/**
	 * Sanitize Preferences
	 */
	public function sanitise_preferences( $data ) {
		$new_data = $data;

		// Apply defaults if any field is left empty
		$new_data['creds'] = ( !empty( $data['creds'] ) ) ? $data['creds'] : $this->defaults['creds'];
		$new_data['log'] = ( !empty( $data['log'] ) ) ? sanitize_text_field( $data['log'] ) : $this->defaults['log'];

		return $new_data;
	}
}






// d($variable);
// sd($variable);
/**
*                                  fin
*                           personalisation 
*                             PROMOFOIRE
*//**
/* Stop Adding Functions Below this Line */
?>