<?php
/*
Plugin Name: taxonomy_promofoire
Description: Site specific code changes for promofoire
*/

/**
 * Registers the 'Produit' taxonomy for users.  This is a taxonomy for the 'user' object type rather than a 
 * post being the object type.
 */
function my_register_Produit_taxonomy() {

	 register_taxonomy(
		'Produit',
		'rtmedia_album',
		array(
			'public' => true,
			'labels' => array(
				'name' => __( 'Produits' ),
				'singular_name' => __( 'Produit' ),
				'menu_name' => __( 'Produits' ),
				'search_items' => __( 'Search Produits' ),
				'popular_items' => __( 'Popular Produits' ),
				'all_items' => __( 'All Produits' ),
				'edit_item' => __( 'Edit Produit' ),
				'update_item' => __( 'Update Produit' ),
				'add_new_item' => __( 'Add New Produit' ),
				'new_item_name' => __( 'New Produit Name' ),
				'separate_items_with_commas' => __( 'Separate Produits with commas' ),
				'add_or_remove_items' => __( 'Add or remove Produits' ),
				'choose_from_most_used' => __( 'Choose from the most popular Produits' ),
			),
			'rewrite' => array(
				'with_front' => true,
				'slug' => 'author/Produit' // Use 'author' (default WP user slug).
			),
			'capabilities' => array(
				'manage_terms' => 'edit_users', // Using 'edit_users' cap to keep this simple.
				'edit_terms'   => 'edit_users',
				'delete_terms' => 'edit_users',
				'assign_terms' => 'read',
			),
			'update_count_callback' => 'my_update_Produit_count' // Use a custom function to update the count.
		)
	);
}


?>
<?php
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_book_taxonomies', 0 );

// create two taxonomies, Types de produit and Tags for the post type "book"
function create_book_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Types de produit', 'taxonomy general name' ),
		'singular_name'     => _x( 'Type de produit', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Types de produit' ),
		'all_items'         => __( 'All Types de produit' ),
		'parent_item'       => __( 'Parent type' ),
		'parent_item_colon' => __( 'Parent type:' ),
		'edit_item'         => __( 'Edit type' ),
		'update_item'       => __( 'Update type' ),
		'add_new_item'      => __( 'Add New type' ),
		'new_item_name'     => __( 'New type Name' ),
		'menu_name'         => __( 'Type de produit' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'Type-de-produit' ),
	);

	register_taxonomy( 'Type_de_produit', array( 'rtmedia_album' ), $args );

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Tags', 'taxonomy general name' ),
		'singular_name'              => _x( 'Tag', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Tags' ),
		'popular_items'              => __( 'Popular Tags' ),
		'all_items'                  => __( 'All Tags' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Tag' ),
		'update_item'                => __( 'Update Tag' ),
		'add_new_item'               => __( 'Add New Tag' ),
		'new_item_name'              => __( 'New Tag Name' ),
		'separate_items_with_commas' => __( 'Separate Tags with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Tags' ),
		'choose_from_most_used'      => __( 'Choose from the most used Tags' ),
		'not_found'                  => __( 'No Tags found.' ),
		'menu_name'                  => __( 'Tags' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'Tag' ),
	);

	register_taxonomy( 'Tag', 'rtmedia_album', $args );
	
	// Add new taxonomy,  user sector
		$labels = array(
		'name'              => _x( 'activities', 'taxonomy general name' ),
		'singular_name'     => _x( 'activity', 'taxonomy singular name' ),
		'search_items'      => __( 'Search activities' ),
		'all_items'         => __( 'All activities' ),
		'parent_item'       => __( 'Parent type' ),
		'parent_item_colon' => __( 'Parent type:' ),
		'edit_item'         => __( 'Edit type' ),
		'update_item'       => __( 'Update type' ),
		'add_new_item'      => __( 'Add New type' ),
		'new_item_name'     => __( 'New type Name' ),
		'menu_name'         => __( 'activity' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'activity' ),
	);

	register_taxonomy( 'sector',  'user', $args );
	register_taxonomy( 'sector',  'rtmedia_album', $args );
	
	 // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name' => _x( 'Secteur', 'taxonomy general name' ),
        'singular_name' => _x( 'Secteur ', 'taxonomy singular name' ),
        'search_items' => __( 'Search Secteur' ),
        'all_items' => __( 'All Secteurs' ),
        'parent_item' => __( 'Parent Secteur Type' ),
        'parent_item_colon' => __( 'Parent Secteur Type:' ),
        'edit_item' => __( 'Edit Secteur Type' ),
        'update_item' => __( 'Update Secteur Type' ),
        'add_new_item' => __( 'Add New Secteur Type' ),
        'new_item_name' => __( 'New Secteur Type Name' ),
        'menu_name' => __( 'Secteur Type' ),
    );
 
    register_taxonomy( 'secteur', 'user', array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'secteur' ),
    ) );   
}
function bbg_save_extra_profile_data() {
        global $bp;
       
        update_user_meta( $bp->loggedin_user->id, 'my_category', $_POST['category'] ); 
}
add_action( 'xprofile_screen_edit_profile', 'bbg_save_extra_profile_data' );
?>