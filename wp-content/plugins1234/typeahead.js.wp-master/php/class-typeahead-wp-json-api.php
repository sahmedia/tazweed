<?php
if(!class_exists(Typeahead_WP_JSON_API)){
  class Typeahead_WP_JSON_API{

    function __construct(){
      if(!empty($_GET['data']) && method_exists($this, $_GET['data'])){
        require('../../../../wp-load.php');
        header('Content-Type: application/json');
        echo $this->$_GET['data']();
      }
    }

    private function tags(){
     
 
 //sleep(1);
 
$query = (!empty($_POST['q'])) ? strtolower($_POST['q']) : null;
 
if (!isset($query)) {
    die('Invalid query.');
}
 
$status = true;
$databaseUsers = array(
    array(
        "id"        => 4152589,
        "username"  => "TheTechnoMan",
        "avatar"    => "https://avatars2.githubusercontent.com/u/4152589"
    ),
    array(
        "id"        => 7377382,
        "username"  => "running-coder",
        "avatar"    => "https://avatars3.githubusercontent.com/u/7377382"
    ),
    array(
        "id"        => 748137,
        "username"  => "juliocastrop",
        "avatar"    => "https://avatars3.githubusercontent.com/u/748137"
    ),
    array(
        "id"        => 619726,
        "username"  => "cfreear",
        "avatar"    => "https://avatars0.githubusercontent.com/u/619726"
    ),
    array(
        "id"        => 5741776,
        "username"  => "solevy",
        "avatar"    => "https://avatars3.githubusercontent.com/u/5741776"
    ),
    array(
        "id"        => 906237,
        "username"  => "nilovna",
        "avatar"    => "https://avatars2.githubusercontent.com/u/906237"
    ),
    array(
        "id"        => 612578,
        "username"  => "Thiago Talma",
        "avatar"    => "https://avatars2.githubusercontent.com/u/612578"
    ),
    array(
        "id"        => 2051941,
        "username"  => "webcredo",
        "avatar"    => "https://avatars2.githubusercontent.com/u/2051941"
    ),
    array(
        "id"        => 985837,
        "username"  => "ldrrp",
        "avatar"    => "https://avatars2.githubusercontent.com/u/985837"
    ),
    array(
        "id"        => 1723363,
        "username"  => "dennisgaudenzi",
        "avatar"    => "https://avatars2.githubusercontent.com/u/1723363"
    ),
    array(
        "id"        => 2649000,
        "username"  => "i7nvd",
        "avatar"    => "https://avatars2.githubusercontent.com/u/2649000"
    ),
    array(
        "id"        => 2757851,
        "username"  => "pradeshc",
        "avatar"    => "https://avatars2.githubusercontent.com/u/2757851"
    )
);
 
$resultUsers = [];
foreach ($databaseUsers as $key => $oneUser) {
    if (strpos(strtolower($oneUser["username"]), $query) !== false ||
        strpos(str_replace('-', '', strtolower($oneUser["username"])), $query) !== false ||
        strpos(strtolower($oneUser["id"]), $query) !== false) {
        $resultUsers[] = $oneUser;
    }
}
 
$databaseProjects = array(
    array(
        "id"        => 1,
        "project"   => "jQuery Typeahead",
        "image"     => "http://www.runningcoder.org/assets/jquerytypeahead/img/jquerytypeahead-preview.jpg",
        "version"   => "1.7.0",
        "demo"      => 10,
        "option"    => 23,
        "callback"  => 6,
    ),
    array(
        "id"        => 2,
        "project"   => "jQuery Validation",
        "image"     => "http://www.runningcoder.org/assets/jqueryvalidation/img/jqueryvalidation-preview.jpg",
        "version"   => "1.4.0",
        "demo"      => 11,
        "option"    => 14,
        "callback"  => 8,
    )
);
 
$resultProjects = [];
foreach ($databaseProjects as $key => $oneProject) {
    if (strpos(strtolower($oneProject["project"]), $query) !== false) {
        $resultProjects[] = $oneProject;
    }
}
 
// Means no result were found
if (empty($resultUsers) && empty($resultProjects)) {
    $status = false;
}
 
 
 
 

     
      return json_encode(array(
    "status" => $status,
    "error"  => null,
    "data"   => array(
        "user"      => $resultUsers,
        "project"   => $resultProjects
    )
));
    }

    private function categories(){
      $category_names = array();
      $categories = get_categories();
      foreach($categories as $category){
        array_push($category_names, $category->name);
      }
      return json_encode($category_names);
    }
   private function categories1(){
      $category_names = array();
      $categories =  get_terms('Tag','orderby=count&hide_empty=0&hierarchical=0');
      foreach($categories as $category){
        array_push($category_names, $category->name);
      }
      return json_encode($category_names);
    }


private function members_name($user){
        $all_user_names = array();
   // Array of stdClass objects.

	if ( bp_has_members() ) {
    while ( bp_members() ) {
        bp_the_member(); 
		$img_markup= "";
		$img="";
		$replace = array(
        "<p>",
        "<img src=\"",
        "\" alt=\"\" />",
        "</p>"
    );
        $user_last = bp_get_user_meta( bp_get_member_user_id(), '42', true );
		$telephone = xprofile_get_field_data('39', bp_get_member_user_id());		
		$name= xprofile_get_field_data('133', bp_get_member_user_id());
		$img_markup= xprofile_get_field_data('107', bp_get_member_user_id());
		$img=  str_replace($replace ,'',$img_markup);
		$city= xprofile_get_field_data('61', bp_get_member_user_id());
		$desc= xprofile_get_field_data('134', bp_get_member_user_id());
		
		
    
		$member =  array(    
        "name"          => $name,
        "img"           => $img,
        "city"          => "aaaa",
        "id"            => "ANA",
        "conference"    => "Western",
        "division"      => "Pacific"
    );
		 
		
		array_push( $all_user_names , $member ) ;
		    }
}
    return json_encode($all_user_names);
    }
	


	
    private function titles($post_type){
      $all_post_titles = array();
      $post_id_query = new WP_Query(array('post_type' => $post_type, 'posts_per_page' => -1, 'fields' => 'ids'));
      foreach($post_id_query->posts as $id){
        array_push($all_post_titles, get_the_title($id));
      }
      return json_encode($all_post_titles);
    }

    private function post_titles(){
      return $this->titles('post');
    }

    private function page_titles(){
      return $this->titles('page');
    }
	private function album_titles(){
      return $this->titles('rtmedia_album');
    }

    private function users($role){
      $user_names = array();
      $user_query = new WP_User_Query(array('role' => $role));
      foreach($user_query->results as $user){
        array_push($user_names, $user->data->display_name);
      }
      return json_encode($user_names);
    }

    private function authors(){
      return $this->users('Author');
    }

    private function contributors(){
      return $this->users('Contributor');
    }

    private function editors(){
      return $this->users('Editor');
    }
  }
}
