=== WP Optimize Speed By xTraffic ===
Contributors: pepvn
Donate link: http://blog-xtraffic.pep.vn/
Tags:  optimize, cache, javascript, css, stylesheet, style, minify, combine, speed, performance, optimization, performance, render-blocking, speed, yslow, google, pagespeed, google pagespeed, google pagespeed insights, gtmetrix
Requires at least: 2.3
Tested up to: 4.2.2
Stable tag: trunk

Plugin "WP Optimize Speed By xTraffic" speed up WordPress site and increase website's Google PageSpeed Insights point.

== Description == 

Plugin ***[WP Optimize Speed By xTraffic](https://wordpress.org/plugins/wp-optimize-speed-by-xtraffic/ "WP Optimize Speed By xTraffic")*** speed up your WordPress website, save resources and bandwidth of server, increase website's Google PageSpeed Insights point.

Everything is done completely automatically, and you can change the options from the administration settings panel.

= The main features : =

***Optimize Speed*** : Make your WordPress website load faster from 200% or more (speed up Apache and Nginx).

= Details features include : =

* ***Optimize Speed*** : make page speed load amazingly fast by these features "Optimize Javascript + Optimize CSS (Style) + Optimize HTML"
  * Optimize Cache : Make your WordPress website load faster from 80% or more. Prebuild cache the most visited pages. This feature compatible with WooCommerce.
    * Database Cache : Support Multi-Cache : APC + Memcache + File.
  * Optimize Javascript :
    * Combine Javascript.
    * Minify Javascript.
    * Asynchronous Javascript Loading.
  * Optimize CSS (Style) :
    * Combine CSS.
    * Minify CSS.
    * Asynchronous CSS Loading.
  * Optimize HTML :
    * Minify HTML.
  * CDN (Content Delivery Network).

== Installation ==

To install the plugin "***WP Optimize Speed By xTraffic***" and get it working :

1. You can find keyword : "***WP Optimize Speed By xTraffic***" in search plugin of WordPress and install automatically.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Set options in tab "***WP Optimize***"

== Screenshots ==

1. Optimize Speed setting panel
2. Result Google PageSpeed Insights Mobile Score 100/100 after optimize with "WP Optimize Speed By xTraffic" plugin
2. Result Google PageSpeed Insights Desktop Score 100/100 after optimize with "WP Optimize Speed By xTraffic" plugin

== Changelog ==

= 1.0 =
* Plugin **WP Optimize Speed By xTraffic** automatically optimizes speed WordPress site and increase website's Google PageSpeed Insights point.