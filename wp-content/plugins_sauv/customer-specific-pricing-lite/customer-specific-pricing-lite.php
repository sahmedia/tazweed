<?php

/**
 * Plugin Name: Customer Specific Pricing (CSP) Lite for WooCommerce
 * Plugin URI: http://wisdmlabs.com/woocommerce-user-specific-pricing-extension/
 * Description: Allows administrator to add customer specific pricing for Simple WooCommerce Products.
 * Author: WisdmLabs
 * Version: 1.0
 * Author URI: http://wisdmlabs.com
 * License: GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */
/*
  Copyright (C) 2014  WisdmLabs

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_action('activate_customer-specific-pricing-lite/customer-specific-pricing-lite.php', 'showErrorMessageCSP');

add_action('plugins_loaded', 'cspLiteTextInitFunc');


function cspLiteTextInitFunc()
{
    load_plugin_textdomain('customer-specific-pricing-lite', false, dirname(plugin_basename(__FILE__)).'/languages/');
}

function showErrorMessageCSP()
{
    if ($_GET['action'] == 'error_scrape' &&  $_GET['plugin'] == 'customer-specific-pricing-lite/customer-specific-pricing-lite.php') {
        echo '<div class = "csp_main_error"><div class = "csp_error_wrapper">Customer Specific Pricing Lite could not be activated because pro vesrion is already activated.</div></div>' . "<style type='text/css'>" . "
            .csp_main_error {
                height: 100%;
                width: 100%;
                display: table;
            }
            .csp_error_wrapper {
                font-family: 'Open Sans', sans-serif;
                font-size: 13px;
                display: table-cell;
                height: 100%;
                vertical-align: middle;
              }" . "</style>";
    }
}

// Put your plugin code here
include('includes/class-wdm-wusp-install.php');
//Install Tables associated with User Specific Pricing plugin
register_activation_hook(__FILE__, array( 'csplite\WdmWuspInstall', 'createTables'));

/**
* Check if WooCommerce is active
*/

$arrayOfActivatedPlugins = apply_filters('active_plugins', get_option('active_plugins'));
if (in_array('woocommerce/woocommerce.php', $arrayOfActivatedPlugins)) {
    if (is_admin()) {
        include_once('includes/class-wdm-wusp-add-data-in-db.php');
        include_once('includes/class-wdm-wusp-get-data.php');
        include_once('includes/class-wdm-csp-tabs.php');
        include_once('includes/import/class-wdm-customer-specific-pricing-lite-import.php');
        include_once('includes/import/wdm-import-customer-specific-pricing-lite-file.php');
        include_once('includes/export/class-wdm-customer-specific-pricing-lite-export.php');
        include_once('includes/export/class-wdm-user-specific-pricing-export.php');
        include_once('includes/class-wdm-user-specific-pricing.php');
        new csplite\WdmUserSpecificPricing();

        $wdm_import = new cspImport\WdmCustomerSpecificPricingLiteImport();
        //Below values are shown in the dropdown of Import page
        $wdm_import->setOptionValuesPair(array(
            'Wdm_User_Specific_Pricing_Import' => 'User Specific Pricing',
            'Wdm_Role_Specific_Pricing_Import' => 'Role Specific Pricing',
            'Wdm_Group_Specific_Pricing_Import' => 'Group Specific Pricing'
        ));

        $wdm_export = new cspExport\WdmCustomerSpecificPricingExport();
        //Below values are shown in the dropdown of Export page
        $wdm_export->setOptionValuesPair(array(
          'User' => __('User Specific Pricing', 'customer-specific-pricing-lite'),
          'role' => __('Role Specific Pricing', 'customer-specific-pricing-lite'),
          'Group' => __('Group Specific Pricing', 'customer-specific-pricing-lite'),
        ));
    } else {
        include_once('includes/class-wdm-wusp-add-data-in-db.php');
        include_once('includes/class-wdm-wusp-get-data.php');
        include_once('includes/class-wdm-user-specific-pricing.php');
        new csplite\WdmUserSpecificPricing();
    }
} //if ends -- check for woocommerce activation
else {
    add_action('admin_notices', 'cspLiteBasePluginInactiveNotice');
}

if (!function_exists('cspLiteBasePluginInactiveNotice')) {
    function cspLiteBasePluginInactiveNotice()
    {
        if (current_user_can('activate_plugins')) :

        ?>
        <div id="message" class="error">
            <p><?php printf(__('%s Customer Specific Pricing Lite is inactive.%s Install and activate %sWooCommerce%s for Customer Specific Pricing Lite to work.', 'customer-specific-pricing-lite'), '<strong>', '</strong>', '<a href="http://wordpress.org/extend/plugins/woocommerce/">', '</a>'); ?></p>
        </div>
    <?php

        endif;
    }

}//if ends -- function exists

