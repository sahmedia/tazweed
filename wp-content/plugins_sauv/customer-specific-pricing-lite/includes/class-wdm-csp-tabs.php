<?php

namespace csplite;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WdmShowTabs')) {

    class WdmShowTabs
    {
        /**
         * [__construct Adds the Menu Page action]
         */
        public function __construct()
        {

            add_action('admin_menu', array($this, 'cspPageInit'), 99);
            add_action('admin_enqueue_scripts', array($this, 'singleViewEnqueueScripts'), 10, 1);
        //     wp_enqueue_style(
        //         'wdm_user_pricing_tab_css',
        //         plugins_url('/css/wdm-user-pricing-tab.css', dirname(__FILE__))
        //     );
        }

        public function singleViewEnqueueScripts($hook)
        {
            global $singleViewPage;

            if ($hook != $singleViewPage) {
                return;
            }
            wp_enqueue_style(
                'wdm_user_pricing_tab_css',
                plugins_url('/css/wdm-user-pricing-tab.css', dirname(__FILE__))
            );
        }
        /**
         * [cspPageInit Function To add menu page and sub menu page for csp]
         * @return [void]
         */
        public function cspPageInit()
        {
            global $singleViewPage;
            $singleViewPage = add_menu_page('Import', 'CSP', 'manage_options', 'csp_page_single_view', array(
                $this,
                'importExportTabs'
            ));
        }

        /**
         * [pricingManagerShowTabs Function to show The Pricing Manager/Search By/Rule
         * Log tabs of Pricing_MAnager sub menu Page under the CSP menu page]
         * @return [void]
         */
        public function pricingManagerShowTabs($current = 'Pricing_Manager')
        {
            $tabs = array(
                'Pricing_Manager' => __('Set Rules', 'customer-specific-pricing-lite'),
                'search_by' => __('Search By', 'customer-specific-pricing-lite'),
                'rule_log' => __('Rule Log', 'customer-specific-pricing-lite')
            );
            ?>

            <div>
                <ul class="subsubsub nomargin">
                    <?php
                    foreach ($tabs as $tab => $name) {
                        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
                        if ($tab == 'rule_log') {
                            echo '<li><a href="admin.php?page=csp_page_single_view&tabpm='.$tab.'" class="'.$class.'">'.$name.'</a> </li>';
                        } else {
                            echo '<li><a href="admin.php?page=csp_page_single_view&tabpm='.$tab.'" class="'.$class.'">'.$name.'</a> |</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
            <?php
        }

        /**
         * [pricingManagerTabs Function to navigate through Pricing
         *  Manager tabs in sub menu Page under the CSP menu page]
         * @return [void]
         */
        public function pricingManagerTabs()
        {
            global $pagenow;
            ?>
            <div class="wrap">
                <?php
                echo "<h3 class = 'import-export-header'>".__('Pricing Manager', 'customer-specific-pricing-lite')."</h3>";
                if (isset($_GET['tabpm'])) {
                    $this->pricingManagerShowTabs($_GET['tabpm']);
                } else {
                    $this->pricingManagerShowTabs('Pricing_Manager');
                }
                ?>
                <div id="poststuffPM">
                <?php
                if ($pagenow == 'admin.php' && $_GET['page'] == 'csp_page_single_view') {
                    if (isset($_GET['tabpm'])) {
                        $tab = $_GET['tabpm'];
                    } else {
                        $tab = 'Pricing_Manager';
                    }
                    switch ($tab) {
                        case 'search_by':
                            $searchByImg = plugins_url('/images/searchBy.png', dirname(__FILE__));
                            ?>
                            <div id="entry_dummy">
                                <div class="pew_uptp_cont">
                                    <p> <?php _e('This feature is available in the PRO version', 'customer-specific-pricing-lite') ?><a class="wdm_view_det_link"
                                    href="https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/"
                                    target="_blank"> <?php _e('View PRO features', 'customer-specific-pricing-lite') ?></a></p>
                                </div>
                                <div class="layer_parent">
                                    <div class="pew_upgrade_layer">
                                    </div>
                                    <img class = "acc_img" src = "<?php echo $searchByImg; ?>" />
                                </div>
                            </div>
                            <?php
                            break;
                        case 'rule_log':
                            $ruleLogImg = plugins_url('/images/ruleLog.png', dirname(__FILE__));
                            ?>
                            <div id="entry_dummy">
                                <div class="pew_uptp_cont">
                                    <p> <?php _e('This feature is available in the PRO version', 'customer-specific-pricing-lite') ?><a class="wdm_view_det_link"
                                     href="https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/"
                                     target="_blank"> <?php _e('View PRO features', 'customer-specific-pricing-lite') ?></a></p>
                                </div>
                                <div class="layer_parent">
                                    <div class="pew_upgrade_layer">
                                    </div>
                                    <img class = "acc_img" src = "<?php echo $ruleLogImg; ?>" />
                                </div>
                            </div>
                            <?php
                            break;
                        case 'Pricing_Manager':
                            $managerImg = plugins_url('/images/pricingManager.png', dirname(__FILE__));
                            ?>
                             <div id="entry_dummy">
                                <div class="pew_uptp_cont">
                                    <p> <?php _e('This feature is available in the PRO version', 'customer-specific-pricing-lite') ?><a class="wdm_view_det_link"
                                     href="https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/"
                                     target="_blank"> <?php _e('View PRO features', 'customer-specific-pricing-lite') ?></a></p>
                                </div>
                                <div class="layer_parent">
                                    <div class="pew_upgrade_layer">
                                    </div>
                                    <img class = "acc_img" src = "<?php echo $managerImg; ?>" />
                                </div>
                            </div>
                            <?php
                            break;
                    }//end of switch
                }//end of if
                            ?>
                </div>
            </div>
        <?php
        } // end of function pricingManagerTabs

        /**
         * [showImport Function to show the import operation. Allows do_action hook]
         * @return [void]
         */
        public function showImport()
        {
            do_action('show_import');
        }

        /**
         * [showExport Function to show the import operation. Allows do_action hook]
         * @return [void]
         */
        public function showExport()
        {
            do_action('show_export');
        }

        /**
         * [importExportTabs Function to show The Import/Export tabs
         *  of import_export sub menu Page under the CSP menu page]
         * @return [void]
         */
        public function importExportShowTabs($current = 'import')
        {
            $tabs = array(
                'import' => __('Import', 'customer-specific-pricing-lite'),
                'export' => __('Export', 'customer-specific-pricing-lite'),
                'Pricing_Manager' => __('Pricing Manager', 'customer-specific-pricing-lite')
            );
            ?>
            <h2 class="nav-tab-wrapper">
            <?php
            foreach ($tabs as $tab => $name) {
                // echo $name;
                $class = ( $tab == $current ) ? ' nav-tab-active' : '';
                echo "<a class='nav-tab$class' href='admin.php?page=csp_page_single_view&tabie=$tab'>".
                $name."</a>";
            }
            ?>
            </h2>
            <?php
        }

        /**
         * [importExportTabs Function to navigate through Import/Export tabs in the import/export CSP sub menu page]
         * @return [void]
         */
        public function importExportTabs()
        {
            global $pagenow;
            ?>
            <div class="wrap">
                <?php
                if (isset($_GET['tabie'])) {
                    $this->importExportShowTabs($_GET['tabie']);
                } elseif (isset($_GET['tabpm'])) {
                    $this->importExportShowTabs('Pricing_Manager');
                    // $this->pricingManagerTabs();
                } else {
                    $this->importExportShowTabs('import');
                }
                ?>
                <div id="poststuffIE">
                <?php
                if ($pagenow == 'admin.php' && $_GET['page'] == 'csp_page_single_view') {
                    if (isset($_GET['tabie'])) {
                        $tab = $_GET['tabie'];
                    } elseif (isset($_GET['tabpm'])) {
                        $tab = 'Pricing_Manager';
                    } else {
                        $tab = 'import';
                    }

                    switch ($tab) {
                        case 'import':
                            $this->showImport();
                            break;
                        case 'export':
                            $this->showExport();
                            break;
                        case 'Pricing_Manager':
                            // $this->pricingManagerShowTabs();
                            $this->pricingManagerTabs();
                    }//end of switch
                }//end of if
                ?>
                </div>
            </div>
        <?php
        } // end of function importExportTabs
    } //end of class
} //end of if class exists

new WdmShowTabs();
