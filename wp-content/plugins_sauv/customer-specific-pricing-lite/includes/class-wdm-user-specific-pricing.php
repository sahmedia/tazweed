<?php
namespace csplite;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WdmUserSpecificPricing')) {

    class WdmUserSpecificPricing
    {
        public function __construct()
        {
            add_action('woocommerce_product_write_panel_tabs', array(
                $this, 'userSpecificPricingTab'));
            add_action('woocommerce_product_write_panels', array(
                $this, 'userSpecificPricingTabOptions'));
            add_action('woocommerce_process_product_meta', array(
                $this, 'processUserPricingPairs'));
            add_filter('woocommerce_get_price', array($this, 'applyCustomPrice', ), 99, 2);
        }

        /**
         * Shows User Specific Pricing tab on Product create/edit page
         *
         * This tab shows options to add price for specific users
         * while creating a product or editing the product.
         */
        public function userSpecificPricingTab()
        {
            ?>
            <li class="user_specific_pricing_tab show_if_simple">
                <a href="#user_specific_pricing_tab_data"><?php _e('Customer Specific Pricing', 'customer-specific-pricing-lite'); ?></a>
            </li>
            <?php
        }

        /**
         * User Specific Tab Content
         * Shows the tab content i.e. allows admin to add pair and
         * remove user-price pair
         */
        public function userSpecificPricingTabOptions()
        {
            include(
                trailingslashit(
                    dirname(
                        dirname(__FILE__)
                    )
                ) . 'templates/print_user_specific_pricing_tab_content.php'
            );
        }

    /**
     * Process meta
     *
     * Processes the custom tab options when a post is saved
     */
        public function processUserPricingPairs($product_id)
        {
            $username_array  = (isset($_POST['wdm_woo_username']) ? $_POST['wdm_woo_username'] : array());
            $price_array     = (isset($_POST['wdm_woo_price']) ? $_POST['wdm_woo_price'] : array());

            $result = self::updatePairs($product_id, $username_array, $price_array);

            $username_array  = array_values($username_array);
            $price_array     = array_values($price_array);

            $_POST['wdm_woo_username'] = $result['wdm_username'];
            $_POST['wdm_woo_price'] = $result['wdm_prices'];

            if (array_key_exists('wdm_woo_username', $_POST) && count($_POST['wdm_woo_username']) > 0) {

                self::savePricing($_POST['wdm_woo_username'], $price_array, $product_id);

            }//if ends -- array key exist & array wdm_woo_username > 0
        }//function ends

        private function savePricing($username_array, $price_array, $product_id)
        {
            if (count($username_array) > 0) {
                foreach ($username_array as $username) {
                    $i = array_search($username, $username_array);
                    if (isset($price_array[$i]) && ! empty($price_array[$i])) {
                        \csplite\WdmWuspAddDataInDB::insertPricingInDb($username_array[$i], $product_id, isset($price_array[$i]) ? $price_array[$i] : '', 1);
                    }
                }//foreach ends
            }//if ends
        }


        private function updatePairs($product_id, $wdm_woo_username, $wdm_woo_price)
        {
            //Delete unwanted combinations
            global $wpdb;

            $wusp_pricing_table  = $wpdb->prefix . 'wusp_user_pricing_mapping';
            $user_product_mapping   = $wpdb->get_results($wpdb->prepare("SELECT id, user_id, price FROM {$wusp_pricing_table} WHERE product_id=%d", $product_id));

            $result = array();

            if ($user_product_mapping) {
                foreach ($user_product_mapping as $single_mapping) {
                    //Pairs which are in db but not in form submisisons need to be deleted
                    if (! in_array($single_mapping->user_id, $wdm_woo_username)) {
                        $wpdb->delete($wusp_pricing_table, array( 'id' => $single_mapping->id ), array(
                        '%d',
                        ));
                    } else {
                        //Pairs which are in db and also there in new form submissions need to be updated
                        $find_out_user_key = array_search($single_mapping->user_id, $wdm_woo_username);

                        $wpdb->update($wusp_pricing_table, array(
                        'price' => $wdm_woo_price[$find_out_user_key] ), array(
                        'id' => $single_mapping->id, ));

                        unset($wdm_woo_username[$find_out_user_key]);
                        unset($wdm_woo_price[$find_out_user_key]);
                    }
                }
            } //if ends -- User - product mapping exists

            $result['wdm_username'] = $wdm_woo_username;
            $result['wdm_prices'] = $wdm_woo_price;
            //exit;
            return $result;
        }

    /**
     * Apply Custom Price when user adds the product in the cart.
     */
        public function applyCustomPrice($price, $product)
        {
            // echo "Custom Price";
            // die();
            //If user is not logged in, Original Price should be returned
            if (! is_user_logged_in()) {
                return $price;
            }
            $user_id     = get_current_user_id();
            $product_id  = $product->id;
            if (isset($product->variation_id)) {
                $product_id = $product->variation_id;
            }

            if ('simple' == $product->product_type) {
                $product_id = $product->id;
            }

            $db_price = self::getDBPrice($user_id, $product_id, $price);

            if (isset($db_price) && $db_price) {
                return $db_price;
                // var_dump($db_price);
                // exit;
            } else {
                return $price; //return defalult set price
            }
        }

        private function getDBPrice($user_id, $product_id, $price)
        {
            $db_price = $price;

            $csp_price           = \csplite\WdmWuspGetData::getPriceOfProductForUser($user_id, $product_id);

            if (isset($csp_price)&& !empty($csp_price)) {
                $db_price = $csp_price;
            }
            return $db_price;
        }
    }

}

