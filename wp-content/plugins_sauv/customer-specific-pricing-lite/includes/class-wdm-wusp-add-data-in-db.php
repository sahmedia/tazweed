<?php
namespace csplite;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WdmWuspAddDataInDB')) {

    class WdmWuspAddDataInDB
    {
        public function __construct()
        {
        }

        /**
         * inserts pricing and user-product mapping in database
         * 1-- Flat Price , 0 -- Percent discount
         *
         * @global object $wpdb Object responsible for executing db queries
         */
        public static function insertPricingInDb($user_id, $product_id, $pricing, $price_type)
        {
            global $wpdb;
            $wpusp_pricing_table = $wpdb->prefix . 'wusp_user_pricing_mapping';
            if (! empty($user_id) && ! empty($product_id) && ! empty($pricing)) {
                $wpdb->insert($wpusp_pricing_table, array(
                    'price'                  => $pricing,
                    'product_id'             => $product_id,
                    'user_id'                => $user_id,
                    'flat_or_discount_price' => $price_type,
                ), array(
                    '%s',
                    '%d',
                    '%d',
                    '%d',
                ));
            }
        }
    }

}
