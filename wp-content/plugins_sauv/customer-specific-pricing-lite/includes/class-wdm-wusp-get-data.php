<?php
namespace csplite;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class WdmWuspGetData
{
    /**
     * Retireves all prices for a product from Database.
     *
     * @global object $wpdb
     * @param type $product_id
     * @return array returns array of user=>price combination
     */
    public static function getAllPricesForSingleProduct($product_id)
    {
        global $wpdb;
        $user_price_list     = array();
        $user_pricing_table  = $wpdb->prefix . 'wusp_user_pricing_mapping';
        $user_product_result = $wpdb->get_results($wpdb->prepare("SELECT user_id, price FROM {$user_pricing_table} WHERE product_id=%d", $product_id));
        if ($user_product_result) {
            foreach ($user_product_result as $single_product_result) {
                $user_price_list[ $single_product_result->user_id ] = $single_product_result->price;
            }
        }

        if (! empty($user_price_list)) {
            $user_price_list = array_filter($user_price_list);
        }
        return $user_price_list;
    }

    /**
     * Finds out price for specific user for specific product
     *
     * @global object $wpdb
     * @param int $user_id
     * @param int $product_id
     * @return mixed if price is found, price is returned. Otherwise it returns false
     */
    public static function getPriceOfProductForUser($user_id, $product_id)
    {
        global $wpdb;

        $user_pricing_table  = $wpdb->prefix . 'wusp_user_pricing_mapping';
        $price               = $wpdb->get_var($wpdb->prepare("SELECT price FROM {$user_pricing_table} WHERE user_id=%d AND product_id=%d", $user_id, $product_id));
        // var_dump($price);
        // exit;
        if ($price) {
            return $price;
        } else {
            return false;
        }
        return false;
    }
}
