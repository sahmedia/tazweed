<?php
namespace csplite;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('WdmWuspInstall')) {

    class WdmWuspInstall
    {

        /*
         * Creates all tables required for the plugin. It creates two
         * tables in the database. user_product_mapping stores the mapping of User
         * and Product. user_product_price_mapping table stores the
         * mapping of Price with unique key created in user_product_mapping
         * table
         */

        public static function createTables()
        {
            if (!in_array(
                'customer-specific-pricing-for-woocommerce/customer-specific-pricing-for-woocommerce.php',
                apply_filters(
                    'active_plugins',
                    get_option('active_plugins')
                )
            )) {
                global $wpdb;
                $wpdb->hide_errors();

                $collate = '';

                if ($wpdb->has_cap('collation')) {
                    if (!empty($wpdb->charset)) {
                            $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
                    }
                    if (!empty($wpdb->collate)) {
                            $collate .= " COLLATE $wpdb->collate";
                    }
                }

                // update_option('disable_csp_menu', 'no');

                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

                $user_pricing_mapping_table  = $wpdb->prefix . 'wusp_user_pricing_mapping';

                $user_price_mapping_table_query  = "
                CREATE TABLE IF NOT EXISTS {$user_pricing_mapping_table} (
                                    id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
                                    product_id bigint(20),
                                    user_id bigint(20),
                                    price numeric(13,4) UNSIGNED,
                                    flat_or_discount_price TINYINT(1),
                                    INDEX product_id (product_id),
                                    INDEX user_id (user_id)
                                ) $collate;
                                ";
                @dbDelta($user_price_mapping_table_query);
            } else {
                echo '<style type="text/css">.error p{ display:none;}</style>';
                exit();
            }
        }
    }
}
