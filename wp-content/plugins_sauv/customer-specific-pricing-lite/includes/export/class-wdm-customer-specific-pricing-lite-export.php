<?php

namespace cspExport;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
/**
 * Display the export option for customer specific,role specific and group specific pricing
 * @author WisdmLabs
 */
if (!class_exists('WdmCustomerSpecificPricingExport')) {
    class WdmCustomerSpecificPricingExport
    {

        private $_class_value_pairs = array();

        /**
         * call the function for display export option and create csv file
         */
        public function __construct()
        {
            add_action('show_export', array($this, 'wdmShowExportOptions'));
            add_action('wp_ajax_create_csv', array($this, 'wdmCreateCsv'));
        }

        /**
         * store class value pairs for display in dropdown
         * @param array $class_value_pairs
         */
        public function setOptionValuesPair($class_value_pairs)
        {
            $this->_class_value_pairs = $class_value_pairs;
        }

        /**
         * display export form
         */
        public function wdmShowExportOptions()
        {
            $array_to_be_send = array(
                    'ajaxurl'       =>  admin_url('admin-ajax.php'),
                    'please_Assign_valid_user_file_msg' => __('Please Assign User Specific Prices to export the CSV file successfully.', 'customer-specific-pricing-lite'),
                    'please_Assign_valid_role_file_msg' => __('Please Assign Role Specific Prices to export the CSV file successfully.', 'customer-specific-pricing-lite'),
                    'please_Assign_valid_group_file_msg' => __('Please Assign Group Specific Prices to export the CSV file successfully.', 'customer-specific-pricing-lite'),
                    'export_nonce'      => wp_create_nonce('export_nonce'),
                );
            wp_enqueue_style('wdm_csp_export_css', plugins_url('/css/export-css/wdm-csp-export.css', dirname(dirname(__FILE__))));
            wp_enqueue_script('wdm_csp_export_js', plugins_url('/js/export-js/wdm-csp-export.js', dirname(dirname(__FILE__))), array('jquery'), false, true);
            wp_localize_script('wdm_csp_export_js', 'wdm_csp_export_ajax', $array_to_be_send);
            ?>
            <div class="wrap">
                <h3 class="import-export-header"> <?php _e('CSP Export', 'customer-specific-pricing-lite') ?> </h3>
             </div>
            <div id="wdm_message" class="below-h2" style="display: block;"><p class="wdm_message_p"></p></div>
            <form name="export_form" class="wdm_export_form" method="POST">
                <table cellspacing="10px">
                    <tr><td>
                            <label for="dd_show_export_options"><?php _e('Select Export Type :', 'customer-specific-pricing-lite') ?> </label>
                            <select name="dd_show_export_options" id="dd_show_export_options">
                                <?php
                                foreach ($this->_class_value_pairs as $key => $val) {
                                    if ($key == 'User') {
                                        echo '<option value=' . $key . '>' . $val . '</option>';
                                    } else {
                                        echo '<option value=' . $key . ' disabled>' . $val .
                                         ' ('.__('Only for Premium Plugin', 'customer-specific-pricing-lite').')</option>';
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" value="<?php _e('Download Export File', 'customer-specific-pricing-lite') ?>" id="export" name="export" class="button button-primary">
                        </td></tr>
                </table>
            </form>
            <?php
        }

        /**
         * create csv file
         */
        public function wdmCreateCsv()
        {
            //WdmUserSpecificPricingExport
            $nonce = $_REQUEST['_wpnonce'];
            $nonce_verification = wp_verify_nonce($nonce, 'export_nonce');
            // echo "HI";
            //Override nonce verification for extending import functionality in any third party extension
            $nonce_verification = apply_filters('csp_export_nonce_verification', $nonce_verification);
            if (! $nonce_verification) {
                 echo "Security Check";
                 exit;
            } else {
                //Allow only admin to import csv files
                $capability_required_for_export = apply_filters('csp_export_allowed_user_capability', 'manage_options');
                $can_user_export = apply_filters('csp_can_user_export_csv', current_user_can($capability_required_for_export));
                if (!$can_user_export) {
                    echo "Security Check";
                    exit;
                }
            }

            $export_object = new WdmUserSpecificPricingExport();

            $user_product_mapping = $export_object->wdmFetchData();
            if (isset($user_product_mapping)) {
                $file_name = $export_object->wdmFileName();
                $upload_dir = wp_upload_dir();

                $output = fopen($upload_dir['basedir'] . $file_name, 'w');
                fputcsv($output, $user_product_mapping[0]);
                foreach ($user_product_mapping[1] as $row) {
                    $array = (array) $row;
                    fputcsv($output, $array);
                }
                fclose($output);
                echo $upload_dir['baseurl'] . $file_name;
            } else {
                echo menu_page_url('customer_specific_pricing_export');
            }
            exit();
        }
    }

}
