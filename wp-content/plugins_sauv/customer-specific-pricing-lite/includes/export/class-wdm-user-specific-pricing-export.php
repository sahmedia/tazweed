<?php

namespace cspExport;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
/**
 * fetch and return the customer specific pricing data for exporting in csv file
 * @author WisdmLabs
 */
if (! class_exists('WdmUserSpecificPricingExport')) {
    class WdmUserSpecificPricingExport extends WdmCustomerSpecificPricingExport
    {
        /**
         * fetch the data form database
         * @global type $wpdb
         * @return array content for creating csv
         */
        public function wdmFetchData()
        {
            global $wpdb;
            $wpusp_pricing_table      = $wpdb->prefix . 'wusp_user_pricing_mapping';
            $wdm_users                           = $wpdb->prefix . 'users';
            $wdm_post                 = $wpdb->prefix . 'posts';
            $user_headings   = array( 'Product id', 'User', 'Price' );
            $user_product_result            = $wpdb->get_results("SELECT product_id,user_login,price FROM $wpusp_pricing_table,$wdm_users,$wdm_post where $wpusp_pricing_table.user_id=$wdm_users.id and $wpusp_pricing_table.product_id = $wdm_post.id");
            if ($user_product_result) {
                return array( $user_headings, $user_product_result );
            }
        }

        public function wdmFileName()
        {
            return '/user_specific_pricing.csv';
        }
    }

}
