<?php

namespace cspImport;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
/**
 * check if class already exists
 *  @author WisdmLabs
 */
if (!class_exists('WdmCustomerSpecificPricingLiteImport')) {
    /**
     * Provides import option for importing customer specific,role specific and group specific csv files
     */
    class WdmCustomerSpecificPricingLiteImport
    {

        //  @var array save the optins value pairs
        private $classValuePairs = array();

        /**
         * call function for display import form
         */
        public function __construct()
        {
            add_action('show_import', array($this, 'wdmShowImportOptions'));
        }

        /**
         * Set the option value pairs
         * @param array $classValuePairs
         */
        public function setOptionValuesPair($classValuePairs)
        {
            $this->classValuePairs = $classValuePairs;
        }

        /**
         * display import form
         * @global type $post
         */
        public function wdmShowImportOptions()
        {
            wp_enqueue_style(
                'wdm_csp_import_css',
                plugins_url('/css/import-css/wdm-csp-import.css', dirname(dirname(__FILE__)))
            );
            wp_enqueue_script(
                'wdm_csp_import_js',
                plugins_url('/js/import-js/wdm-csp-import.js', dirname(dirname(__FILE__))),
                array('jquery'),
                false,
                true
            );
            wp_enqueue_style(
                'bootstrap_fileinput_css',
                plugins_url('/css/import-css/fileinput.min.css', dirname(dirname(__FILE__)))
            );
            wp_enqueue_script(
                'bootstrap_fileinput_js',
                plugins_url('/js/import-js/fileinput.js', dirname(dirname(__FILE__))),
                array('jquery'),
                false,
                true
            );
            wp_enqueue_script(
                'bootstrap_fileinput_cz_js',
                plugins_url('/js/import-js/fileinput_locale_cz.js', dirname(dirname(__FILE__))),
                array('bootstrap_fileinput_js'),
                false,
                true
            );

            wp_enqueue_style(
                'bootstrap_css',
                plugins_url('/css/import-css/bootstrap.css', dirname(dirname(__FILE__)))
            );
            wp_enqueue_script(
                'bootstrap_js',
                plugins_url('/js/import-js/bootstrap.min.js', dirname(dirname(__FILE__))),
                array('jquery'),
                false,
                true
            );

            wp_localize_script(
                'bootstrap_fileinput_js',
                'wdm_csp_translation',
                array(
                    'removeLabel' => __('Remove', 'customer-specific-pricing-lite'),
                    'removeTitle' => __('Clear selected files', 'customer-specific-pricing-lite'),
                    'browseLabel' => sprintf(__('Browse %s', 'customer-specific-pricing-lite'), '&hellip;'),
                    'uploadLabel' => __('Upload', 'customer-specific-pricing-lite'),
                    'uploadTitle' => __('Upload selected files', 'customer-specific-pricing-lite'),
                    'msgSizeTooLarge' => sprintf(__('File "%s" %s exceeds maximum allowed upload size of %s. Please retry your upload!', 'customer-specific-pricing-lite'), '{name}', '(<b>{size} KB</b>)', '<b>{maxSize} KB</b>'),
                    'msgFilesTooLess' => sprintf(__('You must select at least %s to upload. Please retry your upload!', 'customer-specific-pricing-lite'), '<b>{n}</b> {files}'),
                    'msgFileNotFound' => sprintf(__('File "%s" not found!', 'customer-specific-pricing-lite'), '{name}'),
                    'msgFileSecured' => sprintf(__('Security restrictions prevent reading the file "%s".', 'customer-specific-pricing-lite'), '{name}'),
                    'msgFileNotReadable' => sprintf(__('File "%s" is not readable.', 'customer-specific-pricing-lite'), '{name}'),
                    'msgInvalidFileType' => sprintf(__('Invalid type for file "%s". Only "%s" files are supported.', 'customer-specific-pricing-lite'), '{name}', '{types}'),
                    'msgInvalidFileExtension'=> sprintf(__('Invalid extension for file "%s". Only "%s" files are supported.', 'customer-specific-pricing-lite'), '{name}', '{extensions}'),
                    'msgValidationError' => __('File Upload Error', 'customer-specific-pricing-lite'),
                    'dropZoneTitle' => sprintf(__('Drag & drop files here %s', 'customer-specific-pricing-lite'), '&hellip;'),
                    'msgSelected' => sprintf(__('%s selected', 'customer-specific-pricing-lite'), '{n} {files}'),
                )
            );
            wp_localize_script(
                'wdm_csp_import_js',
                'wdm_csp_import',
                array(
                    'import_nonce'      => wp_create_nonce('import_nonce'),
                    'ajaxurl'       =>  admin_url('admin-ajax.php'),
                    'activateGroupsPlugin' => __('Please Activate the Groups Plugin ', 'customer-specific-pricing-lite'),
                    'importSuccessText'  => __('File Imported Successfully', 'customer-specific-pricing-lite'),
                    'validFileText' => __('Please Select valid file', 'customer-specific-pricing-lite'),
                )
            );
            ?>

            <div class="wrap"><h3 class="import-export-header"><?php _e('CSP Import', 'customer-specific-pricing-lite') ?></h3>
                <div class="update-nag imp-nag">

                    <?php _e('If the customer specific price already exists, the existing values will be overwritten by the new values (Lite version supports only simple products).<br/>Download a sample import template', 'customer-specific-pricing-lite') ?>
                     <a href=
                     "<?php echo plugins_url('/templates/user_specific_pricing.csv', dirname(dirname(__FILE__))); ?>"><?php _e('here', 'customer-specific-pricing-lite') ?>.</a>
                </div>

                  <div id='wdm_message' class='updated'><p class="wdm_message_p"></p></div>
            </div>

            <div id='wdm_import_form'>
                <form name="import_form" class="wdm_import_form" method="POST">
                    <div class="wdm-input-group">
                        <label for="dd_show_import_options"><?php _e('Select Import Type', 'customer-specific-pricing-lite') ?> : </label>
                        <select name="dd_show_import_options" id="dd_show_import_options">
                            <?php
                            foreach ($this->classValuePairs as $key => $val) {
                                if ($key == 'Wdm_User_Specific_Pricing_Import') {
                                    echo '<option value=' . $key . '>' . $val . '</option>';
                                } else {
                                    echo '<option value=' . $key . ' disabled>' . $val .
                                     ' ('.__('Only for Premium Plugin', 'customer-specific-pricing-lite').')</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <input type="file" name="csv" id="csv" title="<?php _e('Select File', 'customer-specific-pricing-lite') ?>" class="file" accept=".csv"
                     data-show-preview="false" data-show-upload="false" required>
                    <div class="wdm-input-group">
                        <input type="submit" value="<?php _e('Import', 'customer-specific-pricing-lite') ?>" id="wdm_import" name="import" class="button button-primary">
                    </div>
                </form>
            </div>
            <div id="wdm_import_data"></div>
            <?php
        }
    }
}