<?php

/**
 * import the customer specific pricing csv file in database
 * @author WisdmLabs
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
add_action('wp_ajax_import_customer_specific_file', 'importCustomerSpecificFileLite');
add_action('wp_ajax_nopriv_import_customer_specific_file', 'importCustomerSpecificFileLite');


function importCustomerSpecificFileLite()
{
    $nonce = $_REQUEST['_wpnonce'];
    $nonce_verification = wp_verify_nonce($nonce, 'import_nonce');

    //Override nonce verification for extending import functionality in any third party extension
    $nonce_verification = apply_filters('csp_import_nonce_verification', $nonce_verification);
    if (! $nonce_verification) {
         echo __("Security Check", 'customer-specific-pricing-lite');
         exit;
    } else {
        //Allow only admin to import csv files
        $capability_required_for_import = apply_filters('csp_import_allowed_user_capability', 'manage_options');
        $can_user_import = apply_filters('csp_can_user_import_csv', current_user_can($capability_required_for_import));
        if (!$can_user_import) {
            echo __("Security Check", 'customer-specific-pricing-lite');
            exit;
        }
    }

    global $wpdb;
    $wdm_user_product_price_mapping  = $wpdb->prefix . 'wusp_user_pricing_mapping';
    $wdm_users                       = $wpdb->prefix . 'users';
    $fetched_users = array();

    $csv_file                        = $_FILES[ 'csv' ][ 'tmp_name' ];

    //for checking headers
    $requiredHeaders                 = array( 'Product id', 'User', 'Price' );
    $fptr                               = fopen($csv_file, 'r');
    $firstLine                       = fgets($fptr); //get first line of csv file
    fclose($fptr);
    $foundHeaders                    = str_getcsv(trim($firstLine), ',', '"'); //parse to array
    //check the headers of file
    if ($foundHeaders !== $requiredHeaders) {
        die();
    }
    //$users     = array();
    if (false !== ($getfile = fopen($csv_file, 'r') )) {
        $data        = fgetcsv($getfile, 1000, ',');
        //display table headers
        echo '<table cellpadding="10"><thead class="stick"><th>'.__('Product id', 'customer-specific-pricing-lite').'</th><th>'.__('User', 'customer-specific-pricing-lite').'</th><th>'.__('Price', 'customer-specific-pricing-lite').'</th><th>'.__('Status', 'customer-specific-pricing-lite').'</th></thead>';

        $update_cnt  = 0;
        $insert_cnt  = 0;
        $count=0;
        while (false !== ($data      = fgetcsv($getfile, 1000, ','))) {
            $count++;
            $result                  = $data;
            $str                     = implode(',', $result);
            $slice                   = explode(',', $str);
            $product_id              = $slice[ 0 ];
            $user                    = $slice[ 1 ];
            $price                   = $slice[ 2 ];

            $status = null;
            $product = get_product($product_id);

            // cspPrintDebug($product);
            //check all values valid or not
            if (floatval($price) || $price==0.0) {
                //check if product exists or not
                if (isset($product->post) && (get_class($product) == 'WC_Product_Simple' && $product->post->post_type == "product")) {
                    if (!isset($fetched_users[$user])) {
                        //get user id
                        $fetched_users[$user] = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$wdm_users} where user_login=%s", $user));
                    }
                    $get_user_id = $fetched_users[$user];

                    if ($get_user_id == null) {
                         $status = __('User does not exist', 'customer-specific-pricing-lite');
                    } else {

                    //Update price for existing one
                        $result = $wpdb->get_var($wpdb->prepare("SELECT id FROM {$wdm_user_product_price_mapping} where product_id=%d and user_id=%d", $product_id, $get_user_id));
                        if ($result != null) {
                            $update_price = $wpdb->update(
                                $wdm_user_product_price_mapping,
                                array(
                                'price' => $price,
                                'flat_or_discount_price' => 1,
                                ),
                                array(
                                'product_id' => $product_id,
                                'user_id'   => $get_user_id,
                                ),
                                array(
                                '%f',
                                '%d',
                                ),
                                array('%d',
                                '%d')
                            );
                            if ($update_price == 0) {
                                $status = __('Record already exists', 'customer-specific-pricing-lite');
                            } else {
                                $status = __('Record Updated', 'customer-specific-pricing-lite');
                                $update_cnt ++;
                            }
                        } else {
                            //add entry in our table
                            if ($wpdb->insert(
                                $wdm_user_product_price_mapping,
                                array(
                                'product_id' => $product_id,
                                'user_id'   => $get_user_id,
                                'price' => $price,
                                'flat_or_discount_price' => 1,
                                ),
                                array(
                                '%d',
                                '%d',
                                '%s',
                                '%d',
                                )
                            )) {
                                $status = __('Record Inserted', 'customer-specific-pricing-lite');
                                $insert_cnt ++;
                            } else {
                                $status = __('Record could not be inserted', 'customer-specific-pricing-lite');
                            }
                        }
                    }
                } else {
                    $status = __('Either Product does not exist or not supported', 'customer-specific-pricing-lite');
                }
            } else {
                $status = __('Invalid field values', 'customer-specific-pricing-lite');
            }
            //display status message
            echo '<tr><td>' . $product_id . '</td><td>' . $user . '</td><td>' . $price . '</td><td>' . $status . '</td></tr>';
        }//end of while
        // $ruleManager->setUnusedRulesAsInactive();
        echo '</table>';
        //display summary
        echo '<div class="wdm_summary"><p>' . sprintf(__('Total number of rows processed %d. Total number of rows inserted %d and total number of rows updated %d', 'customer-specific-pricing-lite'), $count, $insert_cnt, $update_cnt) . '</p></div>';
    }
    die();
}
