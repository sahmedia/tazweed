// @author WisdmLabs

jQuery( 'document' ).ready( function ( jQuery ) {
    jQuery( "#wdm_import_data" ).hide();
    jQuery( '#wdm_message' ).hide();
    jQuery( ".wdm_import_form" ).submit( function () {
        return false;
    } );
    jQuery( '#wdm_import' ).click( function () {
        jQuery( "#wdm_import_data" ).hide();
       //  jQuery( '#wdm_message' ).hide();
        jQuery( '#csv' ).trigger( 'CustomImportEvent' );

    } );
    jQuery( '#dd_show_import_options' ).on( "change", function () {
        jQuery( "#wdm_import_data" ).hide();
        jQuery( '#wdm_message' ).hide();
    } );
    jQuery( '#csv' ).on( "CustomImportEvent", function () {
        formdata = new FormData();
        var i = 0, file;
        var len = this.files.length;
        if ( len > 0 )
        {
            for ( ; i < len; i++ ) {
                file = this.files[i];
                if ( formdata ) {
                    formdata.append( "csv", file );
                    formdata.append("_wpnonce", wdm_csp_import.import_nonce);
                }
            }
        }
        else
        {
            formdata = false;
        }
        if ( formdata ) {
            var val = jQuery( '#dd_show_import_options' ).val();
            if ( val == 'Wdm_User_Specific_Pricing_Import' ) {
                formdata.append('action', 'import_customer_specific_file');
            }
            jQuery.ajax( {
                type: 'POST',
                url: wdm_csp_import.ajaxurl,
                data: formdata,
                processData: false,
                contentType: false,
                success: function ( response ) {//response is value returned from php
                    //console.log(response);
                    if(response == 'group'){
                        jQuery( '#wdm_message' ).removeClass('updated');
                         jQuery( '#wdm_message' ).addClass('error');
                        jQuery( '.wdm_message_p' ).text(wdm_csp_import.activateGroupsPlugin);
                    }
                    else if ( response != '' ) {
                        jQuery( '#wdm_message' ).removeClass('error');
                            jQuery( '#wdm_message' ).addClass('updated');
                        jQuery( '.wdm_message_p' ).text(wdm_csp_import.importSuccessText);
                        jQuery( "#wdm_import_data" ).html( response );
                        jQuery( "#wdm_import_data" ).show();
                    }
                    else {
                        jQuery( '#wdm_message' ).removeClass('updated');
                         jQuery( '#wdm_message' ).addClass('error');
                        jQuery( '.wdm_message_p' ).text(wdm_csp_import.validFileText);
                    }
                    jQuery( '#wdm_message' ).show();
                }
            } );
        }
    } );
} );

