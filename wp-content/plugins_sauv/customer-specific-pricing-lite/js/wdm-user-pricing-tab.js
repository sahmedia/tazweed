jQuery(function() {

    wdm_accordion();
    function wdm_accordion() {
        // console.log("hello");
        jQuery('.accordion').accordion({
            clearStyle: true,
            heightStyle: "content",
            collapsible: true,
            // disabled: true,
        });
    }

    //variable which holds the tbody element of table.
    var scntDiv = jQuery('#wdm_user_specific_pricing_tbody');

    //Add first row of Username And Price
    wdmTempSelectHolder = jQuery(wdmUserPricingObject.wdmUsersDropdownHtml);
    wdmTempSelectHolder.find('option[value="' + wdmUserPricingObject.wdmFirstUsername + '"]').attr('selected', true);

    //Start new row
    startRow = "<tr>";

    //Show User dropdown
    selectHolder = "<td class='wdm_left_td' ><select name='wdm_woo_username[]' class='chosen-select'>" + wdmTempSelectHolder.html() + "</select></td>";

    //Show Price's Textbox
    priceTextbox = "<td colspan=3 class='wdm_left_td'><input type='text' name='wdm_woo_price[]' class='wdm_price' value='" + wdmUserPricingObject.wdmFirstPriceOfUser +"' />";

    //Show Remove row button
    removeRowButton = "<a class='wdm_remove_pair_link' href='#' id='remScnt'  ><img class='remove_user_price_pair_row_image' alt='Remove Row' title='Remove Row' src='" + wdmUserPricingObject.removeImagePath + "'/></a>";

    //If only one row is available
    addNewRow = "";
    if (!wdmUserPricingObject.moreThanOneRow) {
        addNewRow = "<button type='button' class='button add_new_pair_btn' id='wdm_add_new_user_price_pair'><img class='add_new_row_image' src='" + wdmUserPricingObject.addImagePath + "' />"+wdmUserPricingObject.addNewPair+"</button>";
    }

    endRow = "</td></tr>";

    scntDiv.prepend(startRow + selectHolder + priceTextbox + removeRowButton + addNewRow + endRow);

    wdmTempSelectHolder = null;
    wdmTempHtmlHolder = null;
    // jQuery(".chosen-select").chosen({ 'width': '200px'});
    jQuery("select.chosen-select").select2();

    //Get total number of Username-Price Pair rows
    var i = jQuery('#wdm_user_specific_pricing_tbody tr').size() + 1;

    //Add New row on clicking 'Add New User-Price Pair button
    jQuery('#wdm_add_new_user_price_pair').live('click', function() {

    //Show thead if it is hidden
    if (i == 1) {
        jQuery(".username_price_thead").show();
    }

    //Start new row
    startRow ="<tr>";

    //Show User dropdown
    selectHolder = "<td class='wdm_left_td' >" + wdmUserPricingObject.wdmUsersDropdownHtml + "</td>";

    //Show Price's Textbox
    priceTextbox = "<td colspan=3 class='wdm_left_td'><input type='text' name='wdm_woo_price[]' class='wdm_price' />";

    //Show Remove row button
    removeRowButton ="<a class='wdm_remove_pair_link' href='#' id='remScnt'  ><img class='remove_user_price_pair_row_image' alt='Remove Row' title='Remove Row' src='" + wdmUserPricingObject.removeImagePath + "'/></a>";

    //Add new pair button
    addNewRow = "<button type='button' class='button add_new_pair_btn' id='wdm_add_new_user_price_pair'><img class='add_new_row_image' src='" + wdmUserPricingObject.addImagePath + "' />"+wdmUserPricingObject.addNewPair+"</button>";

    //Lets remove add_new_pair_btn associated with earlier row
    jQuery(".add_new_pair_btn").remove();

    //end row
    endRow = "</td></tr>";

    jQuery(startRow + selectHolder + priceTextbox + removeRowButton +  addNewRow + endRow).appendTo(scntDiv);
    jQuery("select.chosen-select").select2();
    i++;
    return false;
    }); //end live

    var wdmFloatPattern = /^-?\d*(\.\d+)?$/; //Float Number Regex Pattern

    //If Price is not valid, then highlight the Price box
    jQuery(".wdm_price").live('focusout', function(){
            if (!wdmFloatPattern.test(jQuery(this).val())) {
                    jQuery(this).addClass('wdm_error').css('background', '#E35152');
            }
    }); //end live

    //When User edits the invalid field, clear the background of that field
    jQuery(".wdm_price").live('focusin', function(){
                    jQuery(this).css('background', '#FFFFFF').removeClass('wdm_error');
    }); //end live

    //On clicking Publish or Update button, check if all values are valid. If there is any invalid field, then show alert.
    jQuery("form#post").submit(function(){
            var wdmErrorFlag = 1;
            jQuery(".wdm_price").filter(function(){
                    if (jQuery(this).hasClass('wdm_error')) {
                            return wdmErrorFlag = 0;
                    }
            });
            if (wdmErrorFlag == 0) {
                    alert(wdmUserPricingObject.priceMappingAlert);
                    return false;
            }
    }); //end submit

    //If Remove Row image is clicked, the corresponding row is removed.
    jQuery('#remScnt').live('click', function()
    {

            var userPricePairRemovalElementVar = jQuery(this);

            //hide thead when there are no pairs of username and price
            if (i <= 2) {
                jQuery(".username_price_thead").hide();
            }

            userPricePairRemovalElementVar.parents('tr').remove();

            //Append Add new pair button to second last row after last row is removed
            if(!jQuery("#wdm_user_specific_pricing_tbody tr:last td:last .add_new_pair_btn").length){
            jQuery("#wdm_user_specific_pricing_tbody tr:last td:last").append("<button type='button' class='button add_new_pair_btn' id='wdm_add_new_user_price_pair'><img class='add_new_row_image' src='" + wdmUserPricingObject.addImagePath + "' />"+wdmUserPricingObject.addNewPair+"</button>");
            }
            i--;

            return false;
    }); //end live

});