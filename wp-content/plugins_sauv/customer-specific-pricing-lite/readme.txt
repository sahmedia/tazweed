=== Customer Specific Pricing Lite For WooCommerce ===

Contributors: WisdmLabs
Plugin URI: http://wordpress.org/plugin/customer-specific-pricing-lite/
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=info%40wisdmlabs%2ecom&lc=US&item_name=WisdmLabs%20Plugin%20Donation&no_note=0&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHostedGuest
Tags: customer specific pricing, user based pricing, dynamic pricing, woocommerce, woocommerce pricing plugin, woocommerce dynamic pricing, product pricing, discount pricing, flexible pricing woocommerce, pricing system woocommerce, price adjustments woocommerce, price control woocommerce, woocommerce exclusive pricing
Requires at least: 4.4
Tested up to: 4.5.1
Stable tag: trunk
WooCommerce Version: 2.5.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Now, dynamically adjust product prices for customers using Customer Specific Pricing Lite for WooCommerce.

== Description ==
= WooCommerce Pricing Plugin = 

 Provide customers a sense of exclusivity, by setting unique product prices for them.

While WooCommerce provides you the option to set a regular and sale price for products, these prices are applicable for all customers alike. But what if you wanted to set special prices *only* for certain customers? Customer Specific Pricing Lite for WooCommerce provides you- the store owner- this very flexibility.

Customers like the feeling of receiving something that others aren’t. By setting a special purchase price for certain customers, is a surefire way to increase your sales. In fact. studies have shown that offering a discounted price can dissuade customers from searching for other offers.

Customer Specific Pricing Lite makes setting these prices simple too. With an easy to use interface, you can quickly set an exclusive product purchase price for certain customers. What's better is the bulk import functionality, that provides you a one-click option to set prices for multiple customers at once.

The plugin works exclusively with WooCommerce 'Simple Products'.

= Offer Select Customers Discounts =
With CSP Lite, you- the WooCommerce store owner- can offer select customers a discount by setting a special purchase price for certain products.

= Bulk Import/Export Prices =
We've added a CSV import/export functionality to make it easy for you to set or view prices. This option allows you to bulk import/export multiple customer-price pairs at once.

= Manage Prices from Product Page =
You can add or remove prices set for individual customers right from the product add/edit page.

= Unlimited Customer-Price Pairs =
There isn't a limit on the customer-price pairs you can create. If a customer-price pair is added more than once, the latest price set is considered.

= Works with Sale Prices =
You do not need to worry about sale prices being set for products. The price you set for an individual customer will always supersede the sale price.

> Looking for more features? **Go Premium!** <br /><br />
> Upgrade to [Customer Specific Pricing Plugin for WooCommerce](https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/) for more features.

= Additional features in Customer Specific Pricing (Premium) Plugin =

= User Role Based Pricing =
Set prices for multiple users at once by setting a price for the User Role. This allows you to quickly setup a membership model with different user roles as membership levels.

= Group Based Pricing =
If you use the [Groups](https://wordpress.org/plugins/groups/) plugin to create user groups on your WooCommerce site, CSP can be used to set different prices for users belonging to particular groups.

= Pricing Manager for Easy Price Management =
Manage pricing for multiple products at once, using the smart pricing manager. The Pricing Manager can be used to quickly set prices for multiple customers, roles or groups and for multiple products in one go!

= Import/Export Prices for Roles/Groups =
Easily import prices for customers, roles and groups using a CSV file. And export these values as needed.

= Works with Simple and Variable products =
 The premium version works with both simple as well variable products. You can set different prices for each product variation for customers, user roles or groups.

> **Unlock more features with the Premium version!** <br /><br />
> Give [Customer Specific Pricing Plugin for WooCommerce](https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/) for more a try.


== Installation ==

= Minimum Requirements =
* WordPress 4.4 or greater
* WooCommerce 2.5.0 or greater
* PHP version 5.3 or greater

= Method 1 -Installing from the WordPress: =
1. In your admin panel, go to Plugins > Add New.
2. In the search field type "CSP Lite for WooCommerce" and click Search Plugins
3. Once you find CSP Lite, click Install Now to install it.
4. Once installed, click Activate Plugin to activate it.


= Method 2- Installing Manually : =
1. Download the plugin to your desktop.
2. Extract the downloaded zip file.
3. With your FTP program, upload the CSP Lite plugin folder to the wp-content/plugins folder in your WordPress directory online.
4. In your admin panel, go to Plugins screen and activate CSP Lite

= Plugin Settings =
* You can add customer-price rules using the 'Customer Specific Pricing' option under the 'Product Data' section.
* Prices can be imported in bulk using the import option under CSP > Import.
* View prices using export option under CSP > Export.

== Frequently Asked Questions ==

= How do I set special prices for select customers? =
1. Go to the add/edit screen of a product
2. Head over to 'Customer Specific Pricing' section under 'Product Data'
3. Under 'Customer Based Pricing' select the 'Customer Name' from the list of users, and add a price value under 'Price'
4. To add a new customer-price pair, click 'Add New Pair' or 'Add New Customer-Price Pair'
5. Publish/Update the product once any changes are made

= How do I import prices for multiple customers at once? =
1. To bulk import prices, create a CSV file with the Product Id, User Name and Price fields (a sample CSV can be downloaded by heading over to CSP > Import)
2. Go to CSP > Import, and select the CSV file you've created
3. Click on 'Import' to import product prices for the customers
4. Do note, that the prices you import will override previously set prices for any customer

= Does CSP Lite for WooCommerce support group and role based pricing? =
Currently, CSP Lite supports user based pricing. For group or role based pricing you will have to upgrade to the [premium version of the Customer Specific Pricing plugin](https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/).

= Does CSP Lite for WooCommerce work with variable products? =
CSP Lite for WooCommerce works exclusively with WooCommerce 'Simple Products'. The [premium version](https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/) helps manage prices of 'Variable Products'.

= What kind of users does this plugin support? =
You can add pricing for any user with an account on your website.
The Premium plugin also allows for role and group base pricing.

= Is there any limit on how many customer-price pairs can be added? =
No, there is no such limit. You can add as many customer-price pairs as you want.

== Screenshots ==

1. Set Customer-Price pair for a product
2. Bulk import Customer-price pairs
3. Export Customer-price pairs

== Upgrade Notice ==

== Changelog ==

= 1.0 =
* Plugin Released
