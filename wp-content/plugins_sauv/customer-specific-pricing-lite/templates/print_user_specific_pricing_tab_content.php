<?php
namespace csplite;

/*Print the content of the tab*/
/*Saving of these custom fields is handled by
 function process_product_meta_user_specific_pricing_tab of Wdm_User_Specific_Pricing class.*/

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
global $post;
wp_enqueue_script('jquery');
wp_enqueue_style('wdm_user_pricing_tab_css', plugins_url('/css/wdm-user-pricing-tab.css', dirname(__FILE__)));

$removeImagePath = plugins_url('/images/minus-icon.png', dirname(__FILE__));
$addImagePath = plugins_url('/images/plus-icon.png', dirname(__FILE__));

/**
 * Fetch WordPress users. Using wdm_usp_user_dropdown_params filter developers
 * can decide what to be shown in the Users dropdown
 */
$args = apply_filters('wdm_usp_user_dropdown_params', array(
    'show_option_all' => null, // string
    'show_option_none' => null, // string
    'hide_if_only_one_author' => null, // string
    'orderby' => 'display_name',
    'order' => 'ASC',
    'include' => null, // string
    'exclude' => null, // string
    'multi' => false,
    'show' => 'display_name',
    'echo' => false,
    'selected' => false,
    'include_selected' => false,
    'name' => 'wdm_woo_username[]', // string
    'id' => null, // integer
    'class' => 'chosen-select', // string
    'blog_id' => $GLOBALS['blog_id'],
    'who' => null // string
    ));

$wdmUsersDropdown = wp_dropdown_users($args);

$arrayOfUsernamePricePair = array();

// Retrieve pricing from db and arrange it in associative array where key is user_id
// and value is price.
$arrayOfUsernamePricePair = WdmWuspGetData::getAllPricesForSingleProduct($post->ID);

if (empty($arrayOfUsernamePricePair)) {
        $wdmFirstUsername = "";
        $wdmFirstPriceOfUser = "";
} else {
    //Retrieve value of first user saved in db for corresponding product
    $listOfAllUsers = array_keys($arrayOfUsernamePricePair);

    // Push value of first user-price to variable. This variable would be
    // passed to JS file.
    $wdmFirstUsername = $listOfAllUsers[0];
    $wdmFirstPriceOfUser = $arrayOfUsernamePricePair[$listOfAllUsers[0]];

    //Unset first user-price pair from array
    unset($arrayOfUsernamePricePair[$listOfAllUsers[0]]);
    unset($listOfAllUsers[0]);

    //Reset array numbring
    $listOfAllUsers = array_values($listOfAllUsers);
}

?>
<div id="user_specific_pricing_tab_data" class="panel show_if_simple  woocommerce_options_panel">
    <p>
        <p><strong><?php _e('Notes', 'customer-specific-pricing-lite') ?>:</strong></p>
        <ol>
            <strong>
                <li><?php _e('If a customer is added more than once, the customer-price combination first in the list will be saved, and other combinations will be removed.', 'customer-specific-pricing-lite') ?></li>
                <li><?php _e('If the price field is left blank, then default product price will be shown.', 'customer-specific-pricing-lite') ?></li>
                <li><?php _e('If a customer belongs to multiple groups (or roles), the least price set for the group (or role) will be applied', 'customer-specific-pricing-lite') ?></li>
                <li><?php _e('The priorities are as follows', 'customer-specific-pricing-lite') ?> -
                <ol>
                    <li><?php _e('Customer Specific Price', 'customer-specific-pricing-lite') ?></li>
                    <li><?php _e('Role Specific Price', 'customer-specific-pricing-lite') ?></li>
                    <li><?php _e('Group Specific Price', 'customer-specific-pricing-lite') ?></li>
                    <li><?php _e('Regular Price', 'customer-specific-pricing-lite') ?></li>
                </ol>
                </li>
            </strong>
        </ol>
        <p>
            <strong><?php _e('Remember to Publish/Update the product to save any changes made.', 'customer-specific-pricing-lite') ?></strong>
        </p>

    </p>
    <div class="accordion">
        <?php do_action('wdm_add_before_csp'); ?>
        <h3 class="wdm-heading"><?php _e('Customer Based Pricing', 'customer-specific-pricing-lite') ?></h3>
        <div>
            <button type="button" class="button" id="wdm_add_new_user_price_pair"><?php _e('Add New Customer-Price Pair', 'customer-specific-pricing-lite') ?></button>
            <div class="options_group wdm_user_pricing_tab_options">
                <table cellpadding="0" cellspacing="0" class="wc-metabox-content" style="display: table;">
                    <thead class="username_price_thead">
                        <tr>
                            <th style="text-align: left">
                                <?php _e('Customer Name', 'customer-specific-pricing-lite') ?>
                            </th>
                            <th colspan=3 style="text-align: left">
                                <?php _e('Price', 'customer-specific-pricing-lite') ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="wdm_user_specific_pricing_tbody"></tbody>
                </table>
                <div class="pew_uptp_cont banner2">
                    <p> <?php _e('Set multiple prices instantly for users using the pricing manager', 'customer-specific-pricing-lite') ?><a class="wdm_view_det_link" href="admin.php?page=csp_page_single_view&tabpm=Pricing_Manager" target="_blank"> <?php _e('View details', 'customer-specific-pricing-lite') ?> </a></p>
                </div>
            </div>
        </div>
        <h3 class="wdm-heading"><?php _e('Role Based Pricing', 'customer-specific-pricing-lite') ?></h3>
        <div style="padding: 0;">
            <?php $roleBasedImg = plugins_url('/images/roleBasedPricing1.png', dirname(__FILE__)); ?>
            <div id="entry_dummy_acc">
                <div class="layer_parent">
                    <div class="pew_upgrade_layer">
                    </div>
                    <img src = "<?php echo $roleBasedImg; ?>" />
                </div>
                <div class="pew_uptp_cont">
                    <p> <?php _e('This feature is available in the PRO version', 'customer-specific-pricing-lite') ?><a class="wdm_view_det_link"
                     href="https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/"
                      target="_blank"> <?php _e('View PRO features', 'customer-specific-pricing-lite') ?> </a></p>
                </div>
            </div>
        </div>
        <h3 class="wdm-heading"><?php _e('Group Based Pricing', 'customer-specific-pricing-lite') ?></h3>
        <div style="padding: 0;">
            <?php $groupBasedImg = plugins_url('/images/groupBasedPricing1.png', dirname(__FILE__)); ?>
            <div id="entry_dummy_acc">
                <div class="layer_parent">
                    <div class="pew_upgrade_layer">
                    </div>
                    <img src = "<?php echo $groupBasedImg; ?>" />
                </div>
                <div class="pew_uptp_cont">
                    <p> <?php _e('This feature is available in the PRO version', 'customer-specific-pricing-lite') ?><a class="wdm_view_det_link"
                     href="https://wisdmlabs.com/woocommerce-user-specific-pricing-extension/"
                      target="_blank"> <?php _e('View PRO features', 'customer-specific-pricing-lite') ?> </a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

//Flag to track if more than one rows are avaialble
$moreThanOneRow = false;

if (count($arrayOfUsernamePricePair) > 0 && $arrayOfUsernamePricePair != false) {
    $moreThanOneRow = true;
    echo "<script type='text/javascript'>var scntDiv = jQuery(
    '#wdm_user_specific_pricing_tbody'
    ); var wdmTempSelectHolder = null; var wdm_temp_html_holder = null;</script>";

    // Javascript is added here because it is going to show all combinations
    // and hence it needs looping. To solve the purpose of looping, javascript
    // is being added. This javascript shows only combinations saved in db.

    for ($j = 0; $j < count($arrayOfUsernamePricePair); $j++) {
?>
    <script type="text/javascript">
        jQuery(function() {
            //Print all combinations saved in the database except first combination.
            wdmTempSelectHolder = jQuery("<?php echo str_replace("\n", "", $wdmUsersDropdown); ?>");
            wdmTempSelectHolder.find('option[value="<?php echo $listOfAllUsers[$j]; ?>"]').attr('selected', true);

            //Start new row
            var startRow = "<tr>";

            //Show User dropdown
            var selectHolder = "<td class='wdm_left_td' >"+
            "<select name='wdm_woo_username[]' class='chosen-select'>"+
            wdmTempSelectHolder.html()+"</select></td>";

            //Show Price's Textbox
            var priceTextbox = "<td colspan=3 class='wdm_left_td'>"+
            "<input type='text' name='wdm_woo_price[]' class='wdm_price' "+
            "value='<?php echo $arrayOfUsernamePricePair[$listOfAllUsers[$j]]; ?>' />"+
            "<a class='wdm_remove_pair_link' href='#' id='remScnt'  >";

            //Show Remove row button
            var removeRowButton = "<img alt='<?php _e('Remove Pair', 'customer-specific-pricing-lite') ?> title='<?php _e('Remove Pair', 'customer-specific-pricing-lite') ?>'"+
            " class='remove_user_price_pair_row_image'"+
            " src='<?php echo $removeImagePath; ?>'/></a>";

            var addNewRow = '';
            <?php
            if ($j == count($arrayOfUsernamePricePair) - 1) {
            ?>
            //Add new pair button
            addNewRow = "<button type='button' class='button add_new_pair_btn' id='wdm_add_new_user_price_pair'><img class='add_new_row_image' src='<?php echo $addImagePath ?>' /> <?php _e('Add New Pair', 'customer-specific-pricing-lite') ?></button>";
            <?php
            }
            ?>
            //end row
            var endRow = "</td></tr>";

            scntDiv.append(
                    startRow +
                    selectHolder +
                    priceTextbox +
                    removeRowButton +
                    addNewRow +
                    endRow
            );
            wdmTempSelectHolder = null;

            jQuery("select.chosen-select").select2();
        });
    </script>
                <?php
    }
}

$arrayOfValuesToBePassedToJs = array(
    'wdmUsersDropdownHtml' => str_replace("\n", "", $wdmUsersDropdown),
    'wdmFirstUsername' => $wdmFirstUsername,
    'wdmFirstPriceOfUser' => $wdmFirstPriceOfUser,
    'removeImagePath' => $removeImagePath,
    'addImagePath' => $addImagePath,
    'moreThanOneRow' => $moreThanOneRow,
    'addNewPair'    => __('Add New Pair', 'customer-specific-pricing-lite'),
    'priceMappingAlert'  => __('It seems that all prices mapped to users are not valid. Please verify it again.', 'customer-specific-pricing-lite'),
);
wp_enqueue_script('select2');
wp_enqueue_script('jquery-ui-accordion');
wp_enqueue_script(
    'wdmUserPricingTabJs',
    plugins_url('/js/wdm-user-pricing-tab.js', dirname(__FILE__)),
    'jquery',
    false,
    true
);

wp_localize_script(
    'wdmUserPricingTabJs',
    'wdmUserPricingObject',
    $arrayOfValuesToBePassedToJs
);
