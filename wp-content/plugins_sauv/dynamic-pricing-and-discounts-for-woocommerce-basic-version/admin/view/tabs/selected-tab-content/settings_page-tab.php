<?php
    
$plugin_name = 'dynamicdiscountpricing'; 
$dummy_settings['product_rules_on_off']='enable';
$dummy_settings['combinational_rules_on_off']='disable';
$dummy_settings['category_rules_on_off']='disable';
$dummy_settings['cart_rules_on_off']='disable';
$dummy_settings['price_table_on_off']='enable';
$dummy_settings['auto_add_free_product_on_off']='disable';
$dummy_settings['pricing_table_qnty_shrtcode']='nos.';



$settings=get_option('xa_dynamic_pricing_setting',$dummy_settings);    
extract($settings);
if(!isset($pricing_table_qnty_shrtcode))
{
    $pricing_table_qnty_shrtcode="nos.";
}

?>

</br><form method="GET">
<table class="table"style="font-size: small;margin-left: 20px;" >
	<tbody style="font-size: inherit;width: 100%;">
            <div style="border-style: solid; border-width: 1px; border-color: grey;background: lightsteelblue">
                <span style='margin-left:20px;'>  Modes</span>
            </div>
                <tr  style='height:50px;'>
                        <td  style='min-width: 300px;'>
                            Product Rules 
                        </td>
                        <td>
                            <select name='product_rules_on_off'  selected='<?php   echo $product_rules_on_off  ?>'>
                                <option value='enable'     <?php  echo(($product_rules_on_off=='enable')?'selected':'');  ?> >Enable</option>
                                <option value='disable'   <?php  echo(($product_rules_on_off=='disable')?'selected':'');  ?>>Disable</option>
                            </select>
                        </td>
                    </tr>
                    <tr  style='height:50px;'>
                        <td style='min-width: 300px;'>
                            Combinational Rules 
                        </td>
                        <td>
                            <select name='combinational_rules_on_off'  selected='<?php   echo $combinational_rules_on_off  ?>'>
                                <option value='disable'  <?php  echo(($combinational_rules_on_off=='disable')?'selected':'');  ?>>Disable</option>
                            </select>
                        </td>
                    </tr>
                    <tr  style='height:50px;'>
                        <td style='min-width: 300px;'>
                            Category Rules 
                        </td>
                        <td>
                            <select name='category_rules_on_off'  selected='<?php   echo $category_rules_on_off  ?>'>
                                <option value='disable'  <?php  echo(($category_rules_on_off=='disable')?'selected':'');  ?>>Disable</option>
                            </select>
                        </td>
                    </tr>
                    <tr  style='height:50px;'>
                        <td style='min-width: 300px;'>
                            Cart Rules 
                        </td>
                        <td>
                            <select name='cart_rules_on_off'  selected='<?php   echo $cart_rules_on_off  ?>'>
                                <option value='disable'  <?php  echo(($cart_rules_on_off=='disable')?'selected':'');  ?>>Disable</option>
                            </select>
                        </td>
                    </tr>
	</tbody>
	<tfoot>
	</tfoot>
</table>
<table class="table"style="font-size: small;margin-left: 20px;" >
	<tbody style="font-size: inherit;width: 100%;">
            <div style="border-style: solid; border-width: 1px; border-color: grey;background: lightsteelblue">
                <span style='margin-left:20px;'> Functionality</span>
            </div>
                <tr  style='height:50px;'>
                        <td   style='min-width: 300px;'>
                            Display Prices Table on Product Page 
                        </td>
                        <td>
                            <select name='price_table_on_off' selected='<?php   echo $price_table_on_off  ?>'>
                                <option value='enable'  <?php  echo(($price_table_on_off=='enable')?'selected':'');  ?>>Enable</option>
                                <option value='disable'  <?php  echo(($price_table_on_off=='disable')?'selected':'');  ?>>Disable</option>
                            </select>
                        </td>
                </tr>
                
                
                <tr  style='height:50px;'>
                        <td>
                            Short Code For Quantity:
                        </td>
                        <td>
                            <input type="text" name="pricing_table_qnty_shrtcode" value='<?php echo $pricing_table_qnty_shrtcode;   ?>' />
                        </td>
                </tr>
                        
                        
                <tr  style='height:50px;'>
                        <td   style='min-width: 300px;'>
                            Automatically Add Free Product to cart 
                        </td>
                        <td>
                            <select name='auto_add_free_product_on_off' selected='<?php   echo $auto_add_free_product_on_off  ?>'>
                                <option value='disable'  <?php  echo(($auto_add_free_product_on_off=='disable')?'selected':'');  ?>>Disable</option>
                            </select>
                        </td>   
                </tr>
                      
                    
	</tbody>
	<tfoot>
	</tfoot>
</table>
    </br>
</br>
<p class="submit"   ><input type="submit" style='width:100px;'   name="submit" id="submit" class="button button-primary" value="Save"></p>

</form>





 

