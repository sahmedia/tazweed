

<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.xadapter.com
 * @since      1.0.0
 *
 * @package    xa_dynamic_pricing_plugin
 * @subpackage xa_dynamic_pricing_plugin/admin/partials
 */
 
 
		
 		/*$prev_data=get_option('xa_dp_rules');
		unset($prev_data['rules']);
		update_option('xa_dp_rules',$prev_data);
		*/
		if(isset($_GET['tab']))
		{
			$active_tab=$_GET['tab'];
		}
		else
		{
			$active_tab='product_rules';
		}
		
		//die(print_r($_GET));
		if(isset($_GET['submit']) && !isset($_GET['update']))				//Submit And Not Edit Then Saving New Record
		{	
			$current_tab_loc=isset($_GET['tab']) && !empty($_GET['tab'])?  $_GET['tab'].'/': 'product_rules/';
                                                            $path=  xa_dp_root_path.'admin/data/'.$current_tab_loc.'save-options.php';
		include_once ( $path );
                
		}
		elseif(isset($_GET['edit']) && !empty($_GET['edit']) )				//Loading Edit Form Or Updating Data
		{
                                        
			$old_option=get_option('xa_dp_rules')[$active_tab];
			$_GET=array_merge($_GET,$old_option[$_GET['edit']]);
			$current_tab_loc=isset($_GET['tab']) && !empty($_GET['tab'])?  $_GET['tab'].'/': 'product_rules/';
			$path=xa_dp_root_path.'admin/data/'.$current_tab_loc.'load-edit.php';
			include_once ( $path );

		}
		elseif(isset($_GET['update']) && !empty($_GET['update']) )
		{
			$path=isset($_GET['tab']) && !empty($_GET['tab'])?  $_GET['tab'].'/': 'product_rules/';
			
			$path=xa_dp_root_path.'admin/data/'.$path.'update-options.php';
			
			include_once ( $path );
		}
		elseif(isset($_GET['delete']))
		{
			$path=isset($_GET['tab']) && !empty($_GET['tab'])?  $_GET['tab'].'/': 'product_rules/';
			$path=xa_dp_root_path.'admin/data/'.$path.'delete-options.php';
			$active_tab=$_GET['tab'];
		include_once ( $path );
		}
		else
		{
			$active_tab='product_rules';
		}

		require('tabs/tabs-html-render.php');


?>


