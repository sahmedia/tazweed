<?php

/**
 *
 * @link              https://www.xadapter.com
 * @package           eh-dynamic-pricing-discounts
 *
 * @wordpress-plugin
 * Plugin Name:       Dynamic Pricing and Discounts for WooCommerce (Basic Version)
 * Plugin URI:        https://www.xadapter.com
 * Description:     This plugin helps you to set discounts and pricing dynamically based on minimum quantity,weight,price and allow you to set maximum allowed discounts on every rule.
 * Version:           1.1.7
 * Author:            extensionhawk,akshayagrawal
 * Author URI:        https://www.xadapter.com/vendor/extensionhawk/
 * Text Domain:       eh-dynamic-pricing-discounts
 * Domain Path:       /languages
 */

    //Languages Updated
    
// If this file is called directly, abort.
define('xa_dp_root_path',plugin_dir_path( __FILE__ ));
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( !class_exists( 'woocommerce' ) ) { 
    
add_action( 'admin_init', 'xa_dp_my_plugin_deactivate' );
          function xa_dp_my_plugin_deactivate() {
              
               if ( !class_exists( 'woocommerce' ) )
               { 
                                    deactivate_plugins( plugin_basename( __FILE__ ) );
                                   wp_safe_redirect( admin_url('plugins.php') );
                                    
               }
          }

    
}

//$GLOBALS['final_discount'] =array('22'=>array(2,),);
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-xa-dynamic-pricing-plugin-activator.php
 */
function xa_dp_activate_xa_dynamic_pricing_plugin_basic() {
    
                        if ( is_plugin_active('eh-dynamic-pricing-discounts/eh-dynamic-pricing-discounts.php') ){ 
        deactivate_plugins( basename( __FILE__ ) );
		wp_die( __("You already have the premium version can not installl the basic version. Kindly deactivate and delete Dynamic Pricing and Discounts for WooCommerce Extension and then try again", "eh-dynamic-pricing-discounts" ), "", array('back_link' => 1 ));
	}
                  if ( !class_exists( 'woocommerce' ) ) { exit('Please Install and Activate Woocommerce Plugin First, Then Try Again!!');}
                  
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-xa-dynamic-pricing-plugin-activator.php';
	xa_dp_dynamic_pricing_plugin_Activator::activate();
}


/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-xa-dynamic-pricing-plugin-deactivator.php
 */
function xa_dp_deactivate_xa_dynamic_pricing_plugin_basic() {
      if ( !class_exists( 'woocommerce' ) )
               { 
               echo 'Dynamic Pricing And Discounts Plugin could not start because WooCommerce Plugin is Deactivated!!';
                //wp_safe_redirect( admin_url('plugins.php') );
               }
               
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-xa-dynamic-pricing-plugin-deactivator.php';
	xa_dynamic_pricing_plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'xa_dp_activate_xa_dynamic_pricing_plugin_basic' );
register_deactivation_hook( __FILE__, 'xa_dp_deactivate_xa_dynamic_pricing_plugin_basic' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-xa-dynamic-pricing-plugin.php';

add_action('init','xa_dp_load_plugin_textdomain'); 

if(!function_exists('xa_dp_load_plugin_textdomain'))
{
    function xa_dp_load_plugin_textdomain() {
load_plugin_textdomain( 'eh-dynamic-pricing-discounts', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
}


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
if(!function_exists('xa_dp_run_xa_dynamic_pricing_plugin'))
{
    function xa_dp_run_xa_dynamic_pricing_plugin() {

	$plugin = new xa_dp_dynamic_pricing_plugin_basic();
	$plugin->run();
	

}
}

if(!function_exists('xa_dp_plugin_settings_link'))
{
    function xa_dp_plugin_settings_link($links) { 
  $settings_link = '<a href="admin.php?page=dynamic-pricing-main-page&tab=settings_page">Settings</a> |
  <a href="https://wordpress.org/support/plugin/dynamic-pricing-and-discounts-for-woocommerce-basic-version" target="_blank" >Support</a> |
   <a href="https://www.xadapter.com/product/dynamic-pricing-discounts-woocommerce/" target="_blank" >Upgrade to Premium</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
}

 
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'xa_dp_plugin_settings_link' );

try{
    xa_dp_run_xa_dynamic_pricing_plugin();
} catch (Exception $ex) {

}
