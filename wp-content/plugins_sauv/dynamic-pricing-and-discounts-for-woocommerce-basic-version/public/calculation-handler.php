<?php
 include  "valid-rules-class/valid-rules.php";
 include "valid-rules-class/product-rules.php";
 
 
 if(!class_exists('xa_dp_calculation_handler'))
{
    class xa_dp_calculation_handler
        {
            protected $cart_array=array();
            protected $all_valid_rules=array();
            public       $running_max_discount=array();
            public       $running_max=array();
            public       $_price=array();
            public       $_weight=array();
            public       $modes=array();
            public       $ps=array();
            public       $modes_obj=array();
             public function __construct($cartobj,$modes=array(),$rules_option_array=array(),&$running_max_discount=array(),&$running_max=array(),&$_price=array(),&$_weight=array()) 
            { 
                                 // echo "<pre>"; die(print_r($this->all_valid_rules));  echo "</pre>";          //1156651
                             
                               $this->init($cartobj,$modes,$rules_option_array,$running_max_discount,$running_max,$_price,$_weight);
                            
            }
            
            
            function re_init()
            {
                 foreach($this->modes as $mode)
                                 {
                                     $classname='xa_'.$mode;
                                     $mode_obj= new $classname($this->cart_array,$this->all_valid_rules[$mode],$this->running_max_discount,$this->running_max,$this->_price,$this->_weight);
                                     $this->all_valid_rules[$mode]= $mode_obj->get_valid_rules();
                                     $this->modes_obj[$mode]=$mode_obj;
                                 }
            }
          public   function init($cartobj,$modes,$rules_option_array,&$running_max_discount,&$running_max,&$_price,&$_weight)
            {
                                if(empty($rules_option_array))
                                {   
                                    $dummy_option=array('product_rules'=>array());
                                   // update_option('xa_dp_rules',$dummy_option);
                                    $rules_option_array=get_option('xa_dp_rules',$dummy_option);
                                    unset($rules_option_array['buy_get_free_rules']);  
                                  
                                    foreach($rules_option_array as $md=>$rules)
                                    {
                                        foreach($rules as $rule_no=>$rule)
                                        {
                                            $rules_option_array[$md][$rule_no]['rule_no']=$rule_no;
                                        }
                                    }
                                }
                                //$this->cart_array=xa_dp_initialize_product_array_from_cart_obj($cartobj);
                              $this->cart_array=$cartobj;
                              //print_r($this->cart_array);
                                $this->modes=$modes;
                                if(empty($running_max_discount))
                                {
                                    $running_max_discount= xa_dp_initialize_running_max_discount($rules_option_array);
                                }
                                 $this->running_max_discount= $running_max_discount;
                                if(empty($running_max))
                                {
                                    $running_max= xa_dp_initialize_running_max($rules_option_array);
                                }
                                $this->running_max=$running_max;
                             
                                 if(empty($_price) || empty($_weight))
                                {
                                     
                                      xa_dp_initialize_price_and_weight_array($this->cart_array,$_price,$_weight);
                                }
                                //echo "<pre>"; print_r($rules_option_array); echo "</pre>";    //1156651
                                  $this->_price=$_price;
                                  $this->_weight= $_weight;
                                  
                                foreach($modes as $mode)
                                 {
                                    if(empty($rules_option_array) || empty($_price))
                                    {
                                        continue;
                                    }
                                    
                                     $classname='xa_'.$mode;
                                    
                                     $mode_obj= new $classname($this->cart_array,$rules_option_array[$mode],$this->running_max_discount,$this->running_max,$this->_price,$this->_weight);
                                    
                                     $this->all_valid_rules[$mode]= $mode_obj->get_valid_rules();
                                    
                                 }
            }
            
            function is_cart_empty()
            {
                $count=0;
                foreach($this->cart_array  as $pid=>$qnty)
                {
                    $count+=$qnty;
                }
                if($count>0)
                {
                    return false;
                }
                else {
                               return true;
                        }
            }
            
            function  xa_get_dynamic_discount()
            {  
                    $dis=0;
                   $i=10;
                    //while(!empty($this->cart_array))
                  $prev_prod_array=array();
                  
                    while($prev_prod_array!= $this->cart_array)
                    {       
                        $prev_prod_array= $this->cart_array;
                       
                            $this->re_init();
                     
                            foreach ($this->cart_array as $pid=>$qnty)
                            {
                                if($qnty<=0)
                                {
                                    unset($this->cart_array[$pid]);
                                }
                            }
                            if(empty($this->cart_array))
                            {
                                 return $dis;
                            }
                            $this->ps=array();
                            foreach($this->modes as $mode)
                            {
                                $this->modes_obj[$mode]->possible_solutions($this->ps);
                            }
                            
                            
                            $result=$this->get_max_from_possible_solutions($this->ps);  //{returns max dp and rule no  and pid's}  (as array)
                         //echo "valid rules<pre>"; print_r($this->all_valid_rules);   echo "</pre>";  //1156651
                       //  echo "<pre>possible solution is "; print_r($this->ps);   echo "</pre>";  //1156651
                            
                          //echo "Get Max From Ps<pre>"; print_r($result);   echo "</pre>";  //1156651
                            if($result===0)
                            {
                                    return $dis;
                            }
                            else
                            {
                                    $rule_no=$result['rule_no'];
                                    $mode=$result['mode'];
                                    $selected_prods=$result['selected_prods'];
                                   // die($selected_prods);
                                    //echo "</br> before discount was:".$dis;   //1156651
                                    //echo "<pre>";print_r($this->all_valid_rules);echo"</pre>";
                                    //echo "</br>Before Execution</br>";
                                    $dis += $this->modes_obj[$mode]->execute_rule($rule_no,$this->running_max_discount, $selected_prods);  	// update $discount_amt and $tmp_pa and  $r_max_discount					
                                    //echo "</br>After Execution   dis=$dis </br>";
                                    //echo "Remaining Cart";print_r($this->cart_array);
                            }
                          
                    }
                     //die($dis);
                    return $dis;

            }       // end  function xa_get_dynamic_discount() 

             public function get_max_from_possible_solutions($_ps)
            {	//echo '<pre>';print_r($_ps); echo "</pre>";
                    
                    $max_dp=0;
                    $tmp_dp=0;
                    $max_dp_rule_no=-1;
                    $max_mode="";
                    $selected_prods=array();
                    foreach($_ps as $rule_no=>$rules_subset)
                    {
                            //print_r($rule_no);
                              $rno=$rule_no;
                              $rule_no= explode(":",$rule_no);
                              $mode=$rule_no[1];
                              $rule_no=$rule_no[0];

                            //-----------------------if max discount for that array reached skip that rule-------
                            if($this->running_max_discount[$rule_no.":".$mode]==0  && !empty($this->running_max_discount[$rule_no.":".$mode]))
                            {         
                                    continue;
                            }

                            //-------------------------------------------
                            $tmp_dp=max(array_keys($rules_subset));
                            //echo "<pre>";print_r(array_keys($rules_subset)); echo "</pre>";
                            //die("max_dp=".$max_dp );
                            if($max_dp<$tmp_dp)
                            {
                                    $max_dp=$tmp_dp;
                                    $max_dp_rule_no=$rule_no;
                                    $max_mode=$mode;
                                    $selected_prods=$_ps[$rno][$max_dp][$max_dp];   
                                    //echo "  prod= ";print_r($selected_prods);
                            }
                    }
                   
                    if($max_dp==0 || $max_dp_rule_no===-1)
                    {      //die("max_dp=".$max_dp."  rule no =".$max_dp_rule_no );
                            return 0;
                    }
                    return array('max_dp'=>$max_dp,'rule_no'=>$max_dp_rule_no,'mode'=>$max_mode,'selected_prods'=>$selected_prods);

          }   // end function get_max_from_possible_solutions


        }         // end class
 }


    
      function xa_dp_initialize_running_max_discount($rules_array)              // array for checking max discount amt that can be used per  rule
        {
            $_result=array();
            foreach($rules_array as $mode=>$rules)
            {	
                    foreach($rules as $rule_no=>$rule)
                    {
                            if($rule['max_discount']===0)
                            {
                                    $rule['max_discount']=99999;
                            }
                            $_result[$rule_no.":".$mode]=$rule['max_discount'];
                    }
            }
            return $_result;
        }
    
     function xa_dp_initialize_running_max($rules_array)               //array for checking min max
        {
              $_result=array();
            foreach($rules_array as $mode=>$rules)
            {	
                    
                    foreach($rules as $rule_no=>$rule)
                    {  //echo "</br></br>"; print_r($rule);
                        if(!isset($rule['check_on']))
                        {
                            continue;                                                          // may create error 1156651 try to set some default check on
                        }
                            if(isset($rule['max']) )
                            {
                                        if( $rule['max']===0)
                                        { $rule['max']=999999;}
                            }
                            else
                            {
                                $rule['max']=999999;
                            }
                            if($mode=='product_rules')
                            {
                                foreach($rule['product_id'] as $pid)
                                {
                                    $_result[$rule_no.":".$mode][$pid][$rule['check_on']]=0;
                                }
                                
                            }
                            else
                            {
                                $_result[$rule_no.":".$mode][$rule['check_on']]=0;
                            }
                            
                    }
            }
            return $_result;
        }
    
    
    function xa_dp_initialize_price_and_weight_array($cart_product_array,&$price,&$weight)
    {
      //echo "<pre>"; print_r($cart_product_array);
      //echo "</pre>";
      //die();
        foreach($cart_product_array as $pid=>$qnty)
        {         
           
          
             $product=wc_get_product( $pid );
               
             $price[$pid]=$product->get_price();
             
             $weight[$pid]=$product->get_weight();
            
        }
    }
    
