<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.xadapter.com
 * @since      1.0.0
 *
 * @package    xa_dynamic_pricing_plugin
 * @subpackage xa_dynamic_pricing_plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    xa_dynamic_pricing_plugin
 * @subpackage xa_dynamic_pricing_plugin/public
 */
//include 'calculation/calculation-handler.php';
//include 'calculation/xa-dynamic-algorithm.php';
include 'valid-rules-class/valid-rules.php';
include 'calculation-handler.php';

add_filter('woocommerce_get_price_html','xa_dp_variable_page_get_price_html',20,2);              // update sale price on product variation page
add_filter('woocommerce_get_price', 'xa_dp_discounted_sale_price', 22, 2);         // update sale price on product page
add_filter('woocommerce_get_sale_price', 'xa_dp_discounted_sale_price', 22, 2);    // update sale price on product page
add_action( 'woocommerce_before_add_to_cart_form', 'xa_dp_func', 40 );
add_action('woocommerce_cart_calculate_fees' , 'xa_dp_calculate_and_apply_discount');

function xa_dp_get_allowed_modes()
{
                    $dummy_settings['product_rules_on_off']='enable';
                    $dummy_settings['combinational_rules_on_off']='disable';
                    $dummy_settings['category_rules_on_off']='disable';
                    $dummy_settings['cart_rules_on_off']='disable';
                    $dummy_settings['price_table_on_off']='disable';

                    $settings=get_option('xa_dynamic_pricing_setting',$dummy_settings);    
                    $active_modes=array();
                    if( $settings['product_rules_on_off']=='enable')
                    {
                        $active_modes[]='product_rules';
                    }
                    return $active_modes;
}

function xa_dp_get_settings_status($setting_name)
{
                    $dummy_settings=array($setting_name=>'enable');
                  
                    $settings=get_option('xa_dynamic_pricing_setting',$dummy_settings);    
                    if(!isset($settings[$setting_name]))
                    {
                        return 'enable';
                    }
                    return $settings[$setting_name];
}
function xa_dp_func($msg)
{
    $dummy_settings['product_rules_on_off']='enable';
    $dummy_settings['combinational_rules_on_off']='disable';
    $dummy_settings['category_rules_on_off']='disable';
    $dummy_settings['cart_rules_on_off']='disable';
    $dummy_settings['price_table_on_off']='enable';

    $settings=get_option('xa_dynamic_pricing_setting',$dummy_settings);    
    if($settings['price_table_on_off']=='enable')
    {
         include "xa-single-product-pricing-table.php";
    }

}
function xa_dp_calculate_and_apply_discount( WC_Cart $cart ){
                    
                   
                    $active_modes= xa_dp_get_allowed_modes();
                    $prod_list= xa_dp_initialize_product_array_from_cart_obj($cart);
                    $obj = new xa_dp_calculation_handler($prod_list,$active_modes);
                    $discount=$obj->xa_get_dynamic_discount();
	$cart->add_fee( 'Discount', -$discount);
}

function xa_dp_variable_page_get_price_html($price,$product ) 
	{
    
                                        if($product->is_type( 'variable' ))
		{
                                                $prices = $product->get_variation_prices( true );
                                                //print_r($prices['sale_price']);
                                                unset($prices['sale_price']);
                                                foreach($prices['regular_price'] as $vpid=>$amt)
                                                {
                                                        $vproduct = new WC_Product( $vpid );
                                                        $sp= xa_dp_discounted_sale_price($amt, $vproduct);
                                                       //if($sp!=$amt)
                                                        {
                                                            $prices['sale_price'][$vpid]= xa_dp_discounted_sale_price($amt,$vproduct);
                                                        }
                                                        //echo xa_discounted_sale_price($amt,$vproduct);
                                                }
                                                if(is_null($prices['sale_price'] ))
                                                {
                                                    $prices['sale_price'] =array();
                                                }
                                                 if(is_null($prices['regular_price'] ))
                                                {
                                                    $prices['regular_price'] =array();
                                                }
                                                $min_sale_price = current( $prices['sale_price'] );
                                                $max_sale_price = end( $prices['sale_price'] );
                                                $min_reg_price=current( $prices['regular_price'] );
                                                $max_reg_price=end( $prices['regular_price'] );
                                               
                                                $min_price =$min_sale_price<$min_reg_price?$min_sale_price:$min_reg_price;
                                                $max_price=$max_sale_price<$max_reg_price?$max_sale_price:$max_reg_price;
                                                
                                                $regular_price     = $min_reg_price !== $max_reg_price ? sprintf( _x( '%1$s&ndash;%2$s', 'Price range: from-to', 'woocommerce' ), wc_price( $min_reg_price ), wc_price( $max_reg_price ) ) : wc_price( $min_reg_price );
                                                if($min_price==$min_reg_price && $max_price ==$max_reg_price)
                                                {
                                                }
                                                else
                                                {
                                                    $price     = $min_price !== $max_price ? sprintf( _x( '%1$s&ndash;%2$s', 'Price range: from-to', 'woocommerce' ), wc_price( $min_price ), wc_price( $max_price ) ) : wc_price( $min_price );
                                                    $price=$product->get_price_html_from_to( $regular_price, $price ) . $product->get_price_suffix();
                                                }
                                                
                                                $price             = apply_filters( 'woocommerce_variable_sale_price_html', $price, $product );

                                               
                                                return $price;
		}
		else
		{
                                            return $price;
		}
		
	}
        
        
         function xa_dp_initialize_product_array_from_cart_obj( $cart )
    {
                    //echo "<pre>"; print_r($cart); echo "</pre>";
        	$product_array=array();
                    foreach($cart->cart_contents as $hash_id=>$hash)
	{
                                        if(strpos( $hash_id, 'free' ) !== false)
                                        {
                                              continue;
                                        }
		if(isset($hash['variation_id']) && $hash['variation_id']!=0)
		{
                                                $pid=$hash['variation_id'];
		}
		else
		{
                                                $pid=$hash['product_id'];
		}	
		$quantity=$hash['quantity'];
                                        if(isset($product_array[$pid])  && is_numeric($product_array[$pid]) && $product_array[$pid]>0)
                                        {
                                                 $product_array[$pid]+=$quantity;
                                        }
                                        else
                                        {
                                                $product_array[$pid]=$quantity;
                                        }
		
	}
                    return $product_array;
    }
    
    function xa_dp_discounted_sale_price($price,$product)
    {                              remove_filter('woocommerce_get_price', 'xa_dp_discounted_sale_price', 22, 2);         // update sale price on product page
                                                    remove_filter('woocommerce_get_sale_price', 'xa_dp_discounted_sale_price', 22, 2);    // update sale price on product page
                                    
                                                    if(empty($price))
                                                    {
                                                        $price=$product->get_price();
                                                    }
													

                    if((is_product() or is_shop()) )
                    {
                            if($product->is_type( 'variation' ))
                            {
                                    $pid=$product->variation_id;
                                    //die('prod1'.$pid);
                            }
                            else
                            {
                                    $pid=$product->id;
                            }
							


                                                                          $prod_list=array($pid=>1);	//return 1;												   	
                                                                           $modes  = xa_dp_get_allowed_modes();
									
                                                                            $obj=new xa_dp_calculation_handler($prod_list,$modes) ;
							
                            $discounted_price=$obj->xa_get_dynamic_discount();

                                                                           if(($price-$discounted_price)==$product->get_price())
                                                                           {  
                                                                               $result=$product->get_price();
add_filter('woocommerce_get_price', 'xa_dp_discounted_sale_price', 22, 2);         // update sale price on product page
add_filter('woocommerce_get_sale_price', 'xa_dp_discounted_sale_price', 22, 2);    // update sale price on product page

                                                                                   return $result;
                                                                           }
                                                                        if($price>=$discounted_price)
                                                                            { 
add_filter('woocommerce_get_price', 'xa_dp_discounted_sale_price', 22, 2);         // update sale price on product page
add_filter('woocommerce_get_sale_price', 'xa_dp_discounted_sale_price', 22, 2);    // update sale price on product page

                                                                                return $price-$discounted_price;
                                                                            }
                                                                            else
                                                                            {	
add_filter('woocommerce_get_price', 'xa_dp_discounted_sale_price', 22, 2);         // update sale price on product page
add_filter('woocommerce_get_sale_price', 'xa_dp_discounted_sale_price', 22, 2);    // update sale price on product page

                                                                                return $price;
                                                                            }
																			

                    }
                    else
                    {	
add_filter('woocommerce_get_price', 'xa_dp_discounted_sale_price', 22, 2);         // update sale price on product page
add_filter('woocommerce_get_sale_price', 'xa_dp_discounted_sale_price', 22, 2);    // update sale price on product page

                            return $price;
                    }

    }

       
class xa_dp_dynamic_pricing_plugin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $xa_dynamic_pricing_plugin    The ID of this plugin.
	 */
	private $xa_dynamic_pricing_plugin;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $xa_dynamic_pricing_plugin       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $xa_dynamic_pricing_plugin, $version ) {

		$this->xa_dynamic_pricing_plugin = $xa_dynamic_pricing_plugin;
		$this->version = $version;
		
	}
	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() 
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in xa_dynamic_pricing_plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The xa_dynamic_pricing_plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->xa_dynamic_pricing_plugin, plugin_dir_url( __FILE__ ) . 'css/xa-dynamic-pricing-plugin-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in xa_dynamic_pricing_plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The xa_dynamic_pricing_plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->xa_dynamic_pricing_plugin, plugin_dir_url( __FILE__ ) . 'js/xa-dynamic-pricing-plugin-public.js', array( 'jquery' ), $this->version, false );

	}

	
}
