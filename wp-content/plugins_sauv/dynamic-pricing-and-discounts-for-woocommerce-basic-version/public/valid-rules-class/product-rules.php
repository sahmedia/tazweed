<?php
    
    if(!class_exists('xa_product_rules'))
    {
        class xa_product_rules extends xa_valid_rules
        {

           function __construct(&$cart_array,$rules_array,&$running_max_discount,&$running_max,$price,$weight)
            {
               if(empty($rules_array) || empty($price))
               {
                  
               }
               else
               {
                 $this->cart_array=&$cart_array;
                 $this->rules_array=$rules_array;
                 $this->running_max_discount=&$running_max_discount;
                 $this->running_max=&$running_max;
                 $this->_price=$price;
                 $this->_weight=$weight;
                 $this->mode='product_rules';
                 
                 $this->update_product_valid_rules();
               }

                 
                 
            }

           
           function  update_product_valid_rules()
           {   // echo "<pre>";
               //print_r($this->rules_array);
              // echo "</pre>";
               //$this->valid_rules=array_filter($this->rules_array,array($this,"check_rule"));   
               $this->valid_rules=array();
                 foreach($this->rules_array as $rule1)
                 {
                     if($this->check_rule($rule1)===true)
                     {
                          $this->valid_rules[$rule1['rule_no']]=$rule1;
                     }
                      
                 }
           }
           
           function check_rule(&$rule)
            {   
               
               $prod_array=$this->cart_array;
                $pids=$rule['product_id'];
                $rule['min_valid_qnty']=$this->get_min_valid_quantity($rule);
                $rule_no=$rule['rule_no'];
                $min=(empty($rule['min'])==true)?1:$rule['min'];
                $max=(empty($rule['max'])==true )?999999:$rule['max'];
                
                if($max<$min && $max!=0)
                {	//echo '</br> there1156651error</br>';
                        return false;
                }
                $rule['mapping']=array();
                foreach($pids as $pid)
                {     // echo '</br>var_dump ';var_dump($this->check_min_max_weight_price_quantity($rule['check_on'],$min,$max,$pid,$rule_no));
                    if($rule['discount_type']=='Flat Discount'  && isset($this->running_max[$rule_no.":".$this->mode][$pid]['min'])  &&   $this->running_max[$rule_no.":".$this->mode][$pid]['min']==1 )
                    {//echo '</br>here 1156651error</br>';
                        return false;
                    }
                 //echo "</br>1156651 every where  ";var_dump($this->check_min_max_weight_price_quantity($rule['check_on'],$min,$max,$pid,$rule_no));
                    if(array_key_exists(strval($pid),$prod_array) &&  $this->check_roles($rule['allow_roles'])==true &&
                         $this->check_min_max_weight_price_quantity($rule['check_on'],$min,$max,$pid,$rule_no)==true  && $this->check_date_range($rule['from_date'],$rule['to_date']) ==true )
                        {   
                                if ( !isset($rule['mapping'])  || !in_array($pid,$rule['mapping'])) 
                                {	$rule['mapping'][]=$pid;
                                        //print_r($rule['mapping']);
                                }
                        }
                }
              //echo "<pre>  max_running_discount=";print_r($this->running_max_discount[$rule_no.':'.$this->mode]);  print_r($rule);   echo "</pre>";
                 if(isset($rule['mapping'])  && !empty($rule['mapping']))
                        {
                            return true;
                        }
                else {   
                            return false;
                        }
                
            }

            function get_min_valid_quantity($rule)
            {

                    
                    extract($rule);
                    $min_valid_qnty=array();
                    foreach($product_id as $pid)
                    {
                               if(!isset($this->_price[$pid]))
                               {
                                   continue;
                               }
                                $unit_price=$this->_price[$pid];
                                $unit_weight=$this->_weight[$pid];
                                $min_valid_qnty[$pid]=1;
                                if($check_on=='Price')
                                {
                                        if(!empty($this->_price) && array_key_exists($pid,$this->_price)===true)
                                        {
                                                $min_valid_qnty[$pid]=ceil(floatval($min)/floatval($unit_price));
                                        }
                                }
                                elseif($check_on=='Quantity')
                                {	if(!empty($this->_weight) && array_key_exists($pid,$this->_weight)===true)
                                        {
                                                 $min_valid_qnty[$pid]=ceil($min);
                                        }
                                }
                                elseif($check_on=='Weight')
                                {
                                        $min_valid_qnty[$pid]=ceil(floatval($min)/floatval($unit_weight));
                                }
                                else
                                {
                                        $min_valid_qnty[$pid]=1;
                                }
                                
                    }
                    return $min_valid_qnty;
            }
            
            function check_roles($role)
            {
                    if($role=='all' || current_user_can($role))
                    {
                            return true;
                    }

                    return false;
            }

        function check_min_max_weight_price_quantity($check_on,$min,$max,$pid,$rule_no)
        {	
            //phpinfo();
               if(isset($this->running_max_discount[$rule_no.":".$this->mode])  && ($this->running_max_discount[$rule_no.":".$this->mode]<=0) && !empty($this->running_max_discount[$rule_no.":".$this->mode]))
                { //echo'<pre>'; print_r($this->running_max_discount); echo '</pre>';
                    return false;
                }
                //$product=wc_get_product( $pid );
                //$Weight=$product->get_weight();
                //$Price=$product->price;
                
                $Weight=$this->_weight[$pid];
                $cq=$this->cart_array[$pid];
                $Price=$this->_price[$pid] ;
                $Quantity=1;
                
                switch ($check_on) 
                {
               case 'Weight':   $unit=$Weight;
                                       break;
                case 'Quantity':   $unit=$Quantity;
                                                       break;
                case 'Price':   $unit=$Price;
                                                       break;
               default:
                   break;
              }
                
                if(isset($this->running_max[$rule_no.":".$this->mode][$pid][$check_on]) && ($this->running_max[$rule_no.":".$this->mode][$pid][$check_on]+$unit)<=$max )
                {
                    
                }
                else
                { 
                return false;
                }
               

                
                if(isset($this->running_max[$rule_no.":". $this->mode][$pid]['min']) &&  $this->running_max[$rule_no.":".$this->mode][$pid]['min']==1 && ($this->running_max[$rule_no.":".$this->mode][$pid][$check_on]<=$max  || $max==0  || empty($max)))
                { 
                   return true;
                }
                
                
                switch($check_on)
                {	
                case "Weight":      if((($Weight * $this->cart_array[$pid])>=$min)  && (($this->running_max[$rule_no.":".$this->mode][$pid]['Weight']+$Weight)<=$max  || $max==0  || empty($max)))
                                                {      return true;}
                                                else    
                                                {   
                                                        return false;
                                                }
                case "Quantity":    if(($cq>=$min)     && (($this->running_max[$rule_no.":".$this->mode][$pid]['Quantity']+$Quantity)<=$max  || $max==0 || empty($max)))
                                                {    
                                                    return true;}
                                                else
                                                {       
                                                        return false;
                                                }
                case "Price":           if((($Price * $this->cart_array[$pid]) >=$min )      && (($this->running_max[$rule_no.":".$this->mode][$pid]['Price']+$Price)<=$max  || $max==0 || empty($max)))
                                                {      return true;}
                                                else
                                                {   
                                                    return false;
                                                }
                }
                 
                return false;
        }

        function check_date_range($fromdate ,$todate)
        {
                $now=date('d-m-Y');
                if(empty($fromdate) && empty($todate))
                {   
                        return true;
                }
                elseif(empty($fromdate) && empty($todate)==false  && (strtotime($now) <= strtotime($todate)))
                {
                        return true;
                }
                elseif(empty($fromdate)==false && (strtotime($now) >= strtotime($fromdate)) && empty($todate))
                {
                        return true;
                }
                elseif((strtotime($now) >= strtotime($fromdate))  && (strtotime($now) <= strtotime($todate)))
                {
                        return true;
                }
                else
                {
                        return false;
                }
                return false;
        }

           function get_valid_rules()
           {
               
              return parent::get_valid_rules();
           }
       

           	public function possible_solutions(&$ps)
	{           

                                $_result=array();
                                $mode=$this->mode;
                                //$this->valid_rules= $rules;
                                if(empty($this->valid_rules))
                                {
                                    return $ps;
                                }
                                 //echo "</br>  product valid rules<pre>";print_r($this->valid_rules);  echo "</pre>"; //1156651
                               
                                foreach( $this->valid_rules as $rule_no=>$rule )
                                    {  
                                            
                                        
                                       if(!isset($rule['mapping']) || empty($rule['mapping']))
                                       {
                                           continue;
                                       }
                                       $pids=$rule['mapping'];
                                        foreach($pids as $pid)
                                        {
                                                         if(!isset($this->cart_array[$pid]))
                                                         {
                                                               continue; 
                                                         }
                                                           if($this->cart_array[$pid]==0)
                                                           {
                                                                unset($this->cart_array[$pid]);  
                                                                continue;
                                                           }
                                                       $dp=0;
                                                       $val=$rule['value'];
                                                       $max_discount=$this->running_max_discount[$rule_no.":".$this->mode];
                                                       $price=$this->_price[$pid];

                                                       if($rule['discount_type']=='Percent Discount')
                                                       {
                                                               $cal_discount=(floatval($price)* $val)/100;
                                                       }
                                                       elseif($rule['discount_type']=='Flat Discount')
                                                       {
                                                               $cal_discount=floatval($val);
                                                       }
                                                       elseif($rule['discount_type']=='Fixed item Price'  || $rule['discount_type']=='Fixed Price' )
                                                       {
                                                               $cal_discount=floatval($price) - $val;
                                                       }
                                                       else
                                                       {die('error on ref:23x22334,  message: discount_type is not set for rule no:'.$rule_no); }

                                                       if($cal_discount>$max_discount && $max_discount<>0)
                                                               {
                                                                       $cal_discount=$max_discount;
                                                               }
                                                       //$dp=floatval((floatval($cal_discount) * 100.00)/floatval($price));		// perform round function here 
                                                       $dp=$cal_discount;	
                                                       $dp=round($dp,2,PHP_ROUND_HALF_UP);
                                                       $_result[$rule_no.':'.$mode][strval($dp)]=array($dp=>$pid);
                                                       $dp=strval($dp);
                                                      if(!isset($ps[$rule_no.':'.$mode][$dp])  ||  !in_array(array($dp=>$pid),$ps[$rule_no.':'.$mode][$dp]))
                                                          $ps[$rule_no.':'.$mode][$dp]=array($dp=>$pid);
                                           
                                        }
                                    }
                                  //echo"possible sol<pre>";  print_r($ps);echo"</pre>"; //1156651
	}   // end function possible_sollution
	
                    
                    public function execute_rule($rule_no,&$running_max_discount, $selected_prods) 
                    {          
                               if(is_cart() )
                                {
                                echo "\n<div id='rules_info' style='display:none'>";
                                echo "mode:".$this->mode."\t\t\t\t\trule:".$rule_no." \npa=";print_r($this->cart_array);
                                
                               }
                               
                               
                                // echo "mode:".$this->mode."\t\t\t\t\trule:".$rule_no." \npa=";print_r($this->cart_array);
                                //echo"</br> before executing rule no $this->mode : $rule_no :";  print_r($this->cart_array); //1156651
                                $mode=$this->mode;
                                $rule=$this->valid_rules[$rule_no];
                                extract($rule);
                               $pids= explode(',',$selected_prods);
                               
                               $product_id= $selected_prods;  // overwriding extractrd product id with the selected one because in this version it is array
                                if(isset($this->running_max[$rule_no.":".$this->mode][$product_id]['min'])  && $this->running_max[$rule_no.":".$this->mode][$product_id]['min']==1)   /// this is to check if the rule has executed once then no need of min qnty or price or weight check
                                {
                                $min_valid_qnty[$product_id]=1;
                                }
                                
                                if($min_valid_qnty[$product_id]>$this->cart_array[$product_id])
                                {
                                    if($this->cart_array[$product_id]>0)
                                    {
                                        $min_valid_qnty[$product_id]=1;
                                    }
                                    else
                                    {
                                        $min_valid_qnty[$product_id]=0;
                                    }
                                }
                                $this->cart_array[$product_id]-=$min_valid_qnty[$product_id];
                                $discount_amt=0;
                                if($discount_type=='Percent Discount')
                                {	
                                        $discount_amt=floatval($value) * floatval($this->_price[$product_id]) * floatval($min_valid_qnty[$product_id])/100;
                                       
                                }
                                elseif($discount_type=='Flat Discount')
                                {
                                        $discount_amt=floatval($value);
                                       
                                }
                                elseif($discount_type=='Fixed Price' )
                                {
                                        $discount_amt=(floatval($this->_price[$product_id]) * floatval($min_valid_qnty[$product_id])) - (floatval($value) * floatval($min_valid_qnty[$product_id]));
                             
                                }
                                else
                                {
                                        $discount_amt=0;
                                }
                                if(isset($running_max_discount[$rule_no.":".$mode])  && $discount_amt>$running_max_discount[$rule_no.":".$mode] )
                                {       
                                        $discount_amt=$running_max_discount[$rule_no.":".$mode];
                                }
                                
                                if(!isset($this->running_max[$rule_no.":".$mode][$product_id][$check_on]))
                                {
                                    $this->running_max[$rule_no.":".$mode][$product_id][$check_on]=0;
                                }
                                
                                if($check_on=="Price")
                                {
                                     //echo "</br>increasing running max</br>".$this->running_max[$rule_no.":".$mode][$check_on]." By + ".$this->_price[$product_id];//1156651
                                     $this->running_max[$rule_no.":".$mode][$product_id][$check_on]+=($this->_price[$product_id] * $min_valid_qnty[$product_id]);
                                }
                                elseif($check_on=="Quantity")
                                {
                                       $this->running_max[$rule_no.":".$mode][$product_id][$check_on]+=$min_valid_qnty[$product_id];
                                }
                                elseif($check_on=="Weight")
                                {
                                       $this->running_max[$rule_no.":".$mode][$product_id][$check_on]+=($this->_weight[$product_id] *$min_valid_qnty[$product_id]);
                                }
                                
                                if(!empty($max_discount))
                                {
                                    $running_max_discount[$rule_no.":".$mode]-=$discount_amt;
                                }
                                
                                $this->running_max[$rule_no.":".$mode][$product_id]["min"]=1;
                                
                                //$this->discount_amt+=$discount_amt;
                                //echo"</br> after executing :discount amt=$discount_amt";  print_r($this->cart_array);      //1156651
                                if($this->cart_array[$product_id]==0)
                                {
                                    unset($this->cart_array[$product_id]);
                                    
                                }
                                $this->update_product_valid_rules();
                                if(is_cart() )
                                {
                               
                                 echo "\n Apa=";print_r($this->cart_array);
                                echo "L-dis=".$discount_amt;
                                echo "</div>";
                               }
                                return $discount_amt;

                    }      //end function execute_rule();
                    
                    
                    
            }
    }