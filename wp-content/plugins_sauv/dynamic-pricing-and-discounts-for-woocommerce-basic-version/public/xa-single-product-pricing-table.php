<?php  
    
                    $dummy_settings['product_rules_on_off']='enable';
                    $dummy_settings['combinational_rules_on_off']='enable';
                    $dummy_settings['category_rules_on_off']='enable';
                    $dummy_settings['cart_rules_on_off']='enable';
                    $dummy_settings['price_table_on_off']='enable';

                    $settings=get_option('xa_dynamic_pricing_setting',$dummy_settings);    
                    if($settings['price_table_on_off']=='enable')
                    {
                      
    ob_start( );?>


<table class='xa_sp_table' style=' width:100%;   margin-right: auto;'>
    <style>
        .xa_sp_table_cell
        {
                padding-right:40px;
                text-align: right;
            
        }
        .xa_sp_table_head2 tr td
        {
            text-align: center;
        }
    </style>
    <thead class='xa_sp_table_head2' style="font-size: 14px;  background:black"><tr ><td width=30% ></td><td width=30% ></td><td  ></td></tr></thead>
    <thead class='xa_sp_table_head1' style="font-size: 14px;   background:lightgrey"><tr><td colspan="3"  style='text-align:center;'>Bulk Product Offers</td></tr></thead>
     <thead class='xa_sp_table_head2' style="font-size: 14px;  background:lightyellow; "><tr ><td width=10px>Min</td><td>Max</td><td>Offer</td></tr></thead>
    <tbody class='xa_sp_table_body'>
        <?php
            global $post;
            $pid = $post->ID;
            $product_rules=get_option('xa_dp_rules')['product_rules'];
            $pricing_table_qnty_shrtcode=get_option('xa_dynamic_pricing_setting')['pricing_table_qnty_shrtcode'];
            
            $count=0;

            $Weight=get_option('woocommerce_weight_unit');
            $Quantity=$pricing_table_qnty_shrtcode;
            $Price=get_option('woocommerce_currency');
            foreach($product_rules as $rule)
            {
               foreach($rule['product_id'] as $rule_pid)
               {       if($rule_pid==$pid)
                        {   
           switch ($rule['check_on']) {
               case 'Weight':   $unit=$Weight;
                                       break;
                case 'Quantity':   $unit=$Quantity;
                                                       break;
                case 'Price':   $unit=$Price;
                                                       break;

               default:
                   break;
           }
                   
                                
                            $count++;
                            echo "<tr  class='xa_sp_table_body_row' style='font-size:14px; font-family: Verdana;'>";
                            echo "<td class='xa_sp_table_cell'>$rule[min] ".$unit." </td>";
                            echo "<td class='xa_sp_table_cell'>";
                            echo(isset($rule['max'])?  $rule['max']: '-');
                            echo " ".$unit."</td>";
                            echo "<td class='xa_sp_table_cell'>$rule[value]";
                            if($rule['discount_type']=='Percent Discount'){   echo "   % Discount</td>";  }
                            elseif($rule['discount_type']=='Flat Discount'){   echo "   $Price Discount</td>";  }
                            else{    echo " $Price ".$rule['discount_type'];}
                            echo "</tr>";
                        }
               }
            }
            ?>
    
    </tbody>
    
    <tfoot></tfoot>
</table>


<?php      $output = ob_get_clean( );

if($count>1)
{
    echo $output;
}
                    }
?>