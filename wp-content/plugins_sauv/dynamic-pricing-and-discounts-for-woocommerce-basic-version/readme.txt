﻿=== Dynamic Pricing and Discounts for WooCommerce ===

Contributors: extensionhawk,akshayagrawal, elvinwf
Donate link: 
Tags: Dynamic Pricing,Dynamic Discount,Price Rule,Buy and Get Free Offers,Bulk Discount, WooCommerce, Wordpress
Requires at least: 3.0.1
Tested up to: 4.7
Stable tag: 1.1.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Rule Based Dynamic Pricing And Discount Plugin Which Dynamically Sets Discount for Customer Based On Parameters Like Quantity,Weight,Price

== Description ==

= Introduction =
When you run an online store, a classic marketing strategy is to offer discounts to customers. It is important that you strategically create rules for the discount offers without affecting the profitability of your store. Setting a maximum discount amount for every applied rule is a simple solution to this predicament.
In addition, things get complicated when you have multiple categories of products listed on your store, and want to offer discounts based on multiple parameters.
Dynamic Pricing and Discounts for WooCommerce plugin offers well-defined solutions to help you set the best discount offers on your store.

= Features =
* <strong>Product Rules To Set discounts on specific products</strong>
* <strong>Rules can be set based on Quantity,Weight,Price of the Product in cart.</strong>
* <strong>Pricing Table Feature will Appear on Product Page to show all offer applicable on that product, If a product has Multiple Product Rules .</strong>
* <strong>Pricing Table Visibility (Hide/Show) in Settings.</strong>
* <strong>Option to Set Maximum Discount to be allowed on every rule.</strong>
* <strong>Option To set Rules Expiry Date (From Date , To Date)</strong>
* <strong>Multiple Discount Type Available i.e (Flat Discount, Percentage Discount , Set a Fix Price For Each Unit)</strong>

Tag: Woocommerce Dynamic Pricing,Woocommerce Dynamic Discount,Woocommerce Price Rule,Woocommerce Buy and Get Free Offers,Woocommerce Bulk Discount, WooCommerce, Wordpress
<blockquote>

= Premium version Features =

<ul>
<li> <strong>Product Rules</strong></li>
<li> <strong>Combinational Rules</strong> </li>
<li> <strong>Category Rules</strong> </li>
<li> <strong>Cart Rules</strong> </li>
<li> <strong>Buy And Get Free (Offers) Feature</strong></li>
<li> <strong>Timely Updates and Bug Fixes</strong></li>
<li> <strong>Premium support!</strong></li>
</ul>

For complete list of features and details, please visit <a rel="nofollow" href="https://www.xadapter.com/product/dynamic-pricing-discounts-woocommerce/">Dynamic Pricing and Discounts for WooCommerce</a>
</blockquote>

= About ExtensionHawk =

ExtensionHawk creates quality WordPress/WooCommerce plug-ins that are easy to use and customize. We are proud to have thousands of customers actively using our plugins across the globe.

== Installation ==

1. Upload the plugin folder to the ‘/wp-content/plugins/’ directory.
2. Activate the plugin through the ‘Plugins’ menu in WordPress.
3. That's it – you can now configure the plugin.

== Frequently Asked Questions ==

= Is the plugin configuration complicated? =
The plugin is very easy to configure. We have a step by step tutorial on setting up this plugin. Our Help Desk also has extensive documentation which includes FAQs, Troubleshooting Guide, Knowledge Base and Code snippets. Please [visit](https://www.xadapter.com/2016/12/01/setting-woocommerce-dynamic-pricing-plugin/) if you need any help. 

== Screenshots ==
1. Price Table On Product Page
2. Cart (Disount Applied After SubTotal)
3. Product Rules Page
4. Settings Page



== Changelog ==

= 1.1.7 =
 * Marketing Content Changed.

= 1.1.6 =
 * Multiple Products in product rules supported.
 
= 1.1.5 =
 * Bug Fixes.
 
= 1.1.4 =
 * Bug Fixes.
 
= 1.1.3 =
 * Compatible with Our Prices by User Role Plugin.
 
= 1.1.2 =
 * Bug Fixes
 
= 1.1.1 =
 * Tested on Wordpress 4.7.
 
= 1.1.0 =
 * Error Handling Improvements.
 
= 1.0.9 =
 * Basic Version Check.
 
= 1.0.8 =
 * Made Compatible with PHP version 7.

= 1.0.7 =
 * WPML Supported.
 
= 1.0.6 =
 * Bug Fix.
 
= 1.0.5 =
 * Bug Fix.
 
= 1.0.4 =
 * .Increased Control Over Functionality From Settings Page
 
= 1.0.3 =
 * Pricing Table.
 
= 1.0.2 =
 * New Feature Buy And Get Free(Offers).
 
= 1.0.1 =
 * Product Rules,Combinational Rules,Category Rules,Cart Rules.
 
== Upgrade Notice ==

= 1.1.4 =
 * Bug Fixes.
 
= 1.1.3 =
 * Compatible with Our Prices by User Role Plugin.
 
= 1.1.2 =
 * Bug Fixes
 
= 1.1.1 =
 * Tested on Wordpress 4.7.
 
= 1.1.0 =
 * Error Handling Improvements.
 
= 1.0.9=
 * Basic Version Check.
 
= 1.0.8=
 * Made Compatible with PHP version 7.
 
= 1.0.7 =
 * WPML Supported.
 
= 1.0.6 =
 * Bug Fix.
 
= 1.0.5 =
 * Bug Fix.
 
= 1.0.4 =
 * .Increased Control Over Functionality From Settings Page
 
= 1.0.3 =
 * Pricing Table.
 
= 1.0.2 =
 * New Feature Buy And Get Free(Offers).
 
= 1.0.1 =
 * Product Rules,Combinational Rules,Category Rules,Cart Rules.
 
