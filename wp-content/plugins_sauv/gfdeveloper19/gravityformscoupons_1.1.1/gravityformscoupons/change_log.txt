---------------------------------------------------------------------
Version 1.1.1
    - Added 'gform_coupons_discount_amount' PHP hook to support modifying the discount amount
        add_filter( 'gform_coupons_discount_amount', 'add_logged_in_user_bonus_discount', 10, 3 );
        function add_logged_in_user_bonus_discount( $discount, $coupon, $price ) {
            if( is_user_logged_in() ) {
                $discount += 5;
            }
            return $discount;
        }
    - Added 'gform_coupons_discount_amount' JS hook
        gform.addFilter( 'gform_coupons_discount_amount', function( discount, couponType, couponAmount, price, totalDiscount ) {
            // you would need to write your own JS-version of is_user_logged_in()
            if( is_user_logged_in() ) {
                discount += 5;
            }
            return discount;
        } );

---------------------------------------------------------------------
Version 1.1
	- Updated plugin updated method so that it takes advantage of proxy manager to get around the blacklisted IP issue.
	- Updated several instances of domain used for translations from gravityformscoupon to the correct domain of gravityformscoupons.
	- Updated javascript in DisableApplyButton function since the button remained disabled for some users.
	- Fixed issue with product info cache not including coupon on certain situations.
	- Fixed issue with text domain of some strings.
    - Fixed another issue with duplicate coupon codes being allowed to be created.
    - Fixed issue with duplicate coupon codes being allowed to be created.
    - Fixed fatal error when plugin is activated without Gravity Forms active.
	- Fixed issue where existing coupon code was not saved when editing an entry.
    - Fixed issue with start and expiration date not taking the configured WP timezone into account.
    - Fixed issue where translations were not being applied on the front end.
    - Fixed issue with coupon not being applied when form failed validation.
    - Fixed issue with coupon being marked as "undefined" after a submission when clicking the back browser button.

---------------------------------------------------------------------
Version 1.0
	- Fixed issue with applying fixed amount coupon code with currencies other than US dollar
    - Fixed issue where changing an existing coupon from being form specific to 'any form' resulted in an 'Invalid coupon.' message on any form other than the original.

---------------------------------------------------------------------
Version 1.0.beta1.1
	- Added POT file

---------------------------------------------------------------------