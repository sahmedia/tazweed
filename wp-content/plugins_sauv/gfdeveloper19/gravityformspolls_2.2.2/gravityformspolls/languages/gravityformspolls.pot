# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Gravity Forms Polls Add-On\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-22 14:27-0500\n"
"PO-Revision-Date: 2014-10-22 14:27-0500\n"
"Last-Translator: Dana Cobb <dana@rocketgenius.com>\n"
"Language-Team: Rocketgenius <customerservice@rocketgenius.com>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _e;__\n"
"X-Poedit-Basepath: .\n"
"X-Generator: Poedit 1.6.10\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../class-gf-polls.php:99
msgid "First Choice"
msgstr ""

#: ../class-gf-polls.php:100
msgid "Second Choice"
msgstr ""

#: ../class-gf-polls.php:101
msgid "Third Choice"
msgstr ""

#: ../class-gf-polls.php:161
msgid "Poll Results"
msgstr ""

#: ../class-gf-polls.php:296
msgid "Polls"
msgstr ""

#: ../class-gf-polls.php:315
msgid "Poll Settings"
msgstr ""

#: ../class-gf-polls.php:319 ../class-gf-polls.php:322
msgid "Results"
msgstr ""

#: ../class-gf-polls.php:322
msgid ""
"Select this option to display the results of submitted poll fields after the "
"form is submitted."
msgstr ""

#: ../class-gf-polls.php:325
msgid "Display results of submitted poll fields after voting"
msgstr ""

#: ../class-gf-polls.php:332 ../class-gf-polls.php:335
msgid "Results Link"
msgstr ""

#: ../class-gf-polls.php:335
msgid ""
"Select this option to add a link to the form which allows the visitor to see "
"the results without voting."
msgstr ""

#: ../class-gf-polls.php:338
msgid "Add a poll results link to the form"
msgstr ""

#: ../class-gf-polls.php:345
msgid "Percentages"
msgstr ""

#: ../class-gf-polls.php:348 ../class-gf-polls.php:1180
msgid "Show Percentages"
msgstr ""

#: ../class-gf-polls.php:348 ../class-gf-polls.php:1180
msgid "Show the percentage of the total votes for each choice."
msgstr ""

#: ../class-gf-polls.php:351
msgid "Display percentages"
msgstr ""

#: ../class-gf-polls.php:358
msgid "Counts"
msgstr ""

#: ../class-gf-polls.php:361 ../class-gf-polls.php:1181
msgid "Show Counts"
msgstr ""

#: ../class-gf-polls.php:361 ../class-gf-polls.php:1181
msgid "Show the total number of votes for each choice."
msgstr ""

#: ../class-gf-polls.php:364
msgid "Display counts"
msgstr ""

#: ../class-gf-polls.php:371 ../pollwidget.php:220
msgid "Style"
msgstr ""

#: ../class-gf-polls.php:376 ../pollwidget.php:222
msgid "Green"
msgstr ""

#: ../class-gf-polls.php:377 ../pollwidget.php:223
msgid "Blue"
msgstr ""

#: ../class-gf-polls.php:378 ../pollwidget.php:224
msgid "Red"
msgstr ""

#: ../class-gf-polls.php:379 ../pollwidget.php:225
msgid "Orange"
msgstr ""

#: ../class-gf-polls.php:383
msgid "Block repeat voters"
msgstr ""

#: ../class-gf-polls.php:386
msgid "Block Repeat Voters"
msgstr ""

#: ../class-gf-polls.php:386 ../class-gf-polls.php:1182
msgid ""
"Choose whether to allow visitors to vote more than once. Repeat voting is "
"controlled by storing a cookie on the visitor's computer."
msgstr ""

#: ../class-gf-polls.php:396 ../class-gf-polls.php:1182 ../pollwidget.php:244
msgid "Repeat Voters"
msgstr ""

#: ../class-gf-polls.php:402 ../pollwidget.php:246
msgid "Don't block repeat voting"
msgstr ""

#: ../class-gf-polls.php:403 ../pollwidget.php:247
msgid "Block repeat voting using cookie"
msgstr ""

#: ../class-gf-polls.php:408
msgid "Expires: "
msgstr ""

#: ../class-gf-polls.php:415 ../pollwidget.php:254
msgid "Never"
msgstr ""

#: ../class-gf-polls.php:416 ../pollwidget.php:255
msgid "1 hour"
msgstr ""

#: ../class-gf-polls.php:417 ../pollwidget.php:256
msgid "6 hours"
msgstr ""

#: ../class-gf-polls.php:418 ../pollwidget.php:257
msgid "12 hours"
msgstr ""

#: ../class-gf-polls.php:419 ../pollwidget.php:258
msgid "1 day"
msgstr ""

#: ../class-gf-polls.php:420 ../pollwidget.php:259
msgid "1 week"
msgstr ""

#: ../class-gf-polls.php:421 ../pollwidget.php:260
msgid "1 month"
msgstr ""

#: ../class-gf-polls.php:605
msgid "Select one"
msgstr ""

#: ../class-gf-polls.php:633
msgid "Repeat voting is not allowed"
msgstr ""

#: ../class-gf-polls.php:758
msgid "View results"
msgstr ""

#: ../class-gf-polls.php:759
msgid "Back to the poll"
msgstr ""

#: ../class-gf-polls.php:770 ../class-gf-polls.php:801
msgid "Poll Entries"
msgstr ""

#: ../class-gf-polls.php:797
msgid "This contact has not submitted any poll entries yet."
msgstr ""

#: ../class-gf-polls.php:806
msgid "Entry Id"
msgstr ""

#: ../class-gf-polls.php:809
msgid "Date"
msgstr ""

#: ../class-gf-polls.php:812
msgid "Form"
msgstr ""

#: ../class-gf-polls.php:983 ../class-gf-polls.php:1243 ../pollwidget.php:14
#: ../pollwidget.php:103 ../pollwidget.php:131 ../pollwidget.php:199
msgid "Poll"
msgstr ""

#: ../class-gf-polls.php:1177
msgid "Display Results After Voting"
msgstr ""

#: ../class-gf-polls.php:1177
msgid "Select this to display the results of submitted poll fields."
msgstr ""

#: ../class-gf-polls.php:1178
msgid "Display Confirmation"
msgstr ""

#: ../class-gf-polls.php:1178
msgid ""
"Select this option to display the form confirmation message after the "
"visitor has voted."
msgstr ""

#: ../class-gf-polls.php:1179
msgid "Show Results Link"
msgstr ""

#: ../class-gf-polls.php:1179
msgid ""
"Add a link to the form which allows the visitor to see the results without "
"voting."
msgstr ""

#: ../class-gf-polls.php:1184 ../class-gf-polls.php:1199
msgid "Poll Question"
msgstr ""

#: ../class-gf-polls.php:1184
msgid ""
"Enter the question you would like to ask the user. The user can then answer "
"the question by selecting from the available choices."
msgstr ""

#: ../class-gf-polls.php:1185 ../class-gf-polls.php:1208
msgid "Poll Type"
msgstr ""

#: ../class-gf-polls.php:1185
msgid "Select the field type you'd like to use for the poll."
msgstr ""

#: ../class-gf-polls.php:1186
msgid "Randomize Choices"
msgstr ""

#: ../class-gf-polls.php:1186
msgid ""
"Check the box to randomize the order in which the choices are displayed to "
"the user. This setting affects only voting - it will not affect the order of "
"the results."
msgstr ""

#: ../class-gf-polls.php:1213
msgid "Drop Down"
msgstr ""

#: ../class-gf-polls.php:1214
msgid "Radio Buttons"
msgstr ""

#: ../class-gf-polls.php:1215
msgid "Checkboxes"
msgstr ""

#: ../class-gf-polls.php:1230
msgid "Randomize order of choices"
msgstr ""

#: ../class-gf-polls.php:1269
msgid "<h2>Original Response</h2>"
msgstr ""

#: ../class-gf-polls.php:1379
msgid "Other"
msgstr ""

#: ../class-gf-polls.php:1871
#, php-format
msgid ""
"Gravity Forms Polls Add-on Shortcode error: Please enter a valid date or "
"time period for the cookie expiration cookie_expiration_date: %s"
msgstr ""

#: ../pollwidget.php:15
msgid "Gravity Forms Poll Widget"
msgstr ""

#: ../pollwidget.php:171
msgid "Title"
msgstr ""

#: ../pollwidget.php:177
msgid "Select a Form"
msgstr ""

#: ../pollwidget.php:197
msgid "Display Mode"
msgstr ""

#: ../pollwidget.php:200
msgid "Results only"
msgstr ""

#: ../pollwidget.php:205
msgid "Use form settings"
msgstr ""

#: ../pollwidget.php:206
msgid "Override form settings"
msgstr ""

#: ../pollwidget.php:214
msgid "Results Settings"
msgstr ""

#: ../pollwidget.php:216
msgid "Show percentages"
msgstr ""

#: ../pollwidget.php:218
msgid "Show counts"
msgstr ""

#: ../pollwidget.php:233
msgid "Display form confirmation"
msgstr ""

#: ../pollwidget.php:235
msgid "Display results of submitted poll fields"
msgstr ""

#: ../pollwidget.php:237
msgid "Show link to view results"
msgstr ""

#: ../pollwidget.php:250
msgid "Expires:"
msgstr ""

#: ../pollwidget.php:277
msgid "Display form title"
msgstr ""

#: ../pollwidget.php:278
msgid "Display form description"
msgstr ""

#: ../pollwidget.php:281
msgid "advanced options"
msgstr ""

#: ../pollwidget.php:285
msgid "Enable AJAX"
msgstr ""

#: ../pollwidget.php:286
msgid "Disable script output"
msgstr ""

#: ../pollwidget.php:287
msgid "Tab Index Start"
msgstr ""

#: ../pollwidget.php:289
msgid ""
"If you have other forms on the page (i.e. Comments Form), specify a higher "
"tabindex start value so that your Gravity Form does not end up with the same "
"tabindices as your other forms. To disable the tabindex, enter 0 (zero)."
msgstr ""
