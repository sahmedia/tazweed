content-rtmedia_album.php

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	



		<div class="author-box"><?php 
		 global $wp_query;
global $rtmedia_query;
$model = new RTMediaModel();	
$album_post =$post->ID;
$results = $model->get(array('media_type' => 'album', 'media_id' => $album_post));

	 ?>	
<header class="page-header">
		

		<div class="entry-meta">
			<?php //the_bootstrap_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->			</div>
	<div class="well well-sm">
                <div class="media">
				<p><?php printf( _x( 'by %s', 'Post written by...', 'the-bootstrap' ), str_replace( '<a href=', '<a rel="author" href=', bp_core_get_userlink( $post->post_author ) ) ); ?></p>
                    <a class="thumbnail pull-left" href="<?php bp_core_get_userlink( $post->post_author ) ; ?>">
                     <?php 	echo get_avatar( get_the_author_meta( 'user_email' ), '50' );?>
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">
			<?php the_title( '<h4 class="entry-format"><a href="' .get_rtmedia_permalink($results[0]->id) .'" title="' . sprintf( esc_attr__( 'Permalink to %s', 'the-bootstrap' ), the_title_attribute( 'echo=0' ) ) . '" rel="bookmark">', '</a></h2>' ); ?>
			<h3 class="entry-format"><?php// echo get_post_format_string(get_post_format()); ?></h3>
		</h4>
                		<p><span class="label label-info">10 photos</span> <span class="label label-primary">89 followers</span></p>
							<div class="entry-content row">
		<?php
		$the_bootstrap_images = get_children( array(
			'post_parent'		=>	$post->ID,
			'post_type'			=>	'attachment',
			'post_mime_type'	=>	'image',
			'orderby'			=>	'menu_order',
			'order'				=>	'ASC',
			'numberposts'		=>	5
		) );
		if ( $the_bootstrap_images ) :
			$the_bootstrap_total_images	=	count( $the_bootstrap_images );
			$the_bootstrap_images		=	array_slice( $the_bootstrap_images, 0, 10 );
		?>
    <div class="row">
        <div class="col-md-9" id="slider">
            <!-- Top part of the slider -->
            <div class="row">
                <div class="col-md-2" id="carousel-bounding-box">
                    <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                      
                        <div class="carousel-inner">
                          <?php 	  $first = TRUE;
						  foreach ( $the_bootstrap_images as $the_bootstrap_image ) :
    if($first)
    {
        echo '<div class="item active" data-slide-number="'.$the_bootstrap_image->ID.'">';
        $first = FALSE;
    }
    else
    {    
        echo '<div class="item" data-slide-number="'.$the_bootstrap_image->ID.'">';
    }


?>
           
              <a href="<?php echo  get_rtmedia_permalink($results[0]->id)?>">	
		
					<?php
$imageUrl = wp_get_attachment_image_src( $the_bootstrap_image->ID, 'full' );

echo '<img class="img-rounded img-responsive" src="'; echo $imageUrl[0]; echo '"/> ';

				
				
				 ?>
			
				</a>
                            </div>
			  
			<?php endforeach; ?>
                           
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <div class="carousel-controls-mini">ddf<a href="#myCarousel" data-slide="prev">‹</a>sdqsd
 <a href="#myCarousel" data-slide="next">›</a>sdq

                    </div>
                </div>
                <div class="col-md-4" id="carousel-text"></div>
                <div style="display:none;" id="slide-content"> 
				<?php foreach ( $the_bootstrap_images as $the_bootstrap_image ) :?>
                    <div id="slide-content-<?php echo  $the_bootstrap_image->ID?>">
                         <h5>Slide One</h5>

                        <p>	<?php the_content($the_bootstrap_image->ID);?>		
                            Lorem dolor?</p> <small>October 13 2013 - <a href="#">Read more</a></small>

                    </div>
					<?php endforeach; ?>
                   
                </div>
            </div>
        </div>
    </div>
	<?php endif; /* if images */ ?>
		    </div>
	</div><!-- .entry-content -->
	
                        <p>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-comment"></span> Message</a>
                            <a href="#" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-heart"></span> Favorite</a>
                        </p>
                    </div>
                </div>
               </div>

	
</article><!-- #post-<?php the_ID(); ?> -->
<?php


/* End of file content-gallery.php */
/* Location: ./wp-content/themes/the-bootstrap/partials/content-rtmedia_album.php */
?>
