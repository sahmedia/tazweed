<?php
/* * **************************************
 * This is rtmedia plugin support file
 *
 * The main template file, that loads the header, footer and sidebar
 * apart from loading the appropriate rtMedia template
 * *************************************** */
// by default it is not an ajax request
global $rt_ajax_request, $firmasite_settings;
$rt_ajax_request = false ;

// check if it is an ajax request
if ( ! empty ( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) &&
        strtolower ( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) == 'xmlhttprequest'
 ) {
    $rt_ajax_request = true ;
	?>
	<div id="buddypress">
	<?php
}
//if it's not an ajax request, load headers
    if ( ! $rt_ajax_request ) {
        // if this is a BuddyPress page, set template type to
        // buddypress to load appropriate headers
        if ( class_exists ( 'BuddyPress' ) && ! bp_is_blog_page () && apply_filters( 'rtm_main_template_buddypress_enable', true ) ) {
            $template_type = 'buddypress' ;
        }
        else {
            $template_type = '' ;
        }
        //get_header( $template_type );

        if ( $template_type == 'buddypress' ) {
            //load buddypress markup
            if ( bp_displayed_user_id () ) {

                //if it is a buddypress member profile
                ?>
                <div id="item-header" class="well well-sm media clearfix">

                    <?php bp_get_template_part ( 'members/single/member-header' ) ?>

                </div><!--#item-header-->

                 <nav id="item-nav" class="navbar <?php if (isset($firmasite_settings["menu-style"]) && "alternative" == $firmasite_settings["menu-style"]){ echo " navbar-default";} else { echo "  navbar-inverse"; } ?>" role="navigation">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#object-nav">
                        <span class="sr-only"><?php _e( 'Toggle navigation', 'firmasite' ); ?></span>
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                      </button>
                    </div>
                
                    <div class="item-list-tabs no-ajax collapse navbar-collapse tabs-top" id="object-nav">
                      <ul class="nav navbar-nav">
            
                            <?php bp_get_displayed_user_nav(); ?>
            
                            <?php do_action( 'bp_member_options_nav' ); ?>
            
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav><!-- #item-nav -->

                <div id="item-body">

                    <?php do_action ( 'bp_before_member_body' ) ; ?>
                    <?php do_action ( 'bp_before_member_media' ) ; ?>
						<div class="modal firmasite-modal-static">
							<div class="modal-dialog"><div class="modal-content">
							<div class="modal-body">

<div class="profile" role="main">	
                    <div class="item-list-tabs no-ajax" id="subnav">
                        <ul class="nav nav-pills" role="navigation">

                            <?php rtmedia_sub_nav () ; ?>

                            <?php do_action ( 'rtmedia_sub_nav' ) ; ?>

                        </ul>
                    </div><!-- .item-list-tabs -->

                    <?php
                }
                else if ( bp_is_group () ) {

                    //not a member profile, but a group
                    ?>

                    <?php
                    if ( bp_has_groups () ) : while ( bp_groups () ) : bp_the_group () ;
                            ?>
                            <div id="item-header" class="well well-sm media clearfix">

                                <?php bp_get_template_part ( 'groups/single/group-header' ) ; ?>

                            </div><!--#item-header-->

                            <nav id="item-nav" class="navbar <?php if (isset($firmasite_settings["menu-style"]) && "alternative" == $firmasite_settings["menu-style"]){ echo " navbar-default";} else { echo "  navbar-inverse"; } ?>" role="navigation">
                              <div class="container-fluid">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#object-nav">
                                    <span class="sr-only"><?php _e( 'Toggle navigation', 'firmasite' ); ?></span>
                                    <i class="icon-bar"></i>
                                    <i class="icon-bar"></i>
                                    <i class="icon-bar"></i>
                                  </button>
                                </div>
                            
                                <div class="item-list-tabs no-ajax collapse navbar-collapse tabs-top" id="object-nav">
                                  <ul class="nav navbar-nav">
                        
                                        <?php bp_get_options_nav(); ?>
                        
                                        <?php do_action( 'bp_group_options_nav' ); ?>
                        
                                  </ul>
                                </div><!-- /.navbar-collapse -->
                              </div><!-- /.container-fluid -->
                            </nav><!-- #item-nav -->


                            <div id="item-body">

                                <?php do_action ( 'bp_before_group_body' ) ; ?>
                                <?php do_action ( 'bp_before_group_media' ) ; ?>
							<div class="modal firmasite-modal-static">
							<div class="modal-dialog"><div class="modal-content">
							<div class="modal-body">

<div class="profile" role="main">	
                                <div class="item-list-tabs no-ajax" id="subnav">
                                    <ul class="nav nav-pills" role="navigation">

                                        <?php rtmedia_sub_nav () ; ?>

                                        <?php do_action ( 'rtmedia_sub_nav' ) ; ?>

                                    </ul>
                                </div><!-- .item-list-tabs -->
                                <?php
                            endwhile ;
                        endif ;
                    } // group/profile if/else
                    ?>
                <?php
            }else{ ////if BuddyPress
                ?>
                            <div id="item-body">
                            <?php
            }
        } // if ajax
        // include the right rtMedia template
        rtmedia_load_template () ;
?>
                            </div>
<?php
        
        if ( ! $rt_ajax_request ) {
            if ( function_exists ( "bp_displayed_user_id" ) && $template_type == 'buddypress' && (bp_displayed_user_id () || bp_is_group ()) ) {

                if ( bp_is_group () ) {
                    do_action ( 'bp_after_group_media' ) ;
                    do_action ( 'bp_after_group_body' ) ;
                }
                if ( bp_displayed_user_id () ) {
                    do_action ( 'bp_after_member_media' ) ;
                    do_action ( 'bp_after_member_body' ) ;
                }
            }
        } else {
        //close all markup
        ?>
   		</div></div></div></div></div><!--#buddypress-->
        <?php
		}
            //get_sidebar($template_type);
            //get_footer($template_type);
        // if ajax

        