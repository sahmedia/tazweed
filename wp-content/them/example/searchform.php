searchform.php<?php
/** searchform.php
 *
 * The template for displaying search forms
 *
 * @author		Konstantin Obenland
 * @package		The Bootstrap
 * @since		1.0.0 - 07.02.2012
 */

?>
<?php echo do_shortcode('[searchandfilter fields="search,post_types" post_types="all" headings=",Post Types"]');
 ?>
<?php


/* End of file searchform.php */
/* Location: ./wp-content/themes/the-bootstrap/searchform.php */