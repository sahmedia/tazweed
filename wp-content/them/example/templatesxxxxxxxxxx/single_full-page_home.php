single_full-page.php 
<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package firmasite
 */
global $firmasite_settings;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 <div class="panel panel-default">
   <div class="panel-body">
  
    <div class="entry-content">
 		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'firmasite' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><ul class="pagination pagination-lg"><li><span>' . __( 'Pages:', 'firmasite' ) . '</span></li>', 'after' => '</ul></div>','link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
      
 
       
    </div>
   </div>
 </div>
</article><!-- #post-<?php the_ID(); ?> -->