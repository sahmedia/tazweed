<?php
/**
 *
 * @package    lobster
 * @subpackage Functions
 * @version    1.9.6
 * @since      1.0
 * @author     Ruairi Phelan <rory@cyberdesigncraft.com>
 * @copyright  2013, Cyberdesign Craft
 * @link       http://cyberdesigncraft.com/themes/wordpress/lobster/
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
 *
 *
 * Defining constants
 *
 * @since 1.0
 */


/**
 * Constants
 *
 * @since 1.0
 */
 
 // d($variable); /* will output a styled, collapsible container with your variable information */
// dd($variable) ;/* will do exactly as d() except halt execution of the script */
// s($variable) ; /* will output a simple, un-styled whitespace container */
// sd($variable) ; /* will do exactly as s() except halt execution of the script */
// /* Backtrace is also easy: */

//Kint::trace() ; /* /* The displayed information for each step of the trace includes the source snippet, passed arguments and the object at the time of calling 
// We've also baked in a few functions that are WordPress specific: */

/* dump_wp_query();
dump_wp() ;
dump_post();
 
 Kint::trace(); */
 /**
 * Proper way to enqueue scripts and styles
 */
/* function theme_name_scripts() {
// wp_enqueue_style( 'bootstrap.min', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
	//wp_enqueue_style( 'styles', get_stylesheet_directory_uri() . '/css/styles.css' );
	wp_enqueue_script( 'bootstrap.min', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
	wp_enqueue_script( 'scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array(), '1.0.0', true );
} */

//add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );


add_filter('firmasite_pre_get_posts_ekle', "firmasite_custom_cpt_add");
function firmasite_custom_cpt_add($array){ 
	$array[]='rtmedia_album'; 
	return $array; 
	
}
add_filter('firmasite_showcase_content', "firmasite_showcase_content_add");
function firmasite_showcase_content_add($test){ 
$test= do_shortcode('[widgets_on_pages id=login]') ; 
	return $test;
	
}
function add_last_nav_item($items) {
  $items= do_shortcode('[widgets_on_pages id=login]') ; 
  return $items;
}
add_filter('firmasite_showcase_content','wp_nav_menu_items');
global $firmasite_showcase,$firmasite_settings;
print_r($firmasite_settings);
global $blog_id;
if ( 1 == $blog_id) :
 
add_filter( "wp_nav_menu_items", function ($items, $args){
	$items .= firmasite_custom_bpmenu();
	return $items; 
},10,2 );
 
add_action("wp_head", function(){ ?>
	<style type="text/css" media="screen">
    .dropdown-form { padding: 15px; }
    </style>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			jQuery("li.member-menu ul.dropdown-menu span").addClass("label label-info");
		});
    </script>
    <?php         	
},11);
 
add_action("wp_footer","firmasite_custom_bpmenu");
function firmasite_custom_bpmenu() {
    ob_start();
?>
<?php if ( !is_user_logged_in() ) { ?>
<li class="dropdown member-menu"><a class="dropdown-toggle" data-target="#" href="#"><i class="icon-briefcase"></i> <?php _e( 'Account', 'firmasite' ) ?> <b class="caret"></b></a>
<ul class="dropdown-menu dropdown-form">
<li>
<form name="login-form" id="login-form" action="<?php echo site_url( 'wp-login.php' ) ?>" method="post">
	<div class="input-prepend">
		<span class="add-on"><i class="icon-user"></i></span>
		<input type="text" name="log" id="user_login" value="" placeholder="<?php _e( 'Username', 'firmasite' ) ?>" />
	</div>
	<div class="input-prepend">
		<span class="add-on"><i class="icon-lock"></i></span>
		<input type="password" name="pwd" id="user_pass" value="" placeholder="<?php _e( 'Password', 'firmasite' ) ?>" />
	</div>
    <label for="rememberme" class="checkbox"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_attr_e('Remember Me', 'firmasite'); ?></label>
    <input type="submit" name="wp-submit" id="wp-submit" value="<?php _e( 'Log In', 'firmasite' ) ?>"/>
    <?php if ( bp_get_signup_allowed() ) { ?>
    <input class="btn btn-primary pull-right" type="button" name="signup-submit" id="signup-submit" value="<?php _e( 'Create an Account', 'firmasite' ) ?>" onclick="location.href='<?php echo bp_signup_page() ?>'" />
    <?php } ?>
	<?php
	// http://kovshenin.com/2012/current-url-in-wordpress/
	global $wp;
	$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
	?>
    <input type="hidden" name="redirect_to" value="<?php echo $current_url; ?>" />
    <input type="hidden" name="testcookie" value="1" />
    <?php do_action( 'bp_login_bar_logged_out' ) ?>
</form>
</li>
<?php } else { ?>
<li class="dropdown member-menu"><a class="dropdown-toggle" data-target="#" href="#"><i class="icon-user"></i> <?php echo bp_core_get_username( bp_loggedin_user_id() ); ?> <b class="caret"></b></a>
<ul class="dropdown-menu">
<li>
       <?php firmasite_bp_adminbar_notifications_menu() ?>
        <?php bp_get_loggedin_user_nav() ?>  
 
<?php } ?>
</ul>
</li>
<?php
    return ob_get_clean();
}
 
function firmasite_bp_adminbar_notifications_menu() {
 
	if ( !is_user_logged_in() )
		return false;
 
	echo '<li id="bp-adminbar-notifications-menu" class="dropdown-submenu"><a href="' . bp_loggedin_user_domain() . '">';
	_e( 'Notifications', 'buddypress' );
 
	if ( $notifications = bp_core_get_notifications_for_user( bp_loggedin_user_id() ) ) { ?>
		<span><?php echo count( $notifications ) ?></span>
	<?php
	}
 
	echo '</a>';
	echo '<ul class="dropdown-menu">';
 
	if ( $notifications ) {
		$counter = 0;
		for ( $i = 0, $count = count( $notifications ); $i < $count; ++$i ) {
			$alt = ( 0 == $counter % 2 ) ? ' class="alt"' : ''; ?>
 
			<li<?php echo $alt ?>><?php echo $notifications[$i] ?></li>
 
			<?php $counter++;
		}
	} else { ?>
 
		<li><a href="<?php echo bp_loggedin_user_domain() ?>"><?php _e( 'No new notifications.', 'buddypress' ); ?></a></li>
 
	<?php
	}
 
	echo '</ul>';
	echo '</li>';
}
 
endif;

// Removing wordpress version from script and styles
add_action("wp_head", "firmasite_remove_version_from_assets",1);
function firmasite_remove_version_from_assets(){
	function remove_cssjs_ver( $src ) {
		if( strpos( $src, '?ver=' ) )
			$src = remove_query_arg( 'ver', $src );
		return $src;
	}
	add_filter( 'style_loader_src', 'remove_cssjs_ver', 999 );
	add_filter( 'script_loader_src', 'remove_cssjs_ver', 999 );
}






add_action('generate_rewrite_rules', 'themes_dir_add_rewrites');  
  
function themes_dir_add_rewrites() {  
  $theme_name = next(explode('/themes/', get_stylesheet_directory()));  
  
  global $wp_rewrite;  
  $new_non_wp_rules = array(  
    'css/(.*)'       => 'wp-content/themes/'. $theme_name . '/css/$1',  
    'js/(.*)'        => 'wp-content/themes/'. $theme_name . '/js/$1',  
    'images/wordpress-urls-rewrite/(.*)'    => 'wp-content/themes/'. $theme_name . '/images/wordpress-urls-rewrite/$1',  
  );  
  $wp_rewrite->non_wp_rules += $new_non_wp_rules;  
} 
?>
