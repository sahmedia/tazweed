
$(document).ready(function(){/* jQuery toggle layout */
jQuery('#btnToggle').click(function(){
  if (jQuery(this).hasClass('on')) {
    $('#main .col-md-6').addClass('col-md-4').removeClass('col-md-6');
    $(this).removeClass('on');
  }
  else {
    jQuery('#main .col-md-4').addClass('col-md-6').removeClass('col-md-4');
    jQuery(this).addClass('on');
  }
});
$('#myCarousel').carousel({
  interval: 5000
});

$('#carousel-text').html($('#slide-content-0').html());

//Handles the carousel thumbnails
$('[id^=carousel-selector-]').click( function(){
  var id_selector = $(this).attr("id");
  var id = id_selector.substr(id_selector.length-1);
  var id = parseInt(id);
  $('#myCarousel').carousel(id);
});


// When the carousel slides, auto update the text
$('#myCarousel').on('slid.bs.carousel', function (e) {
  var id = $('.item.active').data('slide-number');
  $('#carousel-text').html($('#slide-content-'+id).html());
});	
});