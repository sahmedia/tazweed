<?php
/*
 * Child Theme Functions
 * Version: 1.0.1
 */

// we are automatically getting child theme folder name
global $firmasite_child;	
	$firmasite_child['name'] = basename(dirname(dirname(__FILE__)));	

/* 
 * This action loads child theme's translations if have any
 */
add_action('after_setup_theme', "firmasite_childtheme_setup" );
function firmasite_childtheme_setup() {
	global $firmasite_child;
	
	// Make theme available for translation
	load_child_theme_textdomain( $firmasite_child['name'], get_stylesheet_directory() . '/languages' );
}

/* 
 * This action loads this child theme's style.css 
 */
add_action('wp_enqueue_scripts', "firmasite_childtheme_enqueue_script", 11 );
function firmasite_childtheme_enqueue_script() {
	
	// child_style	
	// Load Angular
	wp_enqueue_script( 'ae-angular', '//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js', array( ), '', true );
	wp_register_style( 'childtheme_style', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_style( 'childtheme_style' );
	wp_register_style( 'bootstrap-tagsinput', get_stylesheet_directory_uri() . '/_inc/bootstrap-tagsinput.css' );
	wp_enqueue_style( 'bootstrap-tagsinput' );
	wp_register_style( 'jquery_typeahead', get_stylesheet_directory_uri() . '/_inc/jquery.typeahead.css' );
	wp_enqueue_style( 'jquery_typeahead' );
	wp_register_script('background-check', get_stylesheet_directory_uri(). '/_inc/background-check.min.js');
    wp_enqueue_script( 'background-check' );
	wp_register_script('tagsinput-angular', get_stylesheet_directory_uri(). '/_inc/bootstrap-tagsinput-angular.js');
    wp_enqueue_script( 'tagsinput-angular' );	
	wp_register_script('bootstrap-tagsinput', get_stylesheet_directory_uri(). '/_inc/bootstrap-tagsinput.js');
    wp_enqueue_script( 'bootstrap-tagsinput' );	
	wp_register_script('bloodhound', get_stylesheet_directory_uri(). '/_inc/typeahead/bloodhound.js');
    wp_enqueue_script( 'bloodhound' );
	wp_register_script('handlebars', get_stylesheet_directory_uri(). '/_inc/typeahead/handlebars.js');
    wp_enqueue_script( 'handlebars' );
	wp_register_script('xdomainrequest', get_stylesheet_directory_uri(). '/_inc/typeahead/jquery.xdomainrequest.min.js');
    wp_enqueue_script( 'xdomainrequest' );
	/* wp_register_script('typeahead.bundle', get_stylesheet_directory_uri(). '/_inc/typeahead/typeahead.bundle.js');
    wp_enqueue_script( 'typeahead.bundle' ); */
	/* wp_register_script('typeahead.jquery', get_stylesheet_directory_uri(). '/_inc/typeahead/typeahead.jquery.js');
    wp_enqueue_script( 'typeahead.jquery' ); */
	
	

	// Load custom app script
}


/* 
 This action copies old theme's menu location saves to 
 new theme if new theme doesnt have saves before. Helpful for child theme switches
*/
add_action( 'after_switch_theme',  'firmasite_childtheme_locations_rescue' );
function firmasite_childtheme_locations_rescue() {
	// bug report / support: http://unsalkorkmaz.com/
	// We got old theme's slug name
	$old_theme = get_option( 'theme_switched' );
	// Getting old theme's settings
	$old_theme_mods = get_option("theme_mods_{$old_theme}");
	// Getting old theme's theme location settings
	if (isset($old_theme_mods['nav_menu_locations']))
		$old_theme_navs = $old_theme_mods['nav_menu_locations'];
	
	// Getting new theme's theme location settings
	$new_theme_navs = get_theme_mod( 'nav_menu_locations' );
	
	// If new theme's theme location is empty (its not empty if theme was activated and set some theme locations before)
	if (!$new_theme_navs && (isset($old_theme_navs) && is_array($old_theme_navs) )) {
		// Getting registered theme locations on new theme
		$new_theme_locations = get_registered_nav_menus();

		foreach ($new_theme_locations as $location => $description ) {
			// We setting same nav menus for each theme location 
			$new_theme_navs[$location] = $old_theme_navs[$location];
		}
		
		set_theme_mod( 'nav_menu_locations', $new_theme_navs );
		
	}
}


/* 
 This action preventing child theme update checks from WordPress.Org reposity.
 Do note that this will only hide the ACTIVE theme. 
 http://markjaquith.wordpress.com/2009/12/14/excluding-your-plugin-or-theme-from-update-checks/
*/
add_filter( 'http_request_args', 'firmasite_childtheme_dont_update', 5, 2 );
function firmasite_childtheme_dont_update( $r, $url ) {
    if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
        return $r; // Not a theme update request. Bail immediately.
    $themes = unserialize( $r['body']['themes'] );
    unset( $themes[ get_option( 'template' ) ] );
    unset( $themes[ get_option( 'stylesheet' ) ] );
    $r['body']['themes'] = serialize( $themes );
    return $r;
}



 // Promofoire Remove css .media class from body class
// add_filter ( 'body_class', 'rt_bp_get_the_body_class', 11, 2 );  
function rt_bp_get_the_body_class($wp_classes, $custom_classes = false){  
    $wp_classes = array_replace ( $wp_classes, array_fill_keys (  
            array_keys ( $wp_classes, 'media' ), 'multimedia'  
        )  
    );  
    return $wp_classes;  
}  


add_filter('firmasite_pre_get_posts_ekle', "firmasite_custom_cpt_add");
function firmasite_custom_cpt_add($array){ 
	$array[]='rtmedia_album'; 
	return $array; 
	
}
//add_filter('firmasite_showcase_content', "firmasite_showcase_content_add");
function firmasite_showcase_content_add($test){ 
$test= do_shortcode('[widgets_on_pages id="showcase"]') ; 
	return $test;
	
}
function add_last_nav_item($items) {
  $items= do_shortcode('[widgets_on_pages id="showcase"]') ; 
  return $items;
}
add_filter('firmasite_showcase_content','wp_nav_menu_items');
global $firmasite_showcase,$firmasite_settings;
print_r($firmasite_settings);
global $blog_id;
if ( 1 == $blog_id) :
 
add_filter( 'wp_nav_menu_items',"fin_bug",10,2 );
  
  
  function fin_bug ($items, $args){
	$items .= firmasite_custom_bpmenu();
	return $items; 
}
add_action('wp_head',"fin_bug1" ,11);
function fin_bug1(){ ?>
	<style type="text/css" media="screen">
    .dropdown-form { padding: 15px; }
    </style>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			jQuery("li.member-menu ul.dropdown-menu span").addClass("label label-info");
		});
    </script>
    <?php         	
}
 
add_action("wp_footer","firmasite_custom_bpmenu");
function firmasite_custom_bpmenu() {
    ob_start();
?>
<?php if ( !is_user_logged_in() ) { ?>
<li class="dropdown member-menu"><a class="dropdown-toggle" data-target="#" href="#"><i class="icon-briefcase"></i> <?php _e( 'Account', 'firmasite' ); echo do_shortcode('[Alimir_BootModal_Login]');?> <b class="caret"></b></a>
<ul class="dropdown-menu dropdown-form">
<li>
<form name="login-form" id="login-form" action="<?php echo site_url( 'wp-login.php' ) ?>" method="post">
	<div class="input-prepend">
		<span class="add-on"><i class="icon-user"></i></span>
		<input type="text" name="log" id="user_login" value="" placeholder="<?php _e( 'Username', 'firmasite' ) ?>" />
	</div>
	<div class="input-prepend">
		<span class="add-on"><i class="icon-lock"></i></span>
		<input type="password" name="pwd" id="user_pass" value="" placeholder="<?php _e( 'Password', 'firmasite' ) ?>" />
	</div>
    <label for="rememberme" class="checkbox"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_attr_e('Remember Me', 'firmasite'); ?></label>
    <input type="submit" name="wp-submit" id="wp-submit" value="<?php _e( 'Log In', 'firmasite' ) ?>"/>
    <?php if ( bp_get_signup_allowed() ) { ?>
    <input class="btn btn-primary pull-right" type="button" name="signup-submit" id="signup-submit" value="<?php _e( 'Create an Account', 'firmasite' ) ?>" onclick="location.href='<?php echo bp_signup_page() ?>'" />
    <?php } ?>
	<?php
	// http://kovshenin.com/2012/current-url-in-wordpress/
	global $wp;
	$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
	?>
    <input type="hidden" name="redirect_to" value="<?php echo $current_url; ?>" />
    <input type="hidden" name="testcookie" value="1" />
    <?php do_action( 'bp_login_bar_logged_out' ) ?>
</form>
</li>
<?php } else { ?>
<li class="dropdown member-menu"><a class="dropdown-toggle" data-target="#" href="#"><i class="icon-user"></i> <?php echo bp_core_get_username( bp_loggedin_user_id() ); ?> <b class="caret"></b></a>
<ul class="dropdown-menu">
<li>
       <?php firmasite_bp_adminbar_notifications_menu() ?>
        <?php bp_get_loggedin_user_nav() ?>  
 
<?php } ?>
</ul>
</li>
<?php
    return ob_get_clean();
}
 
function firmasite_bp_adminbar_notifications_menu() {
 
	if ( !is_user_logged_in() )
		return false;
 
	echo '<li id="bp-adminbar-notifications-menu" class="dropdown-submenu"><a href="' . bp_loggedin_user_domain() . '">';
	_e( 'Notifications', 'buddypress' );
 
	if ( $notifications = bp_core_get_notifications_for_user( bp_loggedin_user_id() ) ) { ?>
		<span><?php echo count( $notifications ) ?></span>
	<?php
	}
 
	echo '</a>';
	echo '<ul class="dropdown-menu">';
 
	if ( $notifications ) {
		$counter = 0;
		for ( $i = 0, $count = count( $notifications ); $i < $count; ++$i ) {
			$alt = ( 0 == $counter % 2 ) ? ' class="alt"' : ''; ?>
 
			<li<?php echo $alt ?>><?php echo $notifications[$i] ?></li>
 
			<?php $counter++;
		}
	} else { ?>
 
		<li><a href="<?php echo bp_loggedin_user_domain() ?>"><?php _e( 'No new notifications.', 'buddypress' ); ?></a></li>
 
	<?php
	}
 
	echo '</ul>';
	echo '</li>';
}
 
endif;

// Removing wordpress version from script and styles
add_action("wp_head", "firmasite_remove_version_from_assets",1);
function firmasite_remove_version_from_assets(){
	function remove_cssjs_ver( $src ) {
		if( strpos( $src, '?ver=' ) )
			$src = remove_query_arg( 'ver', $src );
		return $src;
	}
	add_filter( 'style_loader_src', 'remove_cssjs_ver', 999 );
	add_filter( 'script_loader_src', 'remove_cssjs_ver', 999 );
}







//add_action('generate_rewrite_rules', 'themes_dir_add_rewrites');  
  
function themes_dir_add_rewrites() {  
  $theme_name = next(explode('/themes/', get_stylesheet_directory()));  
  
  global $wp_rewrite;  
  $new_non_wp_rules = array(  
    'css/(.*)'       => 'wp-content/themes/'. $theme_name . '/css/$1',  
    'js/(.*)'        => 'wp-content/themes/'. $theme_name . '/js/$1',  
    'images/wordpress-urls-rewrite/(.*)'    => 'wp-content/themes/'. $theme_name . '/images/wordpress-urls-rewrite/$1',  
  );  
  $wp_rewrite->non_wp_rules += $new_non_wp_rules;  
} 


 
add_action( 'after_setup_theme', 'baw_theme_setup' );
function baw_theme_setup() {
  add_image_size( 'category-thumb', 300 ); // 300 pixels wide (and unlimited height)
  add_image_size( 'homepage-thumb', 220, 180, true ); // (cropped)

}
        add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );
function custom_image_sizes_choose( $sizes ) {
    $custom_sizes = array(
        'featured-image' => 'customFeatured Image'
    );
    return array_merge( $sizes, $custom_sizes );
}
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

?>
