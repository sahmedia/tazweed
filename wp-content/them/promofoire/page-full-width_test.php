page-full-width.php
<?php
/* 
Template Name: Full Width test
*/
/**
 * @package firmasite
 */
// this one letting us translate page template names
esc_attr__( 'Full Width test', 'firmasite' );
//global $firmasite_settings;
 
 ?>
<!--[if IE 8]> <html class="lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		 <meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<title>
		PROMOFOIRE &#171; AMELIOREZ VOTRE STRATEGIE MARKETING	</title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
 
	<meta name='robots' content='noindex,follow' />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/www.promofoire.com\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var c=b.createElement("canvas"),d=c.getContext&&c.getContext("2d");return d&&d.fillText?(d.textBaseline="top",d.font="600 32px Arial","flag"===a?(d.fillText(String.fromCharCode(55356,56812,55356,56807),0,0),c.toDataURL().length>3e3):(d.fillText(String.fromCharCode(55357,56835),0,0),0!==d.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
		<style id="custom-custom-css" type="text/css" media="screen">
				</style>
				<style type="text/css" media="screen">
							body, h1, h2, h3, h4, h5, h6, .btn, .navbar { font-family: Ubuntu Condensed,sans-serif !important;}
						           </style>
<script type='text/javascript'>
/* <![CDATA[ */
var BP_DTheme = {"accepted":"Accepted","close":"Close","comments":"comments","leave_group_confirm":"Are you sure you want to leave this group?","mark_as_fav":"Favorite","my_favs":"My Favorites","rejected":"Rejected","remove_fav":"Remove Favorite","show_all":"Show all","show_all_comments":"Show all comments for this thread","show_x_comments":"Show all %d comments","unsaved_changes":"Your profile has unsaved changes. If you leave the page, the changes will be lost.","view":"View"};
/* ]]> */
</script>

<script type='text/javascript'>
/* <![CDATA[ */
var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Download File":"Download File","Download Video":"Download Video","Play\/Pause":"Play\/Pause","Mute Toggle":"Mute Toggle","None":"None","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Unmute":"Unmute","Mute":"Mute","Captions\/Subtitles":"Captions\/Subtitles"}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>

<script type='text/javascript'>
/* <![CDATA[ */
var rtmedia_ajax_url = "http:\/\/www.promofoire.com\/wp-admin\/admin-ajax.php";
var rtmedia_media_slug = "media";
var rtmedia_lightbox_enabled = "1";
var rtmedia_gallery_reload_on_upload = "1";
var rtmedia_empty_activity_msg = "Please enter some content to post.";
var rtmedia_empty_comment_msg = "Empty Comment is not allowed.";
var rtmedia_media_delete_confirmation = "Are you sure you want to delete this media?";
var rtmedia_media_comment_delete_confirmation = "Are you sure you want to delete this comment?";
var rtmedia_album_delete_confirmation = "Are you sure you want to delete this Album?";
var rtmedia_drop_media_msg = "Drop files here";
var rtmedia_album_created_msg = " album created successfully.";
var rtmedia_something_wrong_msg = "Something went wrong. Please try again.";
var rtmedia_empty_album_name_msg = "Enter an album name.";
var rtmedia_max_file_msg = "Max file Size Limit : ";
var rtmedia_allowed_file_formats = "Allowed File Formats";
var rtmedia_select_all_visible = "Select All Visible";
var rtmedia_unselect_all_visible = "Unselect All Visible";
var rtmedia_no_media_selected = "Please select some media.";
var rtmedia_selected_media_delete_confirmation = "Are you sure you want to delete the selected media?";
var rtmedia_selected_media_move_confirmation = "Are you sure you want to move the selected media?";
var rtmedia_waiting_msg = "Waiting";
var rtmedia_uploaded_msg = "Uploaded";
var rtmedia_uploading_msg = "Uploading";
var rtmedia_upload_failed_msg = "Failed";
var rtmedia_close = "Close";
var rtmedia_edit = "Edit";
var rtmedia_delete = "Delete";
var rtmedia_edit_media = "Edit Media";
var rtmedia_remove_from_queue = "Remove from queue";
var rtmedia_add_more_files_msg = "Add more files";
var rtmedia_file_extension_error_msg = "File not supported";
var rtmedia_more = "more";
var rtmedia_less = "less";
var rtmedia_delete_uploaded_media = "This media is uploaded. Are you sure you want to delete this media?";
var rtm_wp_version = "4.2.2";
var rtmedia_masonry_layout = "true";
/* ]]> */
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var rtmedia_load_more = "Loading media";
/* ]]> */
</script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.promofoire.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.promofoire.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.2.2" />
<link rel='canonical' href='http://www.promofoire.com/' />
<link rel='shortlink' href='http://www.promofoire.com/' />

	<script type="text/javascript">var ajaxurl = 'http://www.promofoire.com/wp-admin/admin-ajax.php';</script>

	<style>
	#settings_panel { position:fixed; z-index:10000; left:0; top: 120px }
	#settings_panel_content form { display:inline-block; margin-bottom:0}
	#settings_panel_content .btn-group, #settings_panel_content .help-inline { vertical-align:top; display: inline-block;}
	#settings_panel .popover { z-index:10001;}
	
	</style>
	<style type='text/css'>
img.lazy { display: none; }
</style>
		<style type="text/css">
				            .rtmedia-activity-container .rtmedia-list .rtmedia-item-thumbnail,.bp_media_content img{
                max-width: 320px;
                max-height: 240px;
            }
			.rtmedia-activity-container .mejs-container.mejs-video{
				min-height: 240px;
				min-width: 320px;
			}
        			.rtmedia-container .rtmedia-list  .rtmedia-list-item .rtmedia-item-thumbnail {
				max-height: 150px;
			}
				.rtmedia-container .rtmedia-list  .rtmedia-list-item .rtmedia-item-thumbnail {
				max-width: 150px;
			}
					</style>
			<style type="text/css" media="screen">
    .dropdown-form { padding: 15px; }
    </style>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			jQuery("li.member-menu ul.dropdown-menu span").addClass("label label-info");
		});
    </script>
    		<script type="text/javascript">
			function visible_lg(){ return (jQuery("#visible-lg").css("display") === "block") ? true : false; }
			function visible_md(){ return (jQuery("#visible-md").css("display") === "block") ? true : false; }
			function visible_sm(){ return (jQuery("#visible-sm").css("display") === "block") ? true : false; }
			function visible_xs(){ return (jQuery("#visible-xs").css("display") === "block") ? true : false; }
			
			// http://remysharp.com/2010/07/21/throttling-function-calls/
			function throttle(d,a,h){a||(a=250);var b,e;return function(){var f=h||this,c=+new Date,g=arguments;b&&c<b+a?(clearTimeout(e),e=setTimeout(function(){b=c;d.apply(f,g)},a)):(b=c,d.apply(f,g))}};
        </script>
		</head>
<body class="home-page home page page-id-196 page-template page-template-home_promo page-template-home_promo-php logged-in acps_results_archive search search_results no-js">

<a href="#primary" class="sr-only">Skip to content</a>

<div id="page" class="hfeed site site-sidebar-content showcase-style-1 lumen-theme">

	    
	<header id="masthead" class="site-header" role="banner">
  <div id="masthead-inner" class="container">

      
    <div id="logo" class="pull-left">         
        <a href="http://www.promofoire.com/" title="PROMOFOIRE" rel="home" id="logo-link" class="logo" data-section="body">
                        <img src="http://www.promofoire.com/wp-content/uploads/2015/05/create_thumb-12.png" alt="AMELIOREZ VOTRE STRATEGIE MARKETING" title="PROMOFOIRE" id="logo-img" />
                     </a>
    </div>
    
        
    <div id="logo-side" class="pull-right">
       
              	<div id="site-description" class="no-margin-bot text-right text-muted hidden-xs hidden-sm">AMELIOREZ VOTRE STRATEGIE MARKETING</div>
       
                  
    </div>
    
        
    <div id="navbar-splitter" class="clearfix"></div>
    
       
        
  </div>
</header><!-- #masthead .site-header -->
    
	     
					                    <a data-slide="prev" href="#firmasite-showcase" class="left carousel-control"><span class="icon-prev"></span></a>
                    <a data-slide="next" href="#firmasite-showcase" class="right carousel-control"><span class="icon-next"></span></a>
                               
				</div>
            </div>
			    
	<div id="main" class="site-main container">
        <div class="row">
            

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    
	<div id='side_1' class='widgets_on_page'>
    <ul><li id="bps_widget-3" class="widget widget_bps_widget">
<!-- BP Profile Search 4.2 171 members/bps-form-legacy widget -->
<div id='buddypress'><form action='http://www.promofoire.com/' method='POST' id='bps_widget171' class='standard-form'>
<div class='editfield field_61 field_type-dactivite '>
<div class='checkbox'>
<span class='label'>Type d&#039;activité</span>
<label><input  type='checkbox' name='field_61[]' value='Commerce en gros'>Commerce en gros</label>
<label><input  type='checkbox' name='field_61[]' value='Commerce en détails'>Commerce en détails</label>
<label><input  type='checkbox' name='field_61[]' value='Prestation de service'>Prestation de service</label>
<label><input  type='checkbox' name='field_61[]' value='Distribution'>Distribution</label>
<label><input  type='checkbox' name='field_61[]' value='Industrie'>Industrie</label>
<label><input  type='checkbox' name='field_61[]' value='Administration'>Administration</label>
</div>
</div>
<div class='editfield field_32 field_adresse alt'>
<label for='field_32'>zone</label>
<input style='width: 10%;' type='text' name='field_32_min' id='field_32' value=''>&nbsp;-&nbsp;<input style='width: 10%;' type='text' name='field_32_max' value=''>
</div>
<div class='submit'>
<input type='submit' value='Search'>
</div>
<input type='hidden' name='bp_profile_search' value='171'>
</form>
</div>

<!-- BP Profile Search 4.2 171 members/bps-form-legacy widget - end -->
</li></ul>
  </div><!-- widgets_on_page -->

<div id='side_2' class='widgets_on_page'>
    <ul></ul>
  </div><!-- widgets_on_page -->

&nbsp;    
	</div>
</div>

<div id="secondary" class="widget-area clearfix col-xs-12 col-md-4" role="complementary">
      
				<div class="hidden-xs carousel vertical slide carousel-mini open_sidebar" id="firmasite-promotionbar" data-rel="carousel">
                                        <div class="carousel-inner">
													  <div class="item active">
									<div class="row">
								        
                                        <div class="col-xs-12 col-md-12">
                                            <div class="real_item ">
	    <div class="well well-sm">
        <h4>
            <a href="http://www.promofoire.com/aenean-tellus-metus-bibendum/">
                A propos de Promofoire            </a>
        </h4>
        <p>QuelSoft est le guide des solutions informatiques professionnelles. Par solution, nous entendons tous les progiciels ou applications s&#8217;adressant aux professionnels, entreprises, associations, institutions, collectivités, etc. Il peut s&#8217;agir de solutions [&hellip;]</p>
     </div>
</div>

                                    
                                        </div>
								                
                                    </div>
								  </div>
																				  <div class="item ">
									<div class="row">
								        
                                        <div class="col-xs-12 col-md-12">
                                            <div class="real_item ">
	    <div class="well well-sm">
        <h4>
            <a href="http://www.promofoire.com/204/">
                ANNUAIRE ET CATALOGUE PRODUITS            </a>
        </h4>
        <p>Service web permettant à des fournisseurs (contributeurs) de décrire leur activité et de publier le détail de leurs prestations ou de leurs produits. Cette application est personnalisée à vos couleurs et paramétrée selon vos [&hellip;]</p>
     </div>
</div>

                                    
                                        </div>
								                
                                    </div>
								  </div>
																				  <div class="item ">
									<div class="row">
								        
                                        <div class="col-xs-12 col-md-12">
                                            <div class="real_item ">
	    <div class="well well-sm">
        <h4>
            <a href="http://www.promofoire.com/at-vero-eos-et/">
                Mentions légales            </a>
        </h4>
        <p>PROPRIETE Le présent site est la propriété de : ALPHAMOSA 14 bis rue Howard Houston 51170 Arcis-le-Ponsart Tél : 03 26 48 17 56 Fax : 03 26 48 10 87 Directeur de [&hellip;]</p>
     </div>
</div>

                                    
                                        </div>
								                
                                    </div>
								  </div>
																	</div>
													<a data-slide="prev" href="#firmasite-promotionbar" class="left carousel-control"><span class="icon-prev"></span></a>
							<a data-slide="next" href="#firmasite-promotionbar" class="right carousel-control"><span class="icon-next"></span></a>
										</div>
			    <article id="text-2" class="widget clearfix widget_text"><h4>recherche</h4>			<div class="textwidget"><form action="" method="post" class="searchandfilter">
<div>
<ul>
<li><input type="text" name="ofsearch" placeholder="Search &hellip;" value=""></li>
<li><select name='ofcategory' id='ofcategory' class='postform' ><option value='0' selected='selected'>All Categories</option><option class="level-0" value="1">Non classé</option><option class="level-0" value="319">test promo category</option></select><br />
<input type="hidden" name="ofcategory_operator" value="and" /></li>
<li><select name='ofpost_tag' id='ofpost_tag' class='postform' ><option value='0' selected='selected'>All Tags</option><option class="level-0" value="223">test mot cle</option><option class="level-0" value="293">test1</option><option class="level-0" value="320">yrdy 253</option></select><br />
<input type="hidden" name="ofpost_tag_operator" value="and" /></li>
<li><select name='ofType_de_produit' id='ofType_de_produit' class='postform' ><option value='0' selected='selected'>All Types de produit</option><option class="level-0" value="23">Maison &amp; Jardin</option></select><br />
<input type="hidden" name="ofType_de_produit_operator" value="and" /></li>
<li><select name='ofsecteur' id='ofsecteur' class='postform' ><option value='0' selected='selected'>All Secteurs</option><option class="level-0" value="39">Agriculture</option><option class="level-0" value="312">horticulture</option><option class="level-0" value="315">pêche</option><option class="level-0" value="313">sylviculture</option><option class="level-0" value="314">viticulture</option></select><br />
<input type="hidden" name="ofsecteur_operator" value="and" /></li>
<li><input type="hidden" name="ofsubmitted" value="1"><br />
							<input type="submit" value="Submit">
						</li>
</ul>
</div></form>
<form role="search" method="post" id="acps_form_705" class="acps_form" action="http://www.promofoire.com/" ><input type="hidden" name="acps_post_type" value="rtmedia_album" /><input type="hidden" name="acps_form_id" value="705" />
<div class="acps_form_container has_title title_inside" >
<h3 class="acps_form_title">test advanced search</h3>
<p><span class="acps_form_label">Keyword</span><span class="acps_form_control_wrap"><input class="acps_text_input" type="text" placeholder="" name="keywords"/></span></p>
<p class="acps_keyword_input"><span class="acps_form_label">test Types de produit</span><span class="acps_form_control_wrap Type_de_produit"><select name="Type_de_produit"><option value="">Select Types de produit...</option><option value="0">0</option><option value="accessoires-ceintures">Accessoires ceintures</option><option value="accessoires-dalimentation-de-courant">Accessoires d'alimentation de courant</option><option value="accessoires-decriture">Accessoires d'écriture</option><option value="accessoires-de-cablage">Accessoires de câblage</option><option value="accessoires-externes">Accessoires externes</option><option value="accessoires-interieurs">Accessoires intérieurs</option><option value="accessoires-moto">Accessoires moto</option><option value="adhesifs-et-ruban-adhesif-pour-bureau">Adhésifs et ruban adhésif pour bureau</option><option value="agriculture">Agriculture</option><option value="alimentation-de-courant">Alimentation de courant</option><option value="alimentation-et-boissons">Alimentation et boissons</option><option value="appareillage-et-approvisionnements-electriques">Appareillage et approvisionnements électriques</option><option value="approvisionnements-dongle">Approvisionnements d'ongle</option><option value="articles-pour-fetes-et-soirees">Articles pour fêtes et soirées</option><option value="artisanant-nature">Artisanant nature</option><option value="artisanat-artificiel">Artisanat artificiel</option><option value="artisanat-dimitation-antique">Artisanat d'imitation antique</option><option value="artisanat-de-broderie">Artisanat de broderie</option><option value="artisanat-de-sculpture">Artisanat de sculpture</option><option value="artisanat-des-pierres-semi-precieuses">Artisanat des pierres semi-précieuses</option><option value="artisanat-en-argile">Artisanat en argile</option><option value="artisanat-en-bambou">Artisanat en bambou</option><option value="artisanat-en-bois">Artisanat en bois</option><option value="artisanat-en-crystal">Artisanat en crystal</option><option value="artisanat-en-cuir">Artisanat en cuir</option><option value="artisanat-en-metal">Artisanat en métal</option><option value="artisanat-en-osier">Artisanat en osier</option><option value="artisanat-en-papier">Artisanat en papier</option><option value="artisanat-en-pierre">Artisanat en pierre</option><option value="artisanat-en-plastique">Artisanat en plastique</option><option value="artisanat-en-resine">Artisanat en résine</option><option value="artisanat-en-verre">Artisanat en verre</option><option value="atv">ATV</option><option value="audio-video-et-eclairage-professionnels">Audio, vidéo et éclairage professionnels</option><option value="automobile">Automobile</option><option value="automobiles-motos">Automobiles &amp; Motos</option><option value="autres-appareils-electriques">Autres appareils électriques</option><option value="autres-chaussures">Autres chaussures</option><option value="autres-crayons">Autres crayons</option><option value="autres-fournitures-bureau-scolaires">Autres fournitures bureau &amp; scolaires</option><option value="autres-pieces-dautomobile">Autres pièces d'automobile</option><option value="autres-portefeuille-divers">Autres portefeuille &amp; divers</option><option value="autres-produits-de-beaute-bien-etre">Autres produits de beauté &amp; bien-être</option><option value="batterie">Batterie</option><option value="beaute-bien-etre">Beauté &amp; bien-être</option><option value="bijoux-fantaisie">Bijoux fantaisie</option><option value="bn">bn</option><option value="body-art">Body Art</option><option value="bottes">Bottes</option><option value="bottes-chaussures-et-accessoires">Bottes</option><option value="broche-accessoires">Broche &amp; accessoires</option><option value="bt">bt</option><option value="bureau-fournitures-scolaires">Bureau &amp; fournitures scolaires</option><option value="cadeaux-artisanat">Cadeaux &amp; Artisanat</option><option value="cahiers-et-blocs-bureau">Cahiers et blocs bureau</option><option value="calculatrice">Calculatrice</option><option value="calendrier">Calendrier</option><option value="caoutchouc-et-plastiques">Caoutchouc et plastiques</option><option value="carte">Carte</option><option value="ceinture">Ceinture</option><option value="ceramique-electrique">Céramique électrique</option><option value="chaussures-a-lusage-special">Chaussures à l'usage spécial</option><option value="chaussures-denfants">Chaussures d'enfants</option><option value="chaussures-denfants-chaussures-et-accessoires">Chaussures d'enfants</option><option value="chaussures-doccasion">Chaussures d'occasion</option><option value="chaussures-doccasion-chaussures-et-accessoires">Chaussures d'occasion</option><option value="chaussures-de-bebe">Chaussures de bébé</option><option value="chaussures-de-bebe-chaussures-et-accessoires">Chaussures de bébé</option><option value="chaussures-de-danse">Chaussures de danse</option><option value="chaussures-de-sport">Chaussures de sport</option><option value="chaussures-de-ville">Chaussures de ville</option><option value="chaussures-en-cuir">Chaussures en cuir</option><option value="chaussures-et-accessoires">Chaussures et Accessoires</option><option value="chaussures-pour-dames">Chaussures pour dames</option><option value="chaussures-pour-hommes">Chaussures pour hommes</option><option value="chaussures-style-decontracte">Chaussures style décontracté</option><option value="chaussures-style-decontracte-chaussures-et-accessoires">Chaussures style décontracté</option><option value="chemises-de-livre">Chemises de livre</option><option value="chevalet">Chevalet</option><option value="colis-postaux">Colis postaux</option><option value="collier">Collier</option><option value="composants-accessoires-electroniques">Composants &amp; accessoires électroniques</option><option value="composants-de-fusibles">Composants de fusibles</option><option value="concessionnaire-de-chaussures">Concessionnaire de chaussures</option><option value="concessionnaire-de-produits-de-bureau-scolaires">Concessionnaire de produits de bureau &amp; scolaires</option><option value="concessionnaire-des-produits-electriques">Concessionnaire des produits électriques</option><option value="concessionnaires-daccessoires-de-mode">Concessionnaires d'accessoires de mode</option><option value="concessionnaires-des-pieces-auto-moto">Concessionnaires des pièces auto &amp; moto</option><option value="connecteurs-et-terminaux">Connecteurs et terminaux</option><option value="construction-immobilier">Construction &amp; Immobilier</option><option value="contacteurs">Contacteurs</option><option value="cosmetiques">Cosmétiques</option><option value="couvre-chef">Couvre-chef</option><option value="crayon">Crayon</option><option value="crayon-en-bois">Crayon en bois</option><option value="crayon-mecanique">Crayon mécanique</option><option value="crayon-standard">Crayon standard</option><option value="dfgd">dfgd</option><option value="disjoncteur">Disjoncteur</option><option value="distribution">Distribution</option><option value="dtdt">dtdt</option><option value="electromenager">Electroménager</option><option value="electronique-grand-public">Electronique grand public</option><option value="electroniques-automobile">Electroniques Automobile</option><option value="emballage-impression">Emballage &amp; Impression</option><option value="embrayage-automatique">Embrayage automatique</option><option value="enveloppe-papier">Enveloppe papier</option><option value="environnement">Environnement</option><option value="equipement-de-beaute">Equipement de beauté</option><option value="equipement-de-bureau">Équipement de bureau</option><option value="equipement-de-reparation-de-chaussures">Equipement de réparation de chaussures</option><option value="equipements-de-distribution-denergie">Equipements de distribution d'énergie</option><option value="equipements-de-salon-de-coiffure">Equipements de salon de coiffure</option><option value="equipements-de-service">Equipements de service</option><option value="equipements-vehicules">Equipements véhicules</option><option value="essieux">Essieux</option><option value="fils-cables-composants-de-cables">Fils, câbles &amp; composants de câbles</option><option value="founitures-spa">Founitures Spa</option><option value="fourniture-dimprimantes">Fourniture d'imprimantes</option><option value="fourniture-de-bureau-pour-reliure">Fourniture de bureau pour reliure</option><option value="fourniture-de-toilette">Fourniture de toilette</option><option value="fourniture-des-pieces-mecaniques-et-services-de-fabrication">Fourniture des pièces mécaniques et services de fabrication</option><option value="fournitures-dart">Fournitures d'art</option><option value="fournitures-de-bureau-pour-la-coupe">Fournitures de bureau pour la coupe</option><option value="fusible">Fusible</option><option value="gant-faisant-des-machines">Gant faisant des machines</option><option value="gants">Gants</option><option value="generateur">Générateur</option><option value="hygiene-bucco-dentaire">Hygiène bucco-dentaire</option><option value="hygiene-feminine">Hygiène féminine</option><option value="informatique-et-logiciels">Informatique et logiciels</option><option value="instruments-de-mesure-et-danalyse">Instruments de mesure et d'analyse</option><option value="inventaire-daccessoires-de-mode">Inventaire d'accessoires de mode</option><option value="inventaire-de-chaussures">Inventaire de chaussures</option><option value="inventaire-de-chaussures-chaussures-et-accessoires">Inventaire de chaussures</option><option value="inventaire-excessif">Inventaire excessif</option><option value="jouets">Jouets</option><option value="lenergie">L'énergie</option><option value="lassaad">lassaad</option><option value="lots-decharpe-bonnet-gants">Lots d'écharpe, bonnet &amp; gants</option><option value="lumiere-et-eclairage">Lumière et éclairage</option><option value="lunettes-de-soleil">Lunettes de soleil</option><option value="machines-de-cordonnerie">Machines de cordonnerie</option><option value="maison-jardin">Maison &amp; Jardin</option><option value="materiel-de-chaussures">Matériel de chaussures</option><option value="mecanique">Mécanique</option><option value="mecanisme-a-manivelle">Mécanisme à manivelle</option><option value="mens-care">Men's Care</option><option value="meubles">Meubles</option><option value="mineraux-et-metallurgie">Minéraux et métallurgie</option><option value="montres-bijoux-lunettes">Montres, bijoux, lunettes</option><option value="moteur-dauto">Moteur d'auto</option><option value="moto">Moto</option><option value="outils">Outils</option><option value="outils-de-maquillage">Outils de maquillage</option><option value="outils-de-soins-de-la-peau">Outils de soins de la peau</option><option value="pantoufles">Pantoufles</option><option value="pantoufles-chaussures-et-accessoires">Pantoufles</option><option value="papier-de-toilette">Papier de toilette</option><option value="parfums-deodorants">Parfums &amp; déodorants</option><option value="parties-accessoires-de-chaussures">Parties &amp; accessoires de chaussures</option><option value="perte-de-poids">Perte de poids</option><option value="produits-chimiques">Produits chimiques</option><option value="produits-pour-bebes">Produits pour bébés</option><option value="prolongements-et-perruques-de-cheveux">Prolongements et perruques de cheveux</option><option value="quincaillerie">Quincaillerie</option><option value="rasage-epilation">Rasage &amp; épilation</option><option value="rouleaux-pour-charpies">Rouleaux pour charpies</option><option value="sabots">Sabots</option><option value="sandales">Sandales</option><option value="sante-et-medecine">Santé et médecine</option><option value="sd">sd</option><option value="securite-protection">Sécurité &amp; protection</option><option value="services">Services</option><option value="services-de-design-chaussures">Services de design chaussures</option><option value="sports-et-loisirs">Sports et Loisirs</option><option value="telecommunication">Télécommunication</option><option value="test">test</option><option value="test-lassaad">test lassaad</option><option value="textiles-et-produits-en-cuir">Textiles et produits en cuir</option><option value="toutes-categories">Toutes Catégories</option><option value="transports">Transports</option><option value="tryt">tryt</option><option value="valises-sacs-caisses">Valises, sacs &amp; caisses</option><option value="vetements">Vêtements</option><option value="xt">xt</option></select></span></p>
<p><span class="acps_form_control_wrap submit_wrap"><input type="submit" class="acps_submit" value="Submit" /></span></p>
</div>
</form>
<p> [mctl]</p>
</div>
		</article><article id="categorizedtagcloudwidget-3" class="widget clearfix widget_categorizedtagcloudwidget"><h4>Categorized Tag Cloud</h4>
    <div id="categorized-tag-cloud"><span id="categorized-tag-cloud-el-1"><a href='http://www.promofoire.com/tag/test1/' class='tag-link-293' title='1 topic' style='font-size: 7pt;'>test1</a></span> <span id="categorized-tag-cloud-el-2"><a href='http://www.promofoire.com/tag/yrdy-253/' class='tag-link-320' title='1 topic' style='font-size: 7pt;'>yrdy 253</a></span> <span id="categorized-tag-cloud-el-3"><a href='http://www.promofoire.com/tag/test-mot-cle/' class='tag-link-223' title='1 topic' style='font-size: 7pt;'>test mot cle</a></span> </div>
  	<style>
  	
  		#categorized-tag-cloud a, #categorized-tag-cloud a:visited { text-decoration:none; }
      #categorized-tag-cloud a:hover { text-decoration:none; color:black; }
  		#categorized-tag-cloud-el-1 a, #categorized-tag-cloud-el-1 a:visited { color:#0d6; }
  		#categorized-tag-cloud-el-2 a, #categorized-tag-cloud-el-2 a:visited { color:#149; }
  		#categorized-tag-cloud-el-3 a, #categorized-tag-cloud-el-3 a:visited { color:#292; }
  	</style></article><article id="bps_widget-2" class="widget clearfix widget_bps_widget">
<!-- BP Profile Search 4.2 171 members/bps-form-legacy widget -->
<div id='buddypress'><form action='http://www.promofoire.com/at-vero-eos-et/' method='POST' id='bps_widget171' class='standard-form'>
<div class='editfield field_61 field_type-dactivite '>
<div class='checkbox'>
<span class='label'>Type d&#039;activité</span>
<label><input  type='checkbox' name='field_61[]' value='Commerce en gros'>Commerce en gros</label>
<label><input  type='checkbox' name='field_61[]' value='Commerce en détails'>Commerce en détails</label>
<label><input  type='checkbox' name='field_61[]' value='Prestation de service'>Prestation de service</label>
<label><input  type='checkbox' name='field_61[]' value='Distribution'>Distribution</label>
<label><input  type='checkbox' name='field_61[]' value='Industrie'>Industrie</label>
<label><input  type='checkbox' name='field_61[]' value='Administration'>Administration</label>
</div>
</div>
<div class='editfield field_32 field_adresse alt'>
<label for='field_32'>zone</label>
<input style='width: 10%;' type='text' name='field_32_min' id='field_32' value=''>&nbsp;-&nbsp;<input style='width: 10%;' type='text' name='field_32_max' value=''>
</div>
<div class='submit'>
<input type='submit' value='Search'>
</div>
<input type='hidden' name='bp_profile_search' value='171'>
</form>
</div>

<!-- BP Profile Search 4.2 171 members/bps-form-legacy widget - end -->
</article><article id="acps_widget-3" class="widget clearfix widget_acps_widget"><form role="search" method="post" id="acps_form_705" class="acps_form acps_widget" action="http://www.promofoire.com/" ><input type="hidden" name="acps_post_type" value="rtmedia_album" /><input type="hidden" name="acps_form_id" value="705" /><div class="acps_form_container has_title title_inside" ><h3 class="widget-title acps_title">test advanced search</h3><p><span class="acps_form_label">Keyword</span><span class="acps_form_control_wrap"><input class="acps_text_input" type="text" placeholder="" name="keywords"/></span></p><p><span class="acps_form_label">test Types de produit</span><span class="acps_form_control_wrap Type_de_produit"><select name="Type_de_produit"><option value="">Select Types de produit...</option><option value="0">0</option><option value="accessoires-ceintures">Accessoires ceintures</option><option value="accessoires-dalimentation-de-courant">Accessoires d'alimentation de courant</option><option value="accessoires-decriture">Accessoires d'écriture</option><option value="accessoires-de-cablage">Accessoires de câblage</option><option value="accessoires-externes">Accessoires externes</option><option value="accessoires-interieurs">Accessoires intérieurs</option><option value="accessoires-moto">Accessoires moto</option><option value="adhesifs-et-ruban-adhesif-pour-bureau">Adhésifs et ruban adhésif pour bureau</option><option value="agriculture">Agriculture</option><option value="alimentation-de-courant">Alimentation de courant</option><option value="alimentation-et-boissons">Alimentation et boissons</option><option value="appareillage-et-approvisionnements-electriques">Appareillage et approvisionnements électriques</option><option value="approvisionnements-dongle">Approvisionnements d'ongle</option><option value="articles-pour-fetes-et-soirees">Articles pour fêtes et soirées</option><option value="artisanant-nature">Artisanant nature</option><option value="artisanat-artificiel">Artisanat artificiel</option><option value="artisanat-dimitation-antique">Artisanat d'imitation antique</option><option value="artisanat-de-broderie">Artisanat de broderie</option><option value="artisanat-de-sculpture">Artisanat de sculpture</option><option value="artisanat-des-pierres-semi-precieuses">Artisanat des pierres semi-précieuses</option><option value="artisanat-en-argile">Artisanat en argile</option><option value="artisanat-en-bambou">Artisanat en bambou</option><option value="artisanat-en-bois">Artisanat en bois</option><option value="artisanat-en-crystal">Artisanat en crystal</option><option value="artisanat-en-cuir">Artisanat en cuir</option><option value="artisanat-en-metal">Artisanat en métal</option><option value="artisanat-en-osier">Artisanat en osier</option><option value="artisanat-en-papier">Artisanat en papier</option><option value="artisanat-en-pierre">Artisanat en pierre</option><option value="artisanat-en-plastique">Artisanat en plastique</option><option value="artisanat-en-resine">Artisanat en résine</option><option value="artisanat-en-verre">Artisanat en verre</option><option value="atv">ATV</option><option value="audio-video-et-eclairage-professionnels">Audio, vidéo et éclairage professionnels</option><option value="automobile">Automobile</option><option value="automobiles-motos">Automobiles &amp; Motos</option><option value="autres-appareils-electriques">Autres appareils électriques</option><option value="autres-chaussures">Autres chaussures</option><option value="autres-crayons">Autres crayons</option><option value="autres-fournitures-bureau-scolaires">Autres fournitures bureau &amp; scolaires</option><option value="autres-pieces-dautomobile">Autres pièces d'automobile</option><option value="autres-portefeuille-divers">Autres portefeuille &amp; divers</option><option value="autres-produits-de-beaute-bien-etre">Autres produits de beauté &amp; bien-être</option><option value="batterie">Batterie</option><option value="beaute-bien-etre">Beauté &amp; bien-être</option><option value="bijoux-fantaisie">Bijoux fantaisie</option><option value="bn">bn</option><option value="body-art">Body Art</option><option value="bottes">Bottes</option><option value="bottes-chaussures-et-accessoires">Bottes</option><option value="broche-accessoires">Broche &amp; accessoires</option><option value="bt">bt</option><option value="bureau-fournitures-scolaires">Bureau &amp; fournitures scolaires</option><option value="cadeaux-artisanat">Cadeaux &amp; Artisanat</option><option value="cahiers-et-blocs-bureau">Cahiers et blocs bureau</option><option value="calculatrice">Calculatrice</option><option value="calendrier">Calendrier</option><option value="caoutchouc-et-plastiques">Caoutchouc et plastiques</option><option value="carte">Carte</option><option value="ceinture">Ceinture</option><option value="ceramique-electrique">Céramique électrique</option><option value="chaussures-a-lusage-special">Chaussures à l'usage spécial</option><option value="chaussures-denfants">Chaussures d'enfants</option><option value="chaussures-denfants-chaussures-et-accessoires">Chaussures d'enfants</option><option value="chaussures-doccasion">Chaussures d'occasion</option><option value="chaussures-doccasion-chaussures-et-accessoires">Chaussures d'occasion</option><option value="chaussures-de-bebe">Chaussures de bébé</option><option value="chaussures-de-bebe-chaussures-et-accessoires">Chaussures de bébé</option><option value="chaussures-de-danse">Chaussures de danse</option><option value="chaussures-de-sport">Chaussures de sport</option><option value="chaussures-de-ville">Chaussures de ville</option><option value="chaussures-en-cuir">Chaussures en cuir</option><option value="chaussures-et-accessoires">Chaussures et Accessoires</option><option value="chaussures-pour-dames">Chaussures pour dames</option><option value="chaussures-pour-hommes">Chaussures pour hommes</option><option value="chaussures-style-decontracte">Chaussures style décontracté</option><option value="chaussures-style-decontracte-chaussures-et-accessoires">Chaussures style décontracté</option><option value="chemises-de-livre">Chemises de livre</option><option value="chevalet">Chevalet</option><option value="colis-postaux">Colis postaux</option><option value="collier">Collier</option><option value="composants-accessoires-electroniques">Composants &amp; accessoires électroniques</option><option value="composants-de-fusibles">Composants de fusibles</option><option value="concessionnaire-de-chaussures">Concessionnaire de chaussures</option><option value="concessionnaire-de-produits-de-bureau-scolaires">Concessionnaire de produits de bureau &amp; scolaires</option><option value="concessionnaire-des-produits-electriques">Concessionnaire des produits électriques</option><option value="concessionnaires-daccessoires-de-mode">Concessionnaires d'accessoires de mode</option><option value="concessionnaires-des-pieces-auto-moto">Concessionnaires des pièces auto &amp; moto</option><option value="connecteurs-et-terminaux">Connecteurs et terminaux</option><option value="construction-immobilier">Construction &amp; Immobilier</option><option value="contacteurs">Contacteurs</option><option value="cosmetiques">Cosmétiques</option><option value="couvre-chef">Couvre-chef</option><option value="crayon">Crayon</option><option value="crayon-en-bois">Crayon en bois</option><option value="crayon-mecanique">Crayon mécanique</option><option value="crayon-standard">Crayon standard</option><option value="dfgd">dfgd</option><option value="disjoncteur">Disjoncteur</option><option value="distribution">Distribution</option><option value="dtdt">dtdt</option><option value="electromenager">Electroménager</option><option value="electronique-grand-public">Electronique grand public</option><option value="electroniques-automobile">Electroniques Automobile</option><option value="emballage-impression">Emballage &amp; Impression</option><option value="embrayage-automatique">Embrayage automatique</option><option value="enveloppe-papier">Enveloppe papier</option><option value="environnement">Environnement</option><option value="equipement-de-beaute">Equipement de beauté</option><option value="equipement-de-bureau">Équipement de bureau</option><option value="equipement-de-reparation-de-chaussures">Equipement de réparation de chaussures</option><option value="equipements-de-distribution-denergie">Equipements de distribution d'énergie</option><option value="equipements-de-salon-de-coiffure">Equipements de salon de coiffure</option><option value="equipements-de-service">Equipements de service</option><option value="equipements-vehicules">Equipements véhicules</option><option value="essieux">Essieux</option><option value="fils-cables-composants-de-cables">Fils, câbles &amp; composants de câbles</option><option value="founitures-spa">Founitures Spa</option><option value="fourniture-dimprimantes">Fourniture d'imprimantes</option><option value="fourniture-de-bureau-pour-reliure">Fourniture de bureau pour reliure</option><option value="fourniture-de-toilette">Fourniture de toilette</option><option value="fourniture-des-pieces-mecaniques-et-services-de-fabrication">Fourniture des pièces mécaniques et services de fabrication</option><option value="fournitures-dart">Fournitures d'art</option><option value="fournitures-de-bureau-pour-la-coupe">Fournitures de bureau pour la coupe</option><option value="fusible">Fusible</option><option value="gant-faisant-des-machines">Gant faisant des machines</option><option value="gants">Gants</option><option value="generateur">Générateur</option><option value="hygiene-bucco-dentaire">Hygiène bucco-dentaire</option><option value="hygiene-feminine">Hygiène féminine</option><option value="informatique-et-logiciels">Informatique et logiciels</option><option value="instruments-de-mesure-et-danalyse">Instruments de mesure et d'analyse</option><option value="inventaire-daccessoires-de-mode">Inventaire d'accessoires de mode</option><option value="inventaire-de-chaussures">Inventaire de chaussures</option><option value="inventaire-de-chaussures-chaussures-et-accessoires">Inventaire de chaussures</option><option value="inventaire-excessif">Inventaire excessif</option><option value="jouets">Jouets</option><option value="lenergie">L'énergie</option><option value="lassaad">lassaad</option><option value="lots-decharpe-bonnet-gants">Lots d'écharpe, bonnet &amp; gants</option><option value="lumiere-et-eclairage">Lumière et éclairage</option><option value="lunettes-de-soleil">Lunettes de soleil</option><option value="machines-de-cordonnerie">Machines de cordonnerie</option><option value="maison-jardin">Maison &amp; Jardin</option><option value="materiel-de-chaussures">Matériel de chaussures</option><option value="mecanique">Mécanique</option><option value="mecanisme-a-manivelle">Mécanisme à manivelle</option><option value="mens-care">Men's Care</option><option value="meubles">Meubles</option><option value="mineraux-et-metallurgie">Minéraux et métallurgie</option><option value="montres-bijoux-lunettes">Montres, bijoux, lunettes</option><option value="moteur-dauto">Moteur d'auto</option><option value="moto">Moto</option><option value="outils">Outils</option><option value="outils-de-maquillage">Outils de maquillage</option><option value="outils-de-soins-de-la-peau">Outils de soins de la peau</option><option value="pantoufles">Pantoufles</option><option value="pantoufles-chaussures-et-accessoires">Pantoufles</option><option value="papier-de-toilette">Papier de toilette</option><option value="parfums-deodorants">Parfums &amp; déodorants</option><option value="parties-accessoires-de-chaussures">Parties &amp; accessoires de chaussures</option><option value="perte-de-poids">Perte de poids</option><option value="produits-chimiques">Produits chimiques</option><option value="produits-pour-bebes">Produits pour bébés</option><option value="prolongements-et-perruques-de-cheveux">Prolongements et perruques de cheveux</option><option value="quincaillerie">Quincaillerie</option><option value="rasage-epilation">Rasage &amp; épilation</option><option value="rouleaux-pour-charpies">Rouleaux pour charpies</option><option value="sabots">Sabots</option><option value="sandales">Sandales</option><option value="sante-et-medecine">Santé et médecine</option><option value="sd">sd</option><option value="securite-protection">Sécurité &amp; protection</option><option value="services">Services</option><option value="services-de-design-chaussures">Services de design chaussures</option><option value="sports-et-loisirs">Sports et Loisirs</option><option value="telecommunication">Télécommunication</option><option value="test">test</option><option value="test-lassaad">test lassaad</option><option value="textiles-et-produits-en-cuir">Textiles et produits en cuir</option><option value="toutes-categories">Toutes Catégories</option><option value="transports">Transports</option><option value="tryt">tryt</option><option value="valises-sacs-caisses">Valises, sacs &amp; caisses</option><option value="vetements">Vêtements</option><option value="xt">xt</option></select></span></p><p><span class="acps_form_control_wrap submit_wrap"><input type="submit" class="acps_submit" value="Submit" /></span></p></div></form></article>    </div><!-- #secondary .widget-area -->
		</div><!--  .row -->
            
	</div><!-- #main .site-main -->

	<footer id="footer" class="site-footer clearfix" role="contentinfo">
    <div class="site-info container">
                    <div class="row">
                                       </div>
               

                                   <small class="text-muted designer">Theme: <a href="//firmasite.com/" class="text-muted" rel="designer">FirmaSite</a></small>
             </div><!-- .site-info -->
</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

		<div id="visible-lg" class="visible-lg"></div>
		<div id="visible-md" class="visible-md"></div>
		<div id="visible-sm" class="visible-sm"></div>
		<div id="visible-xs" class="visible-xs"></div>
		
<!-- Generated in 27.742 seconds. (121 q) -->

		<div id="settings_panel" class="pull-left hidden-xs hidden-sm">  
	 <div class="pull-left" data-container="body" data-toggle="tooltip" data-rel="tooltip" data-placement="top" data-title="You can change some options and watch the theme's magic :)">	
		<button id="settings_panel_icon" type="button" class="btn btn-sm btn-danger pull-left"><i class="icon-cog"></i></button>
	 </div>
	 <div id="settings_panel_content" class="pull-left" style="display: none;"><div class="panel firmasite-modal-static"><div class="panel-footer">
				            <p class="form-control-static pull-left" data-toggle="popover" data-html="true" data-trigger="hover" data-rel="popover" data-placement="bottom" data-content="Only <strong>latin-ext</strong> supported fonts are displaying. You can modify font filter for supporting your language!">	
                Font: <i class="icon-globe"></i>&nbsp;
             </p>
           <form method="get" class="form-inline">
          <div id="firmasite-font" class="bfh-selectbox bfh-googlefonts" data-subsets="latin-ext" data-family="Ubuntu Condensed">
            <input type="hidden" name="font" value="Ubuntu Condensed">
            <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">
            <span class="bfh-selectbox-option" data-option=""></span>
            <b class="caret"></b>
            </a>
            <div class="bfh-selectbox-options">
              <input type="text" class="bfh-selectbox-filter form-control">
              <ul role="option">
              </ul>
            </div>
          </div>
          </form>
          <script>
		  jQuery("input[name=font]").change(function() {
			  jQuery(this).closest("form").submit();
		  });
              </script>
<div class="btn-group">
<button class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown">Layout <span class="caret"></span></button>
<ul class="dropdown-menu">
			 <li class="">
         	<a href="?layout=content-sidebar" rel="nofollow" >
				Content - Sidebar             </a>
         </li>
			 <li class="active">
         	<a href="?layout=sidebar-content" rel="nofollow" >
				<i class="icon-ok"></i> Sidebar - Content             </a>
         </li>
			 <li class="">
         	<a href="?layout=only-content" rel="nofollow" >
				Only Content. No sidebar (Short)             </a>
         </li>
			 <li class="">
         	<a href="?layout=only-content-long" rel="nofollow" >
				Only Content. No sidebar (Long)             </a>
         </li>
	</ul>
</div>
<div class="btn-group">
<button class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown">Loop Style <span class="caret"></span></button>
<ul class="dropdown-menu">
			 <li class="">
         	<a href="?loop-style=loop-list" rel="nofollow" >
				Ordered List             </a>
         </li>
			 <li class="">
         	<a href="?loop-style=loop-excerpt" rel="nofollow" >
				Ordered List (Excerpt)             </a>
         </li>
			 <li class="active">
         	<a href="?loop-style=loop-tile" rel="nofollow" >
				<i class="icon-ok"></i> Vertical List (Excerpt)             </a>
         </li>
	</ul>
</div>
<div class="btn-group">
<button class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown">Theme Style <span class="caret"></span></button>
<ul class="dropdown-menu">
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/promofoire/assets/themes/pink/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=pink" rel="nofollow" >
				Pink             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/amelia/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=amelia" rel="nofollow" >
				Amelia             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/cerulean/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=cerulean" rel="nofollow" >
				Cerulean             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/cosmo/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=cosmo" rel="nofollow" >
				Cosmo             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/cyborg/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=cyborg" rel="nofollow" >
				Cyborg             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/darkly/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=darkly" rel="nofollow" >
				Darkly             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/flatly/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=flatly" rel="nofollow" >
				Flatly             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/journal/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=journal" rel="nofollow" >
				Journal             </a>
         </li>
			 <li class="active" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/lumen/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=lumen" rel="nofollow" >
				<i class="icon-ok"></i> Lumen             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/paper/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=paper" rel="nofollow" >
				Paper             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/readable/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=readable" rel="nofollow" >
				Readable             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/sandstone/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=sandstone" rel="nofollow" >
				Sandstone             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/simplex/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=simplex" rel="nofollow" >
				Simplex             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/slate/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=slate" rel="nofollow" >
				Slate             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/spacelab/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=spacelab" rel="nofollow" >
				Spacelab             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/superhero/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=superhero" rel="nofollow" >
				Superhero             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/united/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=united" rel="nofollow" >
				United             </a>
         </li>
			 <li class="" data-rel="popover" data-placement="right" data-container="body" data-trigger="hover" data-html="true" data-content="<img src='http://www.promofoire.com/wp-content/themes/firma-promo/assets/themes/yeti/thumbnail.png' alt='' style='width:180px; height:auto;' />">
         	<a href="?style=yeti" rel="nofollow" >
				Yeti             </a>
         </li>
	</ul>
</div>
	 </div></div></div>
	</div>
	<script>
		jQuery('#settings_panel_icon').click(function() {
			jQuery("#settings_panel_content").slideToggle();
		});
	</script>
	<script type="text/javascript">
(function($){
  $("img.lazy").show().lazyload({effect: "fadeIn"});
})(jQuery);
</script>
<!-- site performed by: WP Remove Css-Js see: http://github.com/GiuB/WP-Remove-Css-Js | Author: http://giub.it -->  <script type="text/javascript">	
	   
	/* jQuery Easing Plugin, v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/ */
	jQuery.easing.jswing=jQuery.easing.swing;jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,f,a,h,g){return jQuery.easing[jQuery.easing.def](e,f,a,h,g)},easeInQuad:function(e,f,a,h,g){return h*(f/=g)*f+a},easeOutQuad:function(e,f,a,h,g){return -h*(f/=g)*(f-2)+a},easeInOutQuad:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f+a}return -h/2*((--f)*(f-2)-1)+a},easeInCubic:function(e,f,a,h,g){return h*(f/=g)*f*f+a},easeOutCubic:function(e,f,a,h,g){return h*((f=f/g-1)*f*f+1)+a},easeInOutCubic:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f+a}return h/2*((f-=2)*f*f+2)+a},easeInQuart:function(e,f,a,h,g){return h*(f/=g)*f*f*f+a},easeOutQuart:function(e,f,a,h,g){return -h*((f=f/g-1)*f*f*f-1)+a},easeInOutQuart:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f+a}return -h/2*((f-=2)*f*f*f-2)+a},easeInQuint:function(e,f,a,h,g){return h*(f/=g)*f*f*f*f+a},easeOutQuint:function(e,f,a,h,g){return h*((f=f/g-1)*f*f*f*f+1)+a},easeInOutQuint:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f*f+a}return h/2*((f-=2)*f*f*f*f+2)+a},easeInSine:function(e,f,a,h,g){return -h*Math.cos(f/g*(Math.PI/2))+h+a},easeOutSine:function(e,f,a,h,g){return h*Math.sin(f/g*(Math.PI/2))+a},easeInOutSine:function(e,f,a,h,g){return -h/2*(Math.cos(Math.PI*f/g)-1)+a},easeInExpo:function(e,f,a,h,g){return(f==0)?a:h*Math.pow(2,10*(f/g-1))+a},easeOutExpo:function(e,f,a,h,g){return(f==g)?a+h:h*(-Math.pow(2,-10*f/g)+1)+a},easeInOutExpo:function(e,f,a,h,g){if(f==0){return a}if(f==g){return a+h}if((f/=g/2)<1){return h/2*Math.pow(2,10*(f-1))+a}return h/2*(-Math.pow(2,-10*--f)+2)+a},easeInCirc:function(e,f,a,h,g){return -h*(Math.sqrt(1-(f/=g)*f)-1)+a},easeOutCirc:function(e,f,a,h,g){return h*Math.sqrt(1-(f=f/g-1)*f)+a},easeInOutCirc:function(e,f,a,h,g){if((f/=g/2)<1){return -h/2*(Math.sqrt(1-f*f)-1)+a}return h/2*(Math.sqrt(1-(f-=2)*f)+1)+a},easeInElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return -(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e},easeOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return g*Math.pow(2,-10*h)*Math.sin((h*k-i)*(2*Math.PI)/j)+l+e},easeInOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k/2)==2){return e+l}if(!j){j=k*(0.3*1.5)}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}if(h<1){return -0.5*(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e}return g*Math.pow(2,-10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j)*0.5+l+e},easeInBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*(f/=h)*f*((g+1)*f-g)+a},easeOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*((f=f/h-1)*f*((g+1)*f+g)+1)+a},easeInOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}if((f/=h/2)<1){return i/2*(f*f*(((g*=(1.525))+1)*f-g))+a}return i/2*((f-=2)*f*(((g*=(1.525))+1)*f+g)+2)+a},easeInBounce:function(e,f,a,h,g){return h-jQuery.easing.easeOutBounce(e,g-f,0,h,g)+a},easeOutBounce:function(e,f,a,h,g){if((f/=g)<(1/2.75)){return h*(7.5625*f*f)+a}else{if(f<(2/2.75)){return h*(7.5625*(f-=(1.5/2.75))*f+0.75)+a}else{if(f<(2.5/2.75)){return h*(7.5625*(f-=(2.25/2.75))*f+0.9375)+a}else{return h*(7.5625*(f-=(2.625/2.75))*f+0.984375)+a}}}},easeInOutBounce:function(e,f,a,h,g){if(f<g/2){return jQuery.easing.easeInBounce(e,f*2,0,h,g)*0.5+a}return jQuery.easing.easeOutBounce(e,f*2-g,0,h,g)*0.5+h*0.5+a}});
	  </script>
  	  <script type="text/javascript">	
		(function ($) {
				$(document).ready(function() {
		firmasite_social_edits();	

		$(".makeit-pag-small").find("ul.pagination-lg").addClass("pagination-sm").removeClass("pagination-lg");
       
	});
	$(document).on("DOMNodeInserted", throttle(function(){
          firmasite_social_edits();
    },250));
	
	function firmasite_social_edits(){
		$("img.avatar").parent("a").addClass("thumbnail pull-left");
		/* pull-left fixes */
		$("#wpadminbar").find("a").removeClass("thumbnail pull-left");
        
		$("#message").addClass("alert alert-info");
        
		$("div[role=search]").addClass("pull-right"); // search boxes
		$("#main [role=navigation]").find("span").addClass("badge");
		
		$("div#item-actions").find("ul").addClass("list-unstyled"); 		
		$(".pagination-links").addClass("pager lead").children("span").wrap('<li class="disabled" />').parent().parent().children("a").wrap("<li>");
	}
		})(jQuery);	
	  </script>

<script type='text/javascript'>
/* <![CDATA[ */
var template_url = "http:\/\/www.promofoire.com\/wp-admin\/admin-ajax.php?action=rtmedia_get_template&template=media-gallery-item";
var rtMedia_plupload_config = {"url":"\/upload\/","runtimes":"html5,flash,html4","browse_button":"rtMedia-upload-button","container":"rtmedia-upload-container","drop_element":"drag-drop-area","filters":[{"title":"Media Files","extensions":"jpg,jpeg,png,gif"}],"max_file_size":"128M","multipart":"1","urlstream_upload":"1","flash_swf_url":"http:\/\/www.promofoire.com\/wp-includes\/js\/plupload\/plupload.flash.swf","silverlight_xap_url":"http:\/\/www.promofoire.com\/wp-includes\/js\/plupload\/plupload.silverlight.xap","file_data_name":"rtmedia_file","multi_selection":"1","multipart_params":{"redirect":"no","action":"wp_handle_upload","_wp_http_referer":"\/","mode":"file_upload","rtmedia_upload_nonce":"1585eddc88"},"max_file_size_msg":"128M"};
var rMedia_loading_file = "http:\/\/www.promofoire.com\/wp-admin\/images\/loading.gif";
var rMedia_loading_media = "http:\/\/www.promofoire.com\/wp-content\/plugins\/buddypress-media4566\/app\/assets\/img\/boxspinner.gif";
var rtmedia_load_more_or_pagination = "load_more";
var rtmedia_bp_enable_activity = "1";
var rtmedia_upload_progress_error_message = "There are some uploads in progress. Do you want to cancel them?";
/* ]]> */
</script>

  <script type="text/javascript">	
  (function ($) {
	  	var $carousels = $('[data-rel=carousel]');
	function firmasite_edits(){
        $inputs = $("input");
        $inputs.filter("[type='submit'], [type='button']").addClass("btn btn-default");
        $inputs.filter("[type='text'], :not([type]), [type='password'], [type='search'], [type='email']").addClass("form-control"); //not([type]) included as browser defaults to text when attribute not present
		$("textarea").addClass("form-control");
		$("select").addClass("form-control");
		$("table").addClass("table");
    	$("dl").addClass("dl-horizontal");

		$("li.selected").addClass("active");//current
		$("li.current").addClass("active");//current
        $("ul.page-numbers").addClass("pagination pagination-lg");
        $(".pager").find("ul.pagination").removeClass("pagination pagination-lg");
		$('[data-toggle=tooltip]').tooltip();
		$('[data-toggle=popover]').popover();
	}
	$(document).ready(function() {
		firmasite_edits();
		$('.widget').find("ul").addClass("list-unstyled");
		$carousels.carousel({interval: 6000});
	});
	$(document).on("DOMNodeInserted", throttle(function(){
    	firmasite_edits();
    }, 250));
    $('[data-toggle=dropdown]').on('click.bs.dropdown', function () {
		        if (visible_xs() || visible_sm()) { 
            var menu = $(this).parent().find("ul:first");
            var menupos = $(this).offset();

            if($(this).parent().hasClass("pull-right")){
                menupos_right = $(window).width() - (menupos.left + $(this).outerWidth());
                if (menupos_right + menu.width() > $(window).width()) {
                    var newpos = -(menupos_right + menu.width() - $(window).width());
                    menu.css({ right: newpos });    
                }
            } else {
                 if (menupos.left + menu.width() > $(window).width()) {
                    var newpos = -(menupos.left + menu.width() - $(window).width());
                    menu.css({ left: newpos });    
                }
            }
		}	
    });   
    //Stack menu when collapsed
    $('.simple-menu-collapse').on('show.bs.collapse', function() {
        $('.nav-pills').addClass('nav-stacked');
    });
    
    //Unstack menu when not collapsed
    $('.simple-menu-collapse').on('hide.bs.collapse', function() {
        $('.nav-pills').removeClass('nav-stacked');
    });     
	

    })(jQuery);
  </script>
  			<script type="text/javascript">
				(function ($) {
							function firmasite_hover_nav() {
			var $hover_nav_style = "<style id='hover-nav' type='text/css'> ul.nav li.dropdown:hover > .dropdown-menu{ display: block; } .nav-tabs .dropdown-menu, .nav-pills .dropdown-menu, .navbar .dropdown-menu { margin-top: 0; margin-bottom: 0; } </style>";
			var $hover_style_inserted = $("style#hover-nav");
			var $bootstrap_css = $("link#bootstrap-css");
            if (visible_md() || visible_lg()){
				if(!$hover_style_inserted.length) {
                	if($bootstrap_css.length) {
                    	$bootstrap_css.after($hover_nav_style);
                    } else {
                    	$("head").append($hover_nav_style);
                    }
                    $('a.dropdown-toggle').each(function(){
                        var data_toggle = $(this).attr('data-toggle');
                        $(this).attr('data-toggle-removed',data_toggle).removeAttr('data-toggle');
                    });
                }						
			} else {
				$hover_style_inserted.remove();
				$('[data-toggle-removed]').each(function(){
					var data_toggle_removed = $(this).attr('data-toggle-removed');
					$(this).attr('data-toggle',data_toggle_removed).removeAttr('data-toggle-removed');
				});						
			}
		}
		$(window).resize(throttle(function(){
        	firmasite_hover_nav();
		},250));
				})(jQuery);
			</script>
			<script type="text/javascript">
		( function ( $ ) {
			var $showcases = $('.firmasite-showcase');
				var $showcases = $('.firmasite-showcase');
	function firmasite_showcase_resize(){
        $showcases.each(function(){
            var $showcase_items = $(this).find('.item');
            var $showcase_items_inside = $showcase_items.find(".firmasite-showcase-content");
            $showcase_items.css({position: 'absolute', visibility:'hidden', display:'block'});
            $showcase_items_inside.css({height: ''});
            $(this).parent().css({height: ''});
            $item_heights = $showcase_items.map(function (){
                return $(this).height();
            }).get();
            $item_maxHeight = Math.max.apply(null, $item_heights);
                
            $showcase_items.css({position: '', visibility:'', display:''})
            $showcase_items_inside.css('height', $item_maxHeight + 30);
            $(this).parent().css('height', $item_maxHeight + 30);
        });
		BackgroundCheck.refresh();
	}
	$(window).resize(throttle(function(){
		firmasite_showcase_resize();          
	},250));

	$(document).ready(function() {
	BackgroundCheck.refresh();
    	var $showcase_item = $showcases.find('.item'),
		$showcasesThumbnail = $showcase_item.find('.hero-thumbnail'),			
		$showcasesCat = $showcase_item.find('.hero-cat'), 
		$showcasesTitle = $showcase_item.find('.hero-title'),
		$showcasesContent = $showcase_item.find('.hero-content'),
		$showcasesLinks = $showcase_item.find('.hero-link');
	
		anim = {'margin-left':0, opacity:1};	
		anim1 = {'margin-left':0, opacity:1};
		anim_out = {'margin-left':100, opacity:0};		
		anim1_out = {'margin-left':0, opacity:0};
		anim2 = {'margin-left':0, opacity:1};
		anim2_out = {'margin-left':-100, opacity:0};
			
		$showcases.on('slid.bs.carousel', function () {
			var $item = $showcases.find('.item.active');
           $item.find('.hero-thumbnail').clearQueue().delay(0).animate(anim1, { duration: 600, easing: 'easeOutQuint' });         
			$item.find('.hero-title').clearQueue().delay(200).animate(anim2, { duration: 600, easing: 'easeOutQuint' });
					
			$item.find('.hero-content').clearQueue().delay(400).animate(anim, { duration: 600, easing: 'easeOutQuint' });
			$item.find('.hero-link').clearQueue().delay(600).animate(anim2, { duration: 600, easing: 'easeOutQuint' });		
			$item.find('.hero-cat').clearQueue().delay(600).animate(anim, { duration: 600, easing: 'easeOutQuint',complete: function(){
        /* hook promofoire pour BackgroundCheck */
BackgroundCheck.refresh();
		 BackgroundCheck.init({
      targets: '.firmasite-showcase-content',
      images: '.hero-background',
	
    });
	/* fin hook promofoire pour BackgroundCheck */;
    } });
			

		}).on('slide.bs.carousel', function () {
			// Reset styles
			$showcasesTitle.css(anim2_out).clearQueue();		
			$showcasesThumbnail.css(anim1_out).clearQueue();		
			$showcasesCat.css(anim_out).clearQueue();			
			$showcasesContent.css(anim_out).clearQueue();
			$showcasesLinks.css(anim2_out).clearQueue();
		
		}).hover(
			function(){ $showcases.carousel("pause"); },
			function(){ $showcases.carousel("cycle");  }
		);
	});

		
			
		} ( jQuery ) );
	</script>
  <script type="text/javascript">	
  (function ($) {
	$(window).load().trigger("resize");
  })(jQuery);
  </script>
  <div id="sitewide-notice" class="admin-bar-off"></div>127 queries in 28.000 seconds.
<?php get_footer(); ?>
</body>
</html>
<!-- Performance optimized by W3 Total Cache. Learn more: http://www.w3-edge.com/wordpress-plugins/

 Served from: www.promofoire.com @ 2015-05-21 09:49:55 by W3 Total Cache -->