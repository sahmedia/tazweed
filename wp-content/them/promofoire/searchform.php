searchform.php
<?php
/** searchform.php
 *
 * The template for displaying search forms
 *
 * @author		Konstantin Obenland
 * @package		The Bootstrap
 * @since		1.0.0 - 07.02.2012
 */

?>
<?php
echo do_shortcode('[searchandfilter fields="search,post_types" post_types="all" headings=",Post Types"]');
 ?>  <div class="sticky-footer-wrapper">
      <div class="masthead">
        
        <div class="Demo">
          <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
            <input type="hidden" name="mode" value="users">
            <div class="Typeahead Typeahead--twitterUsers">
              <div class="u-posRelative">
                <input class="Typeahead-hint" type="text" tabindex="-1" readonly>
                <input class="Typeahead-input" id="demo-input" type="text" name="s" placeholder="Search Twitter users..." value="<?php echo esc_attr( get_search_query() ); ?>"  x-webkit-speech>
                <img class="Typeahead-spinner" src="<?php echo get_stylesheet_directory_uri(); ?>/_inc/img/spinner.gif">
              </div>
              <div class="Typeahead-menu"></div>
            </div>
            <button class="u-hidden" type="submit">blah</button>
          </form>
        </div>
      

   
      </div>
      <div class="sticky-footer-push"></div>
    </div>

    <script id="result-template" type="text/x-handlebars-template">
      <div class="ProfileCard u-cf">
        <img class="ProfileCard-avatar" src="{{img}}">

        <div class="ProfileCard-details">
          <div class="ProfileCard-realName">{{name}}</div></br>
          <div class="ProfileCard-screenName">{{conference}}</div></br>
          <div class="ProfileCard-description">{{division}}</div>
        </div>

        <div class="ProfileCard-stats">
          <div class="ProfileCard-stat"><span class="ProfileCard-stat-label">Tweets:</span> {{statuses_count}}</div>
          <div class="ProfileCard-stat"><span class="ProfileCard-stat-label">Following:</span> {{friends_count}}</div>
          <div class="ProfileCard-stat"><span class="ProfileCard-stat-label">Followers:</span> {{followers_count}}</div>
        </div>
      </div>
    </script>

    <script id="empty-template" type="text/x-handlebars-template">
      <div class="EmptyMessage">Your search turned up 0 results. This most likely means the backend is down, yikes!</div>
    </script>

	
   



 
          
   

<?php


/* End of file searchform.php */
/* Location: ./wp-content/themes/the-bootstrap/searchform.php */