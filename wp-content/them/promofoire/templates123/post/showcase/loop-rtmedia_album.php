<?php
global $wp_query;
global $rtmedia_query;
$model      = new RTMediaModel();
$album_post = $post->ID;
global $firmasite_showcase, $post, $firmasite_settings, $bp;
$results   = $model->get(array(
    'media_type' => 'album',
    'media_id' => $album_post
));
$mail      = xprofile_get_field_data('42', $post->post_author);
$telephone = xprofile_get_field_data('39', $post->post_author);
?>  
<div class="firmasite-showcase-content8 jumbotron8 clearfix">                                       
    
    
                    
            

    
    <link href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css" rel="stylesheet" media="screen">  

            <div class="row coralbg white ">
                <div class="col-md-6 no-pad">
                    <div class="user-pad">
                    <a  class="thumbnail hero-thumbnail pull-left" href="<?php
echo bp_core_get_userlink($post->post_author, false, true);
?>">
                     <?php
echo get_avatar(get_the_author_meta('user_email'), '50');
?>
                   </a> 
                    <h3 class="hero-title"><?php
printf(str_replace('<a href=', '<a class="" rel="author" href=', bp_core_get_userlink($post->post_author)));
?></h2>
                            
                <!-- <h2 class="hero-title"><?php
/* the_title_attribute(); */
?></h2> -->
             <div class="hero-content hidden-xs">
                <?php
ob_start();
if (isset($firmasite_settings["showcase-calltoaction"]) && !empty($firmasite_settings["showcase-calltoaction"])) {
    echo $firmasite_settings["showcase-calltoaction"];
} else {
    _e('Continue reading <span class="meta-nav">&rarr;</span>', 'firmasite');
}
$more_button = ob_get_contents();
ob_end_clean();
if (!preg_match('/<!--more(.*?)?-->/', $post->post_content)) {
    the_excerpt();
} else {
    the_content('');
}
?>
            </div>    
                        
                        <h6 class="white"><i class="fa  fa-envelope-o"> </i> <?php
echo $mail;
?></h4>
                        <h6 class="white"><i class="fa fa-phone-square"> </i> <?php
echo $telephone;
?></h4> 
                 
                     </div>
                </div>
 <div class="col-md-6 no-pad">
                    <div class="user-image ">
                    <?php
if (xprofile_get_field_data('108', $post->post_author) != "") {
$cover = "";
		$large_image_url = "";
		$replace = array("<p>", "<img src=\"", "\" alt=\"\" />", "</p>");
		
		$cover1 = xprofile_get_field_data( '108',$post->post_author);
		$large_image_url =  str_replace($replace, "", $cover1);
		 
		if ($large_image_url > (get_option( 'large_size_w' ) / 1.3 )) { 
			$cover = "background-size:cover;";
		}  
?>  

    <?php
    $membername = xprofile_get_field_data('133', $post->post_author);
    //$image_url=get_user_meta($post->post_author);
    // $large_image_url = wp_get_attachment_image_src( $image_url, 'large');
?>

    <?php
  /* ******* */
 // echo  $large_image_url ;
   /* ***********    */
?>
                     <img src="<?php
    echo $large_image_url;
?>" class="img-responsive thumbnail">
                <?php
} else {
?>            <img src="https://farm7.staticflickr.com/6163/6195546981_200e87ddaf_b.jpg" class="img-responsive thumbnail">
                 <?php
}
?>    
                    </div>
                </div>
            </div>
            
        </div>
       