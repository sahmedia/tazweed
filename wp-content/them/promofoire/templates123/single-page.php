single-page.php
<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package firmasite
 */
//global $firmasite_settings;

?>


 <?php	// Passing an object
    // Why another var?? $output = 'objects'; // name / objects
    $taxonomies = get_object_taxonomies( 'rtmedia_album');


 $taxo_names = array();
// For each taxonomy get a select input with al terms
foreach ( $taxonomies as $taxonomy ) {
$tax = get_taxonomy( $taxonomy );
$name = $tax->labels->name;
$terms = get_terms( $taxonomy,'orderby=count&hide_empty=0&hierarchical=1');

 // echo '<pre>';print_r ($terms);echo '</pre>';
 
}
	
	?>
	
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 <div class="panel panel-default">
   <div class="panel-body">
    <header class="entry-header">
        <h1 class="page-header page-title">
            <strong><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'firmasite' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a></strong>
			<?php if (!empty($post->post_excerpt)){ ?>
                <small><?php the_excerpt(); ?></small>
            <?php } ?>
        </h1>
    </header>
    <div class="entry-content">
 		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'firmasite' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><ul class="pagination pagination-lg"><li><span>' . __( 'Pages:', 'firmasite' ) . '</span></li>', 'after' => '</ul></div>','link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
        <?php if (empty($post->post_title)){ ?>
        <a class="pull-right" href="<?php the_permalink(); ?>" rel="bookmark">
			<small><i class="icon-bookmark"></i><?php  _e( 'Permalink', 'firmasite' ); ?></small>
        </a>
        <?php }$tags = get_terms('Tag','orderby=count&hide_empty=0&hierarchical=0');

      foreach($tags as $tag){
        array_push($tag_names, $tag->name);
		 print_r ( $tag->name);
      }
	  
	  
	 ?>


    </div>
   </div>
 </div>
</article>

<!-- #post-<?php the_ID(); ?> -->