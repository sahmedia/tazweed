<?php
global $firmasite_showcase, $post, $firmasite_settings , $bp;

	if ( has_post_thumbnail()){ 
		$cover = "";
		$large_image_url = "";
		$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large');
		if ($large_image_url[1] > (get_option( 'large_size_w' ) / 1.3 )) { 
			$cover = "background-size:cover;";
		}  
	?>  	
	<?php  
	// *******
	// print_r ($large_image_url) ;
	// ***********
	?>
     <div class="firmasite-showcase-content jumbotron hero-background clearfix" style="background-image:url(<?php echo $large_image_url[0]; ?>); background-repeat:no-repeat; background-position:center right 15%;background-size:cover; <?php  //echo $cover;?>">
	<?php }elseif (xprofile_get_field_data( '108',$post->post_author)!=""){ 
		$cover = "";
		$large_image_url1 = "";
		$replace = array("<p>", "<img src=\"", "\" alt=\"\" />", "</p>");
		
		$cover1 = xprofile_get_field_data( '108',$post->post_author);
		$large_image_url1 =  str_replace($replace, "", $cover1);
		 
		if ($large_image_url1 > (get_option( 'large_size_w' ) / 1.3 )) { 
			$cover = "background-size:cover;";
		}  ?> 
		<div class="firmasite-showcase-content jumbotron hero-background clearfix" style="background-image:url(<?php echo $large_image_url1; ?>); background-repeat:no-repeat; background-position:center right 15%; background-size:cover;">
		<?php }else {  ?>  
     <div class="firmasite-showcase-content jumbotron clearfix">                                       
	<?php }  ?>  
        <div class="caption">
<?php printf( str_replace( '<a href=', '<a rel="author" href=', bp_core_get_userlink( $post->post_author ) ) ); ?></p>
                  		<a class="thumbnail hero-thumbnail pull-left" href="<?php bp_core_get_userlink( $post->post_author ) ; ?>">
                     <?php 	echo get_avatar( get_the_author_meta( 'user_email' ), '100' );?>
                    </a><br><br>
            <h2 class="hero-title"><?php the_title_attribute(); ?></h2>
             <div class="hero-content hidden-xs">
	            <?php ob_start(); 
                if (isset($firmasite_settings["showcase-calltoaction"]) && !empty($firmasite_settings["showcase-calltoaction"])) {
                    echo $firmasite_settings["showcase-calltoaction"];
                } else {
                    _e( 'Continue reading <span class="meta-nav">&rarr;</span>', 'firmasite' ); 
                }
                $more_button = ob_get_contents();
                ob_end_clean(); 
				
                if ( !preg_match('/<!--more(.*?)?-->/', $post->post_content) ){
                    the_excerpt(); 
				} else {
                    the_content( '' );
                } ?>
             </div>
             <a class="hero-link btn btn-primary" href="<?php the_permalink(); ?>">
                <?php echo $more_button; ?>
             </a>
        </div>
    </div> 
<?php
