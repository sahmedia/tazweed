<?php
/**
 * TokoPress main template.
 * 
 * @package TokoPress
 */

$sidebar = of_get_option( 'tokopress_hide_blog_sidebar' ) ? false : true;
$column = intval( of_get_option( 'tokopress_blog_column_per_row' ) );
if ( $column < 1 ) $column = 2;

get_header(); ?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">
		
	<div class="container">

		<div class="content-block col-md-12">
			
			<section class="main-content <?php echo ( $sidebar ? 'col-md-9 has-sidebar' : 'col-md-12' ); ?> ">
				<div id="content" class="content-area">

					<?php if ( have_posts() ) : ?>
					
						<div class="blog blog-col-<?php echo $column; ?>">

							<?php while ( have_posts() ) : the_post(); ?>

									<?php get_template_part( 'content', get_post_format() ); ?>

							<?php endwhile; ?>

						</div>
						
						
						<?php tokopress_paging_nav(); ?>

					<?php else : ?>

						<?php get_template_part( 'content', '404' ); ?>

					<?php endif; ?>

					<!-- Content here -->
				</div><!-- #content .site-content .hfeed -->

			</section><!-- End #content-area #primary -->
			
			<?php if ( $sidebar ) get_sidebar(); ?>
		
		</div>

	</div>

</div><!-- .site-main #main -->

<?php do_action( 'tokopress_after_content_wrapper' ); ?>

<?php get_footer(); ?>
