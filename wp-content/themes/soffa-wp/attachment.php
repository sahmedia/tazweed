<?php
/**
 * TokoPress attchments template
 *
 * @package TokoPress
 */

$sidebar = of_get_option( 'tokopress_hide_post_sidebar' ) ? false : true;

get_header(); ?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">
		
	<div class="container">

		<div class="content-block col-md-12">
			
			<section class="main-content <?php echo ( $sidebar ? 'col-md-9 has-sidebar' : 'col-md-12' ); ?> ">
				<div id="content" class="content-area">

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php if ( wp_attachment_is_image( $post->id ) ) {
							$att_image = wp_get_attachment_image_src( $post->id, 'full' );
							?>
							<div class="attachment-content">
								<a href="<?php echo wp_get_attachment_url( $post->id ); ?>" title="<?php the_title(); ?>">
								<img src="<?php echo $att_image[0];?>" width="<?php echo $att_image[1];?>" height="<?php echo $att_image[2];?>"  class="attachment-image" alt="<?php $post->post_excerpt; ?>" />
								</a>
							</div>
						<?php } ?>

					</div>

				</div>
			</section>

			<?php if ( $sidebar ) get_sidebar(); ?>
		</div>

	</div>

</div>

<?php do_action( 'tokopress_after_content_wrapper' ); ?>

<?php get_footer(); ?>