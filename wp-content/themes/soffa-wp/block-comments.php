<?php
/**
 * TokoPress comments block template
 *
 * @package TokoPress
 */

function mytheme_comment( $comment, $args, $depth ) {
	
	$GLOBALS['comment'] = $comment; ?>

	<div id="comment-<?php echo get_comment_ID(); ?>" class="comment-block">
		
		<div class="gravatar">
			<a href="<?php echo get_comment_author_url(); ?>"><?php echo get_avatar( $comment, $size='60' ); ?></a>
		</div><!-- .gravatar -->

		<div class="comment-text clearfix">
			
			<span class="comment-info">
				
				<?php printf(__( '<span class="bold">%s</span>', 'tokopress' ), get_comment_author_link() ); ?>
				&nbsp;-&nbsp;
				<?php printf(__( '<span class="italic" title="%1$s at %2$s">%1$s at %2$s</span>', 'tokopress' ), get_comment_date(),  get_comment_time() ); ?>
				&nbsp;-&nbsp;
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'tokopress' ), 'after' => '', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			
			</span>
			
			<?php comment_text() ?>
		
		</div><!-- end comment-text -->

	</div><!-- end comment-block -->



	<?php if ( $comment->comment_approved == '0' ) : ?>

		<em><?php _e( 'Your comment is awaiting moderation.', 'tokopress' ) ?></em>
		<br />

	<?php endif;

}