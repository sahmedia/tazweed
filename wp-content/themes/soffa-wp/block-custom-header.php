<?php
/**
 * TokoPress custom header template
 *
 * @package TokoPress
 */
?>

<div id="site-custom-header">
	<div class="<?php echo apply_filters( 'tokopress_custom_header_class_filter', 'container' ); ?>">
		<div class="col-md-12">
			<img src="<?php header_image(); ?>" alt="<?php bloginfo( 'name' ); ?>">
		</div>
	</div>
</div>