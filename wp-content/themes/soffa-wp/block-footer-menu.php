<?php
/**
 * TokoPress footer menu template
 *
 * @package TokoPress
 */
?>
<?php if ( has_nav_menu( 'footer_menu' ) ) : ?>
	
	<?php
	/**
	 * Displays a navigation footer menu
	 */
	$args = array(
		'theme_location' 	=> 'footer_menu',
		'container'			=> 'nav',
		'container_class' 	=> 'footer-navbar',
		'menu_id'         	=> 'footer-navbar',
		'menu_class'		=> 'navbar-nav'
	);

	wp_nav_menu( $args );

	?>
<?php endif; ?>