<?php
/**
 * TokoPress sosial media icons
 *
 * @package TokoPress
 */
?>

<?php
$socials = array(
	'rss'			=> 'RSS Feed',
	'twitter'		=> 'Twitter',
	'facebook'		=> 'Facebook',
	'google-plus'	=> 'Google+',
	'youtube'		=> 'Youtube',
	'tumblr'		=> 'Tumblr',
	'instagram'		=> 'Instagram',
	'linkedin'		=> 'Linkedin',
	'pinterest'		=> 'Pinterest',
	'github'		=> 'Github',
	'flickr'		=> 'Flickr',
	'vimeo-square'	=> 'Vimeo'
);
?>

<nav class="social">
	
	<?php $numbers = count( $socials ); ?>					
	<ul class="navbar-nav">
		<?php for ( $i = 1; $i <= $numbers; $i++ ) : ?>
			<?php $social = of_get_option( "tokopress_social_{$i}" ); ?>
			<?php $social_url = of_get_option( "tokopress_social_{$i}_url" ); ?>
			<?php if ( $social ) echo '<li><a href="' . ( $social_url ? esc_url( $social_url ) : '#' ) . '" title="' . $socials[$social] . '"><i class="fa fa-' . $social . '"></i></a></li>'; ?>
		<?php endfor; ?>
	</ul>

</nav>