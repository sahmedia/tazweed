<?php
/**
 * TokoPress footer widget template
 *
 * @package TokoPress
 */
?>

<div class="footer-widget">

	<div class="container">
			
		<div class="footercol col-md-3">

			<?php if ( is_active_sidebar( 'footer_widget_1' ) ) : ?>
			
				<?php dynamic_sidebar( 'footer_widget_1' ); ?>

			<?php else : ?>
				
				<div class="widget widget_text">
					<div class="widget-wrap widget-inside">
						<h3 class="widget-title group">Footer Widget #1</h3>

						<div class="textwidget">
							<p>
								This is "Footer Widget #1" widget area. Visit your <a href="<?php echo home_url() ?>/wp-admin/widgets.php">Widgets Page</a> to add new widget to this area.
							</p>
						</div>
					</div>
				</div>
				

			<?php endif; ?>

		</div>

		<div class="footercol col-md-3">
			
			<?php if ( is_active_sidebar( 'footer_widget_2' ) ) : ?>
			
				<?php dynamic_sidebar( 'footer_widget_2' ); ?>

			<?php else : ?>
				
				<div class="widget widget_text">
					<div class="widget-wrap widget-inside">
						<h3 class="widget-title group">Footer Widget #1</h3>

						<div class="textwidget">
							<p>
								This is "Footer Widget #2" widget area. Visit your <a href="<?php echo home_url() ?>/wp-admin/widgets.php">Widgets Page</a> to add new widget to this area.
							</p>
						</div>
					</div>
				</div>
				

			<?php endif; ?>

		</div>

		<div class="footercol col-md-3">
	        
	        <?php if ( is_active_sidebar( 'footer_widget_3' ) ) : ?>
			
				<?php dynamic_sidebar( 'footer_widget_3' ); ?>

			<?php else : ?>
				
				<div class="widget widget_text">
					<div class="widget-wrap widget-inside">
						<h3 class="widget-title group">Footer Widget #1</h3>

						<div class="textwidget">
							<p>
								This is "Footer Widget #3" widget area. Visit your <a href="<?php echo home_url() ?>/wp-admin/widgets.php">Widgets Page</a> to add new widget to this area.
							</p>
						</div>
					</div>
				</div>
				

			<?php endif; ?>

	    </div>

		<div class="footercol col-md-3">
			<?php if ( is_active_sidebar( 'footer_widget_4' ) ) : ?>
			
				<?php dynamic_sidebar( 'footer_widget_4' ); ?>

			<?php else : ?>
				
				<div class="widget widget_text">
					<div class="widget-wrap widget-inside">
						<h3 class="widget-title group">Footer Widget #4</h3>

						<div class="textwidget">
							<p>
								This is "Footer Widget #4" widget area. Visit your <a href="<?php echo home_url() ?>/wp-admin/widgets.php">Widgets Page</a> to add new widget to this area.
							</p>
						</div>
					</div>
				</div>
				

			<?php endif; ?>
			
		</div>
		
	</div>
</div>