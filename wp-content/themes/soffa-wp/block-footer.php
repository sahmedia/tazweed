<?php
/**
 * TokoPress footer creadit template
 *
 * @package TokoPress
 */
?>

<div class="footer-credit">

	<div class="container">
		<div class="footer-left col-md-6">
			
			<div class="copy">

				<?php if( "" != of_get_option( 'tokopress_footer_text' ) ) : ?>
				
					<?php echo of_get_option( 'tokopress_footer_text' ); ?>

				<?php else : ?>

					<a href="<?php home_url(); ?>"><?php _e( 'Copyright &copy; Tokokoo', 'tokopress' ); ?></a>

				<?php endif; ?>
				
			</div>

		</div>

		<div class="footer-right col-md-6">
			
			<?php if ( !of_get_option( 'tokopress_hide_footer_menu' ) ): ?>
				
				<?php get_template_part( 'block-footer-menu' ); ?>
			
			<?php endif ?>
			
			<?php if ( !of_get_option( 'tokopress_hide_social' ) ) : ?>
				
				<?php get_template_part( 'block-footer-social' ); ?>
			
			<?php endif ?>


		</div>
	</div>

</div>