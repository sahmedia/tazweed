<?php
/**
 * TokoPress Home Widget Area
 *
 * @package TokoPress
 */
?>

<div class="home-footer-widget">

	<div class="container">

		<?php if ( is_active_sidebar( 'home_widget' ) ) : ?>
			
			<?php dynamic_sidebar( 'home_widget' ); ?>

		<?php else : ?>
			
			<div class="col-xs-12 col-sm-6 col-md-4">
				<aside id="default_widget_1" class="home-widget widget default_widget_1">
					<div class="widget-wrap">

						<h3 class="widget-title">Default Home Widget #1</h3>

						<div class="widget-inside">
							<p>This is "Home Widget #1" widget area. Visit your <a href="<?php echo home_url() ?>/wp-admin/widgets.php">Widgets Page</a> to add new widget to this area.</p>
						</div>

					</div>
				</aside>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4">
				<aside id="default_widget_2" class="home-widget widget default_widget_2">
					<div class="widget-wrap">

						<h3 class="widget-title">Default Home Widget #2</h3>

						<div class="widget-inside">
							<p>This is "Home Widget #2" widget area. Visit your <a href="<?php echo home_url() ?>/wp-admin/widgets.php">Widgets Page</a> to add new widget to this area.</p>
						</div>

					</div>
				</aside>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-4">
				<aside id="default_widget_3" class="home-widget widget default_widget_3">
					<div class="widget-wrap">

						<h3 class="widget-title">Default Home Widget #3</h3>

						<div class="widget-inside">
							<p>This is "Home Widget #3" widget area. Visit your <a href="<?php echo home_url() ?>/wp-admin/widgets.php">Widgets Page</a> to add new widget to this area.</p>
						</div>

					</div>
				</aside>
			</div>
			

		<?php endif; ?>

	</div>

</div>