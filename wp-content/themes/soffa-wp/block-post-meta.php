<?php
/**
 * TokoPress meta post template
 *
 * @package TokoPress
 */
?>

<?php
	$post_format = get_post_format();

	switch ( $post_format ) {
		case 'aside':
			$post_format_icon = 'fa-plus';
			break;
		case 'audio':
			$post_format_icon = 'fa-volume-up';
			break;
		case 'chat':
			$post_format_icon = 'fa-comments';
			break;
		case 'image':
			$post_format_icon = 'fa-camera';
			break;
		case 'gallery':
			$post_format_icon = ' fa-picture-o';
			break;
		case 'link':
			$post_format_icon = 'fa-link';
			break;
		case 'quote':
			$post_format_icon = 'fa-quote-left';
			break;
		case 'status':
			$post_format_icon = 'fa-pencil';
			break;
		case 'video':
			$post_format_icon = 'fa-video-camera';
			break;
		
		default:
			$post_format_icon = 'fa-calendar-o';
			break;
	}
?>

<div class="meta-block">
	<div class="meta-time">
		<div class="post-format-icon">
			<i class="fa <?php echo $post_format_icon; ?>"></i>
		</div>
		<div class="meta">
			<p><?php echo get_the_date(); ?></p>
			<p><?php comments_number( __( 'No Comment', 'tokopress' ), __( '1 Comment', 'tokopress' ), __( '% Comments', 'tokopress' ) ); ?></p>
			<p><strong><?php echo get_the_author(); ?></strong></p>
			<p class="tags"><?php echo get_the_term_list( $post->ID, 'post_tag', 'Tags:<br>', ', ', '' ); ?> </p>
		</div>
	</div>
	<div class="meta-author">
		<div class="gravatar">
			<?php echo get_avatar( get_the_author_meta( 'ID' ), 60 ); ?>
		</div>
		<div class="description">
			<p class="author-name"><strong><?php echo get_the_author(); ?></strong></p>
			<p><?php the_author_meta( 'description' ); ?></p>
		</div>
	</div>
</div>