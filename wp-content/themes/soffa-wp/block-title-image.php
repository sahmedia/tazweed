<?php
/**
 * TokoPress title with image template
 *
 * @package TokoPress
 */
?>
<div id="site-title">
	<div class="site-header-image">
		
		<?php if ( is_home() ) { ?>

			<h1 class="loop-title"><?php echo get_post_field( 'post_title', get_queried_object_id() ); ?></h1>

		<?php } elseif ( is_category() ) { ?>

			<h1 class="loop-title"><?php _e( 'Categories: ', 'tokopress' ) . single_cat_title(); ?></h1>

		<?php } elseif ( is_tag() ) { ?>

			<h1 class="loop-title"><?php _e( 'Tags: ', 'tokopress' ) . single_tag_title(); ?></h1>

		<?php } elseif ( is_tax() ) { ?>

			<h1 class="loop-title"><?php single_term_title(); ?></h1>

		<?php } elseif ( is_author() ) { ?>

			<h1 class="loop-title fn n"><?php the_author_meta( 'display_name', get_query_var( 'author' ) ); ?></h1>

		<?php } elseif ( is_search() ) { ?>

			<h1 class="loop-title"><?php echo esc_attr( get_search_query() ); ?></h1>

		<?php } elseif ( is_day() || is_month() || is_year() ) { ?>

			<?php
				if ( is_day() )
					$date = get_the_time( __( 'F d, Y', 'tokopress' ) );
				elseif ( is_month() )
					$date = get_the_time( __( 'F Y', 'tokopress' ) );
				elseif ( is_year() )
					$date = get_the_time( __( 'Y', 'tokopress' ) );
			?>

			<h1 class="loop-title"><?php echo $date; ?></h1>
		
		<?php } elseif ( is_archive() ) { ?>

				<?php if( is_post_type_archive( 'project' ) ) { ?>

					<h1 class="loop-title"><?php _e( 'Projects', 'tokopress' ); ?></h1>

				<?php } elseif ( class_exists( 'woocommerce' ) && is_post_type_archive( 'product' ) ) { ?>

					<h1 class="loop-title"><?php _e( 'Shop', 'tokopress' ); ?></h1>

				<?php } else { ?>

					<h1 class="loop-title"><?php _e( 'Archives', 'tokopress' ); ?></h1>

				<?php } ?>

		<?php } elseif ( is_single() ) { ?>
	
			<h1 itemprop="name" class="loop-title"><?php the_title(); ?></h1>

		<?php } elseif ( is_page() ) { ?>
		
			<h1 class="loop-title"><?php the_title(); ?></h1>
		
		<?php } elseif( is_404() ) { ?>

			<h1 class="loop-title"><?php _e( 'No Result Found', 'tokopress' ); ?></h1>

		<?php } ?>

	</div>
</div>