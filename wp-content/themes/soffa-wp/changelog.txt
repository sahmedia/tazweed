2.0.2
added: dummy content for Toko Slider Plugin
updated: Recolution SLider Plugin v5.2.6
fixed: dummy content

2.0.1
added: option to use Google Maps API key for Contact page template
updated: option framework v1.8.5
updated: translation string
fixed: dropdown menu active style

2.0.0
added: compatible with site identity WordPress v4.5
added: compatible with WooCommerce v2.6
added: styling My Account tab navigation on WooCommerce v2.6
added: TokoPress Slider Plugin (included)
updated: outdate template files on WooCommerce v2.6.1
updated: FontAwesome v4.6.3
updated: TGM Plugin Activation v2.6.1
updated: Visual Composer v4.12
updated: Revolution Sider Plugin v5.2.5.4
updated: OWL Carousel v2
added: option to enable Visual Composer License Page (Appearance - Customize - Visual Composer). It is useful if you purchase Visual Composer license to get direct plugin updates and official support from Visual Composer developer.

1.4.0
- updated: Visual Composer 4.11.2.1
- updated: Revo Slider 5.2.3.5

1.3.9
- updated: minor version
- removed: WooCommerce Catalog Mode by XtensionPress Plugin

1.3.8
- updated: Visual Composer Plugin v4.9.2
- updated: Revolution Slider Plugin v5.1.6
- updated: TGMPA plugin installer
- updated: TokoPress Heading Typography Shortcode v1.3
- updated: TokoPress Image Slider Shortcode v1.5
- updated: TokoPress Shortcode v1.3
- updated: TokoPress Widget Shortcode v1.2
- fixed: Responsive menu level
- removed: Optopn to change favicon

1.3.7
- fixed: Isotop JS for image loaded
- updated: contact form
- updated: Visual Composer Plugin v4.8.1
- updated: Revolution Slider Plugin v5.1.2

1.3.6
- updated: Revolution Slider plugin v5.0.9
- updated: Visual Composer Plugin v4.7.4
- updated: TokoPress Heading Typography Plugin v1.2
- updated: TokoPress Image Slider Plugin v1.4
- updated: TokoPress Shortcode Plugin v1.2

1.3.5
- added: option to change favicon
- updated: Revolution Slider 4.6.93

1.3.4
- fixed: minor update

1.3.3
- fixed: TGM security issue
- update: Theme Security
- improve: Responsive header style 

1.3.2
- update: style woocommerce for visual composer elements
- update: theme customizer and woocommerce customizer
- fixed: wp theme check issue
- fixed: some issue style
- fixed: TGM bulk installer
- update: tokopress-heading-typography-shortcode plugin
- update: tokopress-image-slider-shortcode plugin
- update: tokopress-shortcode plugin
- update: tokopress-widget-shortcode plugin

1.3.1
- update wordpress importer v2.0
- improve single product style
- improve default botton
- improve widget style
- improve missing style

1.3.0
- fix responsive tokopress images slider plugin
- fix grid layout category project by woothemes plugin
- add image lightbox popup gallery in single project
- fix some issue style

1.2.9
- added: child theme sample
- added: option for filterable projects/portfolio
- added: woocommerce redirect after login customer
- added: header and footer script options
- updated: separate team members (build on plugin)
- updated: Visual Composer 4.4.2
- updated: Revolution Slider 4.6.5
- updated: customizer issue
- fixed: minor TGMPA issue
- fixed: related projects issue
- fixed: wp theme check issue

1.2.8
- updated: Visual Composer 4.3.4
- updated: Revolution Slider 4.6.0
- added: option to change project button text

1.2.7
- added: automatic theme update using purchase code. Go to Appearance – Theme Update page to enter purchase code.
- added: option to disable sidebar on shop and product page.
- added: option to change post columns per row on blog page.
- added: option to disable sidebar on blog page.
- added: option to disable sticky header on desktop.
- fixed: few minor bugs.

1.2
- added: PSD Files are included to the full package.
- added: sticky header for desktop.
- added: option to hide bottom widget area on the frontpage.
- added: more popular social icon options on the footer.
- updated: TGMPA class with ability to update bundled plugins.
- updated: set Visual Composer as part of the theme and disable updater (to be handled by TGMPA).
- updated: Visual Composer 4.2.3
- updated: Revolution Slider 4.5.9

1.1
- Added: Translation support (.po/.mo)
- Added: WPML compatibility
- Added: New dummy data and video tutorial to use it http://www.youtube.com/watch?v=ArA7wI7FRh4
- Improved: better responsive header
- Improved: better responsive footer
- Fixed: Revolution Slider prepackaged and installation issue
- and some improvements on code and backend