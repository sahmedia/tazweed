<?php
/**
 * TokoPress comment template
 *
 * @package TokoPress
 */

if ( post_password_required() )
	return;
?>
	<div class="comment-block-row">

		<?php if( '0' != get_comments_number() ) : ?>
			<div class="comments-title">
				<h6>COMMETS</h6>
			</div>
		<?php else : ?>
			<div class="comments-title">
				<h6>THERE ARE NO COMMENTS YET</h6>
			</div>
		<?php endif; ?>

		<?php if ( have_comments() ) : ?>			

				<?php wp_list_comments( 'callback=mytheme_comment' ); ?>

				<div class="paginate-com">
					<?php paginate_comments_links(); ?>
				</div>

		<?php endif; ?>

	</div>

	<?php
    if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
    
    	<p class="no-comments"><?php _e( 'Comments are closed.', 'tokopress' ); ?></p>
	
	<?php endif; ?>

	<div id="comments-form">

		<?php
		$comment_args = array(
			'title_reply'	=> '<div class="comments-title"><h6>' .__( 'Post Your Comment', 'tokopress' ). '</h6></div>',
	 		'fields' 		=> apply_filters( 
	 								'comment_form_default_fields',

									array(
										'block-open' => '<div class="left-data-comment">',
										'author' => '<input id="author" name="author" type="text" placeholder="' .__( 'Your name', 'tokopress' ). '" />',   
										'email'  => '<input id="email" name="email" type="email" placeholder="' .__( 'Your email', 'tokopress' ). '" />',
										'url'    => '<input id="url" name="url" type="text" placeholder="' .__( 'Your website URL', 'tokopress' ). '" />',
										'block-close' => '</div>'
									)
								),
								 
			'comment_field' 		=> '<div class="right-data-comment"><textarea id="comment" name="comment" placeholder="' . __( 'post your comment here!', 'tokopress' ) . '" aria-required="true"></textarea></div>',
			'comment_notes_after' 	=> '',
			'label_submit'      	=> __( 'Submit Comment', 'tokopress' ) 
		);

		?>

		<?php comment_form( $comment_args ); ?>

	</div><!-- #comments-form -->