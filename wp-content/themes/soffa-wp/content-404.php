<?php
/**
 * TokoPress not-found template
 *
 * @package TokoPress
 */
?>

<div class="content-not-found">
	<h1><?php _e( 'Not Found', 'tokopress' ); ?></h1>

	<div class="addition-widget">
		<div class="col-md-6">
			
			<?php the_widget( 'WP_Widget_Categories' ); ?>

		</div>
		<div class="col-md-6">
			
			<?php the_widget( 'WP_Widget_Archives', array( 'count' => '1', 'title' => 'Monthly Archives' ) ); ?>

		</div>
	</div>
</div>