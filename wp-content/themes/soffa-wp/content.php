<?php
/**
 * TokoPress content template
 *
 * @package TokoPress
 */

if( is_sticky() ) {
	$add_class = "sticky";
} else {
	$add_class = "post-loop";
}

$column = intval( of_get_option( 'tokopress_blog_column_per_row' ) );
if ( $column < 1 ) $column = 2;
$column = 12 / $column;
?>

<article class="col-md-<?php echo $column; ?> blogpost <?php echo $add_class; ?>">
	<div class="entry-wrapper">
		
		<div class="thumbnail">
			<?php if ( has_post_thumbnail() ) : ?>
				
				<figure><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumb-blog' ); ?></a></figure>

			<?php else : ?>

				<figure><a href="<?php the_permalink(); ?>"><img src="http://placehold.it/430x220&text=thumbnail" alt="thumbnail"></a></figure>
			
			<?php endif; ?>
			
			<div class="caption">
				<h2 class="post-title entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo text_limiter( get_the_title(), 100 ); ?></a></h2>
				<time class="published" datetime="<?php the_date(); ?>" title="<?php echo get_the_date().' - '.get_the_time(); ?>"><?php echo get_the_date(); ?></time>
			</div>
		</div>
	
	</div>
</article>