<?php
/**
 * TokoPress footer template
 *
 * @package TokoPress
 */
?>
			<?php if ( !of_get_option( 'tokopress_hide_home_widget' ) && is_front_page() ) get_template_part( 'block-home-sidebar' ); ?>
			
			<?php if ( !of_get_option( 'tokopress_hide_footer_widget' ) || !of_get_option( 'tokopress_hide_footer' ) ) : ?>	
			<footer id="footer-block" class="site-footer">

				<?php if( !of_get_option( 'tokopress_hide_footer_widget' ) ) : ?>
					<?php get_template_part( 'block-footer-widget' ); ?>
				<?php endif; ?>

				<?php if( !of_get_option( 'tokopress_hide_footer' ) ) : ?>
					<?php get_template_part( 'block-footer' ); ?>
				<?php endif; ?>

				<?php do_action( 'tokopress_footer' ); ?>
				
			</footer>
			<?php endif ?>

		<?php wp_footer(); ?>

	</body>
</html>