<?php
/**
 * Tokopress header template
 *
 * @package TokoPress
 */
?>

<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<?php wp_head(); ?>
		
	</head>

	<body <?php body_class(); ?>>
	
		<header id="masthead" class="site-header">

			<div class="container">

				<div class="header-left">
					<div id="site-logo">
						
						<a href="<?php echo home_url(); ?>">

							<?php if ( function_exists( 'the_custom_logo' ) ) : ?>
                                <?php if( has_custom_logo() ) : ?>
                                	<?php the_custom_logo(); ?>
                                <?php else : ?>
                                	<img src="<?php echo get_template_directory_uri(); ?>/img/dummy/logo.png" alt="<?php bloginfo( 'name' ); ?>">
                                <?php endif; ?>

                            <?php else : ?>

								<?php if( !of_get_option( 'tokopress_logo' ) ) : ?>

									<img src="<?php echo get_template_directory_uri(); ?>/img/dummy/logo.png" alt="<?php bloginfo( 'name' ); ?>">

								<?php else : ?>
									
									<img src="<?php echo of_get_option( 'tokopress_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>">

								<?php endif; ?>

							<?php endif; ?>

						</a>
						
					</div>
					
					<?php if ( !of_get_option( 'tokopress_hide_header_desc' ) ): ?>
						<div id="site-description">
							<p class="desc"><?php echo bloginfo( 'description' ); ?></p>
						</div>
					<?php endif ?>

				</div> 
				<!-- #header-logo -->

				<div class="header-right">
				<?php if ( !of_get_option( 'tokopress_hide_header_menu' ) && has_nav_menu( 'header_menu' ) ): ?>
					<div class="primary-menu-block pull-left">
						<div id="primary_menu" class="collapse navbar-collapse">
							<?php
							$args = array(
								'theme_location' 	=> 'header_menu',
								'depth'             => 5,
					            'container'         => 'nav',
					            'container_class'   => 'primary-navbar',
					        	'container_id'      => 'access',
					            'menu_class'        => 'navbar-nav',
					            'menu_id'			=> 'primary-navbar'
							);
							wp_nav_menu( $args );
							?>
						</div>
					</div>
				<?php endif ?>
				
				<?php if ( 
					!of_get_option( 'tokopress_hide_header_search' ) || 
					( !of_get_option( 'tokopress_hide_header_minicart' ) && class_exists( 'woocommerce' ) ) || 
					( !of_get_option( 'tokopress_hide_header_menu' ) && has_nav_menu( 'header_menu' ) )
				) : ?>
					<nav class="additional-menu-block pull-right">
						<ul class="additional-menu">
							<?php if ( !of_get_option( 'tokopress_hide_header_search' ) ): ?>
								<li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
									<ul class="dropdown-menu">
										<li>
											<?php get_search_form(); ?>
										</li>
									</ul>
								</li>
							<?php endif; ?>
							
							<?php if( !of_get_option( 'tokopress_hide_header_minicart' ) && class_exists( 'woocommerce' ) ) : ?>
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i></a>
								<ul class="dropdown-menu">
									<li>
										<?php the_widget('WC_Widget_Cart'); ?>
									</li>
								</ul>
							</li>
							<?php endif; ?>
							
							<?php if ( !of_get_option( 'tokopress_hide_header_menu' ) && has_nav_menu( 'header_menu' ) ): ?>
								<li class="hidden-md hidden-lg">
									<a href="#" class="snavbar-toggle" data-toggle="collapse" data-target="#primary_menu">
								        <i class="fa fa-align-justify"></i>
								      </a>
								</li>
							<?php endif; ?>
						</ul>
					</nav>
				<?php endif ?>

				</div>
				
			</div><!-- .container -->

		</header> <!-- End #masthead -->