<?php

/**
 * Theme Customizer Framework
 * 
 *
 * @author 		Alispx, Ahmad
 * @license 	license.txt
 * @since 		1.1
 */

/**
 * Get all register panel
 */
function tokopress_get_customizer_panels() {
	$cf_panels = array();
	return apply_filters( 'cf_panels', $cf_panels );
}

/**
 * Get Customizer Sections
 */
function tokopress_customizer_sections() {
    $cf_sections = array();
    return apply_filters( 'cf_sections', $cf_sections );
}

/**
 * Get Customizer Colors
 */
function tokopress_customizer_options() {
    $cf_colors = array();
    return apply_filters( 'cf_colors', $cf_colors );
}

/**
 * Register Custom Sections, Settings, And Controls
 *
 * @since 1.1
 */
add_action( 'customize_register', 'tokopress_theme_customizer_register' );
function tokopress_theme_customizer_register( $wp_customize ) {
	
	$cf_panels		= tokopress_get_customizer_panels();
	$cf_sections 	= tokopress_customizer_sections();
	$cf_colors 		= tokopress_customizer_options();

	// create panel from array data
	foreach ( $cf_panels as $panels ) {
		if ( isset ( $panels['priority'] ) ) {
			$priority = $panels['priority'];
		} else {
			$priority = '';
		}

		$wp_customize->add_panel( $panels['ID'], array(
			'priority'       => $panels['priority'],
			'capability'     => 'edit_theme_options',
			'title'          => $panels['title'],
			'description'    => $panels['description'],
		) );
	}

	//create the section from array data
	foreach ( $cf_sections as $section ) {

		if( isset( $section['panel_id'] ) ) {
			$panel = $section['panel_id'];
		} else {
			$panel = '';
		}

		if ( ! empty( $section['priority'] ) ) {
			$priority = $section['priority'];
		} else {
			$priority = '';
		}

		$wp_customize->add_section( $section['slug'], 
			array(
				'panel'		=> $panel,
				'title'		=> $section['label'],
				'priority'	=> $priority,
		));

	}

	//create the componen from array data
	foreach ( $cf_colors as $color ) {
		
		if ( ! empty( $color['transport'] ) ) {
			$transport = $color['transport'];
		} else {
			$transport = 'postMessage';
		}

		if ( ! empty( $color['priority'] ) ) {
			$priority = $color['priority'];
		} else {
			$priority = '';
		}

		// Define each customizer type 
		switch ( $color['type'] ) {
			
			case 'color':

				$wp_customize->add_setting( $color['slug'], 
					array( 
						'default' 			=> $color['default'], 
						'type' 				=> 'theme_mod', 
						'transport'			=> $transport, 
						'capability' 		=> 'edit_theme_options',
						'sanitize_callback'	=> 'esc_attr'
					) );
				
				$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $color['slug'], 
					array( 
						'label' 		=> $color['label'], 
						'section' 		=> $color['section'], 
						'priority'		=> $priority, 
						'settings' 		=> $color['slug'] ) 
					) );
				
				break;
			
			case 'select' :

				$wp_customize->add_setting( $color['slug'], 
					array(
						'default' 			=> $color['default'],
						'type' 				=> 'theme_mod', 
						'transport'   		=> $transport,
						'capability' 		=> 'edit_theme_options',
						'sanitize_callback'	=> 'esc_attr'
					) );
				
				$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 
					$color['slug'], 
					array( 
						'label' 		=> $color['label'], 
						'section' 		=> $color['section'],
						'default' 		=> $color['default'],
						'priority'		=> $priority,
						'settings' 		=> $color['slug'], 
						'choices'		=> $color['choices'], 
						'type'			=> 'select'
						)
					));

				break;

			case 'select_font' :

				$wp_customize->add_setting( $color['slug'], 
					array(
						'default' 			=> $color['default'],
						'type' 				=> 'theme_mod', 
						'transport'   		=> $transport,
						'capability' 		=> 'edit_theme_options',
						'sanitize_callback'	=> 'esc_attr'
					) );
				
				$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 
					$color['slug'], 
					array( 
						'label' 		=> $color['label'], 
						'section' 		=> $color['section'],
						'default' 		=> $color['default'],
						'priority'		=> $priority,
						'settings' 		=> $color['slug'], 
						'choices'		=> $color['choices'], 
						'type'			=> 'select'
						)
					));

				break;

			case 'text':

				$wp_customize->add_setting( $color['slug'] , 
					array(
						'default'			=> $color['default'],
						'type'				=> 'theme_mod',
						'transport'   		=> $transport,
						'capability'		=> 'edit_theme_options',
						'sanitize_callback'	=> 'esc_attr'
				) );

				$wp_customize->add_control( $color['slug'] , 
					array(
						'label'		=> $color['label'],
						'section'	=> $color['section'],
						'priority'	=> $priority,
						'settings'	=> $color['slug'],
						'type'		=> 'text' 
					) );

				break;

			case 'images' :

				$wp_customize->add_setting( $color['slug'], 
					array( 
						'default' 			=> $color['default'], 
						'capability' 		=> 'edit_theme_options', 
						'type' 				=> 'theme_mod',
						'sanitize_callback'	=> 'esc_attr'
						));   

				$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 
					$color['slug'], 
					array( 
						'label' 		=> $color['label'], 
						'section' 		=> $color['section'],
						'priority'		=> $priority, 
						'settings' 		=> $color['slug']
						 )));

				break;

			default:
				
				break;
		}

	}
	
}

/**
 * Used by hook: 'customize_preview_init'
 *
 * @see add_action( 'customize_preview_init', $func )
 */
add_action( 'customize_preview_init', 'tokopress_customizer_live_preview' , 1 );
function tokopress_customizer_live_preview() {
	$cf_colors = tokopress_customizer_options();
	
	wp_enqueue_script( 'customizer-preview', get_template_directory_uri() .'/inc/customizer/js/customizer-preview.js', array( 'jquery', 'customize-preview' ), '', true );
	wp_localize_script(	'customizer-preview', 'custStyle', $cf_colors );

 }

/**
 * Sanitize Print To Head
 *
 * @since 1.0
 */
add_action( 'tokopress_custom_styles', 'tokopress_customizer_css' );
function tokopress_customizer_css() { 
	
	$cf_colors = tokopress_customizer_options();

	$style = '';
	
	foreach ( $cf_colors as $color ) {

		$newvalue	= get_theme_mod( $color['slug'] );

		if ( $newvalue ) {
			if ( ! empty( $color['property'] ) ) {
				$property = $color['property'];
			} else {
				$property = '';
			}

			if ( ! empty( $color['property2'] ) ) {
				$property2 = $color['property2'];
			} else {
				$property2 = '';
			}	

			$selectors 	= $color['selector'];

			if ( $color['type'] == 'color' ) {
				$style .=  $selectors. " { $property : ".$newvalue."; } ";
			} else if ( $color['type'] == 'images' ) {
				$style .=  $selectors. " { $property : url('".$newvalue."') $property2; } ";
			} else if ( $color['type'] == 'select_font' ) {
				$style .=  $selectors. " { $property : '".$newvalue."' $property2; } ";
			}
		}
	}

	$style = trim( $style );

	echo $style;

}
