<?php
/**
 * TokoPress function and definitions Projects Plugins
 *
 * @package TokoPress
 */

/**
 * Disable projects style
 */
function tokopress_disable_projects_style() {
	// disable styles projects style
	wp_dequeue_style( 'projects-styles' );

	// disable styles handheld devices
	wp_dequeue_style( 'projects-handheld' );

	// disable styles dashicons
	wp_dequeue_style( 'dashicons' );
}
add_action( 'wp_enqueue_scripts', 'tokopress_disable_projects_style' );

/**
 * Add Post Class projects
 */
function tokopress_projects_column( $classes ) {

	$column_option = '3';

	if( is_post_type_archive( 'project' ) || is_tax( 'project-category' ) ) :
		if( '1' == $column_option )
			$classes[] = "col-md-12";
		if( '2' == $column_option )
			$classes[] = "col-md-6";
		if( '3' == $column_option )
			$classes[] = "col-md-4";
		if( '4' == $column_option )
			$classes[] = "col-md-3";
		if( '5' == $column_option )
			$classes[] = "col-md-2-5";
		if( '6' == $column_option )
			$classes[] = "col-md-6";

		$projects_tax = 'project-category';
		$terms = get_the_terms( get_the_ID(), $projects_tax );

		foreach ($terms as $term) {
			$classes[] = $term->slug;
		}
	endif;
	
	return $classes;

}
add_filter( 'post_class', 'tokopress_projects_column' );

/**
 * Change Project Column Number
 */
add_filter( 'projects_loop_columns', 'tokopress_projects_columns' );
function tokopress_projects_columns( $cols ) {
	$cols = '3';
	return $cols;
}

/**
 * Add Wrapper Loop Items start
 */
function tokopress_projects_wrapper_loop_items_start() {
	echo '<div class="project-wrapper-items">';
}
add_action( 'projects_before_loop_item', 'tokopress_projects_wrapper_loop_items_start', 5 );

/**
 * Add Wrapper Loop Items end
 */
function tokopress_projects_wrapper_loop_items_end() {
	echo '</div>';
}
add_action( 'projects_after_loop_item', 'tokopress_projects_wrapper_loop_items_end', 10 );

/**
 * Remove Deskription Project Items
 */
remove_action( 'projects_after_loop_item', 'projects_template_short_description', 10 );

/**
 * Create caption wrapper start
 */
function tokopress_projects_wrapper_caption_start() {
	echo '<div class="project-caption">';
}
add_action( 'projects_loop_item', 'tokopress_projects_wrapper_caption_start', 15 );

/**
 * Create caption wrapper end
 */
function tokopress_projects_wrapper_caption_end() {
	$project_cats = get_the_term_list( get_the_ID(), 'project-category', '', ', ', '' );

	echo '<p class="project-meta">' . $project_cats . '</p>';
	echo '<a class="project-button" href="'. get_permalink() .'">'. ( !of_get_option( 'tokopress_project_button_text' ) ? __( 'View Project', 'tokopress' ) : of_get_option( 'tokopress_project_button_text' ) ) .'</a>';
	echo '</div>';
}
add_action( 'projects_loop_item', 'tokopress_projects_wrapper_caption_end', 25 );

/**
 * Remove short description single project
 */
remove_action( 'projects_before_single_project_summary', 'projects_template_single_short_description', 20 );

/**
 * Placement gallery single project
 */
remove_action( 'projects_before_single_project_summary', 'projects_template_single_gallery', 40 );

/**
 * Placement Featured Image single project
 */
remove_action( 'projects_before_single_project_summary', 'projects_template_single_feature', 30 );

/**
 * Disable single project title
 */
remove_action( 'projects_before_single_project_summary', 'projects_template_single_title', 10 );

/* Add Wrapper Image And Gallery Image - Single Project */
add_action( 'projects_before_single_project_summary', 'tokopress_entry_image_wrap_start', 0 );
function tokopress_entry_image_wrap_start() {
	echo '<div class="tokopress-entry-image-wrap">';
}
add_action( 'projects_before_single_project_summary', 'tokopress_entry_image_wrap_end', 999 );
function tokopress_entry_image_wrap_end() {
	echo '</div>';
}

/**
 * Featured Image and Image Gallery
 */
function tokopress_project_image() {
	$attachment_ids = projects_get_gallery_attachment_ids();

	if( !empty( $attachment_ids ) ) {
		wp_enqueue_style( 'toko-owlcarousel' );
		wp_enqueue_script( 'toko-owlcarousel' );

		$project_galleries_owl = "$('.projects .gallery').addClass('owl-carousel owl-theme');$('.projects .gallery').owlCarousel({items : 1,autoplay : true,autoplayHoverPause : true,nav: true,navText: ['<i class=\"fa fa-chevron-left\"></i>','<i class=\"fa fa-chevron-right\"></i>'],loop: true,dot: false,lazyLoad : true});";

		toko_enqueue_js( $project_galleries_owl );
		projects_template_single_gallery();
	} else {
		projects_template_single_feature();
	}
}
add_action( 'projects_before_single_project_summary', 'tokopress_project_image', 30 );

function tokopress_filter_image_gallery_projects() {
	return 'thumb-projects';
}
add_filter( 'single_project_small_thumbnail_size', 'tokopress_filter_image_gallery_projects' );

/**
 * Display Category Project in Loop
 */
function tokopress_display_cats_project() {
	$projects_tax = 'project-category';
	$terms = get_terms( $projects_tax );

	if( is_post_type_archive( 'project' ) ) {
		if( $terms && !is_wp_error( $terms ) ) {
			?>
			<ul id="isotope-projects-filters">
				<li data-filter="*" class="current"><?php _e( 'All', 'tokopress' ); ?></li>
			<?php
			foreach ($terms as $term) {
				?>
				<li data-filter=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></li>
				<?php
			}
			?>
			</ul>
			<?php
		}
	}
}
if( of_get_option( 'tokopress_project_filterable' ) ) {
	add_action( 'projects_before_loop', 'tokopress_display_cats_project', 99 );
}

/**
 * Add Js Isotop in Portfolio page
 */
function tokopress_display_js_option_isotope() {
	if( is_post_type_archive( 'project' ) ) {
		?>
		<script type="text/javascript">
		var filterableItem = jQuery('.projects ul.projects');
		jQuery(window).load(function() {
			filterableItem.isotope({
			  itemSelector: '.type-project',
			  layoutMode: 'masonry',
			  
			});
		});
		jQuery(document).ready(function($){
			$('#isotope-projects-filters li').click(function(){
		        $('#isotope-projects-filters .current').removeClass('current');
		        $(this).addClass('current');

		        var selector = $(this).data('filter');

		        filterableItem.isotope({
		            filter: selector,
		            animationOptions: {
		                duration: 750,
		                easing: 'linear',
		                queue: false
		            }
		         });
		         return false;
		    });
		});
		</script>
		<?php
	}
}
if( !of_get_option( 'tokopress_hide_filter_projects' ) ) {
	add_action( 'wp_footer', 'tokopress_display_js_option_isotope', 999 );
}
