<?php
/**
 * TokoPress Project/Portfolio Options
 *
 * @package Projects
 */

function tokopress_project_settings( $options ) {
	$options[] = array(
		'name' => __( 'Portfolio', 'tokopress' ),
		'type' => 'heading'
	);

		$options[] = array(
			'name' => __( 'ENABLE Filterable Project/Portfolio', 'tokopress' ),
			'desc' => __( 'ENABLE filterable project/portfolio', 'tokopress' ),
			'id' => 'tokopress_project_filterable',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'Change Button Text Project/Portfolio', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_project_button_text',
			'std' => __( 'View Project', 'tokopress' ),
			'type' => 'text'
		);

	return $options;
}
add_filter( 'of_options', 'tokopress_project_settings', 55 );