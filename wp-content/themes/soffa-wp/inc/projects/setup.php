<?php
/**
 * TokoPress function and definitions Projects Plugins
 *
 * @package TokoPress
 */

/**
 * Remove sidebar
 */
remove_action( 'projects_sidebar', 'projects_get_sidebar', 10 );