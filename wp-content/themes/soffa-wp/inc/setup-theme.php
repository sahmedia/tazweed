<?php
/**
 * Setup theme specific functions
 *
 * WARNING: This file is part of the TokoPress parent theme.
 * Please do all modifications in the form of a child theme.
 *
 * @category   TokoPress
 * @package    Setup
 * @author     TokoPress
 * @link       http://www.tokopress.com
 */

/**
 * Include Customizer Framework
 */
tokopress_require_file( get_template_directory() . '/inc/customizer/customizer-framework.php' );

if( is_admin() ) :

	/* Include theme options */
	tokopress_require_file( get_template_directory() . '/inc/theme/options.php' );
	tokopress_require_file( get_template_directory() . '/inc/theme/plugins.php' );


else :

	/* Include frontend theme */
	tokopress_require_file( get_template_directory() . '/inc/theme/frontend.php' );

	tokopress_require_file( get_template_directory() . '/inc/theme/contact-form.php' );

endif;

/* Include Theme Design / Customizer */
	tokopress_require_file( get_template_directory() . '/inc/theme/designs.php' );

/* Include CMB Metabox */
	tokopress_require_file( get_template_directory() . '/inc/metabox/custom-meta-boxes.php' );
	tokopress_require_file( get_template_directory() . '/inc/theme/metabox.php' );

/**
 * Include function theme
 */
tokopress_require_file( get_template_directory() . '/inc/theme/function.php' );

/* Include Theme Design / Customizer */
	tokopress_require_file( get_template_directory() . '/inc/theme/update.php' );

/**
 * Include Custom Widget
 */
tokopress_require_file( get_template_directory() . '/inc/widget/blog_widget.php' );

/**
 * Include Bootstrap navwalker
 */
tokopress_require_file( get_template_directory() . '/inc/theme/wp_bootstrap_navwalker.php' );

/**
 * WooCommerce
 */
if( class_exists( 'woocommerce' ) ) :

	/* Include WooCommerce setup */
	tokopress_require_file( get_template_directory() . '/inc/woocommerce/setup.php' );
	/* Include WooCommerce Customizer */
	tokopress_require_file( get_template_directory() . '/inc/woocommerce/designs.php' );
	/* Include WooCommerce frontend */
	tokopress_require_file( get_template_directory() . '/inc/woocommerce/frontend.php' );
	/* Include WooCommerce function */
	tokopress_require_file( get_template_directory() . '/inc/woocommerce/function.php' );
	/* Include WooCommerce options */
	tokopress_require_file( get_template_directory() . '/inc/woocommerce/options.php' );

endif;

if( class_exists( 'Projects' ) ) {

	tokopress_require_file( get_template_directory() . '/inc/projects/setup.php' );
	tokopress_require_file( get_template_directory() . '/inc/projects/frontend.php' );
	tokopress_require_file( get_template_directory() . '/inc/projects/options.php' );

}