<?php
/**
 * TokoPress Designs
 *
 * @package TokoPress
 */

/**
 * Customize Default Section WordPress
 */
function tokopress_custom_section( $wp_customize ) {
	$wp_customize->get_section( 'colors' )->title = __( 'General Colors', 'tokopress' );
	$wp_customize->get_section( 'header_image' )->panel = 'tokopress_header_panels';
	$wp_customize->get_section( 'header_image' )->priority = 1;
}
add_action( 'customize_register', 'tokopress_custom_section' );

/**
 * Setup Customizer Panels
 */
function tokopress_customizer_panel_setup( $cf_panels ) {
	/**
	 * Header panel
	 */
	$cf_panels[] = array(
		'ID'			=> 'tokopress_header_panels',
		'title'			=> __( 'Header Sections', 'tokopress' ),
		'description'	=> __( 'Customize your header in here', 'tokopress' ),
		'priority'		=> 150
	);

	/**
	 * Footer panel
	 */
	$cf_panels[] = array(
		'ID'			=> 'tokopress_footer_panels',
		'title'			=> __( 'Footer Sections', 'tokopress' ),
		'description'	=> __( 'Customize your footer in here', 'tokopress' ),
		'priority'		=> 155
	);

	return $cf_panels;
}
add_filter( 'cf_panels', 'tokopress_customizer_panel_setup' );

/**
 * Setup Customizer Sections
 */
function tokopress_customizer_sections_setup( $cf_sections ) {

	/**
	 * Header style section
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_header_style',
		'label'		=> __( 'Header Style', 'tokopress' ),
		'priority'	=> 2,
		'panel_id'	=> 'tokopress_header_panels'
	);

	/**
	 * Primary menu section
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_header_primarymenu',
		'label'		=> __( 'Primary Menu', 'tokopress' ),
		'priority'	=> 3,
		'panel_id'	=> 'tokopress_header_panels'
	);

	/**
	 * Additional menu section
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_header_additional',
		'label'		=> __( 'Additional Menu', 'tokopress' ),
		'priority'	=> 4,
		'panel_id'	=> 'tokopress_header_panels'
	);

	/**
	 * Header seacrh form section
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_header_search',
		'label'		=> __( 'Search Form', 'tokopress' ),
		'priority'	=> 5,
		'panel_id'	=> 'tokopress_header_panels'
	);

	/**
	 * Mini cart section
	 */
	if( class_exists( 'woocommerce' ) ) {
		$cf_sections[] = array(
			'slug'		=> 'tokopress_header_minicart',
			'label'		=> __( 'Mini Cart', 'tokopress' ),
			'priority'	=> 6,
			'panel_id'	=> 'tokopress_header_panels'
		);
	}

	/**
	 * Footer Widget
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_footer_widget',
		'label'		=> __( 'Footer Widget', 'tokopress' ),
		'priority'	=> 1,
		'panel_id'	=> 'tokopress_footer_panels'
	);

	/**
	 * Footer Block
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_footer_block',
		'label'		=> __( 'Footer Block', 'tokopress' ),
		'priority'	=> 2,
		'panel_id'	=> 'tokopress_footer_panels'
	);

	/**
	 * Pagination Section
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_pagination',
		'label'		=> __( 'Pagination', 'tokopress' ),
		'priority'	=> 156,
	);

	return $cf_sections;
}
add_filter( 'cf_sections', 'tokopress_customizer_sections_setup' );

/**
 * Header section customizer data
 */
function tokopress_customizer_header_section_data( $cf_colors ) {
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_header_bg', 
		'default'	=> '#ffffff', 
		'priority'	=> 1, 
		'label'		=> __( 'Header Background', 'tokopress' ),
		'section'	=> 'tokopress_header_style',
		'selector'	=> '.site-header',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_header_text', 
		'default'	=> '#6e6e6e', 
		'priority'	=> 2, 
		'label'		=> __( 'Header Text Color', 'tokopress' ),
		'section'	=> 'tokopress_header_style',
		'selector'	=> '.site-header #site-description p',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_header_section_data' );

/**
 * Primary menu customizer data
 */
function tokopress_customizer_primarymenu_section_data( $cf_colors ) {
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_menu_background',
		'default'	=> '',
		'priority'	=> 1,
		'label'		=> __( 'Background Color', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.site-header .primary-menu-block',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_menu_text',
		'default'	=> '#3e3e3e',
		'priority'	=> 2,
		'label'		=> __( 'Text Color', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav li a',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_menu_bg_hover',
		'default'	=> '#ededed',
		'priority'	=> 3,
		'label'		=> __( 'Background Color (hover)', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav li a:hover',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_menu_text_hover',
		'default'	=> '#3e3e3e',
		'priority'	=> 4,
		'label'		=> __( 'Text Color (hover)', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav li a:hover',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_menu_bg_active',
		'default'	=> '#ededed',
		'priority'	=> 5,
		'label'		=> __( 'Background Color (active)', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav .current-menu-item a',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_menu_text_active',
		'default'	=> '#3e3e3e',
		'priority'	=> 6,
		'label'		=> __( 'Text Color (active)', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav .current-menu-item a',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_dropdown_bg',
		'default'	=> '#ffffff',
		'priority'	=> 7,
		'label'		=> __( 'Dropdown Background Color', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav .sub-menu',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_dropdown_text',
		'default'	=> '#333333',
		'priority'	=> 8,
		'label'		=> __( 'Dropdown Text Color', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav .sub-menu li a',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_dropdown_bg_hover',
		'default'	=> '#ededed',
		'priority'	=> 9,
		'label'		=> __( 'Dropdown Background Color (hover)', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav .sub-menu li a:hover',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_dropdown_text_hover',
		'default'	=> '#ffffff',
		'priority'	=> 10,
		'label'		=> __( 'Dropdown Text Color (hover)', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav .sub-menu li a:hover',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_dropdown_border',
		'default'	=> '#ededed',
		'priority'	=> 11,
		'label'		=> __( 'Dropdown Border Color', 'tokopress' ),
		'section'	=> 'tokopress_header_primarymenu',
		'selector'	=> '.primary-navbar .navbar-nav .sub-menu li a',
		'property'	=> 'border-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_primarymenu_section_data' );

/**
 * Additional menu customizer data
 */
function tokopress_customizer_additionalmenu_data( $cf_colors ) {
	$cf_colors[] = array(
		'slug'		=> 'tokopress_additional_bg',
		'default'	=> '#ffffff',
		'priority'	=> 1,
		'label'		=> __( 'Background Color', 'tokopress' ),
		'section'	=> 'tokopress_header_additional',
		'selector'	=> '.site-header .additional-menu-block',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_additional_menu_bg',
		'default'	=> '#ffffff',
		'priority'	=> 2,
		'label'		=> __( 'Menu Background Color', 'tokopress' ),
		'section'	=> 'tokopress_header_additional',
		'selector'	=> '.additional-menu-block .additional-menu > li > a',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_additional_menu_text',
		'default'	=> '#8c8c8c',
		'priority'	=> 3,
		'label'		=> __( 'Menu Text Color', 'tokopress' ),
		'section'	=> 'tokopress_header_additional',
		'selector'	=> '.additional-menu-block .additional-menu > li > a',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_additional_menu_bg_hover',
		'default'	=> '#ededed',
		'priority'	=> 4,
		'label'		=> __( 'Menu Background Color (hover)', 'tokopress' ),
		'section'	=> 'tokopress_header_additional',
		'selector'	=> '.additional-menu-block .additional-menu > li > a:hover',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_additional_menu_text_hover',
		'default'	=> '#8c8c8c',
		'priority'	=> 5,
		'label'		=> __( 'Menu Text Color (hover)', 'tokopress' ),
		'section'	=> 'tokopress_header_additional',
		'selector'	=> '.additional-menu-block .additional-menu > li > a:hover',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_additionalmenu_data' );

/**
 * Header search form customizer data
 */
function tokopress_customizer_header_search_data( $cf_colors ) {
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_search_bg',
		'default'	=> '#ffffff',
		'priority'	=> 1,
		'label'		=> __( 'Background Color', 'tokopress' ),
		'section'	=> 'tokopress_header_search',
		'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .searchform',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_search_input_bg',
		'default'	=> '#ffffff',
		'priority'	=> 2,
		'label'		=> __( 'Input Background Color', 'tokopress' ),
		'section'	=> 'tokopress_header_search',
		'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .searchform input[type="text"]',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_search_input_text',
		'default'	=> '',
		'priority'	=> 3,
		'label'		=> __( 'Input Text Color', 'tokopress' ),
		'section'	=> 'tokopress_header_search',
		'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .searchform input[type="text"]',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_search_input_border',
		'default'	=> '#3e3e3e',
		'priority'	=> 4,
		'label'		=> __( 'Input Border Color', 'tokopress' ),
		'section'	=> 'tokopress_header_search',
		'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .searchform input[type="text"]',
		'property'	=> 'border-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_search_btn_bg',
		'default'	=> '',
		'priority'	=> 5,
		'label'		=> __( 'Button Background Color', 'tokopress' ),
		'section'	=> 'tokopress_header_search',
		'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .searchform .searchsubmit',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_search_btn_text',
		'default'	=> '#3e3e3e',
		'priority'	=> 6,
		'label'		=> __( 'Button Text Color', 'tokopress' ),
		'section'	=> 'tokopress_header_search',
		'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .searchform .searchsubmit',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);
	$cf_colors[] = array(
		'slug'		=> 'tokopress_header_search_btn_border',
		'default'	=> '#3e3e3e',
		'priority'	=> 7,
		'label'		=> __( 'Button Border Color', 'tokopress' ),
		'section'	=> 'tokopress_header_search',
		'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .searchform .searchsubmit, .additional-menu-block .additional-menu .dropdown-menu .searchform input[type="submit"]',
		'property'	=> 'border-color',
		'transport'	=> 'postMessage',
		'type'		=> 'color'
	);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_header_search_data' );

/**
 * Mini cart customizer data
 */
if( class_exists( 'woocommerce' ) ) {
	function tokopress_customizer_minicart_data( $cf_colors ) {
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_bg', 
			'default'	=> '#ffffff', 
			'priority'	=> 1, 
			'label'		=> __( 'Background Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart',
			'property'	=> 'background-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_border', 
			'default'	=> '#ffffff', 
			'priority'	=> 2, 
			'label'		=> __( 'Border Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart ul.product_list_widget li,.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart ul.product_list_widget li dl.variation',
			'property'	=> 'border-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_text', 
			'default'	=> '', 
			'priority'	=> 3, 
			'label'		=> __( 'Text Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_text_alt', 
			'default'	=> '', 
			'priority'	=> 4, 
			'label'		=> __( 'Text Color (alternative)', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart p',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_link', 
			'default'	=> '', 
			'priority'	=> 5, 
			'label'		=> __( 'Link Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart a',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_subtotal_bg', 
			'default'	=> '', 
			'priority'	=> 6, 
			'label'		=> __( 'Sub Total Background Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .total',
			'property'	=> 'background-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_subtotal_text', 
			'default'	=> '', 
			'priority'	=> 7, 
			'label'		=> __( 'Sub Total Text Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .total',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_btn_bg', 
			'default'	=> '', 
			'priority'	=> 8, 
			'label'		=> __( 'Button Background Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .buttons .button:first-child',
			'property'	=> 'background-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_btn_text', 
			'default'	=> '#aeaeae', 
			'priority'	=> 9, 
			'label'		=> __( 'Button Text Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .buttons .button:first-child',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_btn_border', 
			'default'	=> '#aeaeae', 
			'priority'	=> 10, 
			'label'		=> __( 'Button Border Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .buttons .button:first-child',
			'property'	=> 'border-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_btn_checkout_bg', 
			'default'	=> '', 
			'priority'	=> 11, 
			'label'		=> __( 'Button Background Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .buttons .button.checkout',
			'property'	=> 'background-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_btn_checkout_text', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 12, 
			'label'		=> __( 'Button Text Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .buttons .button.checkout',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
		$cf_colors[] = array( 
			'slug'		=> 'tokopress_minicart_btn_checkout_border', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 13, 
			'label'		=> __( 'Button Border Color', 'tokopress' ),
			'section'	=> 'tokopress_header_minicart',
			'selector'	=> '.additional-menu-block .additional-menu .dropdown-menu .widget_shopping_cart .buttons .button.checkout',
			'property'	=> 'border-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);

		return $cf_colors;
	}
	add_filter( 'cf_colors', 'tokopress_customizer_minicart_data' );
}

/**
 * Footer widget customizer data
 */
function tokopress_customizer_footerwidget_data( $cf_colors ) {
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_widget_bg', 
		'default'	=> '#2b2b2b', 
		'priority'	=> 1, 
		'label'		=> __( 'Widget Background', 'tokopress' ),
		'section'	=> 'tokopress_footer_widget',
		'selector'	=> '.site-footer',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_widget_heading', 
		'default'	=> '#ffffff', 
		'priority'	=> 2, 
		'label'		=> __( 'Widget Heading Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_widget',
		'selector'	=> '.site-footer .footer-widget .widget-inside .widget-title',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_widget_text', 
		'default'	=> '#a2a2a2', 
		'priority'	=> 3, 
		'label'		=> __( 'Widget Text Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_widget',
		'selector'	=> '.site-footer .footer-widget',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_widget_link', 
		'default'	=> '#a2a2a2', 
		'priority'	=> 4, 
		'label'		=> __( 'Widget Link Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_widget',
		'selector'	=> '.site-footer .footer-widget a',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_widget_border_color', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 5, 
		'label'		=> __( 'Widget List Border Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_widget',
		'selector'	=> '.site-footer .footer-widget .woocommerce ul.product_list_widget li, .site-footer .footer-widget .widget_recent_entries li, .site-footer .footer-widget .widget_categories li, .site-footer .footer-widget .widget_recent_comments li, .site-footer .footer-widget .widget_pages li, .site-footer .footer-widget .widget_archive li',
		'property'	=> 'border-color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_footerwidget_data' );

/**
 * Footer block customizer data
 */
function tokopress_customizer_footerblock_data( $cf_colors ) {
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_credit_bg', 
		'default'	=> '#ffffff', 
		'priority'	=> 1, 
		'label'		=> __( 'Footer Block Background Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.site-footer .footer-credit',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_credit_text', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 2, 
		'label'		=> __( 'Footer Block Text Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.site-footer .footer-credit .copy',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_credit_link', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 3, 
		'label'		=> __( 'Footer Block Link Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.site-footer .footer-credit .copy a',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_credit_hover', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 4, 
		'label'		=> __( 'Footer Block Link Color (Hover)', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.site-footer .footer-credit .copy a:hover',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);

	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_menu_text', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 5, 
		'label'		=> __( 'Menu Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.footer-navbar .navbar-nav li a',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_menu_hover', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 6, 
		'label'		=> __( 'Menu Color (Hover)', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.footer-navbar .navbar-nav li a:hover',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);

	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_social_text', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 7, 
		'label'		=> __( 'Social Color', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.site-footer .footer-credit .social a',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_footer_social_hover', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 8, 
		'label'		=> __( 'Social Color (Hover)', 'tokopress' ),
		'section'	=> 'tokopress_footer_block',
		'selector'	=> '.site-footer .footer-credit .social a:hover',
		'property'	=> 'color',
		'property2'	=> '!important',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_footerblock_data' );

/**
 * Pagination customizer data
 */
function tokopress_customizer_pagination_data( $cf_colors ) {
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_pagination_bg', 
		'default'	=> '#ffffff', 
		'priority'	=> 1, 
		'label'		=> __( 'Background Color', 'tokopress' ),
		'section'	=> 'tokopress_pagination',
		'selector'	=> '.pagination',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_pagination_text', 
		'default'	=> '#6b6b6b', 
		'priority'	=> 2, 
		'label'		=> __( 'Text Color', 'tokopress' ),
		'section'	=> 'tokopress_pagination',
		'selector'	=> '.pagination, .pagination span.page-numbers',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_pagination_link', 
		'default'	=> '#6b6b6b', 
		'priority'	=> 3, 
		'label'		=> __( 'Link Color', 'tokopress' ),
		'section'	=> 'tokopress_pagination',
		'selector'	=> '.pagination a, .pagination a.page-numbers',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_pagination_arrow_bg', 
		'default'	=> '', 
		'priority'	=> 4, 
		'label'		=> __( 'Prev & Next Button Background Color', 'tokopress' ),
		'section'	=> 'tokopress_pagination',
		'selector'	=> '.pagination .next, .pagination .prev',
		'property'	=> 'background-color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_pagination_arrow_text', 
		'default'	=> '#3e3e3e', 
		'priority'	=> 5, 
		'label'		=> __( 'Prev & Next Button Text Color', 'tokopress' ),
		'section'	=> 'tokopress_pagination',
		'selector'	=> '.pagination a.next, .pagination a.prev',
		'property'	=> 'color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);
	$cf_colors[] = array( 
		'slug'		=> 'tokopress_pagination_arrow_border', 
		'default'	=> '', 
		'priority'	=> 6, 
		'label'		=> __( 'Prev & Next Button Border Color', 'tokopress' ),
		'section'	=> 'tokopress_pagination',
		'selector'	=> '.pagination a.next, .pagination a.prev',
		'property'	=> 'border-color',
		'transport'	=> 'postMessage',
		'type' 		=> 'color'
	);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_pagination_data' );