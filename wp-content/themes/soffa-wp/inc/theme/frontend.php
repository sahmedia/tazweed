<?php
/**
 * TokoPress frontend
 *
 * @package TokoPress
 */

/**
 * Helper function to return the theme option value.
 * If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 */
if ( ! function_exists( 'of_get_option' ) ) :

function of_get_option( $name, $default = false ) {
	$config = get_option( 'optionsframework' );

	if ( ! isset( $config['id'] ) ) {
		return $default;
	}

	$options = get_option( $config['id'] );

	if ( isset( $options[$name] ) ) {
		return $options[$name];
	}

	return $default;
}

endif;

/**
 * Theme Breadcrumb
 */
tokopress_require_file( get_template_directory() . '/inc/theme/breadcrumb.php' );
function tokopress_breadcrumb() {
	breadcrumb_trail(
		array( 
			'container' => 'nav', 
			'separator' => '/', 
			'labels'    => array( 
				'browse' => '' 
			) 
		) 
	);
}

/**
 * Theme Title
 */
function tokopress_site_title() {
	
	if ( is_front_page() && get_option('show_on_front') == 'page' && get_option('page_on_front') > 0 ) {
		$id = get_option('page_on_front');
	}
	elseif ( is_home() && get_option('show_on_front') == 'page' && get_option('page_for_posts') > 0 ) {
		$id = get_option('page_for_posts');
	}
	elseif ( is_singular() ) {
		$id = get_the_ID();
	}
	else {
		$id = false;
	}
	
	if( $id && "" != get_post_meta( get_the_ID(), '_tokopress_page_title', true ) )
		return;

	if( $id && "title_image" == get_post_meta( get_the_ID(), '_tokopress_page_title_image', true ) ) {
		get_template_part( 'block-title-image' );
	} 
	else {
		get_template_part( 'block-title' );
	}
	
}
add_action( 'tokopress_before_content_wrapper', 'tokopress_site_title', 10 );

function tokopress_block_title_image() {
	echo '#site-title .site-header-image { background: url(' . get_header_image() . '); }#site-title .site-header-image h1 { color: #' . get_header_textcolor() . '; }';
}
add_action( 'tokopress_custom_styles', 'tokopress_block_title_image' );

function tokopress_sticky_header_body_class( $classes ) {
	$sticky = of_get_option( 'tokopress_no_sticky_header' ) ? false : true;
	if ( $sticky ) {
		$classes[] = 'sticky-header-yes';	
	}
	else {
		$classes[] = 'sticky-header-no';	
	}
	$classes[] = 'header-large';	
	return $classes;
}
add_filter( 'body_class', 'tokopress_sticky_header_body_class' );

/**
 * Pagination
 */
if ( ! function_exists( 'tokopress_paging_nav' ) ) :
function tokopress_paging_nav() {
	
	// Don't print empty markup if there's only one page.
	global $wp_query;
	
	if ( $wp_query->max_num_pages < 2 ) {
		return;
	}
	
	?>
	
	<div class="col-md-12">
		<nav class="pagination for-product cl">

				<?php
				
				echo paginate_links( array(
					'base' 			=> str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
					'format' 		=> '',
					'current' 		=> max( 1, get_query_var( 'paged' ) ),
					'total' 		=> $wp_query->max_num_pages,
					'prev_text' 	=> '&lsaquo; previous',
					'next_text' 	=> 'next &rsaquo;',
					'type'			=> 'plain',
					'end_size'		=> 3,
					'mid_size'		=> 3
				) );
				
				?>
		
		</nav><!-- .navigation -->
	</div>
	
	<?php

}
endif;

/**
 * Display Blog Meta
 */
function tokopress_post_meta() {

	get_template_part( 'block-post-meta' );

}

/**
 * Filter Page Title
 */
function tokopress_wp_title_for_home( $title )
{
	if( empty( $title ) && ( is_home() || is_front_page() ) ) {
	
		return __( 'Home', 'tokopress' );
	
	}
	
	return $title;
}
add_filter( 'wp_title', 'tokopress_wp_title_for_home', 10, 2 );

/**
 * Display Comment
 */
tokopress_require_file( get_template_directory() . '/block-comments.php' );

/**
 * Display Menu Navigation.
 */
function tokopress_menus( $location ) {

	if( 'primary' == $location ) {

		get_template_part( 'menu', 'primary' );

	} elseif( 'secondary' == $location ) {

		get_template_part( 'menu', 'secondary' );

	} else {

		get_template_part( 'menu', 'category' );

	}

}

/**
 * Tokopress Skill Member
 */
function tokopress_skill_member() {
	global $post;
	$members_skill = get_post_meta( $post->ID, '_tokopress_member_skill', true );

	if( "" != $members_skill ) {
	
		$filter_members_skill = str_replace( array("\n", "\r"), '', $members_skill );

		$loop_member_skill = explode( ",", $filter_members_skill );

		echo "\t" . '<div class="team-member-skill">' . "\n";

		echo "\t\t" . '<h3>' . __( 'SKILLS', 'tokopress' ) . '</h3>' . "\n";

		echo "\t\t" . '<div class="team-member-skill-contain">' . "\n";

		foreach ( $loop_member_skill as $member_skill_data ) {

			$skill_data = explode( ":", $member_skill_data );
			echo "\t\t\t" . '<div class="skill-meter">' . "\n";
		    echo "\t\t\t\t" . '<span class="skill-data" style="width: ' .$skill_data[1]. '%;"></span>' . "\n";
		    echo "\t\t\t\t" . '<span class="skill-title">' . $skill_data[0] . '</span>';
			echo "\t\t\t" . '</div>' . "\n";
		}

		echo "\t\t" . '</div>' . "\n";

		echo "\t" . '</div>' . "\n";

	}
}

/**
 * Tokopress Connect Member
 */
function tokopress_connect_member() {
	
	global $post;
	$members_twitter = get_post_meta( $post->ID, '_tokopress_member_connect_twitter', true );
	$members_facebook = get_post_meta( $post->ID, '_tokopress_member_connect_facebook', true );
	$members_gplus = get_post_meta( $post->ID, '_tokopress_member_connect_gplus', true );

	if( "" != $members_twitter || "" != $members_facebook || "" != $members_gplus ) {

		echo "\t" . '<div class="team-member-social">';

		echo "\t\t" . '<h3>' . __( 'CONNECT WITH', 'tokopress' ) . ' ' . strtoupper( get_the_title() ) . '</h3>' . "\n";

		echo "\t\t" . '<div class="team-member-social-contain">' . "\n";

		echo "\t\t\t";

			if( "" != $members_twitter )
				echo '<a href="' . $members_twitter . '"><i class="fa fa-twitter"></i></a>';
			if( "" != $members_facebook )
				echo '<a href="' . $members_facebook . '"><i class="fa fa-facebook"></i></a>';
			if( "" != $members_gplus )
				echo '<a href="' . $members_gplus . '"><i class="fa fa-google-plus"></i></a>';

		echo "\t\t" . '</div>' . "\n";
		
		echo "\t" . '</div>';

	}

}

/**
 * Header script
 */
function tokopress_header_script() {
	if( "" !== of_get_option( 'tokopress_header_script' ) ) {
		echo '<script type="text/javascript">';
		echo of_get_option( 'tokopress_header_script' );
		echo '</script>';
	}
}
add_action( 'wp_head', 'tokopress_header_script', 999 );

/**
 * Footer script
 */
function tokopress_footer_script() {
	if( "" !== of_get_option( 'tokopress_footer_script' ) ) {
		echo '<script type="text/javascript">';
		echo of_get_option( 'tokopress_footer_script' );
		echo '</script>';
	}
}
add_action( 'wp_footer', 'tokopress_footer_script', 999 );
