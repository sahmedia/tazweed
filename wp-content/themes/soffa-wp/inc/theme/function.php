<?php
/**
 * TokoPress Define Function
 *
 * @package tokopress
 */

/**
 * Tokopress Custom Header
 */
$args = array(
	'flex-width'    => true,
	'width'         => 1200,
	'flex-height'    => true,
	'height'        => 150,
	'default-image' => get_template_directory_uri() . '/img/dummy/custom-header.png',
	'uploads'       => true,
	'default-text-color'=> '333333'
);
add_theme_support( 'custom-header', $args );

/**
 * Tokopress Custom Backgorund
 */
$custom_bg = array(
	'default-color' => 'ededed',
	'default-image' => '',
);
add_theme_support( 'custom-background', $custom_bg );

/**
 * Queue some JavaScript code to be output in the footer.
 * @param string $code
 * @source WooCommerce Core Function
 */
function toko_enqueue_js( $code ) {
	global $toko_queued_js;

	if ( empty( $toko_queued_js ) ) {
		$toko_queued_js = '';
	}

	$toko_queued_js .= "\n" . $code . "\n";
}
/**
 * Output any queued javascript code in the footer.
 */
add_action( 'wp_footer', 'toko_print_js', 50 );
function toko_print_js() {
	global $toko_queued_js;

	if ( ! empty( $toko_queued_js ) ) {

		echo "<!-- TokoPress JavaScript -->\n<script type=\"text/javascript\">\njQuery(function($) {";

		// Sanitize
		$toko_queued_js = wp_check_invalid_utf8( $toko_queued_js );
		$toko_queued_js = preg_replace( '/&#(x)?0*(?(1)27|39);?/i', "'", $toko_queued_js );
		$toko_queued_js = str_replace( "\r", '', $toko_queued_js );

		printf( '%s', $toko_queued_js . "});\n</script>\n" );

		unset( $toko_queued_js );
	}
}

/* Fix notif position */
add_action( 'admin_head', 'tokopress_fix_notice_position' );
function tokopress_fix_notice_position() {
	echo '<style>#update-nag, .update-nag { display: block; float: none; }</style>';
}