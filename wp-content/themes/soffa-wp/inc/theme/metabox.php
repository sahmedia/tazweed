<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category Post Type
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'cmb_tokopress_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_tokopress_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_tokopress_';

	$meta_boxes['tokopress_page_opt_metabox'] = array(
		'id'         => 'tokopress_page_opt_metabox',
		'title'      => __( 'Page Option', 'tokopress' ),
		'pages'      => array( 'page', 'post' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array(
			array(
				'id'   => $prefix . 'page_title',
				'name' => 'DISABLE Title',
    			'desc' => 'DISABLE Title on this page.',
				'type' => 'checkbox',
			),
			array(
				'id'   => $prefix . 'page_title_image',
				'name' => 'Title Style',
    			'desc' => 'Choose title style in this page.',
				'type' => 'select',
				'options'	=> array(
						'title_Breadcrumb'	=> __( 'Title + Breadcrumb', 'tokopress' ),
						'title_image'		=> __( 'Title + Custom Header', 'tokopress' )
					),
				'allow_none'	=> false
			)
		)
	);

	$meta_boxes['member_description_metabox'] = array(
		'id'         => 'member_description_metabox',
		'title'      => __( 'Member Description', 'tokopress' ),
		'pages'      => array( 'team_members' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array(
			array(
				'id'   	=> $prefix . 'member_description',
				'name'	=> '',
				'type' 	=> 'wysiwyg',
			),
		)
	);

	$meta_boxes['member_detail_metabox'] = array(
		'id'         => 'member_detail_metabox',
		'title'      => __( 'Member Details', 'tokopress' ),
		'pages'      => array( 'team_members' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array(
			array(
				'id'   => $prefix . 'member_email',
				'name' => __( 'Avatar E-mail address', 'tokopress' ),
				'desc' => sprintf( __( 'Enter in an e-mail address to use <a href="%s">Gravatar</a> or use featured image to bring up image.', 'tokopress' ), 'http://gravatar.com/' ),
				'type' => 'text_email',
			),
			array(
				'id'   => $prefix . 'member_role',
				'name' => __( 'Role', 'tokopress' ),
				'desc' => __( 'Enter a role for your team member. Example "Director of Production".', 'tokopress' ),
				'type' => 'text',
			),
			array(
				'id'   => $prefix . 'member_url',
				'name' => __( 'URL', 'tokopress' ),
				'desc' => __( 'Enter an URL team member. Example <i>http://tokopress.com/</i>.', 'tokopress' ),
				'type' => 'text_url',
			),
		)
	);

	$meta_boxes['member_connect_account_metabox'] = array(
		'id'         => 'member_connect_account_metabox',
		'title'      => __( 'Member Connect Account', 'tokopress' ),
		'pages'      => array( 'team_members' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array(
			array(
				'id'   => $prefix . 'member_connect_twitter',
				'name' => __( 'Twitter URL', 'tokopress' ),
				'desc' => __( 'Enter team member twitter url.', 'tokopress' ),
				'type' => 'text_url',
			),
			array(
				'id'   => $prefix . 'member_connect_facebook',
				'name' => __( 'Facebook URL', 'tokopress' ),
				'desc' => __( 'Enter team member facebook url".', 'tokopress' ),
				'type' => 'text_url',
			),
			array(
				'id'   => $prefix . 'member_connect_gplus',
				'name' => __( 'g Plus URL', 'tokopress' ),
				'desc' => __( 'Enter team member gPlus url.', 'tokopress' ),
				'type' => 'text_url',
			),
		)
	);

	$meta_boxes['member_skill_metabox'] = array(
		'id'         => 'member_skill_metabox',
		'title'      => __( 'Member Skill', 'tokopress' ),
		'pages'      => array( 'team_members' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array(
			array(
				'id'   	=> $prefix . 'member_skill',
				'name'	=> '',
				'desc' 	=> __( 'Enter skill team member. Ex. PHP Programe:80,Web Design:90. Use ":" for Input Value and use "," for add multi skill.', 'tokopress' ),
				'type' 	=> 'textarea',
			),
		)
	);

	return $meta_boxes;

}