<?php
/**
 * TokoPress Theme Options
 *
 * @package tokopress
 */


/*
 * Load Option Framework
 */
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/options-framework/' );
tokopress_require_file( get_template_directory() . '/inc/options-framework/options-framework.php' );

/**
 * Set Option Name For Option Framework
 */
function optionsframework_option_name() {
	$optionsframework_settings = get_option( 'optionsframework' );
	if ( defined( 'THEME_NAME' ) ) {
		$optionsframework_settings['id'] = THEME_NAME;
	}
	else {
		$themename = wp_get_theme();
		$themename = preg_replace("/\W/", "_", strtolower($themename) );
		$optionsframework_settings['id'] = $themename;
	}
	update_option( 'optionsframework', $optionsframework_settings );

    $defaults = optionsframework_defaults();
	add_option( $optionsframework_settings['id'], $defaults, '', 'yes' );
}

/**
 * Get Default Options For Option Framework
 */
function optionsframework_defaults() {
    $options = null;
    $location = apply_filters( 'options_framework_location', array('options.php') );
    if ( $optionsfile = locate_template( $location ) ) {
        $maybe_options = require_once $optionsfile;
        if ( is_array( $maybe_options ) ) {
			$options = $maybe_options;
        } else if ( function_exists( 'optionsframework_options' ) ) {
			$options = optionsframework_options();
		}
    }
    $options = apply_filters( 'of_options', $options );
    $defaults = array();
    if ( !empty( $options ) ) {
	    foreach ($options as $key => $value) {
	    	if( isset($value['id']) && isset($value['std']) ) {
	    		$defaults[$value['id']] = $value['std'];
	    	}
	    }
	}
    return $defaults;
}

/**
 * Load Custom Style For Option Framework
 */
function tokopress_style_option_framework() {
	wp_enqueue_style( 'style-option-framework', get_template_directory_uri() . '/css/options-framework.css' );
}
add_action( 'optionsframework_custom_scripts', 'tokopress_style_option_framework' );

/**
 * Load Theme Options
 */
add_filter( 'of_options', 'tokopress_header_settings', 10 );
add_filter( 'of_options', 'tokopress_footer_settings', 20 );
add_filter( 'of_options', 'tokopress_home_settings', 25 );
add_filter( 'of_options', 'tokopress_blog_settings', 25 );
add_filter( 'of_options', 'tokopress_social_settings', 30 );
add_filter( 'of_options', 'tokopress_contact_settings', 40 );
add_filter( 'of_options', 'tokopress_misc_settings', 50 );

/**
 * Contact Tab Options
 */
function tokopress_contact_settings( $options ) {
	$options[] =array(
		'name' => __( 'Contact Map', 'tokopress' ),
		'type' => 'heading'
	);

		$options[] = array(
			'name'=> __( 'Latitude', 'tokopress' ),
			'desc'=> __( 'Insert map Latitude koordinat', 'tokopress' ),
			'id'=> 'tokopress_contact_lat',
			'type'=> 'text',
			'std'=> '-6.903932'
		);
		$options[] = array(
			'name'=> __( 'Longitude', 'tokopress' ),
			'desc'=> __( 'Insert map Longitude koordinat', 'tokopress' ),
			'id'=> 'tokopress_contact_long',
			'type'=> 'text',
			'std'=> '107.610344'
		);
		$options[] = array(
			'name'=> __( 'Marker Title', 'tokopress' ),
			'desc'=> __( 'Insert marker title', 'tokopress' ),
			'id'=> 'tokopress_contact_marker_title',
			'type'=> 'text',
			'std'=> 'Marker Title'
		);
		$options[] = array(
			'name'=> __( 'Marker Description', 'tokopress' ),
			'desc'=> __( 'Insert marker description', 'tokopress' ),
			'id'=> 'tokopress_contact_marker_desc',
			'type'=> 'textarea',
			'std'=> 'Marker Content'
		);
		$options[] = array(
			'name'=> __( 'Google Maps API Key (optional)', 'tokopress' ),
			'desc'=> __( 'Usage of the Google Maps APIs now requires a key if your domain was not active prior to June 22nd, 2016.', 'tokopress' ).' <br/><a href="https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key">'.__( 'Click here to get your Google Maps API key', 'tokopress' ).'</a>',
			'id'=> 'tokopress_contact_apikey',
			'type'=> 'text',
			'std'=> ''
		);

	return $options;
}

/**
 * Social Tab Options
 */
function tokopress_social_settings( $options ) {
	$options[] = array(
		'name' => __( 'Social', 'tokopress' ),
		'type' => 'heading'
	);

		$options[] = array(
			'name' => __( 'DISABLE Social icons', 'tokopress' ),
			'desc' => __( 'DISABLE social icons in footer', 'tokopress' ),
			'id' => 'tokopress_hide_social',
			'type' => 'checkbox'
		);

		$socials = array(
			'' 				=> '&nbsp;',
			'rss' 			=> 'RSS Feed',
			'facebook' 		=> 'Facebook',
			'twitter' 		=> 'Twitter',
			'google-plus' 	=> 'Google+',
			'youtube' 		=> 'Youtube',
			'tumblr'		=> 'Tumblr',
			'instagram'		=> 'Instagram',
			'linkedin'		=> 'Linkedin',
			'pinterest'		=> 'Pinterest',
			'github'		=> 'Github',
			'flickr'		=> 'Flickr',
			'vimeo-square'	=> 'Vimeo'
		);

		$options[] = array(
			'name' => __( 'Social #1', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_1',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #1', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_1_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #2', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_2',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #2', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_2_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #3', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_3',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #3', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_3_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #4', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_4',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #4', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_4_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #5', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_5',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #5', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_5_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #6', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_6',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #6', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_6_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #7', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_7',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #7', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_7_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #8', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_8',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #8', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_8_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #9', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_9',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #9', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_9_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #10', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_10',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #10', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_10_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #11', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_11',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #11', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_11_url',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Social #12', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_12',
			'type' => 'select',
			'options' => $socials
		);
		$options[] = array(
			'name' => __( 'Social URL #12', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_social_12_url',
			'type' => 'text'
		);

	return $options;
}

/**
 * Footer Tab Options
 */
function tokopress_footer_settings( $options ) {
	
	$this_theme = wp_get_theme();

	$options[] = array(
		'name' => __( 'Footer', 'tokopress' ),
		'type' => 'heading'
	);

		$options[] = array(
			'name' => __( 'Footer Widget', 'tokopress' ),
			'desc' => __( 'DISABLE footer widget', 'tokopress' ),
			'id' => 'tokopress_hide_footer_widget',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'Footer', 'tokopress' ),
			'desc' => __( 'DISABLE footer', 'tokopress' ),
			'id' => 'tokopress_hide_footer',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'Footer Text', 'tokopress' ),
			'desc' => __( 'insert text here, you can use HTML tag.', 'tokopress' ),
			'id' => 'tokopress_footer_text',
			'std' => __( 'Copyright &copy;', 'tokopress' ) . ' ' . $this_theme->get( 'Name' ),
			'type' => 'textarea'
		);

		$options[] = array(
			'name' => __( 'Footer Menu', 'tokopress' ),
			'desc' => __( 'DISABLE footer menu', 'tokopress' ),
			'id' => 'tokopress_hide_footer_menu',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' 	=> __( 'Footer Script', 'tokopress' ),
			'desc' 	=> '',
			'id' 	=> 'tokopress_footer_script',
			'std' 	=> '',
			'type' 	=> 'textarea'
		);

	return $options;
}

/**
 * Home Tab Options
 */
function tokopress_home_settings( $options ) {
	
	$options[] = array(
		'name' => __( 'Home', 'tokopress' ),
		'type' => 'heading'
	);

		$options[] = array(
			'name' => __( 'Frontpage Bottom Widget Area', 'tokopress' ),
			'desc' => __( 'DISABLE frontpage bottom widget', 'tokopress' ),
			'id' => 'tokopress_hide_home_widget',
			'type' => 'checkbox'
		);

	return $options;
}

/**
 * Blog Tab Options
 */
function tokopress_blog_settings( $options ) {
	
	$options[] = array(
		'name' => __( 'Blog', 'tokopress' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Blog Page', 'tokopress' ),
		'type' => 'info'
	);

		$options[] = array(
			'name' => __( 'Post Columns Per Row', 'tokopress' ),
			'desc' => '',
			'id' => 'tokopress_blog_column_per_row',
			'std' => '2',
			'type' => 'select',
			'options' => array(
					'2' => '2',
					'3' => '3',
					'4' => '4'
				)
		);
		$options[] = array(
			'name' => __( 'Blog Sidebar', 'tokopress' ),
			'desc' => __( 'DISABLE sidebar in blog page', 'tokopress' ),
			'id' => 'tokopress_hide_blog_sidebar',
			'type' => 'checkbox'
		);

	$options[] = array(
		'name' => __( 'Single Post Page', 'tokopress' ),
		'type' => 'info'
	);

		$options[] = array(
			'name' => __( 'Single Post Sidebar', 'tokopress' ),
			'desc' => __( 'DISABLE sidebar in single post page', 'tokopress' ),
			'id' => 'tokopress_hide_post_sidebar',
			'type' => 'checkbox'
		);

	return $options;
}

/**
 * Header Tab Options
 */
function tokopress_header_settings( $options ) {

	$options[] = array(
		'name' => __( 'Header', 'tokopress' ),
		'type' => 'heading'
	);

		$options[] = array(
			'name' => __( 'Sticky Header', 'tokopress' ),
			'desc' => __( 'DISABLE Sticky Header in Desktop', 'tokopress' ),
			'id' => 'tokopress_no_sticky_header',
			'type' => 'checkbox'
		);

		if ( !function_exists( 'the_custom_logo' ) ) {
			$options[] = array(
				'name' => __( 'Site Logo', 'tokopress' ),
				'desc' => '',
				'id' => 'tokopress_logo',
				'type' => 'upload'
			);
		}

		$options[] = array(
			'name' => __( 'Site Description', 'tokopress' ),
			'desc' => __( 'DISABLE Site Description', 'tokopress' ),
			'id' => 'tokopress_hide_header_desc',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'Header Menu', 'tokopress' ),
			'desc' => __( 'DISABLE Header Menu', 'tokopress' ),
			'id' => 'tokopress_hide_header_menu',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'Header Search', 'tokopress' ),
			'desc' => __( 'DISABLE Header Search', 'tokopress' ),
			'id' => 'tokopress_hide_header_search',
			'type' => 'checkbox'
		);

		if ( class_exists( 'woocommerce') ) {
			$options[] = array(
				'name' => __( 'Header Mini Cart', 'tokopress' ),
				'desc' => __( 'DISABLE Header Mini Cart', 'tokopress' ),
				'id' => 'tokopress_hide_header_minicart',
				'type' => 'checkbox'
			);
		}

		$options[] = array(
			'name' 	=> __( 'Header Script', 'tokopress' ),
			'desc' 	=> '',
			'id' 	=> 'tokopress_header_script',
			'std' 	=> '',
			'type' 	=> 'textarea'
		);

	return $options;
}

/**
 * Misc Tab Options
 */
function tokopress_misc_settings( $options ) {
	$options[] =array(
		'name' => __( 'Misc', 'tokopress' ),
		'type' => 'heading'
	);

		$options[] = array(
			'name' 	=> __( 'ENABLE Visual Composer License Page', 'tokopress' ),
			'desc' 	=> __( 'ENABLE Visual Composer License Page. It is useful if you purchase Visual Composer license to get direct plugin updates and official support from Visual Composer developer.', 'tokopress' ),
			'id' 	=> 'tokopress_enable_vc_license',
			'std' 	=> '0',
			'type' 	=> 'checkbox'
		);

	return $options;
}