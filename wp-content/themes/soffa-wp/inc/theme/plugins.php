<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * @package	   TGM-Plugin-Activation
 * @subpackage Plugins
 * @author	   Thomas Griffin <thomas@thomasgriffinmedia.com>
 * @author	   Gary Jones <gamajo@gamajo.com>
 * @copyright  Copyright (c) 2012, Thomas Griffin
 * @license	   http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/inc/tgm/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'tokopress_register_required_plugins' );
function tokopress_register_required_plugins() {

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
		$plugins = array(

					/* Required Plugin */
					array(
						'name'		=> 'WooCommerce',
						'slug'		=> 'woocommerce',
						'required'	=> true,
					),

					array(
						'name'     	=> 'Tokopress Shortcode',
						'slug'     	=> 'tokopress-shortcode',
						'source'   	=> get_template_directory() . '/inc/plugins/tokopress-shortcode-v2.0.zip',
						'version'	=> '2.0',
						'required' 	=> true,
					),
					array(
						'name'     	=> 'Tokopress Widget Shortcode',
						'slug'     	=> 'tokopress-widget-shortcode',
						'source'   	=> get_template_directory() . '/inc/plugins/tokopress-widget-shortcode-v2.0.zip',
						'version'	=> '2.0',
						'required' 	=> true,
					),
					array(
						'name'     	=> 'Tokopress Image Slider Shortcode',
						'slug'     	=> 'tokopress-image-slider-shortcode',
						'source'   	=> get_template_directory() . '/inc/plugins/tokopress-image-slider-shortcode-v2.0.zip',
						'version'	=> '2.0',
						'required' 	=> true,
					),
					array(
						'name'     	=> 'Tokopress Heading Typography Shortcode',
						'slug'     	=> 'tokopress-heading-typography-shortcode',
						'source'   	=> get_template_directory() . '/inc/plugins/tokopress-heading-typography-shortcode-v2.0.zip',
						'version'	=> '2.0',
						'required' 	=> true,
					),

					array(
						'name'     	=> 'Revolution Slider',
						'slug'     	=> 'revslider',
						'source'   	=> get_template_directory() . '/inc/plugins/revslider-v5.2.6.zip',
						'version'	=> '5.2.6',
						'required' 	=> false,
					),
					array(
						'name'     	=> 'Visual Composer',
						'slug'     	=> 'js_composer',
						'source'   	=> get_template_directory() . '/inc/plugins/js_composer-v4.12.zip',
						'version'	=> '4.12',
						'required' 	=> true,
					),
					array(
						'name'     	=> 'Team Members by Tokopress',
						'slug'     	=> 'tokopress-team-members',
						'source'   	=> get_template_directory() . '/inc/plugins/tokopress-team-members-v2.0.zip',
						'version'	=> '2.0',
						'required' 	=> true,
					),
					
					array(
						'name'     	=> 'Projects by WooThemes',
						'slug'     	=> 'projects-by-woothemes',
						'required' 	=> true,
					),
					array(
						'name'     	=> 'Testimonials by WooThemes',
						'slug'     	=> 'testimonials-by-woothemes',
						'required' 	=> true,
					),

					array(
						'name'		=> 'MailChimp for WordPress',
						'slug'		=> 'mailchimp-for-wp',
						'required'	=> true,
					),

					/* Recommended Plugin */
					array(
						'name'     	=> 'Toko Sliders',
						'slug'     	=> 'toko-sliders',
						'source'   	=> get_template_directory() . '/inc/plugins/toko-sliders-v1.4.0.zip',
						'version'	=> '1.4.0',
						'required' 	=> false,
					),
					array(
						'name'     	=> 'WooCommerce Product Enquiry by XtensionPress',
						'slug'     	=> 'woocommerce-product-enquiry-by-xtensionpress',
						'source'   	=> get_template_directory() . '/inc/plugins/woocommerce-product-enquiry-by-xtensionpress-v1.0.zip',
						'required' 	=> false,
					),
					array(
						'name'     	=> 'WooCommerce Product Quotation by XtensionPress',
						'slug'     	=> 'woocommerce-product-quotation-by-xtensionpress',
						'source'   	=> get_template_directory() . '/inc/plugins/woocommerce-product-quotation-by-xtensionpress-v1.0.zip',
						'required' 	=> false,
					),
					
					array(
						'name'		=> 'WooCommerce Wishlist',
						'slug'		=> 'yith-woocommerce-wishlist',
						'required'	=> false,
					),
					array(
						'name'		=> 'WooCommerce Compare',
						'slug'		=> 'yith-woocommerce-compare',
						'required'	=> false,
					),
					
					array(
						'name'		=> 'WordPress Importer',
						'slug'		=> 'wordpress-importer',
						'source'   	=> get_template_directory() . '/inc/plugins/wordpress-importer-v2.0.zip',
						'version' 	=> '2.0',
						'required' 	=> false,
					),
					
					array(
						'name'		=> 'Widget Importer Exporter',
						'slug'		=> 'widget-importer-exporter',
						'required'	=> false,
					),
					
					array(
						'name'		=> 'Regenerate Thumbnails',
						'slug'		=> 'regenerate-thumbnails',
						'required'	=> false,
					),
				);

	$config = array(
		'id'           => 'toko-tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'toko-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );

}

/* Set Visual Composer as Theme part and disable Visual Composer Updater */
if ( !of_get_option( 'tokopress_enable_vc_license' ) ) {
	/* Set Visual Composer as Theme part and disable Visual Composer Updater */
	add_action( 'vc_before_init', 'toko_vc_set_as_theme', 9 );
	function toko_vc_set_as_theme() {
		if ( function_exists( 'vc_set_as_theme' ) ) {
			vc_set_as_theme(true);
			vc_manager()->disableUpdater(true);
		}
	}
}

/* Set Revolution Slider as Theme part and disable Revolution Slider Updater */
if ( function_exists( 'set_revslider_as_theme' ) ) {
	set_revslider_as_theme();
}