<?php
/**
 * TokoPress Blog Post Widget
 *
 * @package TokoPress
 */

// Creating the widget 
class tokopress_post_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'tokopress_post_widget', 

			// Widget name will appear in UI
			__('Tokopress Recent Post Widget', 'tokopress'), 

			// Widget description
			array( 'description' => __( 'Display wordpress recent post on your widget.', 'tokopress' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		$title_args = ( isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : __( 'Recent Post', 'tokopress' ) );
		$limit_args = ( isset( $instance[ 'limit' ] ) ? $instance[ 'limit' ] : '1' );
		$imgSize_args = ( isset( $instance[ 'imgSize' ] ) ? $instance[ 'imgSize' ] : 'thumbnail' );
		$imgPosition_args = ( isset( $instance[ 'imgPosition' ] ) ? $instance[ 'imgPosition' ] : 'top' );
		$hide_description_args = ( isset( $instance[ 'hide_description' ] ) ? $instance[ 'hide_description' ] : '0' );


		$title = apply_filters( 'widget_title', $title_args );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		$limit = (int)( $limit_args );
		$imgSize = $imgSize_args;
		$imgPosition = $imgPosition_args;
		$hide_description = $hide_description_args;

		// This is where you run the code and display the output
		$post_args = array( 
			//Type & Status Parameters
    		'post_type'   => 'post',
    		'post_status' => 'publish',
    		
    		//Order & Orderby Parameters
    		'order'     => 'DESC',
    		'orderby'	=> 'date',
    		
    		//Pagination Parameters
    		'posts_per_page'      => $limit,
    		'ignore_sticky_posts' => 1
		);

	    $post_query = new WP_Query( $post_args );	    

	    if( $post_query->have_posts() ) {
	    	while ( $post_query->have_posts() ) {
	    		$post_query->the_post();

	    		echo "\n" . '<div class="tp-widget-loop img-position-' . $imgPosition . '">' . "\n";

	    		if( has_post_thumbnail() ) {
	    			echo "\t" . '<div class="tp-widget-post-thumb"><a href="' . get_permalink() . '">' . get_the_post_thumbnail(get_the_ID(), $imgSize) . '</a></div>' . "\n";
	    		} else {
	    			echo "\t" . '<div class="tp-widget-post-thumb"><a href="' . get_permalink() . '"><img src="http://placehold.it/150x150" alt="post-thumbnail"></a></div>' . "\n";
	    		}

	    		echo "\t\t" . '<div class="tp-widget-post-wrap">' . "\n";
	    		echo "\t\t\t" . '<div class="tp-widget-post-date">' . get_the_date() . '</div>' . "\n";
	    		echo "\t\t\t" . '<div class="tp-widget-post-title"><h3><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3></div>' . "\n";
	    		
	    		if( "1" != $hide_description )
	    			echo "\t\t\t" . '<div class="tp-widget-post-summary">' . text_limiter( get_the_content(), 250 ) . '</div>' . "\n";
	    		
	    		echo "\t\t" . '</div>' . "\n";

	    		echo '</div>';
	    	}
	    }
	    
	    wp_reset_postdata();

		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {

		$title = ( isset( $instance[ 'title' ] ) ? $instance[ 'title' ] : __( 'Recent Post', 'tokopress' ) );
		$limit = ( isset( $instance[ 'limit' ] ) ? $instance[ 'limit' ] : '1' );
		$imgSize = ( isset( $instance[ 'imgSize' ] ) ? $instance[ 'imgSize' ] : 'thumbnail' );
		$imgPosition = ( isset( $instance[ 'imgPosition' ] ) ? $instance[ 'imgPosition' ] : 'top' );
		$hide_description = ( isset( $instance[ 'hide_description' ] ) ? $instance[ 'hide_description' ] : '0' );
		// Widget admin form
		?>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'tokopress' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>"><?php _e( 'Limit:', 'tokopress' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" value="<?php echo $limit; ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'imgSize' ) ); ?>"><?php _e( 'Image Size:', 'tokopress' ); ?></label>
			<select name="<?php echo esc_attr( $this->get_field_name( 'imgSize' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'imgSize' ) ); ?>">
				<option <?php selected( $imgSize, 'thumbnail' ); ?> value="thumbnail">Thumbnail</option>
				<option <?php selected( $imgSize, 'medium' ); ?> value="medium">Medium</option>
				<option <?php selected( $imgSize, 'large' ); ?> value="large">Large</option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'imgPosition' ) ); ?>"><?php _e( 'Image Position', 'tokopress' ); ?></label>
			<select name="<?php echo esc_attr( $this->get_field_name( 'imgPosition' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'imgPosition' ) ); ?>">
				<option <?php selected( $imgPosition, 'top' ) ?> value="top">Top</option>
				<option <?php selected( $imgPosition, 'left' ) ?> value="left">Left</option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'hide_description' ) ); ?>"><?php _e( 'Hide Description', 'tokopress' ); ?></label>
			<input id="<?php echo $this->get_field_id('hide_description'); ?>" name="<?php echo $this->get_field_name('hide_description'); ?>" type="checkbox" value="1" <?php checked( '1', $hide_description ); ?> />
		</p>
		
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['limit'] = (int)( $new_instance['limit'] );
		$instance['imgSize'] = ( ! empty( $new_instance['imgSize']) ? strip_tags( $new_instance['imgSize'] ) : 'thumbnail' );
		$instance['imgPosition'] = ( ! empty( $new_instance['imgPosition'] ) ? strip_tags( $new_instance['imgPosition'] ) : 'top' );
		$instance['hide_description'] = ( "1" != $new_instance['hide_description'] ) ? '0' : strip_tags($new_instance[ 'hide_description' ]);

		return $instance;
	}
} // Class tokopress_blog_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'tokopress_post_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );