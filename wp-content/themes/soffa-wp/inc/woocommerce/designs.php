<?php
/**
 * WooCommerce Designs
 *
 * @package TokoPress
 */

/**
 * Setup Customizer Woocommerce Panel
 */
function tokopress_customizer_woocommerce_panel_setup( $cf_panels ) {
	$cf_panels[] = array(
		'ID'			=> 'tokopress_woo_panels',
		'title'			=> __( 'WooCommerce Sections', 'tokopress' ),
		'description'	=> __( 'Customize your woocommerce in here', 'tokopress' ),
		'priority'		=> 160
	);

	return $cf_panels;
}
add_filter( 'cf_panels', 'tokopress_customizer_woocommerce_panel_setup' );

/**
 * Setup Customizer WooCommerce Sections
 */
function tokopress_customizer_woocommerce_section_setup( $cf_sections ) {
	/**
	 * General section
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_woo_general',
		'label'		=> __( 'General', 'tokopress' ),
		'priority'	=> 1,
		'panel_id'	=> 'tokopress_woo_panels'
	);

	/**
	 * Product Items section
	 */
	$cf_sections[] = array(
		'slug'		=> 'tokopress_woo_items',
		'label'		=> __( 'Product Items', 'tokopress' ),
		'priority'	=> 2,
		'panel_id'	=> 'tokopress_woo_panels'
	);

	return $cf_sections;
}
add_filter( 'cf_sections', 'tokopress_customizer_woocommerce_section_setup' );

/**
 * General customizer data
 */
function tokopress_customizer_general_data( $cf_colors ) {
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_shop_ordering_bg', 
			'default'	=> '#ffffff', 
			'priority'	=> 1, 
			'label'		=> __( 'Catalog Ordering Background Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .wc-shop-ordering, .woocommerce-page .wc-shop-ordering',
			'property'	=> 'background-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_shop_ordering_text', 
			'default'	=> '#6b6b6b', 
			'priority'	=> 2, 
			'label'		=> __( 'Catalog Ordering Text Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .wc-shop-ordering, .woocommerce-page .wc-shop-ordering, .woocommerce .wc-shop-ordering, .woocommerce-page .wc-shop-ordering p',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_shop_ordering_border', 
			'default'	=> '#dedede', 
			'priority'	=> 3, 
			'label'		=> __( 'Catalog Ordering Background Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .wc-shop-ordering, .woocommerce-page .wc-shop-ordering',
			'property'	=> 'border-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_shop_ordering_select_bg', 
			'default'	=> '', 
			'priority'	=> 4, 
			'label'		=> __( 'Select Form Background Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .wc-shop-ordering .woocommerce-ordering .orderby, .woocommerce-page .wc-shop-ordering .woocommerce-ordering .orderby',
			'property'	=> 'background-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_shop_ordering_select_text', 
			'default'	=> '', 
			'priority'	=> 5, 
			'label'		=> __( 'Select Form Text Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .wc-shop-ordering .woocommerce-ordering .orderby, .woocommerce-page .wc-shop-ordering .woocommerce-ordering .orderby',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_shop_ordering_select_border', 
			'default'	=> '#dedede', 
			'priority'	=> 6, 
			'label'		=> __( 'Select Form Border Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .wc-shop-ordering .woocommerce-ordering .orderby, .woocommerce-page .wc-shop-ordering .woocommerce-ordering .orderby',
			'property'	=> 'border-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);

	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_button_bg', 
			'default'	=> '', 
			'priority'	=> 7, 
			'label'		=> __( 'Button (background)', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce #content input.button, .woocommerce #response input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce-page #content input.button, .woocommerce-page #response input#submit, .woocommerce-page a.button, .woocommerce-page button.button, .woocommerce-page input.button',
			'property'	=> 'background-color',
			'property2'	=> '!important',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_button_text', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 8, 
			'label'		=> __( 'Button (Text Color)', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce #content input.button, .woocommerce #response input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce-page #content input.button, .woocommerce-page #response input#submit, .woocommerce-page a.button, .woocommerce-page button.button, .woocommerce-page input.button',
			'property'	=> 'color',
			'property2'	=> '!important',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_button_border', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 9, 
			'label'		=> __( 'Button (Border Color)', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce #content input.button, .woocommerce #response input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce-page #content input.button, .woocommerce-page #response input#submit, .woocommerce-page a.button, .woocommerce-page button.button, .woocommerce-page input.button',
			'property'	=> 'border-color',
			'property2'	=> '!important',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);

	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_button_alt_bg', 
			'default'	=> '', 
			'priority'	=> 10, 
			'label'		=> __( 'Button ALT (Background)', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .summary-right .single_add_to_cart_button.button.alt, .woocommerce-page .summary-right .single_add_to_cart_button.button.alt',
			'property'	=> 'background-color',
			'property2'	=> '!important',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_button_alt_text', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 11, 
			'label'		=> __( 'Button ALT (Text Color)', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .summary-right .single_add_to_cart_button.button.alt, .woocommerce-page .summary-right .single_add_to_cart_button.button.alt',
			'property'	=> 'color',
			'property2'	=> '!important',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_button_alt_border', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 12, 
			'label'		=> __( 'Button ALT (Border Color)', 'tokopress' ),
			'section'	=> 'tokopress_woo_general',
			'selector'	=> '.woocommerce .summary-right .single_add_to_cart_button.button.alt, .woocommerce-page .summary-right .single_add_to_cart_button.button.alt',
			'property'	=> 'border-color',
			'property2'	=> '!important',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_general_data' );

/**
 * Product items customizer data
 */
function tokopress_customizer_product_item_data( $cf_colors ) {
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_items_bg', 
			'default'	=> '#ffffff', 
			'priority'	=> 1, 
			'label'		=> __( 'Product Background', 'tokopress' ),
			'section'	=> 'tokopress_woo_items',
			'selector'	=> '.woocommerce ul.products li.product, .woocommerce-page ul.products li.product',
			'property'	=> 'background-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_items_border', 
			'default'	=> '#ededed', 
			'priority'	=> 2, 
			'label'		=> __( 'Product Border Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_items',
			'selector'	=> '.woocommerce ul.products li.product, .woocommerce-page ul.products li.product',
			'property'	=> 'border-color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_items_rating', 
			'default'	=> '#e0dadf', 
			'priority'	=> 3, 
			'label'		=> __( 'Product Rating', 'tokopress' ),
			'section'	=> 'tokopress_woo_items',
			'selector'	=> '.woocommerce .star-rating:before, .woocommerce-page .star-rating:before',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_items_rating_active', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 4, 
			'label'		=> __( 'Product Rating (active)', 'tokopress' ),
			'section'	=> 'tokopress_woo_items',
			'selector'	=> '.woocommerce .star-rating span, .woocommerce-page .star-rating span',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_items_title', 
			'default'	=> '#3e3e3e', 
			'priority'	=> 5, 
			'label'		=> __( 'Product Title Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_items',
			'selector'	=> '.woocommerce ul.products li.product a h3, .woocommerce-page ul.products li.product a h3',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_items_price', 
			'default'	=> '#747474', 
			'priority'	=> 6, 
			'label'		=> __( 'Product Items Price Color', 'tokopress' ),
			'section'	=> 'tokopress_woo_items',
			'selector'	=> '.woocommerce ul.products li.product .price, .woocommerce-page ul.products li.product .price',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);
	$cf_colors[] = array( 
			'slug'		=> 'tokopress_woo_items_price_old', 
			'default'	=> '#ededed', 
			'priority'	=> 7, 
			'label'		=> __( 'Product Price Color (Old Price)', 'tokopress' ),
			'section'	=> 'tokopress_woo_items',
			'selector'	=> '.woocommerce ul.products li.product .price del, .woocommerce-page ul.products li.product .price del',
			'property'	=> 'color',
			'transport'	=> 'postMessage',
			'type' 		=> 'color'
		);

	return $cf_colors;
}
add_filter( 'cf_colors', 'tokopress_customizer_product_item_data' );