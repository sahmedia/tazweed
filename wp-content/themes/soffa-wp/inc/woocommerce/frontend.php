<?php
/**
 * TokoPress frontend woocommerce
 *
 * @package TokoPress
 */

/**
 * add container start content block
 */
function tokopress_shop_content_block_start() {
	echo '<div class="content-block col-md-12">';
}
add_action( 'woocommerce_before_main_content', 'tokopress_shop_content_block_start', 25 );

/**
 * add container end content block
 */
function tokopress_shop_content_block_end() {
	echo '</div>';
}
add_action( 'woocommerce_after_main_content', 'tokopress_shop_content_block_end', 50 );

/**
 * add container start main shop
 */
function tokopress_shop_content_start() {
	if ( is_product() ) {
		$sidebar = of_get_option( 'tokopress_hide_product_sidebar' ) ? false : true;
	}
	else {
		$sidebar = of_get_option( 'tokopress_hide_shop_sidebar' ) ? false : true;
	}
	if ( $sidebar ) {
		echo '<div class="main-content col-md-9 has-sidebar">';
	}
	else {
		echo '<div class="main-content col-md-12">';
		remove_all_actions( 'woocommerce_sidebar', 99 );
		remove_action( 'woocommerce_after_main_content', 'woocommerce_get_sidebar', 5 );
	}
}
add_action( 'woocommerce_before_main_content', 'tokopress_shop_content_start', 30 );

/**
 * add container end main shop
 */
function tokopress_shop_content_end() {
	echo '</div>';
}
add_action( 'woocommerce_after_main_content', 'tokopress_shop_content_end', 4 );

/**
 * add container start ordering form
 */
function tokopress_shop_ordering_start() {
	echo '<div class="wc-shop-ordering">';
}
add_action( 'woocommerce_before_shop_loop', 'tokopress_shop_ordering_start', 5 );

/**
 * add container end ordering form
 */
function tokopress_shop_ordering_end() {
	echo '</div>';
}
add_action( 'woocommerce_before_shop_loop', 'tokopress_shop_ordering_end', 40 );

/**
 * add container thumbnail block start product
 */
function tokopress_product_thumb_block_start() {
	echo '<div class="product-thumb-block">';
}
add_action( 'woocommerce_after_shop_loop_item_title', 'tokopress_product_thumb_block_start', 14 );

/**
 * add container thumbnail block end product
 */
function tokopress_product_thumb_block_end() {
	echo '</div>';
}
add_action( 'woocommerce_after_shop_loop_item_title', 'tokopress_product_thumb_block_end', 16 );

/**
 * add container up block start product
 */
function tokopress_product_up_block_start() {
	echo '<div class="product-up-block">';
}
add_action( 'woocommerce_before_shop_loop_item_title', 'tokopress_product_up_block_start', 1 );

/**
 * add container up block end product
 */
function tokopress_product_up_block_end() {
	echo '</div>';
}
add_action( 'woocommerce_after_shop_loop_item_title', 'tokopress_product_up_block_end', 12 );

/**
 * 
 */
function tokopress_product_down_block_start() {
	echo '<div class="product-down-block">';
}
add_action( 'woocommerce_after_shop_loop_item', 'tokopress_product_down_block_start', 1 );

/**
 * 
 */
function tokopress_add_whishlist_button() {
	echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
}
if( function_exists( 'yith_wishlist_constructor' ) && !of_get_option( 'tokopress_wc_hide_products_wishlist_button' ) ) {
	add_action( 'woocommerce_after_shop_loop_item', 'tokopress_add_whishlist_button', 5 );
}

/**
 * 
 */
function tokopress_product_down_block_end() {
	echo '</div>';
}
add_action( 'woocommerce_after_shop_loop_item', 'tokopress_product_down_block_end', 10 );

/**
 * 
 */
function tokopress_summary_container_start() {
	echo '<div class="row">';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_container_start', 1 );

/**
 * 
 */
function tokopress_summary_container_end() {
	echo '</div>';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_container_end', 56 );

/**
 * 
 */
function tokopress_summary_block_start() {
	echo '<div class="col-md-6">';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_start', 2 );
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_start', 26 );

/**
 * 
 */
function tokopress_summary_block_end() {
	echo '</div>';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_end', 25 );
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_end', 55 );

/**
 * 
 */
function tokopress_summary_block_left_start() {
	echo '<div class="summary-left">';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_left_start', 3 );

/**
 * 
 */
function tokopress_summary_block_left_end() {
	echo '</div>';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_left_end', 24 );

/**
 * 
 */
function tokopress_summary_block_right_start() {
	echo '<div class="summary-right">';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_right_start', 27 );

/**
 * 
 */
function tokopress_summary_block_right_end() {
	echo '</div>';
}
add_action( 'woocommerce_single_product_summary', 'tokopress_summary_block_right_end', 54 );

/**
 * Custom product per page
 */
function tokopress_custom_loop_shop_per_page( $cols ) {
	
	$shop_per_page = intval( of_get_option( 'tokopress_wc_products_per_page' ) );
	if ( $shop_per_page < 1 ) $shop_per_page = 12;
	return $shop_per_page;

}
add_filter( 'loop_shop_per_page', 'tokopress_custom_loop_shop_per_page', 20 );

/**
 * show / hide product sale flash
 */
if( of_get_option( 'tokopress_wc_hide_products_sale_flash' ) ) :
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
endif;

/**
 * Hide products title
 */
if( of_get_option( 'tokopress_wc_hide_products_title' ) )
	add_action( 'tokopress_custom_styles', 'tokopress_wc_hide_shop_title' );

function tokopress_wc_hide_shop_title() {
	echo '.woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3{ display:none; }';
}

/**
 * show / hide product rating
 */
if( of_get_option( 'tokopress_wc_hide_products_rating' ) ) :
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
endif;

/**
 * show / hide product price
 */
if( of_get_option( 'tokopress_wc_hide_products_price' ) ) :
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
endif;

if( of_get_option( 'tokopress_wc_hide_products_title' ) && of_get_option( 'tokopress_wc_hide_products_rating' ) && of_get_option( 'tokopress_wc_hide_products_price' ) ) :
	
	function tokopress_wc_hide_product_up_block() {
		echo '.woocommerce ul.products li.product .product-up-block, .woocommerce-page ul.products li.product .product-up-block { display:none; }';
	}
	add_action( 'tokopress_custom_styles', 'tokopress_wc_hide_product_up_block' );

endif;

/**
 * DISABLE products "add to cart" button
 */
if( of_get_option( 'tokopress_wc_hide_products_cart_button' ) )
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

/**
 * DISABLE result count
 */
if( of_get_option( 'tokopress_wc_hide_result_count' ) )
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

/**
 * DISABLE catalog ordering
 */
if( of_get_option( 'tokopress_wc_hide_catalog_ordering' ) )
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

if( of_get_option( 'tokopress_wc_hide_result_count' ) && of_get_option( 'tokopress_wc_hide_catalog_ordering' ) ) :
	function tokopress_wc_hide_ordering_block() {
		echo '.woocommerce .wc-shop-ordering, .woocommerce-page .wc-shop-ordering { display:none; }';
	}
	add_action( 'tokopress_custom_styles', 'tokopress_wc_hide_ordering_block' );
endif;

function tokopress_product_thumb_carousel() {
	if( is_product() ) {
		// add owl carousel js
		wp_enqueue_script( 'toko-owlcarousel' );
		wp_enqueue_style( 'toko-owlcarousel' );
		
		$js_code = "$('div.thumbnails').addClass('owl-carousel owl-theme');$('div.thumbnails').owlCarousel({items:6,autoPlay:false,loop:true,nav:true,navText:['<i class=\"fa fa-chevron-left\"></i>','<i class=\"fa fa-chevron-right\"></i>'],dots:false});";
		wc_enqueue_js( $js_code );
	}
}
add_action( 'wp_enqueue_scripts', 'tokopress_product_thumb_carousel' );

/**
 * Hide product single sale flash
 */
if( of_get_option( 'tokopress_wc_hide_product_sale_flash' ) )
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

/**
 * Hide product single price
 */
if( of_get_option( 'tokopress_wc_hide_product_price' ) )
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

/**
 * Hide product single excerpt
 */
if( of_get_option( 'tokopress_wc_hide_product_excerpt' ) )
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

/**
 * DISABLE product single "add to cart" button
 */
if( of_get_option( 'tokopress_wc_hide_product_cart_button' ) )
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

/**
 * DISABLE product single meta (categories/tags)
 */
if( of_get_option( 'tokopress_wc_hide_product_meta_tags' ) )
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/**
 * DISABLE related products
 */
if( of_get_option( 'tokopress_wc_hide_related_products' ) )
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

/**
 * SET related product limit number
 */
function tokopress_related_product_number() {

	$posts_per_page = intval( of_get_option( 'tokopress_wc_products_related_number' ) );
	if ( $posts_per_page < 1 ) $posts_per_page = 3;

	$args = array(
			'post_type' => 'product',
			'posts_per_page' => $posts_per_page
		);
	return $args;
}
add_filter( 'woocommerce_related_products_args', 'tokopress_related_product_number' );

/**
 * SET related column product
 */
if ( ! function_exists( 'woocommerce_output_related_products' ) ) {
	function woocommerce_output_related_products() {
		global $woocommerce;
		$posts_per_page = intval( of_get_option( 'tokopress_wc_products_related_number' ) );
		if ( $posts_per_page < 1 ) $posts_per_page = 3;
		$columns = intval( of_get_option( 'tokopress_wc_products_related_column' ) );
		if ( $columns < 1 ) $columns = 3;
		if ( is_object( $woocommerce ) && version_compare( $woocommerce->version, '2.1', '>=' ) ) {
			woocommerce_related_products( $args = array(
					'posts_per_page' => $posts_per_page,
					'columns' => $columns,
					'orderby' => 'rand'
				)
			);
		}
		else {
			woocommerce_related_products( $posts_per_page, $columns );
		}
	}
}

/**
 * Add related products column body class.
 */
function tokopress_wc_related_body_class( $classes ) {
	if ( !is_product() ) return $classes;
	$columns = intval( of_get_option( 'tokopress_wc_products_related_column' ) );
	if ( $columns < 1 ) $columns = 3;
	$classes[] = 'woocommerce-related-col-'.$columns;
	return $classes;
}
add_filter( 'body_class', 'tokopress_wc_related_body_class' );


/**
 * DISABLE up-sells products
 */
if( of_get_option( 'tokopress_wc_hide_upsells_products' ) )
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

/**
 * SET per-page and column up-sells product
 */
function woocommerce_upsell_display( $posts_per_page = 2, $columns = 2, $orderby = 'rand' ) {
	$posts_per_page = intval( of_get_option( 'tokopress_wc_products_upsells_number' ) );
	if ( $posts_per_page < 1 ) $posts_per_page = 3;
	$columns = intval( of_get_option( 'tokopress_wc_products_upsells_column' ) );
	if ( $columns < 1 ) $columns = 3;
	woocommerce_get_template( 'single-product/up-sells.php', array(
			'posts_per_page'  => $posts_per_page,
			'orderby'    => $orderby,
			'columns'    => $columns
		) );
}

/**
 * Add up-sells column body class.
 */
function tokopress_wc_upsells_body_class( $classes ) {
	if ( !is_product() ) return $classes;
	$columns = intval( of_get_option( 'tokopress_wc_products_upsells_column' ) );
	if ( $columns < 1 ) $columns = 3;
	$classes[] = 'woocommerce-upsells-col-' . $columns;
	return $classes;
}
add_filter( 'body_class', 'tokopress_wc_upsells_body_class' );

/**
 * DISABLE cross-sells product on cart page
 */
if( of_get_option( 'tokopress_wc_hide_crosssells_products' ) )
	remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

/**
 * Redirect Option After customer Login
 */
function tokopress_wc_login_redirect( $redirect_to ) {
    $redirect_to = esc_url( of_get_option( 'tokopress_wc_red_cus_login' ) );
    return $redirect_to;
}
if( of_get_option( 'tokopress_wc_red_cus_login' ) ) {
	add_filter( 'woocommerce_login_redirect', 'tokopress_wc_login_redirect' );
}