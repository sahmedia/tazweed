<?php
/**
 * TokoPress function and definitions woocommerce
 *
 * @package TokoPress
 */

/**
 * WooCommerce Theme Support
 */
add_theme_support( 'woocommerce' );

/**
 * Add Class Woocommerce in body class if woocommerce class not exist
 */
function tokopress_wc_add_class( $classes ) {

	if ( is_shop() || is_product_category() || is_product_tag() ){

		$get_column = intval( of_get_option( 'tokopress_wc_products_column_per_row' ) );
		if ( $get_column < 1 ) $get_column = 3;

		$classes[] = 'woocommerce-product-col-' . $get_column;
	
	}

	if( !is_woocommerce() ){

		$classes[] = 'woocommerce';
	}
	
	return $classes;
}
add_filter( 'body_class', 'tokopress_wc_add_class' );

/**
 * Change product column per row
 */
function wc_loop_shop_columns( $number_columns ) {
	
	$count_column = intval( of_get_option( 'tokopress_wc_products_column_per_row' ) );
	if ( $count_column < 1 ) $count_column = 3;

	return $count_column;

}
add_filter( 'loop_shop_columns', 'wc_loop_shop_columns', 1, 10 );

/**
 * woocommerce sidebar placement
 */
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
add_action( 'woocommerce_after_main_content', 'woocommerce_get_sidebar', 5 );

/**
 * woocommerce shop rating placement
 */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

/**
 * woocommerce shop thumbnail placement
 */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 15 );

/**
 * remove woocommerce breadcrumb
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

/**
 * remove woocommerce single product title
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating' );

/**
 * change pagination woocommerce
 */
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
add_action( 'woocommerce_after_shop_loop', 'tokopress_paging_nav', 10 );