<?php
/**
 * Tokopress main page
 *
 * @package TokoPress
 */

get_header(); ?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">
		
	<div class="container">

		<div class="content-block col-md-12">
			
			<section class="main-content col-md-9 has-sidebar">
				<div id="content" class="content-area">

					<div id="page-<?php echo get_the_ID(); ?>" class="page-area">
						<?php if ( have_posts() ) : ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php the_content(); ?>

							<?php endwhile; ?>


						<?php else : ?>

							<?php get_template_part( 'content', '404' ); ?>

						<?php endif; ?>
					</div>

				</div>
			</section>

			<?php get_sidebar(); ?>

		</div>

	</div>

</div>

<?php do_action( 'tokopress_after_content_wrapper' ); ?>

<?php get_footer(); ?>