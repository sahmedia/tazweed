<?php
/**
 * Template Name:Archive
 *
 * @package TokoPress
 */
?>

<?php get_header(); ?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">
		
	<div class="container">

		<div class="content-block col-md-12">
			
			<section class="main-content col-md-9 has-sidebar">
				<div id="archives" class="archives-area">
				<h2><?php _e( 'THE LAST 20 POSTS', 'tokopress' ); ?></h2>
				<?php 
					query_posts( 'posts_per_page=20' );
					if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<div class="archives-loop">
							<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3> - <?php echo get_the_date(); ?> - <b><?php comments_number( __( 'No Comments', 'tokopress' ), __( 'One comments', 'tokopress' ), __( '% comments', 'tokopress' ) ); ?></b>
						</div>

					<?php endwhile; ?>

					<div class="addition-widget">
						<div class="col-md-6">
							
							<?php the_widget( 'WP_Widget_Categories' ); ?>

						</div>
						<div class="col-md-6">
							
							<?php the_widget( 'WP_Widget_Archives', array( 'count' => '1', 'title' => 'Monthly Archives' ) ); ?>

						</div>
					</div>

				<?php else : ?>

					<?php get_template_part( 'content', '404' ); ?>

				<?php endif; ?>

				</div>
			</section>

			<?php get_sidebar(); ?>

		</div>

	</div>

</div>

<?php do_action( 'tokopress_after_content_wrapper' ); ?>

<?php get_footer(); ?>