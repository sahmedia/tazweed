<?php
/*
Template Name: Contact
*/

get_header(); ?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">
		
	<div class="container">

		<div class="content-block col-md-12">
			
			<section class="main-content col-md-9 has-sidebar">
				<div id="content" class="content-area">

					<?php if ( have_posts() ) : ?>

						<div class="map-section map-block">
							<?php 
								$latitude = of_get_option( 'tokopress_contact_lat' );
								$longitude = of_get_option( 'tokopress_contact_long' );

								$get_marker_title = of_get_option( 'tokopress_contact_marker_title' );
								$get_marker_content = of_get_option( 'tokopress_contact_marker_desc' );
								$get_zoom = 15;
							 ?>
								<script type="text/javascript">
									var map;
									jQuery(document).ready(function(){
									  map = new GMaps({
										el: '#map',
										lat: <?php $map_latitude = ( ! empty( $latitude ) ) ? $latitude : -6.903932 ; echo $map_latitude; ?>,
										lng: <?php $map_longitude = ( ! empty( $longitude ) ) ? $longitude : 107.610344 ; echo $map_longitude; ?>,
										zoom :<?php echo $get_zoom; ?>,
										
									  });

									 <?php 	$marker_title = ( ! empty( $get_marker_title ) ) ? $get_marker_title : 'Marker Title' ;
											$clear_marker_title = str_replace( "\r\n", "<br/>", $marker_title );
													?>

									 <?php 	$marker_content = ( ! empty( $get_marker_content ) ) ? $get_marker_content : 'Marker Content' ;
											$clear_marker_content = str_replace( "\r\n", "<br/>", $marker_content );
											?>

									 var markerTitle = "<h1><?php echo $clear_marker_title; ?></h1>";
									 var markerContent = "<p><?php echo $clear_marker_content; ?></p>";

									 map.addMarker({
										lat: <?php echo $map_latitude; ?>,
										lng: <?php echo $map_longitude; ?>,
										title: 'Marker with InfoWindow',
										infoWindow: {
										  content: markerTitle + markerContent,
													
										}
									  });

									});
								</script>
			
							<div id="map" style="height:500px;"></div>

						</div>

						<?php while ( have_posts() ) : the_post(); ?>
							<?php if( "" != get_the_content() ): ?>
							
							<div id="page-<?php echo get_the_ID(); ?>" class="page-area">

								<?php the_content(); ?>

							</div><!-- Content Block -->
							
							<?php endif; ?>
						<?php endwhile; ?>

						<?php
						$current_page_id = $wp_query->get_queried_object_id();
						
						$args = array();

						$email = toko_get_post_meta( '_toko_contact_email', $current_page_id );
						if ( $email ) $args['email'] = $email;

						$subject = toko_get_post_meta( '_toko_contact_subject', $current_page_id );
						if ( $subject ) $args['subject'] = $subject;

						$sendcopy = toko_get_post_meta( '_toko_contact_sendcopy', $current_page_id );
						if ( $sendcopy ) $args['sendcopy'] = $sendcopy;

						$button_text = toko_get_post_meta( '_toko_contact_button', $current_page_id );
						if ( $button_text ) $args['button_text'] = $button_text;

						echo toko_get_contact_form( $args );
						?>
						<!-- Contact Form -->

					<?php else : ?>

						<?php get_template_part( 'content', '404' ); ?>

					<?php endif; ?>

				</div>
			</section>

			<?php get_sidebar(); ?>

		</div>

	</div>

</div>

<?php do_action( 'tokopress_after_content_wrapper' ); ?>

<?php get_footer(); ?>