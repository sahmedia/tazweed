<?php
/**
 * The template for displaying project content in the single-project.php template
 *
 * Override this template by copying it to yourtheme/projects/content-single-project.php
 *
 * @author 		WooThemes
 * @package 	Projects/Templates
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * projects_before_single_project hook
	 *
	 */
	 do_action( 'projects_before_single_project' );
?>

<div id="project-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * projects_before_single_project_summary hook
		 * @hooked projects_template_single_title - 10
		 * @hooked projects_template_single_short_description - 20
	 	 * @hooked projects_template_single_feature - 30 : removed
	 	 * @hooked projects_template_single_gallery - 40 : removed
		 */
		do_action( 'projects_before_single_project_summary' );
	?>

	<div class="summary entry-summary">

		<?php
			/**
			 * projects_single_project_summary hook
			 *
			 * @hooked projects_template_single_description - 10
			 * @hooked projects_template_single_meta - 20
			 */
			do_action( 'projects_single_project_summary' );
		?>

	</div><!-- .summary -->

	<?php
		/**
		 * projects_after_single_project_summary hook
		 *
		 */
		do_action( 'projects_after_single_project_summary' );
	?>

</div><!-- #project-<?php the_ID(); ?> -->

<?php
	$terms = wp_get_post_terms( get_the_ID(), 'project-category', array( 'fields' => 'ids' ) );

	$taxonomy = 'project-category';
	$taxarray = $terms;

	if( empty( $terms ) )
		return;

	$args = array(
			'post_type'=> 'project',
			'post_per_page'=> 3,
			'tax_query'=> array(
							array(
								'taxonomy'=> $taxonomy,
								'terms'=> $taxarray,
								'field'=> 'id'
							)
						),
			'post__not_in' => array( get_the_ID() )
		);
	$query = new WP_Query( $args );

	if( $query->have_posts() ){

		echo "\t" . '<div class="related-project-wrap">' . "\n";
		echo "\t\t" . '<div class="related-project-title">' . "\n";
		echo "\t\t\t" . '<h2>' . __( 'RELATED PORTFOLIO', 'tokopress' ) . '</h2>' . "\n";
		echo "\t\t" . '</div>' . "\n";

		echo "\t\t" . '<ul class="projects related-project-inner">' . "\n";

		while ( $query->have_posts() ) {
			$query->the_post();

			echo "\t\t\t" . '<li class="project realted-project-item col-md-4">' . "\n";

			echo "\t\t\t\t" . '<div class="project-wrapper-items">' . "\n";
			echo "\t\t\t\t\t" . '<a href="' . get_permalink() . '" class="project-permalink">' . "\n";
			echo "\t\t\t\t\t\t" . '<figure class="project-thumbnail">' . "\n";
			echo "\t\t\t\t\t\t\t" . get_the_post_thumbnail( get_the_ID(), 'project-archive' ) . "\n";
			echo "\t\t\t\t\t\t" . '</figure>' . "\n";
			echo "\t\t\t\t\t" . '</a>';
			
			echo "\t\t\t\t\t" . '<div class="project-caption">' . "\n";
			echo "\t\t\t\t\t\t" . '<a href="' . get_permalink() . '" class="project-permalink">' . "\n";
			echo "\t\t\t\t\t\t\t" . '<h3>' . get_the_title() . '</h3>' . "\n";
			echo "\t\t\t\t\t\t" . '</a>' . "\n";
			echo "\t\t\t\t\t\t" . '<a class="project-button" href="' . get_permalink() . '">' . ( !of_get_option( 'tokopress_project_button_text' ) ? __( 'View Project', 'tokopress' ) : of_get_option( 'tokopress_project_button_text' ) ) . '</a>' . "\n";
			echo "\t\t\t\t\t" . '</div>' . "\n";
			echo "\t\t\t\t" . '</div>' ."\n";

			echo "\t\t\t" . '</li>' . "\n";
			
		}

		wp_reset_query();

		echo "\t\t" . '</ul>' . "\n";

		echo "\t" . '</div>' . "\n";

	}

?>