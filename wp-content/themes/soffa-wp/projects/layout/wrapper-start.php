<?php
/**
 * TokoPress Projects Wrapper Start
 *
 * @package TokoPress
 */
?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">
	<div class="container">

		<section class="main-content col-md-12">
			<div id="content" class="content-area clearfix">