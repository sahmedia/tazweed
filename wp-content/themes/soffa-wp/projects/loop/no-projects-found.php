<?php
/**
 * TokoPress Projects Not Found
 *
 * @package TokoPress
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<h1 class="projects-info"><?php _e( 'No projects found which match your selection.', 'tokopress' ); ?></h1>