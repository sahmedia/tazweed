<?php
/**
 * TokoPress Searchform template
 *
 * @package TokoPress
 */
?>

<form role="search" method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
	<div>
		<label class="screen-reader-text" for="s">Search for:</label>
		<input type="text" value="" name="s" placeholder="Search ...">
		<input type="submit" class="searchsubmit" value="Search">
	</div>
</form>