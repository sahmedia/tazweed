<?php
/**
 * TokoPress sidebar template
 *
 * @package TokoPress
 */
?>

<aside id="primary-sidebar" class="col-md-3 sidebar">

	<div class="sidebar">

		<?php if ( is_active_sidebar( 'primary_widget' ) ) : ?>
			
			<?php dynamic_sidebar( 'primary_widget' ); ?>

		<?php else : ?>

			<?php the_widget( 'WP_Widget_Search' ); ?>

			<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

			<?php the_widget( 'WP_Widget_Meta' ); ?>

		<?php endif; ?>

	</div>

</aside><!-- End #secondary .sidebar -->