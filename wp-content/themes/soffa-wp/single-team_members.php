<?php get_header(); ?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">

	<div class="container">
			
		<section class="main-content no-sidebar">
			<div id="content" class="content-area">

				<div id="page-<?php echo get_the_ID(); ?>" class="team-member">
					<?php if ( have_posts() ) : ?>

						<div class="team-member-single-wrap">

							<?php while ( have_posts() ) : the_post(); ?>

								<div class="team-member-image">
								<?php if( "" != get_post_meta( get_the_ID(), '_tokopress_member_email', true ) ) : ?>

									<?php echo get_avatar( get_post_meta( get_the_ID(), '_tokopress_member_email', true ), 500, '', get_the_title() ); ?> 

								<?php else : ?>

									<?php the_post_thumbnail( 'large' ); ?>

								<?php endif; ?>
								<!-- Team Member Image -->
								</div>

								<div class="team-member-summary">

									<div class="description">
										<?php echo get_post_meta( get_the_ID(), '_tokopress_member_description', true ); ?>
									</div>
								
									<?php tokopress_skill_member(); ?>

									<?php tokopress_connect_member(); ?>

								</div>

							<?php endwhile; ?>

						</div>

						<?php 
						$args = array(
							'posts_per_page'	=> 6,
							'orderby' 		=> 'post_date',
							'order' 		=> 'DESC',
							'post_type'		=> 'post',
							'post_status' 	=> 'publish'
						);

						$recent_posts = new WP_Query( $args );
						?>

						<?php if( $recent_posts->have_posts() ) : ?>
							<div class="related-post-wrap">
								<div class="related-post-title">
									<h2><?php _e( 'Related Posts', 'tokopress' ); ?></h2>
								</div>
								<div class="related-post-inner">

								<?php while ($recent_posts->have_posts()) {
									$recent_posts->the_post();
								?>
								
									<div class="related-item col-md-4">
										<div class="related-thumbnail">
											<?php 
											if( has_post_thumbnail() ){
												the_post_thumbnail( 'thumb-blog' );
											} else {
												?><img src="http://placehold.it/430x220&text=thumbnail" alt="post tumbnails"><?php
											}
											?>
										</div>
										<div class="related-meta">
											<h2><?php echo text_limiter( get_the_title(), 70 ); ?></h2>
											<p><?php echo get_the_date(); ?></p>
										</div>
									</div>	

								<?php
								} ?>
									
								</div>
							</div>
						<?php endif; ?>

						<?php wp_reset_query(); ?>

					<?php else : ?>

						<?php get_template_part( 'content', '404' ); ?>

					<?php endif; ?>
				</div>

			</div>
		</section>

	</div>
	
</div>

<?php do_action( 'tokopress_after_content_wrapper' ); ?>

<?php get_footer(); ?>