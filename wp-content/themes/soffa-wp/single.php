<?php
/**
 * Tokopress main single page
 *
 * @package TokoPress
 */

$sidebar = of_get_option( 'tokopress_hide_post_sidebar' ) ? false : true;

get_header(); ?>

<?php do_action( 'tokopress_before_content_wrapper' ); ?>

<div class="site-content" id="main">
		
	<div class="container">

		<div class="content-block col-md-12">
			
			<section class="main-content <?php echo ( $sidebar ? 'col-md-9 has-sidebar' : 'col-md-12' ); ?> ">
				<div id="content" class="content-area">

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php if ( have_posts() ) : ?>

							<?php while ( have_posts() ) : the_post(); ?>

								<?php if( has_post_thumbnail() ) : ?>
									<div class="has-thumbnail">
										<?php the_post_thumbnail( 'full' ); ?>
									</div>
								<?php endif; ?>

								<?php tokopress_post_meta(); ?>

								<div class="the-content">
									<?php the_content(); ?>
									<?php wp_link_pages( array( 'before' => '<p class="page-links">' . '<span class="before">' . __( 'Pages:', 'tokopress' ) . '</span>', 'after' => '</p>' ) ); ?>
								</div>

								<div class="post-nav">
									<?php
										previous_post_link( '<div class="nav-previous">%link</div>', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'tokopress' ) );
										next_post_link( '<div class="nav-next">%link</div>', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'tokopress' ) );
									?>
								</div>

								<?php if ( comments_open() ): ?>
								
									<!-- COMMENTS BLOCK -->
									<div id="comments-block">
										
										<?php
											comments_template();
										?>
											
									</div><!-- .comments-block -->
									<!-- END COMMENTS BLOCK -->
								
								<?php endif ?>

							<?php endwhile; ?>
							
						<?php else : ?>

							<?php get_template_part( 'content', '404' ); ?>

						<?php endif; ?>

					</div>

				</div>
			</section>

			<?php if ( $sidebar ) get_sidebar(); ?>
		</div>

	</div>

</div>

<?php do_action( 'tokopress_after_content_wrapper' ); ?>

<?php get_footer(); ?>